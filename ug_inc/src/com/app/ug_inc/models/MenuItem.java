package com.app.ug_inc.models;

public class MenuItem {
	private String id;
	private String priority;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {

		return "[id=" + this.id + ", priority=" + this.priority + "] ";
	}
}
