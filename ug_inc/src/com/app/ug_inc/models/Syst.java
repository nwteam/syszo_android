package com.app.ug_inc.models;

import java.io.Serializable;

/**
 * Chiebukuro
 * Created by Will on 2015/5/2.
 */
public class Syst implements Serializable {
    private static final long serialVersionUID = -801235221071501625L;

    private String id = "";
    private String time = "";
    private String content = "";
    private User user;

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    private String picUrl;
    private boolean liked;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}