package com.app.ug_inc.models;

import java.io.Serializable;

public class Workplace implements Serializable {
	private static final long serialVersionUID = 4350934362265256454L;
	private String name;
	private String id;
	private Company company; // which compnay it belongs

	private String mockSum; // mock sum

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getMockSum() {
		return mockSum;
	}

	public void setMockSum(String mockSum) {
		this.mockSum = mockSum;
	}

}
