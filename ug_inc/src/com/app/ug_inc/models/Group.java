package com.app.ug_inc.models;

import java.io.Serializable;
import java.util.List;

public class Group implements Serializable {
    private static final long serialVersionUID = -8039101886947933840L;
    private int memSum;
    private String name;
    private String id;
    private String pic;
    private List<User> memberList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMemSum() {
        return memSum;
    }

    public void setMemSum(int memSum) {
        this.memSum = memSum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setMemberList(List<User> memberList) {
        this.memberList = memberList;
    }

    public List<User> getMemberList() {
        return memberList;
    }
}