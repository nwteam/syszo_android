package com.app.ug_inc.models;

import java.util.List;

/**
 * Created by WilliZ on 2015/5/15.
 */
public class SPGroup {
    private int addSum; // 添加人数
    private int addedSum; // 被添加人数
    private int groupSum; // 群总数
    private int friendSum; // 好友总数
    private List<Group> groupList; // 群列表
    private List<User> friendList; // 好友列表

    public int getAddedSum() {
        return addedSum;
    }

    public void setAddedSum(int addedSum) {
        this.addedSum = addedSum;
    }

    public int getAddSum() {
        return addSum;
    }

    public void setAddSum(int addSum) {
        this.addSum = addSum;
    }

    public List<User> getFriendList() {
        return friendList;
    }

    public void setFriendList(List<User> friendList) {
        this.friendList = friendList;
    }

    public int getFriendSum() {
        return friendSum;
    }

    public void setFriendSum(int friendSum) {
        this.friendSum = friendSum;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    public int getGroupSum() {
        return groupSum;
    }

    public void setGroupSum(int groupSum) {
        this.groupSum = groupSum;
    }
}
