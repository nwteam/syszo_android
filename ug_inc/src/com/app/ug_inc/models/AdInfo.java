package com.app.ug_inc.models;

import java.io.Serializable;

/**
 * Created by yangshiqin on 16/5/1.
 */
public class AdInfo implements Serializable {
    String ad_id;
    String ad_name;
    String ad_type;
    String ad_size;
    String ad_img;
    String link_url;

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getAd_id() {
        return ad_id;
    }

    public void setAd_id(String ad_id) {
        this.ad_id = ad_id;
    }

    public String getAd_name() {
        return ad_name;
    }

    public void setAd_name(String ad_name) {
        this.ad_name = ad_name;
    }

    public String getAd_type() {
        return ad_type;
    }

    public void setAd_type(String ad_type) {
        this.ad_type = ad_type;
    }

    public String getAd_size() {
        return ad_size;
    }

    public void setAd_size(String ad_size) {
        this.ad_size = ad_size;
    }

    public String getAd_img() {
        return ad_img;
    }

    public void setAd_img(String ad_img) {
        this.ad_img = ad_img;
    }

    @Override
    public String toString() {
        return "AdInfo{" +
                "ad_id='" + ad_id + '\'' +
                ", ad_name='" + ad_name + '\'' +
                ", ad_type='" + ad_type + '\'' +
                ", ad_size='" + ad_size + '\'' +
                ", ad_img='" + ad_img + '\'' +
                ", link_url='" + link_url + '\'' +
                '}';
    }
}
