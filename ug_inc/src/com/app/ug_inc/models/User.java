package com.app.ug_inc.models;

import java.io.Serializable;

public class User implements Cloneable, Serializable {

    private static final long serialVersionUID = -8466907126624966923L;
    private String id = "1";
    private String token;
    private String nick = ""; // nickname
    private String email = "";
    private String pwd; // password
    private String sex = "";//1男2女3其他
    private String birth = ""; // yyyy-MM-dd
    private String district = ""; //
    private String cityId = "0"; // user_city
    private String city = "";//城市名
    private String jobId = "";//工作id
    private String job = "";//职务
    private String industry = "";//职业种类
    private String industryId = "";//职业种类
    private String sizeCompany = "";//公司规模
    private String sizeCompanyId = "";//公司规模
    private String regMoney = ""; //注册资金
    private String regMoneyId = "";
    private String calendar = ""; //使用时间
    private String calendarId = ""; //使用时间
    private String software = "";//软件
    private String softwareId = "";
    private String hardware = "";//硬件
    private String hardwareId = "";
    private String blood = "";
    private String health = "";
    private String selfIntro = "";
    private String certifaction = "";//拥有资格
    private String allyear = ""; //全年预算
    private String allyearId = ""; //全年预算
    private String certifactionId = "";
    private String score = "0";
    private String followedPeople = "0";
    private String friends = "0";//朋友
    private String favour = "0";//赞
    private String time = "0";//更新日期
    private String introduction = "";//自我介绍
    private String scale = "0"; //资料百分比
    private String isFollowed = "0";
    private String pushToken = "";
    private String balance = "";
    private int vip_flg= 0;

    public int getVip_flg() {
        return vip_flg;
    }

    public void setVip_flg(int vip_flg) {
        this.vip_flg = vip_flg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getSelfIntro() {
        return selfIntro;
    }

    public void setSelfIntro(String selfIntro) {
        this.selfIntro = selfIntro;
    }

    public String getCertifaction() {
        return certifaction;
    }

    public void setCertifaction(String certifaction) {
        this.certifaction = certifaction;
    }

    public String getCertifactionId() {
        return certifactionId;
    }

    public void setCertifactionId(String certifactionId) {
        this.certifactionId = certifactionId;
    }

    public boolean appendCertifactionId( String certifactionId)
    {
        String item[] = this.certifactionId.split(",");
        for (int i = 0; i < item.length;i++){
            if( item[i].equals(certifactionId)){
                return false;
            }
        }
        if (this.certifactionId.isEmpty()){
            this.certifactionId += certifactionId;
        }else {
            this.certifactionId += "," + certifactionId;
        }
        return true;
    }


    public void appendCertifaction( String certifaction)
    {
       if(this.certifaction.isEmpty())
       {
           this.certifaction = certifaction;
       }else
       {
           this.certifaction += ","+certifaction;
       }
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getFollowedPeople() {
        return followedPeople;
    }

    public void setFollowedPeople(String followedPeople) {
        this.followedPeople = followedPeople;
    }

    public String getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(String isFollowed) {
        this.isFollowed = isFollowed;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public String getSizeCompany() {
        return sizeCompany;
    }

    public void setSizeCompany(String sizeCompany) {
        this.sizeCompany = sizeCompany;
    }

    public String getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(String regMoney) {
        this.regMoney = regMoney;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }


    public String getAllyear() {
        return allyear;
    }

    public void setAllyear(String allyear) {
        this.allyear = allyear;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getFavour() {
        return favour;
    }

    public void setFavour(String favour) {
        this.favour = favour;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        User user = new User();
        // user = (User) super.clone();
        user.setId(new String(this.id));
        user.setNick(new String(this.nick));
        user.setSex(new String(this.sex));
        user.setBirth(new String(this.birth));
        user.setDistrict(new String(this.district));
        user.setCityId(new String(this.cityId));
        user.setIndustryId(new String(this.industryId));
        user.setJobId(new String(this.jobId));
        user.setJob(new String(this.job));
        user.setBlood(new String(this.blood));
        user.setHealth(new String(this.health));
        user.setSelfIntro(new String(this.selfIntro));
        user.setCertifactionId(new String(this.certifactionId));
        user.setScore(new String(this.score));
        user.setFollowedPeople(new String(this.followedPeople));
        user.setBalance(new String(this.balance));
        user.setVip_flg(new Integer(this.vip_flg).intValue());
        return user;
    }

    public String getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    public String getSoftwareId() {
        return softwareId;
    }

    public void setSoftwareId(String softwareId) {
        this.softwareId = softwareId;
    }

    public boolean appendSoftwareId( String softwareId)
    {
        String item[] = this.softwareId.split(",");
        for (int i = 0; i < item.length;i++){
            if( item[i].equals(softwareId)){
                return false;
            }
        }
        if (this.softwareId.isEmpty()){
            this.softwareId += softwareId;
        }else {
            this.softwareId += "," + softwareId;
        }
        return true;
    }


    public void appendSoftware( String software)
    {
        if(this.software.isEmpty())
        {
            this.software = software;
        }else
        {
            this.software += ","+software;
        }
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }


    public boolean appendHardwareId( String hardwareId)
    {
        String item[] = this.hardwareId.split(",");
        for (int i = 0; i < item.length;i++){
            if( item[i].equals(hardwareId)){
                return false;
            }
        }
        if (this.hardwareId.isEmpty()){
            this.hardwareId += hardwareId;
        }else {
            this.hardwareId += "," + hardwareId;
        }
        return true;
    }


    public void appendHardware( String hardware)
    {
        if(this.hardware.isEmpty())
        {
            this.hardware = hardware;
        }else
        {
            this.hardware += ","+hardware;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                ", nick='" + nick + '\'' +
                ", email='" + email + '\'' +
                ", pwd='" + pwd + '\'' +
                ", sex='" + sex + '\'' +
                ", birth='" + birth + '\'' +
                ", district='" + district + '\'' +
                ", cityId='" + cityId + '\'' +
                ", city='" + city + '\'' +
                ", jobId='" + jobId + '\'' +
                ", job='" + job + '\'' +
                ", industry='" + industry + '\'' +
                ", industryId='" + industryId + '\'' +
                ", sizeCompany='" + sizeCompany + '\'' +
                ", sizeCompanyId='" + sizeCompanyId + '\'' +
                ", regMoney='" + regMoney + '\'' +
                ", regMoneyId='" + regMoneyId + '\'' +
                ", calendar='" + calendar + '\'' +
                ", calendarId='" + calendarId + '\'' +
                ", software='" + software + '\'' +
                ", softwareId='" + softwareId + '\'' +
                ", hardware='" + hardware + '\'' +
                ", hardwareId='" + hardwareId + '\'' +
                ", blood='" + blood + '\'' +
                ", health='" + health + '\'' +
                ", selfIntro='" + selfIntro + '\'' +
                ", certifaction='" + certifaction + '\'' +
                ", allyear='" + allyear + '\'' +
                ", allyearId='" + allyearId + '\'' +
                ", certifactionId='" + certifactionId + '\'' +
                ", score='" + score + '\'' +
                ", followedPeople='" + followedPeople + '\'' +
                ", friends='" + friends + '\'' +
                ", favour='" + favour + '\'' +
                ", time='" + time + '\'' +
                ", introduction='" + introduction + '\'' +
                ", scale='" + scale + '\'' +
                ", isFollowed='" + isFollowed + '\'' +
                ", pushToken='" + pushToken + '\'' +
                ", balance='" + balance + '\'' +
                ", vip_flg=" + vip_flg +
                '}';
    }

    public String getSizeCompanyId() {
        return sizeCompanyId;
    }

    public void setSizeCompanyId(String sizeCompanyId) {
        this.sizeCompanyId = sizeCompanyId;
    }

    public String getRegMoneyId() {
        return regMoneyId;
    }

    public void setRegMoneyId(String regMoneyId) {
        this.regMoneyId = regMoneyId;
    }

    public String getAllyearId() {
        return allyearId;
    }

    public void setAllyearId(String allyearId) {
        this.allyearId = allyearId;
    }
}