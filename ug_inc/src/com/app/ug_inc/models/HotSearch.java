package com.app.ug_inc.models;

import java.util.List;

/**
 * Created by WilliZ on 2015/5/3.
 */
public class HotSearch {
    private List<String> hotwordList;
    private String updateTime;

    public List<String> getHotwordList() {
        return hotwordList;
    }

    public void setHotwordList(List<String> hotwordList) {
        this.hotwordList = hotwordList;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
