package com.app.ug_inc.models;

import java.io.Serializable;

public class Contract implements Serializable {
	private static final long serialVersionUID = -1201160624382087660L;

	private String id;
	private String name;
	private String publishTime;
	private String costWithoutTax;
	private String publisherRealName;
	private String publisherCode;
	private String publisherAddress;
	private String publisherTel;
	private String publisherFax;
	private String receiverRealName;
	private String jobTime;
	private String payTime;
	private String remark;

	private String status;
	private String agreement;
	private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getCostWithoutTax() {
		return costWithoutTax;
	}

	public void setCostWithoutTax(String costWithoutTax) {
		this.costWithoutTax = costWithoutTax;
	}

	public String getPublisherRealName() {
		return publisherRealName;
	}

	public void setPublisherRealName(String publisherRealName) {
		this.publisherRealName = publisherRealName;
	}

	public String getPublisherCode() {
		return publisherCode;
	}

	public void setPublisherCode(String publisherCode) {
		this.publisherCode = publisherCode;
	}

	public String getPublisherAddress() {
		return publisherAddress;
	}

	public void setPublisherAddress(String publisherAddress) {
		this.publisherAddress = publisherAddress;
	}

	public String getPublisherTel() {
		return publisherTel;
	}

	public void setPublisherTel(String publisherTel) {
		this.publisherTel = publisherTel;
	}

	public String getPublisherFax() {
		return publisherFax;
	}

	public void setPublisherFax(String publisherFax) {
		this.publisherFax = publisherFax;
	}

	public String getReceiverRealName() {
		return receiverRealName;
	}

	public void setReceiverRealName(String receiverRealName) {
		this.receiverRealName = receiverRealName;
	}

	public String getJobTime() {
		return jobTime;
	}

	public void setJobTime(String jobTime) {
		this.jobTime = jobTime;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
