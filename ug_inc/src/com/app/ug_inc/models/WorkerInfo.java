package com.app.ug_inc.models;

import java.io.Serializable;

public class WorkerInfo implements Serializable {
	private static final long serialVersionUID = 4274209231984463265L;

	private String id;
	private String publishTime;
	private String startTime;
	private String endTime;
	private User user; // also publisher
	private String describe;
	private String cityId;
	private String district;
	private String peopleSum;
	private String jobId;
	private String jobName; // when jobid=0 (other)

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPeopleSum() {
		return peopleSum;
	}

	public void setPeopleSum(String peopleSum) {
		this.peopleSum = peopleSum;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}