package com.app.ug_inc.models;

import java.io.Serializable;

public class Company implements Serializable {
	private static final long serialVersionUID = -958957574756168294L;

	private String id = "";
	private String name = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
