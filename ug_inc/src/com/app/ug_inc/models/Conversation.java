package com.app.ug_inc.models;

import java.io.Serializable;
import java.util.List;

public class Conversation implements Serializable {
    private static final long serialVersionUID = -8969434450255408631L;

    private String id;
    private String type;
    private String time;
    private String status;
    private User fromUser;
    private List<Message> chatList;
    // ------ added ------
    private String name; // USER or GROUP(SUM)
    private String intro; // intro
    private boolean isGroup; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public List<Message> getChatList() {
        return chatList;
    }

    public void setChatList(List<Message> chatList) {
        this.chatList = chatList;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setIsGroup(boolean isGroup) {
        this.isGroup = isGroup;
    }
}
