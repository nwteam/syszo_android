package com.app.ug_inc.models;

import java.io.Serializable;

public class SimpleKV2 implements Serializable {
    private static final long serialVersionUID = 216123722245674202L;

    private String key;
    private String value;

    private boolean hasChild;

    public SimpleKV2() {
    }

    public SimpleKV2(String k, String v) {
        this.key = k;
        this.value = v;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "[K=" + getKey() + ", V=" + getValue() + "] ";
    }

    public boolean hasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }
}
