package com.app.ug_inc.models;

import java.io.Serializable;

/**
 * Chiebukuro
 * Created by Will on 2015/5/2.
 */
public class SystComment implements Serializable {
    private static final long serialVersionUID = -801235221071501625L;

    private String time;
    private String content;
    private User user;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
