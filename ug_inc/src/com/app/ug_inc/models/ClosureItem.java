package com.app.ug_inc.models;

public class ClosureItem {
	private String count; // Total comment count to this closure
	private Company company;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}