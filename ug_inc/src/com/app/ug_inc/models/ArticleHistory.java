package com.app.ug_inc.models;

import java.io.Serializable;
import java.util.List;

public class ArticleHistory implements Serializable {
    private static final long serialVersionUID = -5696400968284438560L;

    private List<Chieb> mChiebList;
    private List<Syst> mSystList;

    public List<Syst> getmSystList() {
        return mSystList;
    }

    public void setmSystList(List<Syst> mSystList) {
        this.mSystList = mSystList;
    }

    public List<Chieb> getmChiebList() {
        return mChiebList;
    }

    public void setmChiebList(List<Chieb> mChiebList) {
        this.mChiebList = mChiebList;
    }
}
