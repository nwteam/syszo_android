package com.app.ug_inc.models;

import java.io.Serializable;

/**
 * Chiebukuro
 * Created by Will on 2015/5/2.
 */
public class ChiebComment implements Serializable {
    private static final long serialVersionUID = -801235221071501625L;

    private String id;
    private String time;
    private String content;
    private User user;
    private boolean isSelf;
    private boolean liked;

    private String title = "";
    private String commentCount = "";
    private boolean urgent = false;
    private boolean commited;
    private String picUrl = "";

    private int likeNum = 0; 
    private int comment_goods = 0;
    
    public int getComment_goods() {
		return comment_goods;
	}

	public void setComment_goods(int comment_goods) {
		this.comment_goods = comment_goods;
	}

	public int getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(int likeNum) {
		this.likeNum = likeNum;
	}

	public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public boolean isCommited() {
        return commited;
    }

    public void setCommited(boolean commited) {
        this.commited = commited;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setIsSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ChiebComment{" +
                "id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", content='" + content + '\'' +
                ", user=" + user +
                ", isSelf=" + isSelf +
                ", liked=" + liked +
                ", title='" + title + '\'' +
                ", commentCount='" + commentCount + '\'' +
                ", urgent=" + urgent +
                ", commited=" + commited +
                ", picUrl='" + picUrl + '\'' +
                ", likeNum=" + likeNum +
                ", comment_goods=" + comment_goods +
                '}';
    }
}
