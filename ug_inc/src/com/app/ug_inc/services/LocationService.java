package com.app.ug_inc.services;

import com.app.ug_inc.utils.ZLog;
import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;

public class LocationService extends Service implements LocationListener {
	public static final String TAG = LocationService.class.getSimpleName();

	private static final long TIMEOUT = 60 * 1000;
	private static final long DEF_TIME = 5 * 1000;

	public static final String ACTION_LOCATION_COMPLETE = TAG + ".action.location_complete";

	private LocationManager mLocationManager;

	private LocationService SELF = LocationService.this;

	private CountDownTimer mTimeOutTimer = new CountDownTimer(TIMEOUT, 1000) {

		@Override
		public void onTick(long millisUntilFinished) {
		}

		@Override
		public void onFinish() {
			ZLog.d("Locate timeout.");
			mLocationManager.removeUpdates(SELF);

			Intent intent = new Intent();
			intent.setAction(ACTION_LOCATION_COMPLETE);
			intent.putExtra("result", "timeout");
			sendBroadcast(intent);

			stopSelf(); // stop the service
		}
	};

	private Handler mHandler = new Handler();
	private Runnable mKnownLocRunnable = new Runnable() {

		@Override
		public void run() {
			Location lastKnownLocation = mLocationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (lastKnownLocation != null) {
				ZLog.d("Using last known location: (lat=" + lastKnownLocation.getLatitude()
						+ ", lng=" + lastKnownLocation.getLongitude() + ")");
				receviedLocation(lastKnownLocation);
			}
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	}

	private long mLocateTime;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = mLocationManager.getBestProvider(criteria, true);
		mLocationManager.requestLocationUpdates(provider, 0, 0, SELF);

		ZLog.d("Start locate with BEST PROVIDER(" + provider + ")...");

		mHandler.postDelayed(mKnownLocRunnable, DEF_TIME);

		mTimeOutTimer.start();
		return START_STICKY;
	}

	private void receviedLocation(Location loc) {
		mTimeOutTimer.cancel();

		Intent intent = new Intent();
		intent.setAction(ACTION_LOCATION_COMPLETE);
		intent.putExtra("result", "success");
		intent.putExtra("lat", loc.getLatitude());
		intent.putExtra("lon", loc.getLongitude());
		sendBroadcast(intent);

		stopSelf(); // stop the service when location was archived
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mLocationManager.removeUpdates(SELF);
	}

	@Override
	public void onLocationChanged(Location loc) {
		ZLog.d("Location onChanged: (lat=" + loc.getLatitude() + ", lng=" + loc.getLongitude()
				+ ")" + ". Cost=" + (System.currentTimeMillis() - mLocateTime) + "ms.");
		mHandler.removeCallbacks(mKnownLocRunnable);
		receviedLocation(loc);
	}

	@Override
	public void onProviderDisabled(String arg0) {
	}

	@Override
	public void onProviderEnabled(String arg0) {
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
}