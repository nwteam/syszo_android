package com.app.ug_inc.ui.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.app.ug_inc.R;

/**
 * Created by WilliZ on 2015/5/3.
 */
public class CountNumTextview extends TextView {
    public TextView mLinkedTv;
    public int mMax;

    public CountNumTextview(Context context) {
        super(context);
    }

    public CountNumTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void linkTextView(TextView tv, int max) {
        mLinkedTv = tv;
        mMax = max;

        makeText(max);

        if (mLinkedTv != null) {
            mLinkedTv.addTextChangedListener(mWatcher);
        }
    }

    private void makeText(int len) {
        setText(getContext().getString(R.string.hint_last_input_word, len));
    }

    private TextWatcher mWatcher = new TextWatcher() {
        private CharSequence temp;
        private int selectionStart;
        private int selectionEnd;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void afterTextChanged(Editable s) {
            int number = mMax - s.length();
//            setText("" + number);
            makeText(number);
            selectionStart = mLinkedTv.getSelectionStart();
            selectionEnd = mLinkedTv.getSelectionEnd();
            if (temp.length() > mMax) {
                s.delete(selectionStart - 1, selectionEnd);
                int tempSelection = selectionEnd;
                mLinkedTv.setText(s);
                if (mLinkedTv instanceof EditText) {
                    ((EditText) mLinkedTv).setSelection(tempSelection);
                }
            }
        }
    };
}
