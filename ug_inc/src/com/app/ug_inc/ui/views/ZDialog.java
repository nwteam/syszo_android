package com.app.ug_inc.ui.views;

import com.app.ug_inc.R;
import com.app.ug_inc.ui.utils.FontUtil;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ZDialog {
	private ZDialog mZDialog;

	private Context mContext;

	private AlertDialog mDialog;

	private View mRoot;

	private View mContainer;

	private TextView mTitleTv;

	@SuppressWarnings("unused")
	private LinearLayout mContentLayout;
	private ProgressBar mLoadingPb;
	private TextView mContentTv;

	private LinearLayout mBottomLayout;
	private View mBottomDividerView;
	private Button mLeftButton;
	private Button mRightButton;

	public interface LeftButtonListener {
		public void onLeftButtonClick();
	}

	public interface RightButtonListener {
		public void onRightButtonClick();
	}

	public ZDialog(Context context) {
		mContext = context;
		mZDialog = this;

		init();
	}

	private void init() {
		mDialog = new AlertDialog.Builder(mContext).create();
		mRoot = LayoutInflater.from(mContext).inflate(R.layout.dialog_common, null);

		mContainer = mRoot.findViewById(R.id.rlayout_dialog_common_container);

		mTitleTv = (TextView) mRoot.findViewById(R.id.tv_dialog_common_title);
		FontUtil.makeTextViewFont(mTitleTv, FontUtil.FONT_ROUNDED_MPLUS);

		mContentLayout = (LinearLayout) mRoot.findViewById(R.id.llayout_dialog_common_content);
		mLoadingPb = (ProgressBar) mRoot.findViewById(R.id.pb_dialog_common_loading);
		mContentTv = (TextView) mRoot.findViewById(R.id.tv_dialog_common_content);

		mBottomLayout = (LinearLayout) mRoot.findViewById(R.id.llayout_dialog_common_bottom);

		mLeftButton = (Button) mRoot.findViewById(R.id.btn_dialog_common_left);
		mRightButton = (Button) mRoot.findViewById(R.id.btn_dialog_common_right);
		mBottomDividerView = mRoot.findViewById(R.id.divider_dialog_common_bottom);

		mBottomLayout.setVisibility(View.GONE);
	}

	public ZDialog setBackground(int res) {
		mContainer.setBackgroundResource(res);

		return mZDialog;
	}

	public boolean isShowing() {
		return mDialog.isShowing();
	}

	public ZDialog setCancelable(boolean cancelable) {
		mDialog.setCancelable(cancelable);

		return mZDialog;
	}

	public void show() {
		mDialog.show();
		Window window = mDialog.getWindow();
		window.setContentView(mRoot);
	}

	public void dismiss() {
		mDialog.dismiss();
	}

	public ZDialog setTitle(CharSequence text) {
		mTitleTv.setText(text);
		mTitleTv.setVisibility(View.VISIBLE);

		return mZDialog;
	}

	public ZDialog setContent(CharSequence text) {
		mContentTv.setText(text);

		return mZDialog;
	}

	public ZDialog showProgressBar() {
		mLoadingPb.setVisibility(View.VISIBLE);

		return mZDialog;
	}

	public ZDialog setLeftButton(CharSequence text) {
		return setLeftButton(text, null);
	}

	public ZDialog setLeftButton(CharSequence text, LeftButtonListener l) {
		return setLeftButton(text, l, true);
	}

	public ZDialog setLeftButton(CharSequence text, final LeftButtonListener l,
			final boolean needDismiss) {
		mBottomLayout.setVisibility(View.VISIBLE);
		mLeftButton.setVisibility(View.VISIBLE);
		mLeftButton.setText(text);
		mLeftButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (l != null) {
					l.onLeftButtonClick();
				}

				if (needDismiss) {
					mDialog.dismiss();
				}
			}
		});
		makeBottomDivider();

		return mZDialog;
	}

	public ZDialog setRightButton(CharSequence text) {
		return setRightButton(text, null, true);
	}

	public ZDialog setRightButton(CharSequence text, RightButtonListener l) {
		return setRightButton(text, l, true);
	}

	public ZDialog setRightButton(CharSequence text, final RightButtonListener l,
			final boolean needDismiss) {
		mBottomLayout.setVisibility(View.VISIBLE);
		mRightButton.setVisibility(View.VISIBLE);
		mRightButton.setText(text);
		mRightButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (l != null) {
					l.onRightButtonClick();
				}

				if (needDismiss) {
					mDialog.dismiss();
				}
			}
		});
		makeBottomDivider();

		return mZDialog;
	}

	private void makeBottomDivider() {
		boolean visible = mLeftButton.getVisibility() == View.VISIBLE
				&& mRightButton.getVisibility() == View.VISIBLE;
		mBottomDividerView.setVisibility(visible ? View.VISIBLE : View.GONE);
	}
}