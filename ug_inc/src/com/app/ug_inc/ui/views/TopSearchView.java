package com.app.ug_inc.ui.views;

import com.app.ug_inc.R;
import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class TopSearchView extends RelativeLayout {

	private EditText mSearchEt;

	public TopSearchView(Context context) {
		super(context);
		init();
	}

	public TopSearchView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		inflate(getContext(), R.layout.search_edittext, this);

		mSearchEt = (EditText) findViewById(R.id.et_searchview_text);
	}

	public Editable getText() {
		return mSearchEt.getText();
	}
}
