package com.app.ug_inc.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;

public class UnFollowConfirmDialog extends Dialog {
    private static final int TOP = 35;
    private Context mContext;

    public interface OnClickListener {
        public void onConfirmClick();
    }

    private OnClickListener listener;

    public UnFollowConfirmDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dlg_unfollow);
        ImageView mCloseBtn = (ImageView) findViewById(R.id.iv_showing_dialog_close);
        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UnFollowConfirmDialog.this.dismiss();
            }
        });

        findViewById(R.id.btn_dlg_unfollow_true).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onConfirmClick();
                }

                dismiss();
            }
        });

        findViewById(R.id.btn_dlg_unfollow_false).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Window dialogWindow = this.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        lp.y = (int) (mContext.getApplicationContext().getResources().getDisplayMetrics().density * TOP);
        dialogWindow.setAttributes(lp);
    }

    public void setOnClickListener(OnClickListener l) {
        listener = l;
    }
}
