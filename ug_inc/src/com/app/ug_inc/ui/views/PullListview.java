package com.app.ug_inc.ui.views;

import android.content.Context;
import android.util.AttributeSet;

import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class PullListview extends PullToRefreshListView {

	public PullListview(Context context, AttributeSet attrs) {
		super(context, attrs);

		getRefreshableView().setFooterDividersEnabled(false);
	}

}
