package com.app.ug_inc.ui.views;

import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.ui.utils.DisplayUtil;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ZDropMenu {
	private Context mContext;

	private PopupWindow mPopupWindow;
	private ListAdapter mAdapter;
	private LinearLayout mContainer;
	private ListView mListView;

	private List<String> mItemList;

	public OnItemClickListener mItemClickListener;

	private final int H_SCREEN;

	private int mHeight;

	private boolean mIsItemClickDismiss = true;

	public interface OnItemClickListener {
		public void onItemClicked(int position);
	}

	public ZDropMenu(Context context, List<String> items) {
		this(context, items, false);
	}

	public ZDropMenu(Context context, List<String> items, boolean needBlankAtFirst) {
		mContext = context;

		mItemList = items;
		if (needBlankAtFirst) {
			mItemList.add(0, "指定しない");
		}

		H_SCREEN = mContext.getResources().getDisplayMetrics().heightPixels;

		// 5dp padding
		int _5dp = DisplayUtil.dp2px(mContext, 5);
		// 40dp per item height + 5dp*2 padding(top&bottom)
		mHeight = mItemList.size() * DisplayUtil.dp2px(mContext, 40) + _5dp * 2;

		// set max height to H_SCREEN / 2 - 40dp
		int maxHeight = H_SCREEN / 2 - DisplayUtil.dp2px(mContext, 40);
		mHeight = mHeight > maxHeight ? maxHeight : mHeight;

		mContainer = new LinearLayout(mContext);
		mListView = new ListView(mContext);
		mAdapter = new ListAdapter();
		mListView.setAdapter(mAdapter);
		mListView.setBackgroundResource(R.drawable.img_bg_pulldown);
		mListView.setPadding(_5dp, _5dp, _5dp, _5dp);
		mListView.setDivider(new ColorDrawable(Color.TRANSPARENT));
		mListView.setDividerHeight(0);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (mItemClickListener != null) {
					mItemClickListener.onItemClicked(position);
				}

				if (mIsItemClickDismiss) {
					mPopupWindow.dismiss();
				}
			}
		});
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
		params.setMargins(_5dp * 2, 0, _5dp * 2, 0);
		mContainer.addView(mListView, params);

		// 2/3 screen width
		int screenW = mContext.getResources().getDisplayMetrics().widthPixels;
		int w = screenW - DisplayUtil.dp2px(mContext, 10) * 2;

		mPopupWindow = new PopupWindow(mContainer, w, mHeight);
		mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		mPopupWindow.setOutsideTouchable(true);
		mPopupWindow.setFocusable(true);
		mPopupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
		// mPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
		mPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	public void show(View anchor) {
		if (mPopupWindow.isShowing()) {
			return;
		}

		int[] location = new int[2];
		anchor.getLocationOnScreen(location);

		// mPopupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, location[0],
		// location[1] - mHeight);

		if (location[1] < (double) H_SCREEN * 2.0 / 3.0) {
			mPopupWindow.showAsDropDown(anchor);
		} else {
			showAsUp(anchor);
		}
	}

	public void showAsUp(View v) {
		int[] location = new int[2];
		v.getLocationOnScreen(location);

		mPopupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0], location[1] - mHeight);
	}

	public void dismiss() {
		mPopupWindow.dismiss();
	}

	public void setOnItemClickListener(OnItemClickListener l) {
		mItemClickListener = l;
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mItemList.size();
		}

		@Override
		public Object getItem(int position) {
			return mItemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h = null;
			if (convertView == null) {
				h = new Holder();
				convertView = mInflater.inflate(R.layout.item_list_popup, null);
				h.name = (TextView) convertView.findViewById(R.id.tv_item_list_popup_name);

				convertView.setTag(h);
			} else {
				h = (Holder) convertView.getTag();
			}

			h.name.setText("" + mItemList.get(position));

			return convertView;
		}

		private class Holder {
			public TextView name;
		}
	}
}
