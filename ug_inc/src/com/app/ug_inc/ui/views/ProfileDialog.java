package com.app.ug_inc.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;

/**
 * 图片点击dialog
 * Created by 昕 on 2015/5/9.
 */
public class ProfileDialog extends Dialog {
    private static final int TOP = 35;
    private Context mContext;
    private String mContent;

    public ProfileDialog(Context context, String mContent) {
        super(context);
        this.mContext = context;
        this.mContent = mContent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_showing_dialog);
        ImageView mCloseBtn = (ImageView) findViewById(R.id.iv_showing_dialog_close);
        TextView mContent = (TextView) findViewById(R.id.tv_showing_dialog_content);
        mContent.setText(this.mContent + "");
        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileDialog.this.dismiss();
            }
        });
        Window dialogWindow = this.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        lp.y = (int) (mContext.getApplicationContext().getResources().getDisplayMetrics().density * TOP);
        dialogWindow.setAttributes(lp);
    }
}
