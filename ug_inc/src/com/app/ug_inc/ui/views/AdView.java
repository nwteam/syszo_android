package com.app.ug_inc.ui.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.app.ug_inc.models.AdInfo;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ADController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.utils.ZLog;

/**
 * Created by yangshiqin on 16/5/1.
 */
public class AdView extends ImageView implements UIDelegate , View.OnClickListener{

    //test
//    String url_img="http://t.syszo.com/";

    String url_img="http://syszo.com/";
    AdInfo info;
    ADController mAdcontroller;

    Context mContext;

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        initAd();
    }

    void initAd(){
        mAdcontroller = new ADController(this);
        mAdcontroller.get_ad();

        this.setOnClickListener(this);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        if(ifId == InterfaceIds.GET_AD.IF_ID){
            info = (AdInfo) obj.getResultObject();
            ZImageLoader.asyncLoadImage(url_img+info.getAd_img(),this);
        }
    }

    @Override
    public void onRequestError(int ifId, String errMsg) {

    }

    @Override
    public void onClick(View v) {
        if(info != null && info.getLink_url()!= null && !info.getLink_url().equals("")){
            Uri uri = Uri.parse(info.getLink_url());
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            mContext.startActivity(it);
        }
    }
}
