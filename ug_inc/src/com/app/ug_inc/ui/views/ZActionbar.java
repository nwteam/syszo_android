package com.app.ug_inc.ui.views;

import com.app.ug_inc.R;
import com.app.ug_inc.ui.utils.DisplayUtil;
import com.app.ug_inc.ui.utils.FontUtil;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ZActionbar extends FrameLayout {
    public static final int BG_ACBAR_COLOR_DEFAULT_GREEN = 0xFF76BE9D;
    @SuppressWarnings("unused")
    private int DP_48 = -1; // the real actionbar height
    private int DP_40 = -1; // the inner action item height, layout's
    private int DP_30 = -1; // the inner action item height, layout's
    private int DP_2_HALF = -1;
    // padding=4dp

    private RelativeLayout mRoot;
    private TextView mTitleTv;
    private LinearLayout mRightActionLayout;
    private LinearLayout mLeftActionLatyout;

    private ImageView mBgIv;
    private ImageView mIconIv;

    public ZActionbar(Context context) {
        super(context);
        init();
    }

    public ZActionbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        Context context = getContext();
        DP_48 = DisplayUtil.dp2px(context, 48);
        DP_40 = DisplayUtil.dp2px(context, 40);
        DP_30 = DisplayUtil.dp2px(context, 30);
        DP_2_HALF = DisplayUtil.dp2px(context, 2.5f);
        inflate(context, R.layout.m_actionbar, this);
        mRoot = (RelativeLayout) findViewById(R.id.rlayout_actionbar_container);

        mBgIv = (ImageView) findViewById(R.id.iv_actionbar_bg);
        mBgIv.setBackgroundColor(BG_ACBAR_COLOR_DEFAULT_GREEN);

        Typeface tf = FontUtil.getTypeface(context, FontUtil.FONT_HEADER2);
        mTitleTv = (TextView) findViewById(R.id.tv_actionbar_title);
        mTitleTv.setTypeface(tf);
        mIconIv = (ImageView) findViewById(R.id.iv_actionbar_icon);

        mRightActionLayout = (LinearLayout) findViewById(R.id.llayout_actionbar_action_right);
        mLeftActionLatyout = (LinearLayout) findViewById(R.id.llayout_actionbar_action_left);
    }

    public void setBackgound(Drawable d) {
        mBgIv.setBackgroundDrawable(d);
    }

    public void setBackgroundColor(int color) {
        mBgIv.setBackgroundColor(color);
    }

    public void setTitle(CharSequence title) {
        mTitleTv.setText(title);
    }

    public void setIcon(int res) {
        setIcon(getResources().getDrawable(res));
    }

    public void setIcon(Drawable d) {
        mIconIv.setImageDrawable(d);

        mIconIv.setVisibility(View.VISIBLE);
    }

    public void addLeftActionItem(View child) {
        // set default to h=40dp, and w=-2(WRAP_CONTENT).
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, DP_30);
        addLeftActionItem(child, -1, params);
    }

    public void addLeftActionItem(View child, LinearLayout.LayoutParams params) {
        addLeftActionItem(child, -1, params);
    }

    public void addLeftActionItem(View child, int index, LinearLayout.LayoutParams params) {
        mLeftActionLatyout.addView(child, index, params);
    }

    public void addRightActionItem(View child) {
        // set default to h=40dp, and w=-2(WRAP_CONTENT).
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, DP_30);
        addRightActionItem(child, -1, params);
    }

    public void addRightActionItem(View child, LinearLayout.LayoutParams params) {
        addRightActionItem(child, -1, params);
    }

    public void addRightActionItem(View child, int index, LinearLayout.LayoutParams params) {
        if (mRightActionLayout.getChildCount() > 0) {
            params.setMargins(DisplayUtil.dp2px(getContext(), 5), 0, 0, 0);
        }
        mRightActionLayout.addView(child, index, params);
    }

    public void makeActionItem(Context context, int res, View.OnClickListener l) {
        ImageView iv = new ImageView(context);
        iv.setOnClickListener(l);
        iv.setImageResource(res);
        iv.setPadding(DP_2_HALF, DP_2_HALF, DP_2_HALF, DP_2_HALF);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(DP_30, DP_30));
    }

    public void makeActionItemLong(Context context, int res, View.OnClickListener l) {
        ImageView iv = new ImageView(context);
        iv.setOnClickListener(l);
        iv.setImageResource(res);
        iv.setPadding(DP_2_HALF, DP_2_HALF, DP_2_HALF, DP_2_HALF);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(DP_30, DP_30 * 2));
    }
}
