package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SettingPickListActivity extends BaseActivity {
    private ArrayList<SimpleKV> mList;
    private ListAdapter mAdapter;

    private int mIndex = -1; // default 0

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_pick_list);

        enableBack();

        Intent intent = getIntent();

        makeTitle("" + intent.getStringExtra("title"));

        mList = (ArrayList<SimpleKV>) intent.getSerializableExtra("array");

        mIndex = intent.getIntExtra("check", -1);

        ListView lv = (ListView) findViewById(R.id.lv_setting_pick_list);
        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultOK();
            }
        });
        mAdapter = new ListAdapter();
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                mIndex = arg2;

                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void resultOK() {
        if (mIndex == -1){
            return;
        }else{
            Intent intent = new Intent();
            intent.putExtra("result", mList.get(mIndex));
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return mList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_setting_pick, null);

                h.check = (ImageView) arg1.findViewById(R.id.iv_item_list_setting_pick);
                h.name = (TextView) arg1.findViewById(R.id.tv_item_list_settting_pick_name);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            if (arg0 == mIndex) {
                h.check.setImageResource(R.drawable.ic_btn_list_checked);
            } else {
                h.check.setImageResource(R.drawable.ic_btn_list_normal);
            }

            h.name.setText("" + mList.get(arg0).getValue());

            return arg1;
        }

        private class Holder {
            public TextView name;
            public ImageView check;
        }
    }
}
