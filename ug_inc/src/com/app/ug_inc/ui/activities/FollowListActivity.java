package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FollowListActivity extends BaseListNetActivity {
	private ArrayList<User> mFollowedUserList;

	private PullListview mListview;
	private ListAdapter mAdapter;

	private AccountController mAccountController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follow_list);

		makeTitle("知っている人一覧");
		enableBack();

		mFollowedUserList = new ArrayList<User>();

		final String targetId = getIntent().getExtras().getString("uid");

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_follow_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mAccountController.userGetFollowedList(targetId, mCurrPage + 1);
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mAccountController = new AccountController(this);
		mAccountController.userGetFollowedList(targetId, ConstData.PAGE_FIRST);
		mLoadingDialog.show();
		mCurrStatus = STATUS.REFRESH;

		// test();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.UESR_GET_FOLLOWED_LIST.IF_ID) {
			List<User> list = (ArrayList<User>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mFollowedUserList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}

			mFollowedUserList.addAll(list);
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_FOLLOWED; i++) {
			User user = new User();
			user.setId("" + (i + 1));
			user.setNick("昵称" + i);
			user.setSelfIntro("回家啊哈哈哈哈废话还丢暗示你佛大事");

			mFollowedUserList.add(user);
		}
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mFollowedUserList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mFollowedUserList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_follow, null);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_follow_nick);
				h.city = (TextView) arg1.findViewById(R.id.tv_item_list_follow_city);
				// h.self = (TextView)
				// arg1.findViewById(R.id.tv_item_list_follow_nick);
				h.detail = (ImageView) arg1.findViewById(R.id.iv_item_list_follow_detail);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			final User user = mFollowedUserList.get(arg0);

			h.nick.setText("" + user.getNick());
			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(user.getId());
				}
			});
			h.city.setText("【" + ConstData.getCityById(user.getCityId()) + "】" + user.getJob());
			// h.self.setText("" + user.getNick());

			h.detail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(user.getId());
				}
			});

			return arg1;
		}

		private class Holder {
			private TextView nick;
			private TextView city;
			// private TextView self;
			private ImageView detail;
		}
	}
}