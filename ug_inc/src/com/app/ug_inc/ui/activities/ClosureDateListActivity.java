package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.ui.utils.DisplayUtil;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.ui.views.PullListview;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ClosureDateListActivity extends BaseNetActivity {
	public static final int COUNT_PER_PAGE = 12;

	private ListAdapter mAdapter;
	private PullListview mListview;

	private List<String> mDateList;

	private Calendar mCalendar;

	private ClosureController mClosureController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_closure_date);

		makeTitle("俺の現場がヤバい！！");
		enableBack();

		mDateList = new ArrayList<String>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_closure_date);
		View footer = new View(mContext);
		footer.setBackgroundColor(0xFF000000);
		footer.setLayoutParams(new AbsListView.LayoutParams(-1, DisplayUtil.dp2px(mContext, 1.5f)));
		mListview.getRefreshableView().addFooterView(footer, null, false);
		mListview.getRefreshableView().setFooterDividersEnabled(false);
		mListview.setAdapter(mAdapter);
		mListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent();
				intent.setClass(mContext, ClosureListActivity.class);
				// pull-refresh listview has 1-HEADER
				String date = mDateList.get(position - 1);
				// intent.putExtra("time", date.replace("-", ""));
				intent.putExtra("time", date);

				startActivity(intent);
			}
		});

		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// new Handler().postDelayed(mPullDelay, 500);
			}
		});

		mClosureController = new ClosureController(this);
//		mClosureController.getcl();
		mLoadingDialog.show();

		// initDateList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_CLOSURE_DATE_LIST.IF_ID) {
			mDateList.addAll((List<String>) obj.getResultObject());
			mAdapter.notifyDataSetChanged();
		}
	}

	// private void initDateList() {
	// mCalendar = Calendar.getInstance();
	//
	// for (int i = 0; i < COUNT_PER_PAGE; i++) {
	// mDateList.add(getCurrMonth());
	// mCalendar.add(Calendar.MONTH, -1); // minus 1 month
	// }
	// mAdapter.notifyDataSetChanged();
	// }

	// private String getCurrMonth() {
	// return DateFormat.format(TimeUtil.PATTERN_MONTH, mCalendar).toString();
	// }

	// private Runnable mPullDelay = new Runnable() {
	//
	// @Override
	// public void run() {
	// mListview.onRefreshComplete();
	//
	// for (int i = 0; i < COUNT_PER_PAGE; i++) {
	// mCalendar.add(Calendar.MONTH, -1); // minus 1 month
	// mDateList.add(getCurrMonth());
	// }
	// mAdapter.notifyDataSetChanged();
	// }
	// };

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mDateList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mDateList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_closure_date, null);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_closure_date);
				FontUtil.makeTextViewFont(h.time, FontUtil.FONT_ROUNDED_MPLUS);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			// String[] date = mDateList.get(arg0).split("-");
			String string = mDateList.get(arg0);
			String date = string.substring(0, 4) + "年" + string.substring(4, string.length()) + "月";

			// h.time.setText("" + date[0] + "年" + date[1] + "月");
			h.time.setText("" + date);

			return arg1;
		}

		private class Holder {
			private TextView time;
		}
	}
}
