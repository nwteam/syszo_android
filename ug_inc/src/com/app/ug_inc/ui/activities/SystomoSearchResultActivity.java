package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SystomoResult;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.GroupController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoSearchResultActivity extends BaseListNetActivity {
    public static final String ACTION_FROM_CATEGORY = "action.ACTION_FROM_CATEGORY";
    public static final String ACTION_FROM_FREEWORD = "action.ACTION_FROM_FREEWORD";
    private String ACTION;

    private PullListview mListView;

    private List<User> mUserList;

    private ListAdapter mAdapter;

    private GroupController mRefreshController;
    private GroupController mLoadmoreController;

    private String freeWord;

    private String catId;
    private String groupCatId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo_search_result);

        makeTitle("シストモ検索");
        makeIcon(R.drawable.micon_systomo);
        enableBack();

        // mWord = getIntent().getStringExtra("word");

        Intent intent = getIntent();

        ACTION = intent.getAction();
        makeSepTv(0);

        mUserList = new ArrayList<User>();

        mAdapter = new ListAdapter();

        TextView emptyTv = (TextView) findViewById(R.id.tv_systomo_search_result_empty);

        mListView = (PullListview) findViewById(R.id.plv_systomo_search_result);
        mListView.getRefreshableView().setEmptyView(emptyTv);
        mListView.setAdapter(mAdapter);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (ACTION_FROM_CATEGORY.equals(ACTION)) {
                    mRefreshController.friendSearchListCat(catId, groupCatId, ConstData.PAGE_FIRST);
                } else if (ACTION_FROM_FREEWORD.equals(ACTION)) {
                    mRefreshController.friendSearchList(freeWord, ConstData.PAGE_FIRST);
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (ACTION_FROM_CATEGORY.equals(ACTION)) {
                    mLoadmoreController.friendSearchListCat(catId, groupCatId, mCurrPage + 1);
                } else if (ACTION_FROM_FREEWORD.equals(ACTION)) {
                    mLoadmoreController.friendSearchList(freeWord, mCurrPage + 1);
                }
            }
        });

        mRefreshController = new GroupController(mRefreshDelegate);
        mLoadmoreController = new GroupController(mLoadmoreDelegate);

        if (ACTION_FROM_CATEGORY.equals(ACTION)) {
            catId = getIntent().getStringExtra("catId");
            groupCatId = getIntent().getStringExtra("groupId");
            mRefreshController.friendSearchListCat(catId, groupCatId, ConstData.PAGE_FIRST);
        } else if (ACTION_FROM_FREEWORD.equals(ACTION)) {
            freeWord = intent.getStringExtra("freeWord");
            mRefreshController.friendSearchList(freeWord, ConstData.PAGE_FIRST);
        }

        makeSepTv(0);

        mLoadingDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mRefreshController);
        clearController(mLoadmoreController);
    }

    private void makeSepTv(int num) {
        TextView sepTv = (TextView) findViewById(R.id.tv_item_list_separator_hint);
        String text = "検索結果(" + num + ")";
        if (ACTION_FROM_CATEGORY.equals(ACTION)) {
            text += " - カテゴリ";
        } else if (ACTION_FROM_FREEWORD.equals(ACTION)) {
            text += " - " + freeWord;
        }
        sepTv.setText(text);
    }

    private UIDelegate mRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListView.onRefreshComplete();
            mLoadingDialog.dismiss();
            mCurrPage = ConstData.PAGE_FIRST;
            mUserList.clear();

//            SystomoResult result = (SystomoResult) obj.getResultObject();
//

            List<User> list = (List<User>) obj.getResultObject();
            mUserList.addAll(list);

            makeSepTv(list.size());
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListView.onRefreshComplete();
            mLoadingDialog.dismiss();
            toast(errMsg);
        }
    };

    private UIDelegate mLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListView.onRefreshComplete();
            List<User> list = (List<User>) obj.getResultObject();
            mUserList.addAll(list);

            mCurrPage++;
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListView.onRefreshComplete();
            toast(errMsg);
        }
    };

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        private ListAdapter() {
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mUserList.size();
        }

        @Override
        public User getItem(int position) {
            return mUserList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_systomo_member, null);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_member_name);
                h.profile = (ImageView) convertView.findViewById(R.id.iv_item_list_member_profile);
                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final User user = getItem(position);
            h.name.setText("" + user.getNick());
            h.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProfile(user.getId());
                }
            });

            return convertView;
        }

        private class Holder {
            TextView name;
            ImageView profile;
        }
    }
}
