package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.utils.FontUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MySeriesAddCategoryActivity extends BaseNetActivity {

	private MySeriesController mMySeriesController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myseries_add);

		makeTitle("俺のシリーズ");
		enableBack();

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}

		mMySeriesController = new MySeriesController(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMySeriesController != null) {
			mMySeriesController.clear();
			mMySeriesController = null;
		}
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.ADD_MY_SERIES_CATEGORY.IF_ID) {
			toast(getString(R.string.success_add_my_series_category));
			finish();
		}
	}

	private final int[] TVIEW_IDS = { R.id.tv_myseries_add_hint_top, R.id.tv_myseries_add_hint1,
			R.id.tv_myseries_add_hint2, R.id.tv_myseries_add_hint_ore, R.id.tv_myseries_add_hint_no };

	public void submit(View v) {
		EditText catEt = (EditText) findViewById(R.id.et_myseries_add_category);
		EditText contentEt = (EditText) findViewById(R.id.et_myseries_add_reason);

		String category = catEt.getText().toString();
		String content = contentEt.getText().toString();

		if (checkXX(TextUtils.isEmpty(category), "分類タイトル")) {
			return;
		}

		if (checkXX(TextUtils.isEmpty(content), "理由")) {
			return;
		}

		mMySeriesController.addMyseriesCategory(category, content);
		mLoadingDialog.show();
	}
}
