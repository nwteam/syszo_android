package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Rate;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

public class RateListActivity extends BaseListNetActivity {
	private ArrayList<Rate> mRateList;

	private PullListview mListview;
	private ListAdapter mAdapter;

	private AccountController mAccountController;

	private String mUid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rate_list);

		makeTitle("評価");
		enableBack();

		mUid = getIntent().getStringExtra("uid");

		mRateList = new ArrayList<Rate>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_rate_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mCurrStatus = STATUS.LOAD_MORE;
				mAccountController.userGetRateList(mUid, mCurrPage + 1);
			}
		});

		mAccountController = new AccountController(this);
		mAccountController.userGetRateList(mUid, ConstData.PAGE_FIRST);
		mLoadingDialog.show();
		mCurrStatus = STATUS.REFRESH;
		// test();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.USER_GET_RATE_LIST.IF_ID) {
			if (mCurrStatus == STATUS.REFRESH) {
				mRateList.clear();
				mCurrPage = ConstData.PAGE_FIRST;
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				mCurrPage++;
				mListview.onRefreshComplete();
			}

			mRateList.addAll((ArrayList<Rate>) obj.getResultObject());
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_ARTICLE_HIS; i++) {
			Rate rate = new Rate();
			rate.setId("" + (i + 1));
			rate.setTitle("标题" + i);
			rate.setContent("内容哈哈哈哈哈哈哈哈哈哈哈内容哈哈哈哈哈哈哈哈哈哈哈");
			rate.setTime("2014-08-08");
			rate.setContent("内容内容内容内容内容内容内容" + i);

			mRateList.add(rate);
		}
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mRateList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mRateList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_rate, null);
				h.title = (TextView) arg1.findViewById(R.id.tv_item_list_rate_title);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_rate_content);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_rate_time);
				h.rb = (RatingBar) arg1.findViewById(R.id.rateb_rate_score);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			Rate rate = mRateList.get(arg0);

			h.title.setText((arg0 + 1) + "." + rate.getTitle());
			h.content.setText("" + rate.getContent());
			h.time.setText(rate.getTime());

			try {
				h.rb.setProgress(Double.valueOf(rate.getScore()).intValue());
			} catch (NumberFormatException e) {
				h.rb.setProgress(0);
			}

			return arg1;
		}

		private class Holder {
			private RatingBar rb;
			private TextView time;
			private TextView title;
			private TextView content;
		}
	}
}
