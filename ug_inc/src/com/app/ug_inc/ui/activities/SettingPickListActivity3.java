package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.SimpleKV2;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SettingPickListActivity3 extends BaseNetActivity {
    private ArrayList<SimpleKV2> mList;
    private ListAdapter mAdapter;
    private int mIndex = -1; // default 0
    private AccountController mAccountController;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_pick_list);

        enableBack();

        Intent intent = getIntent();

        String pid = intent.getStringExtra("pid");
        final String array = intent.getStringExtra("array");
        final String title = intent.getStringExtra("title");
        makeTitle("" + title);

        mIndex = intent.getIntExtra("check", -1);
        mAccountController = new AccountController(this);

        ListView lv = (ListView) findViewById(R.id.lv_setting_pick_list);
        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultOK();
            }
        });
        mList = new ArrayList<SimpleKV2>();
        mAdapter = new ListAdapter();
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                SimpleKV2 item = mList.get(arg2);
                if (item.hasChild()) {
                    Intent nIntent = new Intent();
                    nIntent.putExtra("array", array);
                    nIntent.putExtra("pid", item.getKey());
                    nIntent.putExtra("title", title);
                    nIntent.setClass(mContext, SettingPickListActivity3.class);
                    startActivityForResult(nIntent, 1);
                } else {
                    mIndex = arg2;
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        mAccountController.getProfileSettingList(array, pid);
        mLoadingDialog.show();
    }

    private void resultOK() {
        if (mIndex == -1) {
            return;
        } else {
            SimpleKV2 item = mList.get(mIndex);

            SimpleKV kv = new SimpleKV();
            kv.setKey(item.getKey());
            kv.setValue(item.getValue());

            Intent intent = new Intent();
            intent.putExtra("result", kv);
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public SimpleKV2 getItem(int arg0) {
            return mList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_setting_pick, null);

                h.check = (ImageView) arg1.findViewById(R.id.iv_item_list_setting_pick);
                h.name = (TextView) arg1.findViewById(R.id.tv_item_list_settting_pick_name);
                h.sign = (ImageView) arg1.findViewById(R.id.tv_item_list_settting_pick_right);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }


            SimpleKV2 item = getItem(arg0);
            if (arg0 == mIndex) {
                h.check.setImageResource(R.drawable.ic_btn_list_checked);
            } else {
                h.check.setImageResource(R.drawable.ic_btn_list_normal);
            }

            h.name.setText("" + item.getValue());

            if (item.hasChild()) {
                h.check.setVisibility(View.GONE);
                h.sign.setVisibility(View.VISIBLE);
            } else {
                h.check.setVisibility(View.VISIBLE);
                h.sign.setVisibility(View.INVISIBLE);
            }

            return arg1;
        }

        private class Holder {
            public TextView name;
            public ImageView check;
            public ImageView sign;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mAccountController);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_PROFILE_SETTING_LIST.IF_ID) {
            mList = (ArrayList<SimpleKV2>) obj.getResultObject();
        }

        mAdapter.notifyDataSetChanged();
        mLoadingDialog.dismiss();
    }
}
