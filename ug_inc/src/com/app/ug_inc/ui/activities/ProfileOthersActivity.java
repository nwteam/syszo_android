package com.app.ug_inc.ui.activities;

import net.nend.android.NendAdView;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.ProfileDialog;
import com.app.ug_inc.ui.views.UnFollowConfirmDialog;
import com.app.ug_inc.utils.ConstData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * 其他人个人页面
 * Created by 昕 on 2015/5/15.
 */
public class ProfileOthersActivity extends BaseNetActivity {
    public static final String ACTION_FRIEND_NEED_REFRESH = "action.ACTION_FRIEND_NEED_REFRESH";

    private static final int RIGHT_SECOND_INDEX = 0xFF76BE9A;
    private ImageView mMessageIv;
    private ImageView mAddFriendsIv;
    private TextView mNicknameTv;
    private TextView mSexTv;
    private TextView mAddressTv;
    private TextView mJobTv;
    private TextView mCompanyScaleTv;
    private TextView mPositionTv;
    private TextView mBudgetScaleTv;
    private TextView mHistoryTv;
    private TextView mSoftwareTv;
    private TextView mHardwareTv;
    private TextView mCertificationTv;
//    private TextView mAnnualSalaryTv;
    private TextView mFriendsTv;
    private TextView mFavourTv;
    private TextView mSelfIntroductionTv;
    private AccountController mAccountController;
    private Bundle mBundle = null;
    private User mUser;
    private String mInitUid;
    private Context mContext;
//    private AdView mAdView;
//    private NendAdView mNendAdview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_others);
        mContext = ProfileOthersActivity.this;
        initView();
        mInitUid = getIntent().getStringExtra("user_id");

        mAccountController = new AccountController(this);
        mAccountController.getProfile(mInitUid);
        mLoadingDialog.show();
    }

    private void initView() {
        makeTitle("プロフィール");
        enableBack();
        makeIcon(R.drawable.micon_systomo);
        mMessageIv = (ImageView) findViewById(R.id.iv_profile_message);
        mAddFriendsIv = (ImageView) findViewById(R.id.iv_prifile_add_friend);
        mNicknameTv = (TextView) findViewById(R.id.tv_profile_nickname);
        mSexTv = (TextView) findViewById(R.id.tv_profile_sex);
        mAddressTv = (TextView) findViewById(R.id.tv_profile_address);
        mJobTv = (TextView) findViewById(R.id.tv_profile_job);
        mCompanyScaleTv = (TextView) findViewById(R.id.tv_profile_company_scale);
        mPositionTv = (TextView) findViewById(R.id.tv_profile_position);
        mBudgetScaleTv = (TextView) findViewById(R.id.tv_profile_budget_scale);
        mHistoryTv = (TextView) findViewById(R.id.tv_profile_history);
        mSoftwareTv = (TextView) findViewById(R.id.tv_profile_software);
        mHardwareTv = (TextView) findViewById(R.id.tv_profile_hardware);
        mCertificationTv = (TextView) findViewById(R.id.tv_profile_certification);
//        mAnnualSalaryTv = (TextView) findViewById(R.id.tv_profile_annual_salary);
        mFriendsTv = (TextView) findViewById(R.id.tv_profile_friends);
        mFavourTv = (TextView) findViewById(R.id.tv_profile_favour);
        mSelfIntroductionTv = (TextView) findViewById(R.id.tv_profile_self_introduction);
        addRightAction(R.drawable.btn_report, new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                StringBuilder sb = new StringBuilder();
                sb.append("報告するユーザー：" + Account.getUserId());
                sb.append("\n報告されるユーザー：" + mInitUid);
                sb.append("\n");
                sb.append("\nこのユーザーに関して報告したい内容を以下にご入力後送信ください。\n（例）投稿に不適切なコンテンツが多い");

                Intent data = new Intent(Intent.ACTION_SENDTO);
                data.setData(Uri.parse("mailto:support@syszo.com"));
                data.putExtra(Intent.EXTRA_SUBJECT, "ユーザーに関する報告");
                data.putExtra(Intent.EXTRA_TEXT, sb.toString());
                startActivity(data);
            }
        });

      /*  mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());*/
        
//        mNendAdview = (NendAdView)findViewById(R.id.nend);
//        mNendAdview.loadAd();
    }

    private void setParam() {
        mNicknameTv.setText(mUser.getNick() + "");
        mSexTv.setText(ConstData.getSexById(mUser.getSex()) + "");
        mAddressTv.setText(mUser.getCity() + "");
        mJobTv.setText(mUser.getIndustry() + "");
        mCompanyScaleTv.setText(mUser.getSizeCompany() + "");
        mPositionTv.setText(mUser.getJob() + "");
        mBudgetScaleTv.setText(mUser.getRegMoney() + "");
        mHistoryTv.setText(mUser.getCalendar() + "");
        mSoftwareTv.setText(mUser.getSoftware() + "");
        mHardwareTv.setText(mUser.getHardware() + "");
        mCertificationTv.setText(mUser.getCertifaction() + "");
//        mAnnualSalaryTv.setText(mUser.getAllyear() + "");
        mFriendsTv.setText(mUser.getFriends() + "");
        mFavourTv.setText(mUser.getFavour() + "");
        mSelfIntroductionTv.setText(mUser.getSelfIntro() + "");
        mMessageIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("uid", mInitUid);
                intent.putExtra("title", mUser.getNick());
                startActivity(intent);
            }
        });

        makeAddFriend("1".equals(mUser.getIsFollowed()));
    }

    private void makeAddFriend(boolean isFriend) {
        if (isFriend) {
            mAddFriendsIv.setImageResource(R.drawable.ic_followed);
            mAddFriendsIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UnFollowConfirmDialog dlg = new UnFollowConfirmDialog(mContext);
                    dlg.setOnClickListener(new UnFollowConfirmDialog.OnClickListener() {
                        @Override
                        public void onConfirmClick() {
                            mAccountController.userUnfollow(mInitUid);
                            mLoadingDialog.show();
                        }
                    });
                    dlg.show();
                }
            });
        } else {
            mAddFriendsIv.setImageResource(R.drawable.btn_nofollow_big);
            mAddFriendsIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAccountController.userFollow(mInitUid);
                    mLoadingDialog.show();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAccountController != null) {
            mAccountController.clear();
            mAccountController = null;
        }
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.USER_GET_OTHER_PROFILE.IF_ID) {
            mUser = (User) obj.getResultObject();
            setParam();
        } else if (ifId == InterfaceIds.USER_FOLLOW.IF_ID) {
            mUser.setIsFollowed("1");
            makeAddFriend(true);

            sendBroadcast(new Intent(ACTION_FRIEND_NEED_REFRESH));
        } else if (ifId == InterfaceIds.USER_UNFOLLOW.IF_ID) {
            mUser.setIsFollowed("0");
            makeAddFriend(false);

            sendBroadcast(new Intent(ACTION_FRIEND_NEED_REFRESH));
        }
        mLoadingDialog.dismiss();
    }

    @Override
    public void onRequestError(int ifId, String errMsg) {
        super.onRequestError(ifId, errMsg);
        mLoadingDialog.dismiss();
    }
}
