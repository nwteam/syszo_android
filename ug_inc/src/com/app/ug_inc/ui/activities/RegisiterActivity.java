package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.ui.views.ZDropMenu;
import com.app.ug_inc.ui.views.ZDropMenu.OnItemClickListener;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class RegisiterActivity extends BaseNetActivity {
    private EditText mNickNameEt;
    private EditText mEmailEt;
    private EditText mPasswordEt;
    private TextView mSexTv;
    private TextView mBirthTv;
    private TextView mCityTv;
    private EditText mDistrictEt;

    private SimpleKV mSelectedCity;
    private SimpleKV mSelectedSex;

    private AccountController mAccountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        makeTitle("新規登録");
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setMessage(getString(R.string.hint_edit_confirm_content_ok))
                        .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                submit();
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null)
                        .show();
            }
        });

        initViews();

        mAccountController = new AccountController(this);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

        sendBroadcast(new Intent(EntranceActivity.ACTION_NEED_FINISH));

        toast("ログインしました。");

        Account.setAutoLogin(true);

        Intent intent = new Intent();
        intent.setClass(mContext, MenuActivity.class);
        intent.putExtra("auto-login", true);
        startActivity(intent);
        finish();
    }

    private final int[] TVIEW_IDS = {R.id.tv_register_hint_email, R.id.tv_register_hint_nickname,
            R.id.tv_register_hint_password, R.id.tv_register_hint_sex, R.id.tv_register_hint_birth,
            R.id.tv_register_hint_city, R.id.tv_register_hint_district,};

    private void initViews() {
        mNickNameEt = (EditText) findViewById(R.id.et_register_nickname);
        mEmailEt = (EditText) findViewById(R.id.et_register_email);
        mPasswordEt = (EditText) findViewById(R.id.et_register_password);
        mSexTv = (TextView) findViewById(R.id.tv_register_sex);
        mSexTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pickSex();
            }
        });
        mBirthTv = (TextView) findViewById(R.id.tv_register_birth);
        mBirthTv.setText("" + TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE));
        mBirthTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pickBirth();
            }
        });
        mCityTv = (TextView) findViewById(R.id.tv_register_city);
        mCityTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pickCity();
            }
        });
        mDistrictEt = (EditText) findViewById(R.id.et_register_district);

        for (int i = 0; i < TVIEW_IDS.length; i++) {
            FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
                    FontUtil.FONT_ROUNDED_MPLUS);
        }

        mSelectedCity = ConstData.getCityList().get(0);
        mCityTv.setText(mSelectedCity.getValue());

        mSelectedSex = ConstData.getSexList().get(0);
        mSexTv.setText(mSelectedSex.getValue());
    }

    private void pickBirth() {
        String selectedDate = mBirthTv.getText().toString();
        String[] dates = selectedDate.split("-");
        int y = Integer.valueOf(dates[0]);
        int m = Integer.valueOf(dates[1]) - 1; // should -1
        int d = Integer.valueOf(dates[2]);

        DatePickerDialog dpDialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = TimeUtil.format(year, monthOfYear, dayOfMonth,
                                TimeUtil.PATTERN_DATE);

                        mBirthTv.setText("" + date);
                    }
                }, y, m, d);
        DatePicker dp = dpDialog.getDatePicker();
        dp.setMaxDate(TimeUtil.getCurrentStamp());
        dpDialog.show();
    }

    private void pickSex() {
        final List<SimpleKV> sexList = ConstData.getSexList();
        List<String> items = new ArrayList<String>();
        for (SimpleKV kv : sexList) {
            items.add(kv.getValue());
        }
        ZDropMenu menu = new ZDropMenu(mContext, items);
        menu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClicked(int position) {
                mSelectedSex = sexList.get(position);
                mSexTv.setText("" + mSelectedSex.getValue());
            }
        });
        menu.show(mSexTv);
    }

    private void pickCity() {
        final List<SimpleKV> cityList = ConstData.getCityList();
        List<String> items = new ArrayList<String>();
        for (SimpleKV kv : cityList) {
            items.add(kv.getValue());
        }
        ZDropMenu menu = new ZDropMenu(mContext, items);
        menu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClicked(int position) {
                mSelectedCity = cityList.get(position);

                mCityTv.setText("" + mSelectedCity.getValue());
            }
        });
        menu.show(mCityTv);
    }

    private void submit() {
        String nick = mNickNameEt.getText().toString();
        String email = mEmailEt.getText().toString();
        String password = mPasswordEt.getText().toString();
        String sex = mSelectedSex.getKey();
        String birth = mBirthTv.getText().toString();
        String cityId = mSelectedCity.getKey();
        String district = mDistrictEt.getText().toString();

        if (checkXX(TextUtils.isEmpty(nick), "ニックネーム")) {
            return;
        }

        if (checkXX(TextUtils.isEmpty(email), "メールアドレス")) {
            return;
        }

        if (checkXX(TextUtils.isEmpty(password), "パスワード")) {
            return;
        }

//		if (checkXX(TextUtils.isEmpty(birth), "生年月日")) {
//			return;
//		}

//		if (checkXX(TextUtils.isEmpty(district), "市区町村")) {
//			return;
//		}

        User user = new User();
        user.setNick(nick);
        user.setEmail(email);
        user.setPwd(password);
        user.setSex(sex);
        user.setBirth(birth);
        user.setCityId(cityId);
        user.setDistrict(district);

        mAccountController.register(user);
        mLoadingDialog.show();
    }
}