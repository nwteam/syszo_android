package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class SendMessageActivity extends BaseNetActivity {
	private EditText mModifyEt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_input);

		mModifyEt = (EditText) findViewById(R.id.et_setting_input);

		ImageView btn = new ImageView(mContext);
		btn.setImageResource(R.drawable.ic_btn_acbar_decided);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultOK();
			}
		});
        addRightAction(btn);
	}

	private void resultOK() {
		String text = mModifyEt.getText().toString();
		toast(text);
		finish();
	}

}
