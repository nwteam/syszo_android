package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.ClosureItem;
import com.app.ug_inc.models.Company;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.ui.views.TopSearchView;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ClosureListActivity extends BaseListNetActivity {
	private ListAdapter mAdapter;
	private PullListview mListview;

	private ArrayList<ClosureItem> mClosureList;

	private ClosureController mClosureController;

	private TopSearchView mSearchView;

	private String mQueryTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_closure_list);

		makeTitle("俺の現場がヤバい！！");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_search, new OnClickListener() {

			@Override
			public void onClick(View v) {
				search();
			}
		});

		mQueryTime = getIntent().getStringExtra("time");
		mClosureController = new ClosureController(this);
		mClosureController.getClosureList(mQueryTime, ConstData.PAGE_FIRST);
		mLoadingDialog.show();
		mCurrStatus = STATUS.REFRESH;

		mClosureList = new ArrayList<ClosureItem>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_closure_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				final Company company = mClosureList.get(position - 1).getCompany();
				showNeedMoneyDialog(company.getName() + "を見るのに５現使用します。\nよろしいですか？",
						new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								Intent intent = new Intent();
								intent.setClass(mContext, ClosureCommentActivity.class);
								// pull-refresh listview has 1-HEADER
								intent.putExtra("company", company);
								startActivity(intent);
							}
						});
			}
		});
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				if (mIsQuery) {
//					mClosureController.queryClosure(mQueryTime, mTmpSearchWord, mCurrPage + 1);
				} else {
					mClosureController.getClosureList(mQueryTime, mCurrPage + 1);
				}
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mSearchView = (TopSearchView) findViewById(R.id.search_view);
	}

	private void search() {
		String word = mSearchView.getText().toString();
		if (!TextUtils.isEmpty(mTmpSearchWord) && mTmpSearchWord.equals(word)) {
			return;
		}

		mTmpSearchWord = word;

		mClosureList.clear();
		mAdapter.notifyDataSetChanged();

		mCurrPage = ConstData.PAGE_FIRST;
		mCurrStatus = STATUS.REFRESH;
//		mClosureController.queryClosure(mQueryTime, mTmpSearchWord, ConstData.PAGE_FIRST);
		mIsQuery = true;

		mLoadingDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mClosureController != null) {
			mClosureController.clear();
			mClosureController = null;
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_CLOSURE; i++) {
			ClosureItem closure = new ClosureItem();
			closure.setCount("" + (int) (new Random().nextDouble() * 1000));

			Company company = new Company();
			company.setId("" + i);
			company.setName("公司名称公司名称" + i);

			closure.setCompany(company);

			mClosureList.add(closure);
		}
		mAdapter.notifyDataSetChanged();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);

		List<ClosureItem> list = (ArrayList<ClosureItem>) obj.getResultObject();
		System.out.println("success=" + list.size());

		if (ifId == InterfaceIds.GET_CLOSURE_LIST.IF_ID) {
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mClosureList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}
		} else if (ifId == InterfaceIds.QUERY_CLOSURE.IF_ID) {
			System.out.println("query????");
			if (mCurrStatus == STATUS.REFRESH) {
				System.out.println("refresh????");
				mCurrPage = ConstData.PAGE_FIRST;
				mClosureList.clear();
				judgeRefresh(list, "検索結果がありませんでした");
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}
		}

		mClosureList.addAll(list);
		mAdapter.notifyDataSetChanged();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mClosureList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mClosureList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_closure_list, null);
				h.company = (TextView) arg1.findViewById(R.id.tv_item_list_closure_list_company);
				h.count = (TextView) arg1.findViewById(R.id.tv_item_list_closure_list_count);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			ClosureItem closure = mClosureList.get(arg0);

			h.company.setText("" + closure.getCompany().getName());
			h.count.setText("(" + closure.getCount() + ")");

			return arg1;
		}

		private class Holder {
			private TextView company;
			private TextView count;
		}
	}
}
