package com.app.ug_inc.ui.activities;

import net.nend.android.NendAdView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.User;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class AccountActivity extends BaseNetActivity {
//    private AdView mAdView;
//	private NendAdView mNendAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initView();
    }

    private void initView() {
        makeTitle("アカウント");
        enableBack();
        User user = Account.getUser();
        setTvText("" + user.getEmail(), R.id.tv_account_email);

        findViewById(R.id.rlayout_account_pwd).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, PasswordModifyActivity.class);
                        startActivity(intent);
                    }
                });

//        findViewById(R.id.layout_account_hint_email).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new AlertDialog.Builder(mContext).setMessage("ログアウト確認しますか")
//                        .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Account.clear();
//
//                                sendBroadcast(new Intent(ACTION_LOGOUT));
//
//                                Intent intent = new Intent();
//                                intent.setClass(mContext, EntranceActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                            }
//                        })
//                        .setNegativeButton(getString(R.string.cancel), null)
//                        .show();
//            }
//        });

        /*mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());*/
        
//        mNendAdView = (NendAdView) findViewById(R.id.nend);
//        mNendAdView.loadAd();
    }

    private void setTvText(String text, int res) {
        ((TextView) findViewById(res)).setText("" + text);
    }

}
