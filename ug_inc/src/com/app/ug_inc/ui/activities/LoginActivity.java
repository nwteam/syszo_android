package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.utils.DisplayUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends BaseNetActivity {

    private AccountController mAccountController;

    private CheckBox mAutoLoginCb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        makeTitle("ログイン");
        enableBack();

        mAccountController = new AccountController(this);

        String colored = "パスワードを再設定";
        String full = "パスワードを再設定を行ってください";
        SpannableString ss = new SpannableString(full);
        ss.setSpan(new ForegroundColorSpan(0xFF83A7D4), 0, colored.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView hint2 = (TextView) findViewById(R.id.tv_login_hint2);
        hint2.setText(ss);
        hint2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, PasswordResetActivity.class);
                startActivity(intent);
            }
        });

        mAutoLoginCb = (CheckBox) findViewById(R.id.cb_login_auto_login);
        
        loadAdView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mAccountController);
    }

    public void submit(View v) {
        EditText mailEt = (EditText) findViewById(R.id.et_login_email);
        EditText pwdEt = (EditText) findViewById(R.id.et_login_password);
//
        String email = mailEt.getText().toString();
        String pwd = pwdEt.getText().toString();

        if (checkXX(TextUtils.isEmpty(email), "メールアドレス")) {
            return;
        }

        if (checkXX(TextUtils.isEmpty(pwd), "パスワード")) {
            return;
        }

        if (pwd.length() < 6) {
            toast(getString(R.string.hint_input_password_shorter_than_6));
            return;
        }

//        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(pwd)) {
//            mAccountController.login("mike@gmail.com", "123456");
//        } else {
        mAccountController.login(email, pwd);
//        }
        mLoadingDialog.show();
//
//        Intent intent = new Intent();
//        intent.setClass(mContext, MenuActivity.class);
//        startActivity(intent);
//        finish();
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

        sendBroadcast(new Intent(EntranceActivity.ACTION_NEED_FINISH));

        toast("ログインしました。");

        Intent intent = new Intent();
        intent.setClass(mContext, MenuActivity.class);
        intent.putExtra("auto-login", mAutoLoginCb.isChecked());
        startActivity(intent);
        finish();
    }
}