package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Contract;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.utils.TimeUtil;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class ContractEditActivity extends BaseNetActivity {
	private TextView mJobTimeTv;
	private TextView mPayTimeTv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contract_edit);

		enableBack();
		makeTitle("発注書");

		initViews();
	}

	private void initViews() {
		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}

		String time = TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE);
		mJobTimeTv = (TextView) findViewById(R.id.tv_contract_job_time);
		mJobTimeTv.setText("" + time);
		mJobTimeTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				pickJobTime();
			}
		});

		mPayTimeTv = (TextView) findViewById(R.id.tv_contract_pay_time);
		mPayTimeTv.setText("" + time);
		mPayTimeTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				pickPayTime();
			}
		});
	}

	private void pickPayTime() {
		String selectedDate = mPayTimeTv.getText().toString();
		String[] dates = selectedDate.split("-");
		int y = Integer.valueOf(dates[0]);
		int m = Integer.valueOf(dates[1]) - 1; // should -1
		int d = Integer.valueOf(dates[2]);

		DatePickerDialog dp = new DatePickerDialog(mContext,
				new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						String date = TimeUtil.format(year, monthOfYear, dayOfMonth,
								TimeUtil.PATTERN_DATE);

						mPayTimeTv.setText("" + date);
					}
				}, y, m, d);
		dp.show();
	}

	private void pickJobTime() {
		String selectedDate = mJobTimeTv.getText().toString();
		String[] dates = selectedDate.split("-");
		int y = Integer.valueOf(dates[0]);
		int m = Integer.valueOf(dates[1]) - 1; // should -1
		int d = Integer.valueOf(dates[2]);

		DatePickerDialog dp = new DatePickerDialog(mContext,
				new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						String date = TimeUtil.format(year, monthOfYear, dayOfMonth,
								TimeUtil.PATTERN_DATE);
						mJobTimeTv.setText("" + date);
					}
				}, y, m, d);
		dp.show();
	}

	private final int[] TVIEW_IDS = { R.id.tv_contract_hint_contract_name,
			R.id.tv_contract_hint_cost_without_tax, R.id.tv_contract_hint_publisher_real_name,
			R.id.tv_contract_hint_publisher_code, R.id.tv_contract_hint_publisher_address,
			R.id.tv_contract_hint_publisher_tel, R.id.tv_contract_hint_publisher_fax,
			R.id.tv_contract_hint_receiver_real_name, R.id.tv_contract_hint_job_time,
			R.id.tv_contract_hint_pay_time, R.id.tv_contract_hint_remark };

	public void contractPreview(View v) {
		String name = getEditText(R.id.et_contract_contract_name);
		String publishTime = TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE);
		String costWithoutTax = getEditText(R.id.et_contract_cost_without_tax);
		String publisherRealName = getEditText(R.id.et_contract_publisher_real_name);
		String publisherCode = getEditText(R.id.et_contract_publisher_code);
		String publisherAddress = getEditText(R.id.et_contract_publisher_address);
		String publisherTel = getEditText(R.id.et_contract_publisher_tel);
		String publisherFax = getEditText(R.id.et_contract_publisher_fax);
		String receiverRealName = getEditText(R.id.et_contract_receiver_real_name);
		String jobTime = mJobTimeTv.getText().toString();
		String payTime = mPayTimeTv.getText().toString();
		String remark = getEditText(R.id.et_contract_remark);

		if (checkXX(TextUtils.isEmpty(publisherFax), "発注者FAX番号")) {
			return;
		}

		Contract contract = new Contract();
		contract.setName(name);
		contract.setPublishTime(publishTime);
		contract.setCostWithoutTax(costWithoutTax);
		contract.setPublisherRealName(publisherRealName);
		contract.setPublisherCode(publisherCode);
		contract.setPublisherAddress(publisherAddress);
		contract.setPublisherTel(publisherTel);
		contract.setPublisherFax(publisherFax);
		contract.setReceiverRealName(receiverRealName);
		contract.setJobTime(jobTime);
		contract.setPayTime(payTime);
		contract.setRemark(remark);

		Intent intent = new Intent();
		intent.setClass(mContext, ContractPreviewActivity.class);
		intent.putExtra("contract", contract);
		intent.putExtra("uid", getIntent().getStringExtra("uid"));
		intent.putExtra("convid", getIntent().getStringExtra("convid"));
		startActivityForResult(intent, ChatActivity.REQUEST_SEND_CONTRACT);
	}

	private String getEditText(int etId) {
		return ((EditText) findViewById(etId)).getText().toString();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			setResult(RESULT_OK, data);
			finish();
		}
	}
}
