package com.app.ug_inc.ui.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.net.NetResult;

import java.util.ArrayList;
import java.util.List;

public class SystSearchActivity extends BaseListNetActivity {

    private EditText mKeyEt;
    private ListView mListview;

    private List<String> mKeywordList;
    private ListAdapter mAdapter;

    private TextView mTimeTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syst_search);

        makeTitle("知会袋-质问哈哈");
        makeIcon(R.drawable.ic_acbar_chieb);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        mKeywordList = new ArrayList<String>();

        mKeyEt = (EditText) findViewById(R.id.et_syst_search_keyword);

        mAdapter = new ListAdapter();
        mListview = (ListView) findViewById(R.id.lv_syst_search_hot);
        mListview.setAdapter(mAdapter);

        mTimeTv = (TextView) findViewById(R.id.lv_syst_search_update_time);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < 10; i++) {
            mKeywordList.add("热门词汇" + i);
        }

        mAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mKeywordList.size();
        }

        @Override
        public String getItem(int arg0) {
            return mKeywordList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_syst, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_syst_content);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_syst_root);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            String word = getItem(arg0);
            h.content.setText("" + word);
            h.num.setText("" + (arg0 + 1));
            h.bg.setBackgroundColor(arg0 % 2 == 0 ? 0xFF393A40 : 0xFFFFFFFF);

            if (arg0 == 0) {
                h.num.setBackgroundColor(0xFFFFC740);
            } else if (arg0 == 1) {
                h.num.setBackgroundColor(0xFFC1C94A);
            } else if (arg0 == 2) {
                h.num.setBackgroundColor(0xFFC0A78C);
            } else {
                h.num.setBackgroundColor(0xFF76BE9D);
            }

            return arg1;
        }

        private class Holder {
            private TextView content;
            private TextView num;
            private View bg;
        }
    }
}
