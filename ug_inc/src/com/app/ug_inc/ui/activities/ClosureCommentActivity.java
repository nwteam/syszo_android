package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.ClosureComment;
import com.app.ug_inc.models.Company;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.ui.views.TopSearchView;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ClosureCommentActivity extends BaseListNetActivity {
	public static final int COUNT_PER_PAGE = 12;

	private Company mCompany;

	private ListAdapter mAdapter;
	private PullListview mListview;

	private ArrayList<ClosureComment> mClosureList;

	private TopSearchView mSearchView;

	private EditText mCommentEt;

	private ClosureController mClosureController;

	private ClosureComment mNewComment;

	private ImageView mEmoIv1;
	private ImageView mEmoIv2;
	private int mSelectedEmo = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_closure_comment);

		mCompany = (Company) getIntent().getExtras().getSerializable("company");
		makeTitle(mCompany.getName());
		enableBack();

		addRightAction(R.drawable.ic_btn_acbar_search, new OnClickListener() {

			@Override
			public void onClick(View v) {
				search();
			}
		});

		mSearchView = (TopSearchView) findViewById(R.id.search_view);

		mEmoIv1 = (ImageView) findViewById(R.id.iv_closure_comment_emotion1);
		mEmoIv1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mSelectedEmo == 1) {
					mEmoIv1.setBackgroundColor(0x00000000);
					mEmoIv2.setBackgroundColor(0x00000000);
					mSelectedEmo = 0;
				} else {
					mEmoIv1.setBackgroundColor(0xFFCCCCCC);
					mEmoIv2.setBackgroundColor(0x00000000);
					mSelectedEmo = 1;
				}
			}
		});
		mEmoIv2 = (ImageView) findViewById(R.id.iv_closure_comment_emotion2);
		mEmoIv2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mSelectedEmo == 2) {
					mEmoIv1.setBackgroundColor(0x00000000);
					mEmoIv2.setBackgroundColor(0x00000000);
					mSelectedEmo = 0;
				} else {
					mEmoIv1.setBackgroundColor(0x00000000);
					mEmoIv2.setBackgroundColor(0xFFCCCCCC);
					mSelectedEmo = 2;
				}
			}
		});
		mCommentEt = (EditText) findViewById(R.id.et_closure_comment_comment);

		final Company company = (Company) getIntent().getSerializableExtra("company");
		makeTitle("" + company.getName());

		mClosureList = new ArrayList<ClosureComment>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_closure_comment);
		mListview.setAdapter(mAdapter);
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//				if (mIsQuery) {
//					mClosureController.queryClosureComment(mTmpSearchWord, company.getId(),
//							mCurrPage + 1);
//				} else {
//					mClosureController.getClosureCommentList(company.getId(), mCurrPage + 1);
//				}
//				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mClosureController = new ClosureController(this);
		mClosureController.getClosureCommentList(company.getId(), ConstData.PAGE_FIRST);
		mLoadingDialog.show();

		// test();
	}

	private void search() {
		String word = mSearchView.getText().toString();
		if (!TextUtils.isEmpty(mTmpSearchWord) && mTmpSearchWord.equals(word)) {
			return;
		}

		mTmpSearchWord = word;

		mClosureList.clear();
		mAdapter.notifyDataSetChanged();

		mCurrPage = ConstData.PAGE_FIRST;
		mCurrStatus = STATUS.REFRESH;
//		mClosureController.queryClosureComment(mTmpSearchWord, mCompany.getId(),
//				ConstData.PAGE_FIRST);
		mIsQuery = true;

		mLoadingDialog.show();
	}

	public void submitReply(View v) {
		String content = mCommentEt.getText().toString();
		if (checkXX(TextUtils.isEmpty(content), "コメント内容")) {
			return;
		}

//		mClosureController.commentClosure(mCompany.getId(), content, "" + mSelectedEmo);
//		mLoadingDialog.show();

		mNewComment = new ClosureComment();
		mNewComment.setContent(content);
		mNewComment.setTime(TimeUtil.getCurrentTime(TimeUtil.PATTERN_SEC));
		mNewComment.setEmotion("" + mSelectedEmo);
		User user = new User();
		user.setId(Account.getUser().getId());
		user.setNick(Account.getUser().getNick());
		mNewComment.setUser(user);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mClosureController != null) {
			mClosureController.clear();
			mClosureController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
//		if (ifId == InterfaceIds.GET_CLOSURE_COMMENT_LIST.IF_ID) {
//			View bottom = findViewById(R.id.llayout_closure_comment_bottom);
//			if (bottom.getVisibility() != View.VISIBLE) {
//				bottom.setVisibility(View.VISIBLE);
//			}
//
//			List<ClosureComment> list = (ArrayList<ClosureComment>) obj.getResultObject();
//			if (mCurrStatus == STATUS.REFRESH) {
//				mCurrPage = ConstData.PAGE_FIRST;
//				mClosureList.clear();
//			} else if (mCurrStatus == STATUS.LOAD_MORE) {
//				judgeLoadMore(list);
//				mListview.onRefreshComplete();
//			}
//
//			mClosureList.addAll(list);
//			mAdapter.notifyDataSetChanged();
//		} else if (ifId == InterfaceIds.QUERY_CLOSURE_COMMENT.IF_ID) {
//			List<ClosureComment> list = (ArrayList<ClosureComment>) obj.getResultObject();
//			if (mCurrStatus == STATUS.REFRESH) {
//				mCurrPage = ConstData.PAGE_FIRST;
//				mClosureList.clear();
//			} else if (mCurrStatus == STATUS.LOAD_MORE) {
//				judgeLoadMore(list);
//				mListview.onRefreshComplete();
//			}
//
//			mClosureList.addAll(list);
//			mAdapter.notifyDataSetChanged();
//		} else if (ifId == InterfaceIds.COMMENT_CLOSURE.IF_ID) {
//			mCommentEt.setText("");
//
//			mEmoIv1.setBackgroundColor(0x00000000);
//			mEmoIv2.setBackgroundColor(0x00000000);
//			mSelectedEmo = 0;
//
//			mClosureList.add(mNewComment);
//			mAdapter.notifyDataSetChanged();
//		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < COUNT_PER_PAGE; i++) {
			ClosureComment closure = new ClosureComment();
			closure.setId("" + i);
			closure.setTime("2014-12-31 12:00");
			closure.setContent("赶快倒闭赶快倒闭赶快倒闭赶快倒闭赶快倒闭赶快倒闭赶快倒闭赶快倒闭");

			User user = new User();
			user.setId("" + i);
			user.setNick("用户昵称" + i);
			closure.setUser(user);

			mClosureList.add(closure);
		}

		mAdapter.notifyDataSetChanged();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mClosureList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mClosureList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_closure_comment, null);
				h.uid = (TextView) arg1.findViewById(R.id.tv_item_list_closure_comment_uid);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_closure_comment_nickname);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_closure_comment_time);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_closure_comment_content);
				h.emotion = (ImageView) arg1
						.findViewById(R.id.iv_item_list_closure_comment_emotion);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			final ClosureComment closure = mClosureList.get(arg0);

			h.uid.setText("" + closure.getUser().getId());
			h.nick.setText("" + closure.getUser().getNick());
			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					seeProfile(closure.getUser().getId());
				}
			});
			h.time.setText("" + closure.getTime());
			h.content.setText("" + closure.getContent());

			if (closure.getEmotion().equals("1")) {
				h.emotion.setVisibility(View.VISIBLE);
				h.emotion.setImageResource(R.drawable.ic_closure_emotion_1_large);
			} else if (closure.getEmotion().equals("2")) {
				h.emotion.setVisibility(View.VISIBLE);
				h.emotion.setImageResource(R.drawable.ic_closure_emotion_2_large);
			} else {
				h.emotion.setVisibility(View.GONE);
			}

			return arg1;
		}

		private class Holder {
			private ImageView emotion;
			private TextView uid;
			private TextView nick;
			private TextView time;
			private TextView content;
		}
	}
}