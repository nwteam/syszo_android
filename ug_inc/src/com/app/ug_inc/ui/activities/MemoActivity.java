package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Memo;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MemoActivity extends BaseNetActivity {
	private Memo mMemo;

	private TextView mNickTv;
	private TextView mCatTv;
	private TextView mContentTv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memo);

		mMemo = (Memo) getIntent().getSerializableExtra("memo");

		((TextView) findViewById(R.id.tv_memo_time)).setText(mMemo.getTime());

		mNickTv = (TextView) findViewById(R.id.tv_memo_nickname);
		mCatTv = (TextView) findViewById(R.id.tv_memo_cat);
		mContentTv = (TextView) findViewById(R.id.tv_memo_content);

		setDatas();
	}

	// private final int[] TVIEW_IDS = { };

	private void setDatas() {
		mNickTv.setText("" + mMemo.getTargetUser().getNick());
		mCatTv.setText("" + getCategory());
		mContentTv.setText("" + mMemo.getContent());
	}

	public void seeProfile(View v) {
		seeProfile(mMemo.getTargetUser().getId());
		// finish();
	}

	public void editMemo(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MemoEditActivity.class);
		intent.putExtra("memo", mMemo);
		// startActivityForResult(intent, 1);
		startActivity(intent);
		finish();
	}

	private String getCategory() {
		int id = 0;
		try {
			id = Integer.valueOf(mMemo.getCategory());
		} catch (NumberFormatException e) {
		}

		return MemoPickListActivity.ITEM_NAME[id];
	}

	public void closeClick(View v) {
		finish();
	}
}
