package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Company;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.Workplace;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.ui.utils.FontUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MockWorkpListActivty extends BaseNetActivity {
	private ArrayList<Workplace> mWorkpList;
	private ListAdapter mAdapter;

	private MockController mMockController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mock_workp_list);

		makeTitle("現場掲示板");
		enableBack();

		mWorkpList = new ArrayList<Workplace>();

		mAdapter = new ListAdapter();
		ListView lv = (ListView) findViewById(R.id.lv_mock_workp_list);
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Workplace wp = mWorkpList.get(position);
				Intent intent = new Intent();
				intent.setClass(mContext, MockListActivity.class);
				intent.putExtra("workp", wp);
				startActivity(intent);
			}
		});

		// test();

		Bundle datas = getIntent().getExtras();
		Workplace workp = (Workplace) datas.getSerializable("workplace");
		if (workp != null) {
			mWorkpList.add(workp);
			mAdapter.notifyDataSetChanged();
		} else {
			SimpleKV city = (SimpleKV) datas.getSerializable("city");
			Company company = (Company) datas.getSerializable("company");
			String word = datas.getString("word");

			String cityId = city.getKey();
			String companyId = company == null ? "" : company.getId();

			mMockController = new MockController(this);
			mMockController.getWorkplaceList(cityId, companyId, word);
			mLoadingDialog.show();
		}
		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}
	}

	private final int[] TVIEW_IDS = { R.id.tv_mock_worp_list_hint };

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMockController != null) {
			mMockController.clear();
			mMockController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);

		if (ifId == InterfaceIds.GET_WORKPLACE_LIST.IF_ID) {
			mWorkpList.clear();
			mWorkpList.addAll((ArrayList<Workplace>) obj.getResultObject());
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < 20; i++) {
			Workplace wp = new Workplace();
			wp.setId("" + i);
			wp.setName("现场名称现场名称" + i);

			Company company = new Company();
			company.setId("" + i);
			wp.setCompany(company);
			mWorkpList.add(wp);
		}

		mAdapter.notifyDataSetChanged();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mWorkpList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mWorkpList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_mock_workp, null);
				h.name = (TextView) arg1.findViewById(R.id.tv_item_list_workp);
				FontUtil.makeTextViewFont(h.name, FontUtil.FONT_ROUNDED_MPLUS);
				h.sum = (TextView) arg1
						.findViewById(R.id.tv_item_list_closure_list_count);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			h.name.setText("" + mWorkpList.get(arg0).getName());
			h.sum.setText("(" + mWorkpList.get(arg0).getMockSum() + ")");

			return arg1;
		}

		private class Holder {
			private TextView name;
			private TextView sum;
		}
	}
}