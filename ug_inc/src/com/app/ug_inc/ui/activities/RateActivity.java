package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;

public class RateActivity extends BaseNetActivity {
	private EditText mTitleEt;
	private EditText mContentEt;

	private RatingBar mRatingBar;

	private AccountController mAccountController;

	private String mTargetId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rate);

		makeTitle("評価");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm_send))
						.setLeftButton(getString(R.string.cancel))
						.setRightButton(getString(R.string.OK), new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								submit();
							}
						}).show();
			}
		});

		mTargetId = getIntent().getExtras().getString("uid");

		mRatingBar = (RatingBar) findViewById(R.id.rateb_rate_score);
		mTitleEt = (EditText) findViewById(R.id.et_rate_title);
		mContentEt = (EditText) findViewById(R.id.et_rate_content);

		mAccountController = new AccountController(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.USER_RATE.IF_ID) {
			toast(getString(R.string.success_rate));
			finish();
		}
	}

	private void submit() {
		String title = mTitleEt.getText().toString();
		String content = mContentEt.getText().toString();
		int score = mRatingBar.getProgress();

		if (checkXX(TextUtils.isEmpty(title), "タイトル")) {
			return;
		}
		if (checkXX(TextUtils.isEmpty(content), "評価内容")) {
			return;
		}

		if (checkXX(score == 0, "スコア")) {
			return;
		}

		mAccountController.userRate(mTargetId, "" + score, title, content);
		mLoadingDialog.show();
	}
}
