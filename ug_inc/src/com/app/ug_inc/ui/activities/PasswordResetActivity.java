package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class PasswordResetActivity extends BaseNetActivity {
	private AccountController mAccountController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_forget);
		makeTitle("パスワード再発行");
		enableBack();
		mAccountController = new AccountController(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.RESET_PASSWORD.IF_ID) {
			startActivity(new Intent(mContext, PasswordResetSuccessActivity.class));
			finish();
		}
	}

	public void submit(View v) {
		EditText et = (EditText) findViewById(R.id.et_password_forget_email);

		String email = et.getText().toString();
		if (checkXX(TextUtils.isEmpty(email), "メールアドレス")) {
			return;
		}

		mAccountController.resetPassword(email);
		mLoadingDialog.show();

		// startActivity(new Intent(mContext,
		// PasswordResetSuccessActivity.class));
		// finish();
	}
}
