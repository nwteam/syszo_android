package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class WorkPPickActivity extends BaseActivity {

	public static final String ACTION_WORKER = "action.WORKER";
	public static final String ACTION_WORKPLACE = "action.WORKPLACE";

	private String action = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workp_pick);

		enableBack();

		action = getIntent().getAction();

		// these pics may confused, dont metion
		if (action.equals(ACTION_WORKER)) {
			findViewById(R.id.llayout_workp_pick_workplace).setVisibility(
					View.VISIBLE);
			findViewById(R.id.llayout_workp_pick_worker).setVisibility(
					View.GONE);
			makeTitle("手空いてる？");
		} else if (action.equals(ACTION_WORKPLACE)) {
			makeTitle("現場ある？");
			findViewById(R.id.llayout_workp_pick_workplace).setVisibility(
					View.GONE);
			findViewById(R.id.llayout_workp_pick_worker).setVisibility(
					View.VISIBLE);
		}
	}

	public void seeWorkers(View v) {
		Intent intent = new Intent();
		intent.setAction(action);
		intent.setClass(mContext, WorkPListActivity.class);
		startActivity(intent);
	}

	public void submitWorker(View v) {
		Intent intent = new Intent();
		intent.setAction(action);
		intent.setClass(mContext, WorkPPublishActivity.class);
		startActivity(intent);
	}

	public void seeWorkplaces(View v) {
		Intent intent = new Intent();
		intent.setAction(action);
		intent.setClass(mContext, WorkPListActivity.class);
		startActivity(intent);
	}

	public void submitWorkplace(View v) {
		Intent intent = new Intent();
		intent.setAction(action);
		intent.setClass(mContext, WorkPPublishActivity.class);
		startActivity(intent);
	}
}