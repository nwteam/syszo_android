package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;

public class EntranceActivity extends BaseActivity {

    public static final String ACTION_NEED_FINISH = EntranceActivity.class
            .getSimpleName() + ".action.ACTION_NEED_FINISH";

    private FinishReceiver mFinishReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        // loadDatas();
        boolean autoLogin = Account.isAutoLogin();
        String uid = Account.getUserId();
        if (autoLogin && !TextUtils.isEmpty(uid)) {// id not null
            Intent intent = new Intent();
            intent.setClass(mContext, MenuActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_entrance);

        mFinishReceiver = new FinishReceiver();
        mFinishReceiver.register();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mFinishReceiver != null) {
            mFinishReceiver.unregister();
            mFinishReceiver = null;
        }
    }

    // private void loadDatas() {
    // ConstData.init(mContext);
    // }

    public void register(View v) {
        startActivity(new Intent(mContext, RegisiterActivity.class));
    }

    public void login(View v) {
        startActivity(new Intent(mContext, LoginActivity.class));
    }

    private class FinishReceiver extends BroadcastReceiver {

        private void unregister() {
            unregisterReceiver(this);
        }

        private void register() {
            registerReceiver(this, new IntentFilter(ACTION_NEED_FINISH));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }
}