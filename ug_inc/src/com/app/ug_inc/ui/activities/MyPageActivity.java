package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MyPageActivity extends BaseNetActivity {

	private AccountController mAccontController;

	private TextView mBalanceTv;
	private TextView mNickTv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mypage);

		makeTitle("マイページ");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_setting, new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(mContext, SettingActivity.class));
			}
		});

		mBalanceTv = (TextView) findViewById(R.id.tv_mypage_balance);

		mNickTv = (TextView) findViewById(R.id.tv_mypage_nickname);

		mAccontController = new AccountController(this);
		mAccontController.getBalance();
	}

	@Override
	protected void onResume() {
		super.onResume();

		mBalanceTv.setText("" + Account.getUser().getBalance());
		mNickTv.setText("" + Account.getUser().getNick());
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_USER_BALANCE.IF_ID) {
			String balance = (String) obj.getResultObject();
			if (!TextUtils.isEmpty(balance)) {
				Account.getUser().setBalance(balance);

				mBalanceTv.setText(balance);
			}
		}
	}

	public void toProfileSetting(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, ProfileSettingActivity.class);
		startActivity(intent);
	}

	public void toMemo(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MemoListActivity.class);
		startActivity(intent);
	}

	public void toMessage(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MessageBoxActivity.class);
		startActivity(intent);
	}

	public void toContribute(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, ArticleHistoryListActivity.class);
		startActivity(intent);
	}

	public void buyCash(View v) {
		if (ConstData.sIsAppFree) {
			new ZDialog(mContext).setContent("購入機能は現在対応していません。今後、機能として追加しますのでお楽しみに！")
					.setLeftButton(getString(R.string.OK)).show();
		} else {
//			startActivity(new Intent(mContext, InnerShopActivity.class));
		}
	}
}