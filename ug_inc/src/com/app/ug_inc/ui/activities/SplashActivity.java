package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import com.igaworks.IgawCommon;

public class SplashActivity extends BaseActivity{
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Handler x = new Handler();
        x.postDelayed(new splashhandler(), 2000);
        IgawCommon.startApplication(SplashActivity.this);
    }
    class splashhandler implements Runnable{

        public void run() {
            startActivity(new Intent(getApplication(),EntranceActivity.class));
            SplashActivity.this.finish();
        }
        
    }
    
    
    
}
