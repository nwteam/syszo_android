package com.app.ug_inc.ui.activities;

import android.view.*;
import cn.jpush.android.api.JPushInterface;

import com.app.ug_inc.R;
import com.app.ug_inc.ZApplication;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.ui.utils.DisplayUtil;
import com.app.ug_inc.ui.views.ZActionbar;
import com.app.ug_inc.utils.Utils;
import com.app.ug_inc.utils.ZLog;
import com.google.android.gms.analytics.GoogleAnalytics;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.GestureDetector.OnGestureListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.igaworks.IgawCommon;

public abstract class BaseActivity extends Activity {
    public static final String ACTION_LOGOUT = "action.ACTION_LOGOUT";

    protected ActionBar mActionBar;
    protected ZActionbar mAcBarView;

    protected Context mContext;
    
    private boolean isBack = false;

    int width = 300;

    private class LogoutReceiver extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            IntentFilter filter = new IntentFilter(ACTION_LOGOUT);
            registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    private LogoutReceiver mLogoutReceiver;
    
    GestureDetector mGestureDetector;

    

   //实现处理事件

   OnGestureListener onGestureListener = new OnGestureListener() {

	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		if (arg0.getX() - arg1.getX() < -width*1.0f/3 &&  Math.abs(arg0.getY() - arg1.getY())< width*1.0f/12) {
			finish();
            return true;
        }
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	   
   };
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = BaseActivity.this;

        WindowManager wm = this.getWindowManager();

        width = wm.getDefaultDisplay().getWidth();

        mActionBar = getActionBar();
        if (mActionBar != null) {
            initActionbar();
        }
        // 获取Tracker 参数为 MyApplication.TrackerName.APP_TRACKER 追踪本程序 追踪页面
        ((ZApplication) getApplication())
                .getTracker(ZApplication.TrackerName.APP_TRACKER);

        mLogoutReceiver = new LogoutReceiver();
        mLogoutReceiver.register();
        
      //监听手势事件

        mGestureDetector = new GestureDetector(this, onGestureListener);
    }

    @Override
    protected void onStart() {
        // 开启追踪
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        // 停止追踪
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLogoutReceiver.unregister();
    }

    @Override
    protected void onResume() {
        super.onResume();
        JPushInterface.onResume(mContext);
        IgawCommon.startSession(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(mContext);
        IgawCommon.endSession();
    }

    private void initActionbar() {
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);

        mAcBarView = new ZActionbar(mContext);
        mAcBarView.setBackgroundColor(0xFF3B3A39);
        // w = MATCH_PARENT, h = WRAP_CONTENT
        mActionBar.setCustomView(mAcBarView, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
    }

    public void makeTitleBg(int color) {
//        mAcBarView.setBackgroundColor(color);
    }

    public void makeTitle(int resId) {
        mAcBarView.setTitle(getString(resId));
    }

    public void makeTitle(CharSequence title) {
        mAcBarView.setTitle(title);
    }

    public void makeIcon(Drawable d) {
        mAcBarView.setIcon(d);
    }

    public void makeIcon(int res) {
        mAcBarView.setIcon(res);
    }

    public ImageView enableBack() {
    	isBack = true;
        ImageView back = new ImageView(mContext);
        back.setImageResource(R.drawable.ic_btn_acbar_back);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAcBarView.addLeftActionItem(back);

        return back;
    }

    public void disableBack() {
//		mAcBarView.setLeftBtnEnabled(true);
    }

    public boolean checkXX(boolean condition, String hint) {
        if (condition) {
            toast(hint + getString(R.string.hint_xx_not_null));
        }

        return condition;
    }

    public void addRightAction(View child) {
        mAcBarView.addLeftActionItem(child);
    }

    public View addRightAction(int resId, View.OnClickListener l) {
        ImageView btn = new ImageView(mContext);
        btn.setImageResource(resId);
        btn.setOnClickListener(l);

        mAcBarView.addRightActionItem(btn);

        return btn;
    }

    public View addMoreRightAction(int resId, int index, View.OnClickListener l) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, DisplayUtil.dp2px(mContext, 30));
        ImageView btn = new ImageView(mContext);
        btn.setImageResource(resId);
        btn.setOnClickListener(l);

        mAcBarView.addRightActionItem(btn, index, params);

        return btn;
    }

    public void toast(String text) {
        Utils.toast(mContext, text);
    }

    protected void log(String content) {
        ZLog.d(content);
    }

    protected void err(String content) {
        System.err.println(content);
    }

    protected void hideKeyboard() {
        View view = getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected boolean isKeyboradShowing() {
        InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputmanger.isActive();
    }

    protected void toProfile(String uid) {
        if (Account.getUserId().equals(uid)) {
            return;
        }

        Intent intent = new Intent();
        intent.setClass(mContext, ProfileOthersActivity.class);
        intent.putExtra("user_id", uid);
        startActivity(intent);
    }

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		if(isBack){
			if(mGestureDetector.onTouchEvent(ev))
			{
				return false;
			}
		}
		return super.dispatchTouchEvent(ev);
	}
    
}