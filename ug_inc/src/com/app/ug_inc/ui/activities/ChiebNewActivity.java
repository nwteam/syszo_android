package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.views.CountNumTextview;
import com.app.ug_inc.utils.FileUtils;
import com.app.ug_inc.utils.Utils;

public class ChiebNewActivity extends BaseListNetActivity {
    public static final String ACTION_CHIEB_NEED_REFRESH = "action.ACTION_CHIEB_NEED_REFRESH";

    private EditText mTitleEt;
    private EditText mContentEt;
    private CheckBox mErrorCb;

    private ClosureController mClosureControler;

    private String mUploadedPicUrl = "";

    private Chieb mChieb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_new);

        makeTitle("知恵袋");
        makeTitleBg(0xFF76A4CE);
        makeIcon(R.drawable.ic_acbar_chieb);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

            @Override
            public void onClick(View v) {
                submit();
            }
        });

        mChieb = (Chieb) getIntent().getSerializableExtra("chieb");

        mTitleEt = (EditText) findViewById(R.id.et_chieb_new_title);
        CountNumTextview titleCNTV = (CountNumTextview) findViewById(R.id.cntv_hint_chieb_title_char_last);
        titleCNTV.linkTextView(mTitleEt, 25);

        mContentEt = (EditText) findViewById(R.id.et_chieb_new_content);
        CountNumTextview contentCNTV = (CountNumTextview) findViewById(R.id.cntv_hint_chieb_content_char_last);
        contentCNTV.linkTextView(mContentEt, 1000);

        mErrorCb = (CheckBox) findViewById(R.id.cb_chieb_new_error);

        if (mChieb != null) {
            makeModify();
        }

        TextView errorHintTv = (TextView) findViewById(R.id.tv_chieb_new_hint_error);
        
        String s1 = "緊急フラグにチェックを入れると\n";
        String s2 = "「   緊急投稿あり」";
        String s3 = "に投稿され\n早期解決の可能性が高くなります。";
        SpannableString ss = new SpannableString(s1 + s2 + s3);
        ss.setSpan(new ForegroundColorSpan(0xFFFF0000),
                0, s1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(0xFFFF0000),
                s1.length() + s2.length(), s1.length() + s2.length() + s3.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        DynamicDrawableSpan drawableSpan = new DynamicDrawableSpan(
                DynamicDrawableSpan.ALIGN_BOTTOM) {
            @Override
            public Drawable getDrawable() {
                Drawable d = getResources().getDrawable(R.drawable.icon_trouble);
                d.setBounds(0, 0, 50, 50);
                return d;
            }
        };
        ss.setSpan(drawableSpan, s1.length() + 1, s1.length() + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        errorHintTv.setText(ss);

        mMySeriesController = new MySeriesController(this);
        mClosureControler = new ClosureController(this);
    }

    private void makeModify() {
        mTitleEt.setText("" + mChieb.getTitle());
        mContentEt.setText("" + mChieb.getContent());

        mErrorCb.setChecked(mChieb.isUrgent());
        mUploadedPicUrl = mChieb.getPicUrl();
    }

    private void submit() {
        final String title = mTitleEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(title), "タイトル")) {
            return;
        }

        final String content = mContentEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(content), "内容")) {
            return;
        }

//        new AlertDialog.Builder(mContext).setMessage(getString(R.string.hint_edit_confirm_content_ok))
//                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
                        boolean urgent = mErrorCb.isChecked();
                        if (mChieb != null) {
                            mClosureControler.editClosure(mChieb.getId(), title, content, mUploadedPicUrl, urgent);
                        } else {
                            mClosureControler.newClosure(title, content, mUploadedPicUrl, urgent);
                        }


                        mLoadingDialog.show();
//                    }
//                }).setNegativeButton(getString(R.string.cancel), null).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mMySeriesController);
        clearController(mClosureControler);
    }

    @Override
    protected void test() {
        super.test();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.UPLOAD_PIC.IF_ID) {
            toast("アップロードしました");
            mUploadedPicUrl = (String) obj.getResultObject();
        } else if (ifId == InterfaceIds.NEW_CLOSURE.IF_ID || ifId == InterfaceIds.EDIT_COLSURE.IF_ID) {
            toast("送信しました。");
            finish();

            sendBroadcast(new Intent(ACTION_CHIEB_NEED_REFRESH));
        }
    }

    private MySeriesController mMySeriesController;
    private String mTmpPhotoPath;

    public void selectPicture(View v) {
        String[] hints = new String[]{getString(R.string.hint_upload_photo_take),
                getString(R.string.hint_upload_photo_pick)};
        new AlertDialog.Builder(mContext).setItems(hints, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // take picture
                        try {
                            mTmpPhotoPath = Utils.takePhoto(ChiebNewActivity.this);
                        } catch (Exception e) {
                            toast(getString(R.string.hint_sdcard_not_mounted));
                        }
                        break;
                    case 1: // pick picture
                        Utils.pickPhoto(ChiebNewActivity.this);
                        break;

                    default:
                        break;
                }

                dialog.dismiss();
            }
        }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            mTmpPhotoPath = null;

            return;
        }

        switch (requestCode) {
            case Utils.REQUEST_PICK_PHOTO:
                Uri photoUri = data.getData();
                mTmpPhotoPath = FileUtils.getPath(mContext, photoUri);

                break;
            case Utils.REQUEST_TAKE_PHOTO:

                break;

            default:
                break;
        }

        if (!TextUtils.isEmpty(mTmpPhotoPath)) {
            mMySeriesController.uploadPic(mTmpPhotoPath);
            mLoadingDialog.show();
            mLoadingDialog.setCancelable(false);
        }
    }
}
