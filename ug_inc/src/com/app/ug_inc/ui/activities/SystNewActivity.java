package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Mock;
import com.app.ug_inc.models.Syst;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.views.CountNumTextview;
import com.app.ug_inc.utils.FileUtils;
import com.app.ug_inc.utils.Utils;

public class SystNewActivity extends BaseListNetActivity {
    public static final String ACTION_SYST_NEED_REFRESH = "action.ACTION_CHIEB_NEED_REFRESH";

    private MockController mMockController;

    private EditText mContentEt;

    private Syst mSyst;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syst_new);

        makeTitle("シスッター");
        makeIcon(R.drawable.micon_hitorigoto);
        makeTitleBg(0xFFD66968);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

            @Override
            public void onClick(View v) {
                final String content = mContentEt.getText().toString();
                if (checkXX(TextUtils.isEmpty(content), "内容")) {
                    return;
                }

                new AlertDialog.Builder(mContext).setMessage(getString(R.string.hint_edit_confirm_content_ok))
                        .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mSyst != null) {
                                    mMockController.editMock(mSyst.getId(), content, mUploadedPicUrl);
                                    mLoadingDialog.show();
                                } else {
                                    mMockController.commentMock(content, mUploadedPicUrl);
                                    mLoadingDialog.show();
                                }
                            }
                        }).setNegativeButton(getString(R.string.cancel), null).show();

            }
        });

        mSyst = (Syst) getIntent().getSerializableExtra("syst");
        mContentEt = (EditText) findViewById(R.id.et_syst_new_content);
        if (mSyst != null) {
            makeSyst();
        }

        TextView userTv = (TextView) findViewById(R.id.tv_syst_user_name);
        userTv.setText("" + Account.getUser().getNick());
        CountNumTextview contentCNTV = (CountNumTextview) findViewById(R.id.cntv_hint_syst_content_char_last);
        contentCNTV.linkTextView(mContentEt, 140);

        mMySeriesController = new MySeriesController(this);
        mMockController = new MockController(this);
    }

    private void makeSyst() {
        mContentEt.setText("" + mSyst.getContent());

        mUploadedPicUrl = mSyst.getPicUrl();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mMockController);
        clearController(mMySeriesController);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

        if (ifId == InterfaceIds.UPLOAD_PIC.IF_ID) {
            toast("アップロードしました");
            mUploadedPicUrl = (String) obj.getResultObject();
        } else if (ifId == InterfaceIds.COMMENT_MOCK.IF_ID || ifId == InterfaceIds.EDIT_MOCK.IF_ID) {
            sendBroadcast(new Intent(ACTION_SYST_NEED_REFRESH));

            toast("送信しました。");
            finish();
        }
    }


    private String mUploadedPicUrl = "";
    private MySeriesController mMySeriesController;
    private String mTmpPhotoPath;

    public void selectPicture(View v) {
        String[] hints = new String[]{getString(R.string.hint_upload_photo_take),
                getString(R.string.hint_upload_photo_pick)};
        new AlertDialog.Builder(mContext).setItems(hints, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // take picture
                        try {
                            mTmpPhotoPath = Utils.takePhoto(SystNewActivity.this);
                        } catch (Exception e) {
                            toast(getString(R.string.hint_sdcard_not_mounted));
                        }
                        break;
                    case 1: // pick picture
                        Utils.pickPhoto(SystNewActivity.this);
                        break;

                    default:
                        break;
                }

                dialog.dismiss();
            }
        }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            mTmpPhotoPath = null;

            return;
        }

        switch (requestCode) {
            case Utils.REQUEST_PICK_PHOTO:
                Uri photoUri = data.getData();
                mTmpPhotoPath = FileUtils.getPath(mContext, photoUri);

                break;
            case Utils.REQUEST_TAKE_PHOTO:

                break;

            default:
                break;
        }

        if (!TextUtils.isEmpty(mTmpPhotoPath)) {
            mMySeriesController.uploadPic(mTmpPhotoPath);
            mLoadingDialog.show();
            mLoadingDialog.setCancelable(false);
        }
    }
}
