package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * bottom other edittext
 */
public class SettingPickListActivity2 extends BaseActivity {
	private ArrayList<SimpleKV> mList;
	private ListAdapter mAdapter;

	private int mIndex = -2; // default 0

	private ImageView mFooterIv;
	private EditText mFooterEt;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_pick_list);

		enableBack();

		Intent intent = getIntent();

		makeTitle("" + intent.getStringExtra("title"));

		mList = (ArrayList<SimpleKV>) intent.getSerializableExtra("array");
		mList.remove(mList.size() - 1); // other is in footer

		mIndex = intent.getIntExtra("check", -2);

		ListView lv = (ListView) findViewById(R.id.lv_setting_pick_list);
        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultOK();
			}
		});

		View v = LayoutInflater.from(mContext).inflate(
				R.layout.item_list_setting_pick2, null);
		TextView tv = (TextView) v
				.findViewById(R.id.tv_item_list_settting_pick2_name);
		tv.setText("その他");

		mFooterIv = (ImageView) v.findViewById(R.id.iv_item_list_setting_pick2);
		mFooterEt = (EditText) v
				.findViewById(R.id.et_item_list_settting_pick2_name);
		mFooterEt.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mIndex = -1;
				}
				mFooterIv
						.setImageResource(hasFocus ? R.drawable.ic_btn_list_checked
								: R.drawable.ic_btn_list_normal);
				mAdapter.notifyDataSetChanged();
			}
		});
		v.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mIndex == -1) {
					return;
				}

				mFooterIv.setImageResource(R.drawable.ic_btn_list_checked);
				mFooterEt.requestFocus();
				openMME();

				mIndex = -1;
				mAdapter.notifyDataSetChanged();
			}
		});

		lv.addFooterView(v);
		mAdapter = new ListAdapter();
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mFooterIv.setImageResource(R.drawable.ic_btn_list_normal);

				// mRoot.requestFocus();
				closeMME();
				mFooterEt.clearFocus();
				// view.requestFocus();
				mIndex = position;
				mAdapter.notifyDataSetChanged();
			}
		});
	}

	public void closeMME() {
		InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(mFooterEt.getWindowToken(), 0);
	}

	public void openMME() {
		InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		im.showSoftInput(mFooterEt, 0);
	}

	private void resultOK() {
		if (mIndex == -1) {

			String other = mFooterEt.getText().toString();
			if (checkXX(TextUtils.isEmpty(other), "職種")) {
				return;
			}

			SimpleKV kv = new SimpleKV();
			kv.setKey("0");
			kv.setValue(other);

			Intent intent = new Intent();
			intent.putExtra("result", kv);
			setResult(RESULT_OK, intent);
			finish();
		} else if (mIndex >= 0) {
			Intent intent = new Intent();
			intent.putExtra("result", mList.get(mIndex));
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_setting_pick, null);

				h.check = (ImageView) arg1
						.findViewById(R.id.iv_item_list_setting_pick);
				h.name = (TextView) arg1
						.findViewById(R.id.tv_item_list_settting_pick_name);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			if (arg0 == mIndex) {
				h.check.setImageResource(R.drawable.ic_btn_list_checked);
			} else {
				h.check.setImageResource(R.drawable.ic_btn_list_normal);
			}
			h.name.setText("" + mList.get(arg0).getValue());

			return arg1;
		}

		private class Holder {
			public TextView name;
			public ImageView check;
		}
	}
}
