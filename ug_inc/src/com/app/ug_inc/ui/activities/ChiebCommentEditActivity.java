package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.ChiebComment;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChiebCommentEditActivity extends BaseNetActivity {

    private EditText mContentEt;

    private ClosureController mClosureController;

    private String mCommentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_comment_edit);

        makeTitle("編集");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

            @Override
            public void onClick(View v) {
                submit();
            }
        });

        mCommentId = getIntent().getStringExtra("id");

        mContentEt = (EditText) findViewById(R.id.et_chieb_new_title);

        mClosureController = new ClosureController(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mClosureController);
    }

    private String mTmpContent;

    private void submit() {
        mTmpContent = mContentEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(mTmpContent), "内容")) {
            return;
        }

        new AlertDialog.Builder(mContext).setMessage(getString(R.string.hint_edit_confirm_content_ok))
                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mClosureController.editClosureComment(mCommentId, mTmpContent);
                        mLoadingDialog.show();
                    }
                }).setNegativeButton(getString(R.string.cancel), null).show();
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        Intent intent = new Intent();
        intent.putExtra("id", mCommentId);
        intent.putExtra("content", mTmpContent);

        setResult(RESULT_OK, intent);
        finish();
    }
}
