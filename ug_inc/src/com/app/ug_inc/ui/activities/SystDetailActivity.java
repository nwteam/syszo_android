package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.ChiebComment;
import com.app.ug_inc.models.Syst;
import com.app.ug_inc.models.SystComment;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import com.app.ug_inc.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;

public class SystDetailActivity extends BaseListNetActivity {

    private PullListview mListview;
    private ListAdapter mAdapter;

    private List<ChiebComment> mCommentList;

    private MockController mMockController;
    private Button mLikeBtn;
    private MockController mRefreshController;
    private MockController mLoadmoreController;

    private Syst mSyst;

    private ChiebComment mRequestComment;

    private View mHeaderRoot;
    private TextView mUnameTv;
    private TextView mContentTv;
    private TextView mTitleTv;
    private TextView mCountTv;
    private TextView mTimeTv;
    private ImageView mPicIv;
    private ImageView mDelIv,mVipIV;

    private EditText mSendEt;

    private View mEditBtn;

    private RefreshReceiver mRefreshReceiver;

    private class RefreshReceiver extends BroadcastReceiver {

        public void register() {
            IntentFilter filter = new IntentFilter(SystNewActivity.ACTION_SYST_NEED_REFRESH);
            registerReceiver(this, filter);
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mMockController.getSystDetail(mSyst.getId());
            mRefreshController.getMockCommentList(mSyst.getId(), ConstData.PAGE_FIRST);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syst_detail);

        makeTitle("シスッター");
        makeIcon(R.drawable.micon_hitorigoto);
        makeTitleBg(0xFFD66968);
        enableBack();

        mEditBtn = addRightAction(R.drawable.ic_btn_acbar_contribute, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, SystNewActivity.class);
                intent.putExtra("syst", mSyst);
                startActivity(intent);
            }
        });
        mEditBtn.setVisibility(View.INVISIBLE);

        mRefreshReceiver = new RefreshReceiver();
        mRefreshReceiver.register();

        mSyst = (Syst) getIntent().getSerializableExtra("syst");
        View header = makeHeader();

        mCommentList = new ArrayList<ChiebComment>();
        mAdapter = new ListAdapter();
        mListview = (PullListview) findViewById(R.id.plv_chieb_detail);
        mListview.getRefreshableView().addHeaderView(header, null, false);
        mListview.setAdapter(mAdapter);
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mMockController.getSystDetail(mSyst.getId());
                mRefreshController.getMockCommentList(mSyst.getId(), ConstData.PAGE_FIRST);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mLoadmoreController.getMockCommentList(mSyst.getId(), mCurrPage + 1);
            }
        });
        mMockController = new MockController(this);
        mMockController.getSystDetail(mSyst.getId());

        mRefreshController = new MockController(mRefreshDelegate);
        mRefreshController.getMockCommentList(mSyst.getId(), ConstData.PAGE_FIRST);
        mLoadmoreController = new MockController(mLoadmoreDelegate);

        mSendEt = (EditText) findViewById(R.id.et_chieb_detail_chat);

        findViewById(R.id.btn_chieb_detail_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = mSendEt.getText().toString();
                if (checkXX(TextUtils.isEmpty(content), "送信内容")) {
                    return;
                }

                mMockController.commentMock2(mSyst.getId(), content);
                mLoadingDialog.show();
            }
        });
    }

    private View makeHeader() {
        View header = getLayoutInflater().inflate(R.layout.list_header_syst_detail, null);
        mHeaderRoot = header.findViewById(R.id.llayout_syst_detail_root);
        mUnameTv = (TextView) header.findViewById(R.id.tv_syst_detail_uname);
        mPicIv = (ImageView) header.findViewById(R.id.iv_syst_detail_pic);
        mContentTv = (TextView) header.findViewById(R.id.tv_syst_detail_content);
        mTimeTv = (TextView) header.findViewById(R.id.tv_syst_detail_time);
        mVipIV = (ImageView) header.findViewById(R.id.tv_item_list_chieb_comment_vip);

        try {
            if (Account.getUserId().equals(mSyst.getUser().getId())) {
                mUnameTv.setTextColor(0xFFF1A90F);
                ((ImageView) header.findViewById(R.id.iv_syst_detail_avatar))
                        .setImageResource(R.drawable.icon_user_on);
            } else {
                mUnameTv.setTextColor(0xFF393A40);
                ((ImageView) header.findViewById(R.id.iv_syst_detail_avatar))
                        .setImageResource(R.drawable.icon_user_off);
            }

            mUnameTv.setText(mSyst.getUser().getNick());
            header.findViewById(R.id.llayout_item_list_syst_detail_header_uname)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toProfile(mSyst.getUser().getId());
                        }
                    });
        } catch (Exception e) {
        }
        mContentTv.setText("" + mSyst.getContent());
        mTimeTv.setText("" + mSyst.getTime());

        if(mSyst.getUser().getVip_flg() == 1){
            mVipIV.setVisibility(View.VISIBLE);
        }else{
            mVipIV.setVisibility(View.GONE);
        }

        mDelIv = (ImageView) header.findViewById(R.id.iv_item_list_chieb_comment_del);
        mDelIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(mContext).setMessage("投稿を削除しますか？").setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mMockController.deleteMock(mSyst.getId());
                        mLoadingDialog.show();
                    }
                }).setNegativeButton(getString(R.string.cancel), null).show();
            }
        });

        mLikeBtn = (Button) header.findViewById(R.id.btn_syst_detail_iine);
        mLikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoadingDialog.show();
                mMockController.likeMock(mSyst.getId());
            }
        });

        try {
            if (Account.getUser().getId().equals(mSyst.getUser().getId())) {
                mLikeBtn.setVisibility(View.GONE);
                mDelIv.setVisibility(View.VISIBLE);
            } else {
                mLikeBtn.setVisibility(View.VISIBLE);
                mDelIv.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }

        return header;
    }

    @Override
    protected void test() {
        super.test();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        clearController(mMockController);
        clearController(mRefreshController);
        clearController(mLoadmoreController);

        mRefreshReceiver.unregister();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_SYST_DETAIL.IF_ID) {
            final Syst syst = (Syst) obj.getResultObject();
            mHeaderRoot.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    Utils.copyToClipBoard(mContext, syst.getContent());

                    return true;
                }
            });

            mSyst.setUser(syst.getUser());
            mSyst.setContent(syst.getContent());
            mSyst.setTime(syst.getTime());
            mSyst.setLiked(syst.isLiked());
            mSyst.setPicUrl(syst.getPicUrl());

            mLikeBtn.setBackgroundResource(syst.isLiked() ?
                    R.drawable.btn_iine_pressed : R.drawable.btn_iine_normal);

            mContentTv.setText(syst.getContent());
            mTimeTv.setText(syst.getTime());

            final String picUrl = syst.getPicUrl();
            if (!TextUtils.isEmpty(picUrl)) {
                mPicIv.setVisibility(View.VISIBLE);
                ZImageLoader.asyncLoadImage(picUrl, mPicIv);
                mPicIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, PicBrowseActivity.class);
                        intent.putExtra("url", picUrl);
                        startActivity(intent);
                    }
                });
            } else {
                mPicIv.setVisibility(View.GONE);
            }

            if (Account.getUser().getId().equals(syst.getUser().getId())) {
                mEditBtn.setVisibility(View.VISIBLE);
            }
        } else if (ifId == InterfaceIds.LIKE_MOCK.IF_ID) {
            mLikeBtn.setBackgroundResource(R.drawable.btn_iine_pressed);
        } else if (ifId == InterfaceIds.BIRD_COMMENT.IF_ID) {
            toast("送信しました。");
            mSendEt.setText("");
            mRefreshController.getMockCommentList(mSyst.getId(), ConstData.PAGE_FIRST);
        } else if (ifId == InterfaceIds.LIKE_MOCK.IF_ID) {
            mLikeBtn.setBackgroundResource(R.drawable.btn_iine_pressed);
            mLikeBtn.setEnabled(false);
        } else if (ifId == InterfaceIds.LIKE_MOCK_COMMENT.IF_ID) {
            mRequestComment.setLiked(true);
            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.DELETE_MOCK_COMMENT.IF_ID) {
            mCommentList.remove(mRequestComment);
            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.DELETE_MOCK.IF_ID) {
            sendBroadcast(new Intent(SystNewActivity.ACTION_SYST_NEED_REFRESH));

            finish();
        }
    }

    private UIDelegate mRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();

            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            mCommentList.clear();
            mCurrPage = ConstData.PAGE_FIRST;
            mCommentList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    private UIDelegate mLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();
            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            judgeLoadMore(list);

            mCommentList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
            String id = data.getStringExtra("id");
            String newContent = data.getStringExtra("content");

            for (ChiebComment comment : mCommentList) {
                if (comment.getId().equals(id)) {
                    comment.setContent(newContent);
                    break;
                }
            }

            mAdapter.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mCommentList.size();
        }

        @Override
        public ChiebComment getItem(int arg0) {
            return mCommentList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chieb_comment, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_content);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_time);
                h.like = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_like);
                h.del = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_del);
                h.edit = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_edit);
                h.avatar = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_avatar);
                h.selfLayout = arg1.findViewById(R.id.llayout_item_list_chieb_comment_del_edit);
                h.uname = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_uname);
                h.answer = arg1.findViewById(R.id.iv_item_list_chieb_comment_answer);
                h.answer.setVisibility(View.INVISIBLE);
                h.profile = arg1.findViewById(R.id.llayout_list_profile);
                h.vipIv = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_vip);
                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            final ChiebComment item = getItem(arg0);

            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getContent());

            final User user = item.getUser();
            h.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProfile(user.getId());
                }
            });

            if(user.getVip_flg() == 1){
                h.vipIv.setVisibility(View.VISIBLE);
            }else{
                h.vipIv.setVisibility(View.GONE);
            }

            if (Account.getUserId().equals(user.getId())) {
                h.avatar.setImageResource(R.drawable.icon_user_on);
                h.uname.setTextColor(0xFFF1A90F);
                h.selfLayout.setVisibility(View.VISIBLE);
                h.like.setVisibility(View.INVISIBLE);
                h.del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        delete(item);
                    }
                });

                h.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        edit(item);
                    }
                });
            } else {
                h.avatar.setImageResource(R.drawable.icon_user_off);
                h.uname.setTextColor(0xFF393A40);
                h.selfLayout.setVisibility(View.INVISIBLE);
                h.like.setVisibility(View.VISIBLE);
                if (item.isLiked()) {
                    h.like.setEnabled(false);
                    h.like.setImageResource(R.drawable.btn_iine_pressed);
                } else {
                    h.like.setEnabled(true);
                    h.like.setImageResource(R.drawable.btn_iine_normal);
                    h.like.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            like(item);
                        }
                    });
                }
            }
            h.uname.setText(user.getNick());

            h.content.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    copy(item.getContent());
                    return true;
                }
            });

            arg1.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    copy(item.getContent());
                    return true;
                }
            });

            return arg1;
        }

        private void copy(CharSequence s) {
            Utils.copyToClipBoard(mContext, s);
        }

        private void like(ChiebComment comment) {
            mRequestComment = comment;
            mMockController.likeMockComment(mSyst.getId(), comment.getId());
            mLoadingDialog.show();
        }

        private void edit(ChiebComment comment) {
            Intent intent = new Intent();
            intent.setClass(mContext, SystCommentEditActivity.class);
            intent.putExtra("id", comment.getId());
            intent.putExtra("content", comment.getContent());
            startActivityForResult(intent, 5);
        }

        private void delete(final ChiebComment comment) {
            new AlertDialog.Builder(mContext).setMessage("投稿を削除しますか？").setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mRequestComment = comment;
                    mMockController.deleteMockComment(mRequestComment.getId());
                    mLoadingDialog.show();
                }
            }).setNegativeButton(getString(R.string.cancel), null).show();
        }

        private class Holder {
            private TextView content;
            private TextView time;
            private TextView uname;
            private ImageView avatar;
            private View selfLayout;
            private ImageView like;
            private ImageView del;
            private ImageView edit,vipIv;
            private View bg;
            private View answer;
            private View profile;
        }
    }
}
