package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.app.ug_inc.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoSearchActivity extends BaseNetActivity {

    private static final String[] CATEGORIES = {
            "会社規模", "役職・役割"/*, "ハードウェア", "ソフトウェア", "保有資格"*/, "情シス歴",
    };

    private class FinishReceiver extends BroadcastReceiver {

        public void register() {
            registerReceiver(this, new IntentFilter(SystomoCreateGroupActivity.ACTION_GROUP_CREATE_NEED_FINISH));
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    private FinishReceiver mFinishReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo_search);

        makeTitle("シストモ検索");
        makeIcon(R.drawable.micon_systomo);
        enableBack();

        initTopSearch();

        List<Map<String, String>> datas = new ArrayList<Map<String, String>>();
        for (String cat : CATEGORIES) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("cat", cat);
            datas.add(map);
        }

        SimpleAdapter adapter = new SimpleAdapter(mContext, datas, R.layout.item_list_systomo_search,
                new String[]{"cat"}, new int[]{R.id.tv_list_systomo_search});

        GridView gv = (GridView) findViewById(R.id.gridv_systomo_search_category);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, SystomoSearchActivity2.class);
                if(position == 2){
                    intent.putExtra("catId", 6);
                }else {
                    intent.putExtra("catId", position + 1);
                }
                intent.putExtra("catName", CATEGORIES[position]);
                startActivity(intent);
            }
        });

        mFinishReceiver = new FinishReceiver();
        mFinishReceiver.register();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFinishReceiver.unregister();
    }

    private EditText mWordEt;

    private void initTopSearch() {
        findViewById(R.id.rlayout_item_header_systomo_create_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SystomoCreateGroupActivity.class);
                startActivity(intent);
            }
        });

        mWordEt = (EditText) findViewById(R.id.et_item_header_systomo_freeword);
        mWordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND ||
                        (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search();
                }

                return false;
            }
        });

        findViewById(R.id.btn_item_header_systomo_word).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
    }

    private void search() {
        String word = mWordEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(word), "ニックネーム")) {
            return;
        }

        Intent intent = new Intent(mContext, SystomoSearchResultActivity.class);
        intent.setAction(SystomoSearchResultActivity.ACTION_FROM_FREEWORD);
        intent.putExtra("freeWord", word);
        startActivity(intent);
    }
}
