//package com.app.ug_inc.ui.activities;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.app.ug_inc.R;
//import com.app.ug_inc.billingutil.IabHelper;
//import com.app.ug_inc.billingutil.IabResult;
//import com.app.ug_inc.billingutil.Inventory;
//import com.app.ug_inc.billingutil.Purchase;
//import com.app.ug_inc.datas.Account;
//import com.app.ug_inc.models.ShopItem;
//import com.app.ug_inc.net.InterfaceIds;
//import com.app.ug_inc.net.NetResult;
//import com.app.ug_inc.net.controller.AccountController;
//import com.app.ug_inc.net.controller.ShopController;
//import com.app.ug_inc.ui.views.ZDialog;
//import com.app.ug_inc.utils.ZLog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//public class InnerShopActivity extends BaseNetActivity {
//	public static final String TAG = InnerShopActivity.class.getSimpleName();
//
//	private ArrayList<ShopItem> mShopGoodList;
//
//	private ListAdapter mAdapter;
//
//	public IabHelper mHelper;
//
//	private ShopController mShopController;
//	private AccountController mAccountController;
//
//	private TextView mBalanceTv;
//
//	private ZDialog mDialog;
//
//	private String mCurrBuyingItemSKU;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_innershop);
//
//		makeTitle("売店");
//		enableBack();
//
//		mShopGoodList = new ArrayList<ShopItem>();
//
//		mAdapter = new ListAdapter();
//		ListView lv = (ListView) findViewById(R.id.lv_innershop_items);
//		lv.setAdapter(mAdapter);
//
//		mShopController = new ShopController(this);
//		mShopController.getShopItemList();
//		mLoadingDialog.show();
//
//		mAccountController = new AccountController(this);
//		mAccountController.getBalance();
//
//		mBalanceTv = (TextView) findViewById(R.id.tv_mypage_balance);
//		mBalanceTv.setText(Account.getUser().getBalance());
//
//		mDialog = new ZDialog(mContext);
//		mDialog.showProgressBar();
//		mDialog.setCancelable(false);
//		mDialog.setBackground(R.drawable.dialog_full_holo_dark);
//		// test();
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public void onRequestSuccess(int ifId, NetResult obj) {
//		super.onRequestSuccess(ifId, obj);
//		if (ifId == InterfaceIds.GET_SHOP_ITEM_LIST.IF_ID) {
//			mShopGoodList = (ArrayList<ShopItem>) obj.getResultObject();
//
//			int size = mShopGoodList.size();
//			if (size == 0) {
//				toast(getString(R.string.hint_no_inner_app_goods));
//				return;
//			}
//
//			List<String> skuList = new ArrayList<String>();
//			for (ShopItem item : mShopGoodList) {
//				skuList.add(item.getId());
//			}
//
//			initIabHelper(skuList);
//		} else if (ifId == InterfaceIds.GET_USER_BALANCE.IF_ID) {
//			String balance = (String) obj.getResultObject();
//			mBalanceTv.setText("" + balance);
//		} else if (ifId == InterfaceIds.NOTIFY_PURCHASE.IF_ID) {
//			String newBalance = (String) obj.getResultObject();
//			mBalanceTv.setText("" + newBalance);
//		}
//	}
//
//	private void initIabHelper(final List<String> skuList) {
//		String iabKey = getString(R.string.googlepay_key_test);
//		mHelper = new IabHelper(mContext, iabKey);
//
//		// Start setup. This is asynchronous and the specified listener
//		// will be called once setup completes.
//		ZLog.d(TAG, "Starting setup.");
//		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//			public void onIabSetupFinished(IabResult result) {
//				ZLog.d(TAG, "Setup finished.");
//
//				if (!result.isSuccess()) {
//					// Oh noes, there was a problem.
//					String hint = getString(R.string.hint_iap_init_error) + result;
//					// Toast.makeText(getBaseContext(),hint,Toast.LENGTH_LONG).show();
//					ZLog.d(TAG, hint);
//					return;
//				} else {
//					ZLog.d(TAG, "Iab set up ok!");
//					// mIabOk = true;
//					mHelper.queryInventoryAsync(mGotInventoryListener, skuList);
//					mDialog.setContent(getString(R.string.hint_iap_query_items));
//					mDialog.show();
//				}
//			}
//		});
//	}
//
//	// (arbitrary) request code for the purchase flow
//	static final int RC_REQUEST = 10001;
//
//	private void buyItem(String skuId) {
//		mCurrBuyingItemSKU = skuId;
//
//		String payload = "";
//		mHelper.launchPurchaseFlow(this, skuId, RC_REQUEST, mPurchaseFinishedListener, payload);
//
//		mDialog.setContent(getString(R.string.hint_iap_purchase_items));
//		mDialog.show();
//	}
//
//	// We're being destroyed. It's important to dispose of the helper here!
//	@Override
//	public void onDestroy() {
//		super.onDestroy();
//
//		if (mShopController != null) {
//			mShopController.clear();
//			mShopController = null;
//		}
//
//		// very important:
//		ZLog.d(TAG, "Destroying helper.");
//		if (mHelper != null) {
//			mHelper.dispose();
//			mHelper = null;
//		}
//	}
//
//	@Override
//	protected void test() {
//		super.test();
//
//		for (int i = 0; i < 10; i++) {
//			ShopItem item = new ShopItem();
//			item.setId("" + i);
//			item.setName("商品" + i);
//			// item.setPriceInApp("" + ((i + 1) * 100));
//			// item.setRealPrice("" + ((i + 1) * 20));
//
//			mShopGoodList.add(item);
//		}
//		mAdapter.notifyDataSetChanged();
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		ZLog.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
//		if (mHelper == null)
//			return;
//
//		// Pass on the activity result to the helper for handling
//		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//			// not handled, so handle it ourselves (here's where you'd
//			// perform any handling of activity results not related to in-app
//			// billing...
//			super.onActivityResult(requestCode, resultCode, data);
//		} else {
//			ZLog.d(TAG, "onActivityResult handled by IABUtil.");
//		}
//	}
//
//	/** Verifies the developer payload of a purchase. */
//	boolean verifyDeveloperPayload(Purchase p) {
//		String payload = p.getDeveloperPayload();
//
//		/*
//		 * TODO: verify that the developer payload of the purchase is correct.
//		 * It will be the same one that you sent when initiating the purchase.
//		 *
//		 * WARNING: Locally generating a random string when starting a purchase
//		 * and verifying it here might seem like a good approach, but this will
//		 * fail in the case where the user purchases an item on one device and
//		 * then uses your app on a different device, because on the other device
//		 * you will not have access to the random string you originally
//		 * generated.
//		 *
//		 * So a good developer payload has these characteristics:
//		 *
//		 * 1. If two different users purchase an item, the payload is different
//		 * between them, so that one user's purchase can't be replayed to
//		 * another user.
//		 *
//		 * 2. The payload must be such that you can verify it even when the app
//		 * wasn't the one who initiated the purchase flow (so that items
//		 * purchased by the user on one device work on other devices owned by
//		 * the user).
//		 *
//		 * Using your own server to store and verify developer payloads across
//		 * app installations is recommended.
//		 */
//
//		return true;
//	}
//
//	// Listener that's called when we finish querying the items and
//	// subscriptions we own
//	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
//		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//			mDialog.dismiss();
//			ZLog.d(TAG, "Query inventory finished.");
//
//			// Have we been disposed of in the meantime? If so, quit.
//			if (mHelper == null)
//				return;
//
//			// Is it a failure?
//			if (result.isFailure()) {
//				toast(getString(R.string.hint_iap_query_items_err) + result);
//				// complain("Failed to query inventory: " + result);
//				return;
//			}
//
//			ZLog.d(TAG, "Query inventory was successful.");
//
//			/*
//			 * Check for items we own. Notice that for each purchase, we check
//			 * the developer payload to see if it's correct! See
//			 * verifyDeveloperPayload().
//			 */
//
//			for (ShopItem item : mShopGoodList) {
//				String id = item.getId();
//				if (inventory.hasDetails(id)) {
//					item.setPrice(inventory.getSkuDetails(id).getPrice());
//				}
//			}
//
//			ZLog.d(TAG, "Initial inventory query finished; enabling main UI.");
//			mAdapter.notifyDataSetChanged();
//
//			List<Purchase> purchaseList = new ArrayList<Purchase>();
//
//			for (ShopItem item : mShopGoodList) {
//				String id = item.getId();
//
//				Purchase purchase = inventory.getPurchase(id);
//				if (purchase != null && verifyDeveloperPayload(purchase)) {
//					System.out.println("P: " + purchase + " is not null, consuming...");
//					purchaseList.add(purchase);
//				}
//			}
//
//			int size = purchaseList.size();
//			if (size > 0) {
//				ZLog.d(TAG, "We have " + size + " COINS. Consuming it.");
//				mHelper.consumeAsync(purchaseList, mConsumeFinishedListener);
//				return;
//			}
//		}
//	};
//
//	// Callback for when a purchase is finished
//	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
//		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//			mDialog.dismiss();
//			ZLog.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
//			System.out.println("Purchase finished: " + result + ", purchase: " + purchase);
//
//			// if we were disposed of in the meantime, quit.
//			if (mHelper == null)
//				return;
//
//			// user cancel not hint -1005
//			if (result.isFailure()) {
//				if (result.getResponse() != -1005) {
//					toast(getString(R.string.hint_iap_purchase_items_err) + result);
//				}
//				return;
//			}
//			// if (!verifyDeveloperPayload(purchase)) {
//			// complain("Error purchasing. Authenticity verification failed.");
//			// setWaitScreen(false);
//			// return;
//			// }
//
//			ZLog.d(TAG, "Purchase successful.");
//
//			if (purchase.getSku().equals(mCurrBuyingItemSKU)) {
//				// toast("buy [" + mCurrBuyingItemSKU + "] success!");
//				ZLog.d(TAG, "Buy [" + mCurrBuyingItemSKU + "] success!");
//				ZLog.d(TAG, "now consuming it...");
//
//				mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//				mDialog.setContent(getString(R.string.hint_iap_consume_items));
//				mDialog.show();
//			}
//		}
//	};
//
//	// Called when consumption is complete
//	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
//		public void onConsumeFinished(Purchase purchase, IabResult result) {
//			mDialog.dismiss();
//			ZLog.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
//			System.out.println("Consumption finished. Purchase: " + purchase + ", result: "
//					+ result);
//
//			// if we were disposed of in the meantime, quit.
//			if (mHelper == null)
//				return;
//
//			// We know this is the "gas" sku because it's the only one we
//			// consume,
//			// so we don't check which sku was consumed. If you have more than
//			// one
//			// sku, you probably should check...
//			if (result.isSuccess()) {
//				// successfully consumed, so we apply the effects of the item in
//				// our
//				// game world's logic, which in our case means filling the gas
//				// tank a bit
//				ZLog.d(TAG, "Consumption successful. Provisioning.");
//
//				mShopController.notifySuccess(mCurrBuyingItemSKU, purchase.toString());
//			} else {
//				// complain("Error while consuming: " + result);
//				toast(getString(R.string.hint_iap_consume_items_err) + result);
//			}
//			ZLog.d(TAG, "End consumption flow.");
//		}
//	};
//
//	private class ListAdapter extends BaseAdapter {
//		private LayoutInflater mInflater;
//
//		public ListAdapter() {
//			mInflater = LayoutInflater.from(mContext);
//		}
//
//		@Override
//		public int getCount() {
//			return mShopGoodList.size();
//		}
//
//		@Override
//		public Object getItem(int position) {
//			return mShopGoodList.get(position);
//		}
//
//		@Override
//		public long getItemId(int position) {
//			return position;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			Holder h = null;
//			if (convertView == null) {
//				h = new Holder();
//				convertView = mInflater.inflate(R.layout.item_list_innershop, null);
//
//				h.priceInApp = (TextView) convertView
//						.findViewById(R.id.tv_item_list_innershop_priceinapp);
//				h.realPrice = (TextView) convertView
//						.findViewById(R.id.tv_item_list_innershop_realprice);
//				h.buy = (ImageView) convertView.findViewById(R.id.iv_item_list_innershop_buy);
//
//				convertView.setTag(h);
//			} else {
//				h = (Holder) convertView.getTag();
//			}
//
//			final ShopItem item = mShopGoodList.get(position);
//			h.priceInApp.setText("" + item.getName());
//
//			String realPrice = item.getPrice();
//			h.realPrice.setText(realPrice);
//			h.buy.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					buyItem(item.getId());
//				}
//			});
//
//			return convertView;
//		}
//
//		private class Holder {
//			public ImageView buy;
//			public TextView priceInApp;
//			public TextView realPrice;
//		}
//	}
//}
