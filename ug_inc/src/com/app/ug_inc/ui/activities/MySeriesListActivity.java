package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Article;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.net.controller.MySeriesController.TYPE;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MySeriesListActivity extends BaseListNetActivity {
	private static final int TAB1 = 0;
	private static final int TAB2 = 1;
	private static final int TAB3 = 2;

	private ArrayList<Article> mArticleList;

	private PullListview mListView;

	private ListAdapter mAdapter;

	private ImageView mTab1;
	private ImageView mTab2;
	private ImageView mTab3;

	private TYPE mCurrType = TYPE.ALL;

	private MySeriesController mMySeriesController;

	private SimpleKV mCategory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myseries_list);

		mCategory = (SimpleKV) getIntent().getSerializableExtra("cat");
		makeTitle("" + mCategory.getValue());
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_myseries_new_submit, new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, MySeriesNewSubmitActivity.class);
				intent.putExtra("cat", mCategory);
				startActivityForResult(intent, 1);
			}
		});

		mTab1 = (ImageView) findViewById(R.id.btn_myseries_list_tab1);
		mTab1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mCurrType != TYPE.ALL) {
					selectTab(TAB1);
				}
			}
		});
		mTab2 = (ImageView) findViewById(R.id.btn_myseries_list_tab2);
		mTab2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mCurrType != TYPE.FOLLOWED) {
					selectTab(TAB2);
				}
			}
		});
		mTab3 = (ImageView) findViewById(R.id.btn_myseries_list_tab3);
		mTab3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mCurrType != TYPE.SELF) {
					selectTab(TAB3);
				}
			}
		});

		mArticleList = new ArrayList<Article>();

		mAdapter = new ListAdapter();

		mListView = (PullListview) findViewById(R.id.plv_myseries_list);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent();
				intent.setClass(mContext, MySeriesDetailActivity.class);
				// refresh header -1
				intent.putExtra("article", mArticleList.get(arg2 - 1));
				startActivity(intent);
			}
		});
		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mCurrStatus = STATUS.LOAD_MORE;
				mMySeriesController.getMySeriesArticleList(mCategory.getKey(), mCurrType,
						mCurrPage + 1);
			}
		});

		mMySeriesController = new MySeriesController(this);
		selectTab(TAB1);

		// test();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == RESULT_OK) {
			int index = TAB1;
			if (mCurrType == TYPE.FOLLOWED) {
				index = TAB2;
			} else if (mCurrType == TYPE.SELF) {
				index = TAB3;
			}

			selectTab(index);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMySeriesController != null) {
			mMySeriesController.clear();
			mMySeriesController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_MY_SERIES_ARTICLE_LIST.IF_ID) {
			List<Article> list = (ArrayList<Article>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				judgeRefresh(list, getString(R.string.hint_list_no_data_my_series));

				mCurrPage = ConstData.PAGE_FIRST;
				mArticleList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListView.onRefreshComplete();
			}

			mArticleList.addAll(list);
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_MY_SERIES_ARTICLE; i++) {
			Article item = new Article();
			item.setContent("内容内容内容内容" + i);
			item.setTime("2013-02-02 12:00:00");
			item.setPic("/");
			item.setTitle("标题标题" + i);
			User user = new User();
			user.setId("1");
			user.setNick("dadada");
			user.setCityId("1");
			item.setUser(user);

			mArticleList.add(item);
		}
		mAdapter.notifyDataSetChanged();
	}

	private void selectTab(int index) {
		if (index == TAB1) {
			mCurrType = TYPE.ALL;
			mTab1.setImageResource(R.drawable.ic_btn_myseries_tab1_normal);
			mTab2.setImageResource(R.drawable.ic_btn_myseries_tab2_selected);
			mTab3.setImageResource(R.drawable.ic_btn_myseries_tab3_selected);
		} else if (index == TAB2) {
			mCurrType = TYPE.FOLLOWED;
			mTab1.setImageResource(R.drawable.ic_btn_myseries_tab1_selected);
			mTab2.setImageResource(R.drawable.ic_btn_myseries_tab2_normal);
			mTab3.setImageResource(R.drawable.ic_btn_myseries_tab3_selected);
		} else if (index == TAB3) {
			mCurrType = TYPE.SELF;
			mTab1.setImageResource(R.drawable.ic_btn_myseries_tab1_selected);
			mTab2.setImageResource(R.drawable.ic_btn_myseries_tab2_selected);
			mTab3.setImageResource(R.drawable.ic_btn_myseries_tab3_normal);
		}

		mCurrStatus = STATUS.REFRESH;
		mMySeriesController.getMySeriesArticleList(mCategory.getKey(), mCurrType,
				ConstData.PAGE_FIRST);
		mLoadingDialog.show();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mArticleList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mArticleList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_myseries, null);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_nickname);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_time);
				h.title = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_title);
				h.image = (ImageView) arg1.findViewById(R.id.iv_item_list_myseries_img);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_content);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			Article item = mArticleList.get(arg0);
			final User usr = item.getUser();

			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(usr.getId());
				}
			});

			String name = usr.getNick() + " 【" + usr.getCity() + "】" + usr.getJob();
			h.nick.setText("" + name);
			h.time.setText("" + item.getTime());
			h.title.setText("" + item.getTitle());
			String picThumb = item.getPicThumb();

			if (!TextUtils.isEmpty(picThumb)) {
				h.image.setVisibility(View.VISIBLE);
				ZImageLoader.asyncLoadImage(picThumb, h.image);
			} else {
				h.image.setVisibility(View.GONE);
			}

			h.content.setText("" + item.getContent());

			return arg1;
		}

		private class Holder {
			private TextView nick;
			private TextView time;
			private TextView content;
			private TextView title;
			private ImageView image;
		}
	}
}
