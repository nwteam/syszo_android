package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Company;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.Workplace;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.ui.views.ZDropMenu;
import com.app.ug_inc.ui.views.ZDropMenu.OnItemClickListener;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class MockTypeActivity extends BaseNetActivity {
	private TextView mCityTv;
	private TextView mWorkPTv;
	private TextView mCompanyTv;

	private SimpleKV mSelectedCity;
	private Workplace mSelectedWorkp;
	private Company mSelectedCompany;

	private List<Workplace> mWorkplaceList;
	private List<Company> mCompanyList;

	private MockController mMockController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mock_type);

		makeTitle("現場掲示板");
		enableBack();

		initViews();

		// default city
		mSelectedCity = ConstData.getCityList().get(0);

		mMockController = new MockController(this);
		String defCityId = mSelectedCity.getKey();
//		mMockController.getCompanyListByCity(defCityId);
		mLoadingDialog.show();

		mSelectedCity = ConstData.getCityList().get(0);
		mCityTv.setText("" + mSelectedCity.getValue());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMockController != null) {
			mMockController.clear();
			mMockController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
//		if (ifId == InterfaceIds.GET_COMPANY_LIST_BY_CITY.IF_ID) {
//			mCompanyList = (List<Company>) obj.getResultObject();
//
//			mMockController.getWorkplaceList(mSelectedCity.getKey(), "", "");
//
//			mSelectedCompany = null;
//			mCompanyTv.setText(getString(R.string.item_pick_list_blank));
//			mSelectedWorkp = null;
//			mWorkPTv.setText(getString(R.string.item_pick_list_blank));
//
//		} else if (ifId == InterfaceIds.GET_WORKPLACE_LIST.IF_ID) {
//			super.onRequestSuccess(ifId, obj);
//			mWorkplaceList = (List<Workplace>) obj.getResultObject();
//		}
	}

	private final int[] TVIEW_IDS = { R.id.tv_mock_type_hint_city,
			R.id.tv_mock_type_hint_workp_name, R.id.tv_mock_type_hint_company_name,
			R.id.tv_mock_type_hint_free_word, };

	private void initViews() {
		mCityTv = (TextView) findViewById(R.id.tv_mock_type_city);
		mCityTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickCity();
			}
		});
		mWorkPTv = (TextView) findViewById(R.id.tv_mock_type_company_workp_name);
		mWorkPTv.setText(getString(R.string.item_pick_list_blank));
		mWorkPTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickWorkplace();
			}
		});
		mCompanyTv = (TextView) findViewById(R.id.tv_mock_type_company_name);
		mCompanyTv.setText(getString(R.string.item_pick_list_blank));
		mCompanyTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickCompany();
			}
		});

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}
	}

	private void pickCity() {
		final List<SimpleKV> cityList = ConstData.getCityList();
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : cityList) {
			items.add(kv.getValue());
		}
		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String city = cityList.get(position).getValue();
				mCityTv.setText("" + city);

				mSelectedCity = cityList.get(position);

//				mMockController.getCompanyListByCity(mSelectedCity.getKey());
				mLoadingDialog.show();

				mSelectedCompany = null;
				mCompanyTv.setText(R.string.item_pick_list_blank);
				mSelectedWorkp = null;
				mWorkPTv.setText(R.string.item_pick_list_blank);
			}
		});
		menu.show(mCityTv);
	}

	private void pickWorkplace() {
		List<String> items = new ArrayList<String>();

		for (Workplace workplace : mWorkplaceList) {
			items.add(workplace.getName());
		}

		ZDropMenu menu = new ZDropMenu(mContext, items, true);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String workplace = "";
				if (position == 0) {
					mSelectedWorkp = null;
					workplace = getString(R.string.item_pick_list_blank);
				} else {
					workplace += mWorkplaceList.get(position - 1).getName();
					mSelectedWorkp = mWorkplaceList.get(position - 1);
				}

				mWorkPTv.setText("" + workplace);
			}
		});
		menu.show(mWorkPTv);
	}

	private void pickCompany() {
		if (mCompanyList == null && mCompanyList.size() <= 0) {
			return;
		}

		List<String> items = new ArrayList<String>();

		for (Company company : mCompanyList) {
			items.add(company.getName());
		}

		ZDropMenu menu = new ZDropMenu(mContext, items, true);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String company = "";
				if (position == 0) {
					mSelectedCompany = null;
					company = getString(R.string.item_pick_list_blank);
				} else {
					company += mCompanyList.get(position - 1).getName();
					mSelectedCompany = mCompanyList.get(position - 1);
				}

				mCompanyTv.setText("" + company);

				String companyId = "";
				if (mSelectedCompany != null) {
					companyId = mSelectedCompany.getId();
				}

				mSelectedWorkp = null;
				mWorkPTv.setText(R.string.item_pick_list_blank);

				mMockController.getWorkplaceList(mSelectedCity.getKey(), companyId, "");
				mLoadingDialog.show();
			}
		});
		menu.show(mCompanyTv);
	}

	public void addWorkplace(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MockAddWorkplaceActivity.class);
		startActivity(intent);
	}

	public void queryWorkplace(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MockWorkpListActivty.class);
		intent.putExtra("city", mSelectedCity);
		intent.putExtra("company", mSelectedCompany);
		intent.putExtra("workplace", mSelectedWorkp);
		intent.putExtra("word", ((EditText) findViewById(R.id.et_mock_type_free_word)).getText()
				.toString());
		startActivity(intent);
	}
}
