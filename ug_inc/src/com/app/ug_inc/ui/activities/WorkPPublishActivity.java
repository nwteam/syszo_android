package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.WorkerInfo;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.WorkplaceController;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.ui.views.ZDropMenu;
import com.app.ug_inc.ui.views.ZDropMenu.OnItemClickListener;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class WorkPPublishActivity extends BaseNetActivity {

	private boolean mIsWorker;

	private SimpleKV mSelectedCity;
	private SimpleKV mSelectedDist;
	private SimpleKV mSelectedJob;
	private SimpleKV mSelectedPeople;

	private TextView mJobTv;
	private EditText mJobOtherEt; // visible when job-other clicked
	private TextView mPeopleTv;
	private TextView mStartTime;
	private TextView mEndTime;
	private TextView mCityTv;
	private TextView mDistTv;
	private EditText mDescribeEt;

	private WorkplaceController mWorkpController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workp_publish);

		mIsWorker = getIntent().getAction().equals(
				WorkPPickActivity.ACTION_WORKER);

		makeTitle(mIsWorker ? "現場情報書き込み" : "空き情報");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

			@Override
			public void onClick(View v) {
				submit();
			}
		});
		mWorkpController = new WorkplaceController(this);

		TextView hintPlaceTv = (TextView) findViewById(R.id.tv_workp_publish_hint_place);
		hintPlaceTv.setText(mIsWorker ? "作業住所" : "対応可能地域");

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}

		mJobTv = (TextView) findViewById(R.id.tv_workp_publish_job);
		mJobTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickJob();
			}
		});
		mJobOtherEt = (EditText) findViewById(R.id.et_workp_publish_job_other);

		mPeopleTv = (TextView) findViewById(R.id.tv_workp_publish_people);
		mPeopleTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickPeople();
			}
		});
		mStartTime = (TextView) findViewById(R.id.tv_workp_publish_time_start);
		mStartTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickStartTime();
			}
		});
		mEndTime = (TextView) findViewById(R.id.tv_workp_publish_time_end);
		mEndTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickEndTime();
			}
		});
		mCityTv = (TextView) findViewById(R.id.tv_workp_publish_place_city);
		mCityTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickCity();
			}
		});
		mDistTv = (TextView) findViewById(R.id.tv_workp_publish_place_district);
		mDistTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickDist();
			}
		});

		mDescribeEt = (EditText) findViewById(R.id.et_workp_publish_detail);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mWorkpController != null) {
			mWorkpController.clear();
			mWorkpController = null;
		}
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		toast(getString(R.string.success_publish_free_workp));
		finish();
	}

	protected void pickDist() {
		if (mSelectedCity == null) {
			toast(getString(R.string.hint_select_city_first));
			return;
		}

		final List<SimpleKV> distList = ConstData
				.getDistrictListByCity(mSelectedCity);
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : distList) {
			items.add(kv.getValue());
		}
		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String dist = distList.get(position).getValue();
				mDistTv.setText("" + dist);

				mSelectedDist = distList.get(position);
			}
		});
		menu.show(mCityTv);
	}

	protected void pickCity() {
		final List<SimpleKV> cityList = ConstData.getCityList();
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : cityList) {
			items.add(kv.getValue());
		}
		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String city = cityList.get(position).getValue();
				mCityTv.setText("" + city);

				SimpleKV selectedCity = cityList.get(position);
				if (!selectedCity.equals(mSelectedCity)) {
					mSelectedCity = selectedCity;
					mCityTv.setText("" + selectedCity.getValue());

					mDistTv.setText("");
					mSelectedDist = null;
				}
			}
		});
		menu.show(mCityTv);
	}

	protected void pickEndTime() {
		DatePickerDialog dp = new DatePickerDialog(mContext,
				new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Date d = new Date(year - 1900, monthOfYear, dayOfMonth);

						String date = DateFormat.format(TimeUtil.PATTERN_DATE,
								d).toString();
						mEndTime.setText("" + date);
					}
				}, 2014, 5, 31);
		dp.show();
	}

	protected void pickStartTime() {
		DatePickerDialog dp = new DatePickerDialog(mContext,
				new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Date d = new Date(year - 1900, monthOfYear, dayOfMonth);

						String date = DateFormat.format(TimeUtil.PATTERN_DATE,
								d).toString();
						mStartTime.setText("" + date);
					}
				}, 2014, 5, 31);
		dp.show();
	}

	protected void pickPeople() {
		final List<SimpleKV> peopleList = ConstData.getPeopleList();
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : peopleList) {
			items.add(kv.getValue());
		}

		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String people = peopleList.get(position).getValue();
				mPeopleTv.setText("" + people);

				mSelectedPeople = peopleList.get(position);
			}
		});
		menu.show(mPeopleTv);
	}

	protected void pickJob() {
		final List<SimpleKV> jobList = ConstData.getJobList();
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : jobList) {
			items.add(kv.getValue());
		}

		final int size = items.size();
		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				if (position == size - 1) { // last
					mJobOtherEt.setVisibility(View.VISIBLE);
				} else {
					mJobOtherEt.setVisibility(View.GONE);
				}

				String job = jobList.get(position).getValue();
				mJobTv.setText("" + job);

				mSelectedJob = jobList.get(position);
			}
		});
		menu.show(mJobTv);
	}

	private void submit() {
		if (checkXX(mSelectedJob == null, "職種")) {
			return;
		}

		if (checkXX(mSelectedCity == null, "作業住所")) {
			return;
		}

		if (checkXX(mSelectedPeople == null, "人数")) {
			return;
		}

		String job = mSelectedJob.getKey();
		String startTime = mStartTime.getText().toString();
		String endTime = mEndTime.getText().toString();
		String city = mSelectedCity.getKey();
		String dist = ConstData.getDistrict(mSelectedCity,
				mSelectedDist.getKey());
		String people = mSelectedPeople.getKey();
		String describe = mDescribeEt.getText().toString();

		final WorkerInfo info = new WorkerInfo();
		if (mJobOtherEt.getVisibility() == View.VISIBLE) {
			String jobName = mJobOtherEt.getText().toString();

			if (checkXX(TextUtils.isEmpty(jobName), "職種")) {
				return;
			}

			info.setJobId("0");
			info.setJobName(jobName);
		} else {
			info.setJobId(job);
		}
		info.setStartTime(startTime);
		info.setEndTime(endTime);
		info.setCityId(city);
		info.setDistrict(dist);
		info.setPeopleSum(people);
		info.setDescribe(describe);

		// new ZDialog(mContext).setTitle(getString(R.string.confirm))
		// .setContent(getString(R.string.hint_worplace_publish_need_money))
		// .setLeftButton(getString(R.string.cancel))
		// .setRightButton(getString(R.string.OK), new
		// ZDialog.RightButtonListener() {
		//
		// @Override
		// public void onRightButtonClick() {
		if (mIsWorker) {
			mWorkpController.publishFreeWorker(info);
			mLoadingDialog.show();
		} else {
			mWorkpController.publishFreeWorkplace(info);
			mLoadingDialog.show();
		}
		// }
		// }).show();
	}

	private final int[] TVIEW_IDS = { R.id.tv_workp_publish_hint_job,
			R.id.tv_workp_publish_hint_people, R.id.tv_workp_publish_hint_time,
			R.id.tv_workp_publish_hint_place,
			R.id.tv_workp_publish_hint_detail, };
}