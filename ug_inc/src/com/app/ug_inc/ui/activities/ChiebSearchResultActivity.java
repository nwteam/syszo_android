package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChiebSearchResultActivity extends BaseListNetActivity {
    private ListAdapter mAdapter;
    private PullListview mListview;

    private ArrayList<Chieb> mChiebList;

    private ClosureController mRefreshController;
    private ClosureController mLoadmoreController;

    private String mWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_search_result);

        makeTitle("知恵袋-検索");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_totop, new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                sendBroadcast(new Intent(ChiebSearchActivity.ACTION_CHIEB_SEARCH_NEED_FINISH));
            }
        });

        mWord = getIntent().getStringExtra("word");

        TextView topTv = (TextView) findViewById(R.id.tv_search_result_top);
        topTv.setText("「" + mWord + "」の検索結果");

        mChiebList = new ArrayList<Chieb>();

        mAdapter = new ListAdapter();
        mListview = (PullListview) findViewById(R.id.plv_chieb_search_result_list);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListview.getRefreshableView().getHeaderViewsCount();
                Chieb chieb = mChiebList.get(pos);
                Intent intent = new Intent();
                intent.setClass(mContext, ChiebDetailActivity.class);
                intent.putExtra("chieb", chieb);
                startActivity(intent);
            }
        });
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mRefreshController.searchClosure(mWord, ConstData.PAGE_FIRST);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mLoadmoreController.searchClosure(mWord, mCurrPage + 1);
            }
        });

        mRefreshController = new ClosureController(new UIDelegate() {
            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                mLoadingDialog.dismiss();
                mListview.onRefreshComplete();
                List<Chieb> list = (List<Chieb>) obj.getResultObject();

                mChiebList.clear();
                mChiebList.addAll(list);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                mLoadingDialog.dismiss();
                mListview.onRefreshComplete();
                toast(errMsg);
            }
        });

        mLoadmoreController = new ClosureController(new UIDelegate() {
            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                mListview.onRefreshComplete();

                List<Chieb> list = (List<Chieb>) obj.getResultObject();
                judgeLoadMore(list);

                mChiebList.addAll(list);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                mListview.onRefreshComplete();
            }
        });

        mRefreshController.searchClosure(mWord, ConstData.PAGE_FIRST);
        mLoadingDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mRefreshController);
        clearController(mLoadmoreController);
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_CHIEB; i++) {
            Chieb c = new Chieb();
            c.setCommentCount("" + (int) (new Random().nextDouble() * 100));
            c.setTime("2014-12-31 12:00:00");
            c.setTitle("标题标题标题标题标题标题标题标题" + i);
            c.setContent("内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容");
            c.setUrgent(i % 4 == 0);

            User user = new User();
            user.setNick("用户" + i);
            c.setUser(user);

            mChiebList.add(c);
        }
        mAdapter.notifyDataSetChanged();
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mChiebList.size();
        }

        @Override
        public Chieb getItem(int arg0) {
            return mChiebList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chieb, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_content);
                h.count = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_count);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_time);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_chieb_root);
                h.avatar = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_avatar);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            Chieb item = getItem(arg0);
            if (item.isUrgent()) {
                h.bg.setBackgroundColor(RED);
                h.time.setTextColor(WHITE);
                h.count.setTextColor(WHITE);
                h.content.setTextColor(WHITE);
            } else {
                h.bg.setBackgroundColor(arg0 % 2 == 0 ? WHITE : 0xFFFAFAFA);
                h.time.setTextColor(0xFF393A40);
                h.count.setTextColor(0xFFA7ACA9);
                h.content.setTextColor(0xFF393A40);

                h.avatar.setImageResource(item.isCommited() ? R.drawable.icon_comment_on : R.drawable.icon_comment_off);
            }

            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getTitle());
            h.count.setText("" + item.getCommentCount());

            return arg1;
        }

        private class Holder {
            private ImageView avatar;
            private TextView content;
            private TextView time;
            private TextView count;
            private View bg;
        }
    }
}
