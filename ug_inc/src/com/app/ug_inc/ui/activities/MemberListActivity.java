package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.GroupController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.utils.ConstData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class MemberListActivity extends BaseNetActivity {

    private ListView mListView;
    private ListAdapter mAdapter;

    private GroupController mRefreshController;
    private GroupController mLoadmoreController;

    private ArrayList<User> mUserList;

    private boolean mModified = false;

    private ArrayList<String> mGroupMemIdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memberlist);

        makeTitle("メンバー");
        makeIcon(R.drawable.micon_systomo);
        enableBack();

        mUserList = new ArrayList<User>();

        Intent intent = getIntent();
        mGroupMemIdList = (ArrayList<String>) intent.getSerializableExtra("memberlist");

        mAdapter = new ListAdapter();
        mListView = (ListView) findViewById(R.id.lv_memberlist);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mModified = true;

                User user = mUserList.get(position);
                user.setIsFollowed(user.getIsFollowed().equals("1") ? "2" : "1");

                mAdapter.notifyDataSetChanged();
            }
        });

        mRefreshController = new GroupController(mRefreshDelegate);
        mLoadmoreController = new GroupController(mLoadmoreDelegate);

        mRefreshController.addFriendList(ConstData.PAGE_FIRST);
    }

    @Override
    public void finish() {
        if (mModified) {
            Intent intent = new Intent();
            intent.putExtra("list", mUserList);
            setResult(RESULT_OK, intent);
        }

        super.finish();
    }

    private UIDelegate mRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            List<User> list = (List<User>) obj.getResultObject();
            mUserList.addAll(list);

            for (User user : mUserList) {
                if (mGroupMemIdList.contains(user.getId())) {
                    user.setIsFollowed("1");
                }
            }

            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
        }
    };

    private UIDelegate mLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
        }
    };

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mUserList.size();
        }

        @Override
        public User getItem(int position) {
            return mUserList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_member, null);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_member_name);
                h.profile = (ImageView) convertView.findViewById(R.id.iv_item_list_member_profile);
                h.check = (ImageView) convertView.findViewById(R.id.iv_item_list_member_check);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final User user = getItem(position);
            h.name.setText("" + user.getNick());
            h.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProfile(user.getId());
                }
            });

            h.check.setImageResource((user.getIsFollowed().equals("1")) ?
                    R.drawable.checkbox_pressed : R.drawable.checkbox_normal);

            return convertView;
        }


        private class Holder {
            private ImageView check;
            private TextView name;
            private ImageView profile;
        }
    }
}
