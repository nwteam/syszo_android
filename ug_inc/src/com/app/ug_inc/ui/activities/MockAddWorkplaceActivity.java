package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Company;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.ui.utils.FontUtil;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.ui.views.ZDropMenu;
import com.app.ug_inc.ui.views.ZDropMenu.OnItemClickListener;
import com.app.ug_inc.utils.ConstData;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class MockAddWorkplaceActivity extends BaseNetActivity {
	private MockController mMockController;

	private TextView mCityTv;
	private TextView mCompanyTv;

	private Company mSelectedCompany;
	private SimpleKV mSelectedCity;

	private List<Company> mCompanyList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mock_add);

		makeTitle("現場名追加");
		enableBack();

		initViews();

		mSelectedCity = ConstData.getCityList().get(0);
		mCityTv.setText("" + mSelectedCity.getValue());

		mCompanyTv.setText(getString(R.string.item_pick_list_blank));

		mMockController = new MockController(this);
		String defCityId = mSelectedCity.getKey();
//		mMockController.getCompanyListByCity(defCityId);
		mLoadingDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mMockController != null) {
			mMockController.clear();
			mMockController = null;
		}
	}

	private final int[] TVIEW_IDS = { R.id.tv_mock_add_hint_top, R.id.tv_mock_add_hint_district,
			R.id.tv_mock_add_hint_company_name, R.id.tv_mock_add_hint_workp_name, };

	private void initViews() {
		mCityTv = (TextView) findViewById(R.id.tv_mock_add_district);
		mCityTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickCity();
			}
		});
		mCompanyTv = (TextView) findViewById(R.id.tv_mock_add_company_name);
		mCompanyTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pickCompany();
			}
		});

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
//		if (ifId == InterfaceIds.GET_COMPANY_LIST_BY_CITY.IF_ID) {
//			mCompanyList = (List<Company>) obj.getResultObject();
//
//			mSelectedCompany = null;
//			mCompanyTv.setText(getString(R.string.item_pick_list_blank));
//
//		} else if (ifId == InterfaceIds.ADD_WORKPLACE.IF_ID) {
//			toast(getString(R.string.success_add_workplace));
//			finish();
//		}
	}

	public void submit(View v) {
		EditText et = (EditText) findViewById(R.id.et_mock_add_company_workp_name);
		final String name = et.getText().toString();

		if (checkXX(TextUtils.isEmpty(name), "現場名")) {
			return;
		}

		final String companyId = mSelectedCompany == null ? "" : mSelectedCompany.getId();
		// if (checkXX(mSelectedCompany == null, "企業名")) {
		// return;
		// }

		new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm))
				.setLeftButton(getString(R.string.cancel))
				.setRightButton(getString(R.string.OK), new RightButtonListener() {

					@Override
					public void onRightButtonClick() {
						mMockController.addWorkplace(mSelectedCity.getKey(), companyId, name);
						mLoadingDialog.show();
					}
				}).show();
	}

	private void pickCity() {
		final List<SimpleKV> cityList = ConstData.getCityList();
		List<String> items = new ArrayList<String>();
		for (SimpleKV kv : cityList) {
			items.add(kv.getValue());
		}
		ZDropMenu menu = new ZDropMenu(mContext, items);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String city = cityList.get(position).getValue();
				mCityTv.setText("" + city);

				mSelectedCity = cityList.get(position);

//				mMockController.getCompanyListByCity(mSelectedCity.getKey());
//				mLoadingDialog.show();

				mSelectedCompany = null;
				mCompanyTv.setText(getString(R.string.item_pick_list_blank));
			}
		});
		menu.show(mCityTv);
	}

	private void pickCompany() {
		if (mCompanyList == null && mCompanyList.size() <= 0) {
			return;
		}

		List<String> items = new ArrayList<String>();

		for (Company company : mCompanyList) {
			items.add(company.getName());
		}

		ZDropMenu menu = new ZDropMenu(mContext, items, true);
		menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClicked(int position) {
				String workplace = "";
				if (position == 0) {
					mSelectedCompany = null;
					workplace = getString(R.string.item_pick_list_blank);
				} else {
					workplace += mCompanyList.get(position - 1).getName();
					mSelectedCompany = mCompanyList.get(position - 1);
				}

				mCompanyTv.setText("" + workplace);
			}
		});
		menu.show(mCompanyTv);
	}
}