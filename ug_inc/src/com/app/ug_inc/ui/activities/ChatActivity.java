package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Contract;
import com.app.ug_inc.models.Conversation;
import com.app.ug_inc.models.Message;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MessageController;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.FileUtils;
import com.app.ug_inc.utils.TimeUtil;
import com.app.ug_inc.utils.Utils;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ChatActivity extends BaseNetActivity {
    public static final int REQUEST_SEND_CONTRACT = 1001;
    public static final String FILTER_NEW_MSG_RECEIVED = "filter." + ChatActivity.class
            + ".new_msg_received";
    public static final String FILTER_NEED_FINISH = "filter." + ChatActivity.class + ".need_finish";

    private ListView mListView;

    private EditText mSendEt;

    private ListAdapter mAdapter;

    private Message mNewMessage;

    private Conversation mConv;

    private ArrayList<Message> mMessageList;

    private MessageController mMessageController;

//	private String mTargetUid;

    private String mUserid;
    private String mGroupId;

    private ZActivityManager mManager;

    private NewMsgReceiver mNewMsgReceiver;
    private FinishReceiver mFinishReceiver;

//	private View mContractBtn;

    private NotificationManager mNotifManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();

        makeTitle(intent.getStringExtra("title"));
        makeIcon(R.drawable.micon_message);
        makeTitleBg(0xFFE7BC37);
        enableBack();
//
//		mContractBtn = addRightAction(R.drawable.ic_btn_acbar_contract, new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				sendContract();
//			}
//		});
//		mContractBtn.setVisibility(View.INVISIBLE);

//		mTargetUid = intent.getStringExtra("uid");
//		boolean isFromContract = intent.getBooleanExtra("isFromContract", false);

//		if (isFromContract) {
//			mContractBtn.setVisibility(View.VISIBLE);
//		}

        mConv = new Conversation();
        mMessageList = new ArrayList<Message>();

        mSendEt = (EditText) findViewById(R.id.et_chat_comment);

        mAdapter = new ListAdapter();
        mListView = (ListView) findViewById(R.id.lv_chat_main);
        mListView.setAdapter(mAdapter);

        mUserid = intent.getStringExtra("uid");
        mGroupId = intent.getStringExtra("gid");

        mMessageController = new MessageController(this);
        mMessageController.takeChat(mUserid, mGroupId, ConstData.PAGE_FIRST);
//		mLoadingDialog.show();

        mMySeriesController = new MySeriesController(this);
        // test();

        mManager = ZActivityManager.getInstance();

        mManager.chatWithUid = TextUtils.isEmpty(mUserid) ? mGroupId : mUserid;

        mNewMsgReceiver = new NewMsgReceiver();
        mNewMsgReceiver.register();

        mFinishReceiver = new FinishReceiver();
        mFinishReceiver.register();

        mNotifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mManager.isChatForeground = true;

        mNotifManager.cancelAll();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mManager.isChatForeground = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mManager.chatWithUid = "";

        if (mMessageController != null) {
            mMessageController.clear();
            mMessageController = null;
        }

        if (mNewMsgReceiver != null) {
            mNewMsgReceiver.unregister();
            mNewMsgReceiver = null;
        }

        if (mFinishReceiver != null) {
            mFinishReceiver.unregister();
            mFinishReceiver = null;
        }
    }

//	public void sendContract() {
//		Intent intent = new Intent();
//		intent.putExtra("uid", mTargetUid);
//		intent.putExtra("convid", mConv.getId());
//		intent.setClass(mContext, ContractEditActivity.class);
//		startActivityForResult(intent, REQUEST_SEND_CONTRACT);
//	}

    public void sendPhoto(View v) {
        String[] hints = new String[]{getString(R.string.hint_upload_photo_take),
                getString(R.string.hint_upload_photo_pick)};
        new AlertDialog.Builder(mContext).setItems(hints, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // take picture
                        try {
                            mTmpPhotoPath = Utils.takePhoto(ChatActivity.this);
                        } catch (Exception e) {
                            toast(getString(R.string.hint_sdcard_not_mounted));
                        }
                        break;
                    case 1: // pick picture
                        Utils.pickPhoto(ChatActivity.this);
                        break;

                    default:
                        break;
                }

                dialog.dismiss();
            }
        }).show();
    }

    public void submitReply(View v) {
        String content = mSendEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(content.trim()), "入力内容")) {
            return;
        }

        mMessageController.sendMsgText(mGroupId, mUserid, content);
        mLoadingDialog.show();

        mNewMessage = new Message();
        mNewMessage.setFromUser(Account.getUser());
        mNewMessage.setContent(content);
        mNewMessage.setType("0");
    }

    private void addMessage() {
        mSendEt.setText("");

        mNewMessage.setTime(TimeUtil.getCurrentTime("HH:mm"));
        addMessage(mNewMessage);
    }

    private void addMessage(Message msg) {
        mMessageList.add(msg);
        mAdapter.notifyDataSetChanged();

        mListView.setSelection(mListView.getCount() - 1);
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < 10; i++) {
            Message msg = new Message();
            if (i % 2 == 0) {
                User user = new User();
                user.setNick("发送者");
                msg.setFromUser(user);
                msg.setType("1");
            } else {
                msg.setType("0");
            }
            msg.setContent("哈哈哈哈哈哈");
        }
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.TAKE_CHAT.IF_ID) {
            mConv = (Conversation) obj.getResultObject();
            mMessageList = (ArrayList<Message>) mConv.getChatList();

//			if (mConv.getType().equals("1")) {
//				mContractBtn.setVisibility(View.VISIBLE);
//			}
            findViewById(R.id.rlayout_chat_bottom).setVisibility(View.VISIBLE);

            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.UPLOAD_PIC.IF_ID) {
            String uploaded = (String) obj.getResultObject();
            mMessageController.sendMsgPic(mGroupId, mUserid, uploaded);
            mLoadingDialog.show();
        } else if (ifId == InterfaceIds.SEND_MSG.IF_ID) {
            addMessage();
        }
    }

    private MySeriesController mMySeriesController;
    private String mTmpPhotoPath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            if ((requestCode == Utils.REQUEST_PICK_PHOTO || requestCode == Utils.REQUEST_TAKE_PHOTO)) {
                mTmpPhotoPath = null;
            }
            return;
        }

        switch (requestCode) {
            case Utils.REQUEST_PICK_PHOTO:
                Uri photoUri = data.getData();
                mTmpPhotoPath = FileUtils.getPath(mContext, photoUri);

                uploadPic();
                break;
            case Utils.REQUEST_TAKE_PHOTO:
                uploadPic();
                break;

            default:
                break;
        }
    }

    private void uploadPic() {
        if (!TextUtils.isEmpty(mTmpPhotoPath)) {
            // mMySeriesController.uploadPic(mTmpPhotoPath);
            // mLoadingDialog.show();
            // mLoadingDialog.setCancelable(false);

            String path = "file://" + mTmpPhotoPath;

            mNewMessage = new Message();
            mNewMessage.setFromUser(Account.getUser());
            mNewMessage.setPicUrl(path);
            mNewMessage.setType("1");

            mMySeriesController.uploadPic(mTmpPhotoPath);
            mLoadingDialog.show();
        }
    }

    private class NewMsgReceiver extends BroadcastReceiver {

        public void register() {
            registerReceiver(this, new IntentFilter(FILTER_NEW_MSG_RECEIVED));
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Message msg = (Message) intent.getSerializableExtra("msg");
            if (msg != null) {
                addMessage(msg);
            }
        }
    }

    private class FinishReceiver extends BroadcastReceiver {

        public void register() {
            registerReceiver(this, new IntentFilter(FILTER_NEED_FINISH));
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    private class ListAdapter extends BaseAdapter {
        private final int LEFT_TEXT = 0, RIGHT_TEXT = 1, LEFT_PIC = 2, RIGHT_PIC = 3;
        private final int TYPE_COUNT = 4;

        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mMessageList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return mMessageList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }

        @Override
        public int getItemViewType(int position) {
            Message msg = mMessageList.get(position);
            String type = msg.getType();

            boolean isSelf = Account.getUserId().equals(msg.getFromUser().getId());

            if ("0".equals(type)) { // text
                if (isSelf) { // self
                    return RIGHT_TEXT;
                } else { // others
                    return LEFT_TEXT;
                }
            } else if ("1".equals(type)) {
                if (isSelf) { // self
                    return RIGHT_PIC;
                } else { // others
                    return LEFT_PIC;
                }
            }

            return RIGHT_TEXT;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            int type = getItemViewType(position);

            if (convertView == null) {
                h = new Holder();
                switch (type) {
                    case LEFT_TEXT:
                        convertView = mInflater.inflate(R.layout.item_list_chat_left_text, null);
                        h.msg = (TextView) convertView.findViewById(R.id.tv_item_list_chat_content);
                        h.nick = (TextView) convertView.findViewById(R.id.tv_item_list_chat_nick);
                        h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_avatar);
                        h.time = (TextView) convertView.findViewById(R.id.tv_item_list_chat_time);
                        break;
                    case RIGHT_TEXT:
                        convertView = mInflater.inflate(R.layout.item_list_chat_right_text, null);
                        h.msg = (TextView) convertView.findViewById(R.id.tv_item_list_chat_content);
                        h.nick = (TextView) convertView.findViewById(R.id.tv_item_list_chat_nick);
                        h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_avatar);
                        h.time = (TextView) convertView.findViewById(R.id.tv_item_list_chat_time);
                        break;
                    case LEFT_PIC:
                        convertView = mInflater.inflate(R.layout.item_list_chat_left_pic, null);
                        h.pic = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_pic);
                        h.nick = (TextView) convertView.findViewById(R.id.tv_item_list_chat_nick);
                        h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_avatar);
                        h.time = (TextView) convertView.findViewById(R.id.tv_item_list_chat_time);
                        break;
                    case RIGHT_PIC:
                        convertView = mInflater.inflate(R.layout.item_list_chat_right_pic, null);
                        h.pic = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_pic);
                        h.nick = (TextView) convertView.findViewById(R.id.tv_item_list_chat_nick);
                        h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_avatar);
                        h.time = (TextView) convertView.findViewById(R.id.tv_item_list_chat_time);
                        break;

                    default:
                        break;
                }

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final Message msg = mMessageList.get(position);

            String nick = null;
            switch (type) {
                case LEFT_TEXT:
                    nick = msg.getFromUser().getNick();
                    h.msg.setText("" + msg.getContent());
                    h.msg.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            Utils.copyToClipBoard(mContext, msg.getContent());
                            return true;
                        }
                    });
                    break;
                case RIGHT_TEXT:
                    nick = Account.getUser().getNick();
                    h.msg.setText("" + msg.getContent());
                    h.msg.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            Utils.copyToClipBoard(mContext, msg.getContent());
                            return true;
                        }
                    });
                    break;
                case LEFT_PIC:
                    nick = msg.getFromUser().getNick();
                    final String picL = msg.getPicUrl();
                    ZImageLoader.asyncLoadImage(picL, h.pic);
                    h.pic.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            Intent intent = new Intent();
                            intent.setClass(mContext, PicBrowseActivity.class);
                            intent.putExtra("url", picL);
                            startActivity(intent);
                        }
                    });
                    break;
                case RIGHT_PIC:
                    nick = Account.getUser().getNick();
                    final String picR = msg.getPicUrl();
                    ZImageLoader.asyncLoadImage(picR, h.pic);
                    h.pic.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            Intent intent = new Intent();
                            intent.setClass(mContext, PicBrowseActivity.class);
                            intent.putExtra("url", picR);
                            startActivity(intent);
                        }
                    });
                    break;

                default:
                    break;
            }

//            boolean isSelf = type == RIGHT_TEXT || type == RIGHT_PIC;
//            final String uid = isSelf ? Account.getUserId() : msg.getFromUser().getId();

            h.nick.setText("" + nick);
            h.time.setText("" + msg.getTime());
//            h.avatar.setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    seeProfile(uid);
//                }
//            });

            return convertView;
        }

        private void confirmContract(String contractUrl, String uid) {
            Intent intent = new Intent();
            intent.setClass(mContext, WebActivity.class);
            intent.putExtra("url", contractUrl + "&user_id=" + uid);
            intent.putExtra("title", "発注書確認");
            startActivity(intent);
        }

        private class Holder {
            private ImageView avatar;
            private ImageView pic;
            private ImageView contractConfirm;
            private TextView nick;
            private TextView msg;
            private TextView time;
        }
    }

    @Override
    protected void outOfBalanceCancelLeft() {
        // super.outOfBalanceCancelLeft();
        // do nothing
    }

    @Override
    protected void outOfBalanceCancelRight() {
        // super.outOfBalanceCancelRight();
        // do nothing
    }
}
