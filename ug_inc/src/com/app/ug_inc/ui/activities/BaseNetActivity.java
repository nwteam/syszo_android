package com.app.ug_inc.ui.activities;

import net.nend.android.NendAdListener;
import net.nend.android.NendAdView;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.net.NetConst;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.BaseController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.LeftButtonListener;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.ZLog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class BaseNetActivity extends BaseActivity implements UIDelegate {
	protected ZDialog mLoadingDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mLoadingDialog = new ZDialog(mContext);
		mLoadingDialog.setBackground(R.drawable.dialog_full_holo_dark);
		mLoadingDialog.showProgressBar();
		mLoadingDialog
				.setContent(getString(R.string.hint_requesting_please_wait));
	}

	protected void test() {
		ZLog.e("------ TEST DATAS! ------");
	}

	/**
	 * dismiss loading dialog
	 */
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
			mLoadingDialog.setCancelable(true);
		}
	}

	/**
	 * dismiss loading dialog & toast errmsg
	 */
	@Override
	public void onRequestError(int ifId, String errMsg) {
		if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
			mLoadingDialog.setCancelable(true);
		}
		if (errMsg.equals(NetConst.ERR_OUT_OF_BALANCE)) {
			showOutOfBalanceDlg();
		} else {
			toast(errMsg);
			System.out.println("errMsg="+errMsg);
		}
	}

	public void showOutOfBalanceDlg() {
		new ZDialog(mContext)
				.setCancelable(false)
				.setContent(getString(R.string.hint_out_of_balance))
				.setLeftButton(getString(R.string.OK),
						new LeftButtonListener() {

							@Override
							public void onLeftButtonClick() {
								outOfBalanceCancelLeft();
							}
						})
				.setRightButton(getString(R.string.goto_shop),
						new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								// Intent intent = new Intent();
								// intent.setClass(mContext,
								// InnerShopActivity.class);
								// startActivity(intent);
								// outOfBalanceCancelRight();
							}
						}).show();
	}

	public void clearController(BaseController controller) {
		if (controller != null) {
			controller.clear();
			controller = null;
		}
	}

	protected void outOfBalanceCancelLeft() {
		finish();
	}

	protected void outOfBalanceCancelRight() {
		finish();
	}

	public void seeProfile(final String targetUid) {
		if (targetUid.equals(Account.getUserId())) {
			return;
		}

		if (ConstData.sIsAppFree) {
			Intent intent = new Intent();
			intent.setClass(mContext, ProfileActivity.class);
			intent.putExtra("uid", targetUid);
			startActivity(intent);
		} else {
			showNeedMoneyDialog(
					getString(R.string.hint_see_profile_need_money),
					new RightButtonListener() {

						@Override
						public void onRightButtonClick() {
							Intent intent = new Intent();
							intent.setClass(mContext, ProfileActivity.class);
							intent.putExtra("uid", targetUid);
							startActivity(intent);
						}
					});
		}
	}

	public void sendMessage(String targetId) {
		sendMessage(targetId, false);
	}

	public void sendMessage(final String targetUid, final boolean isContract) {
		if (targetUid.equals(Account.getUserId())) {
			toast(getString(R.string.hint_cannot_send_msg_self));
			return;
		}

		if (ConstData.sIsAppFree || targetUid.equals(Account.getUserId())) {
			Intent intent = new Intent();
			intent.setClass(mContext, ChatActivity.class);
			intent.putExtra("uid", targetUid);
			intent.putExtra("isFromContract", isContract);
			startActivity(intent);
		} else {
			showNeedMoneyDialog(getString(R.string.hint_send_msg_need_money),
					new RightButtonListener() {

						@Override
						public void onRightButtonClick() {
							Intent intent = new Intent();
							intent.setClass(mContext, ChatActivity.class);
							intent.putExtra("uid", targetUid);
							intent.putExtra("isFromContract", isContract);
							startActivity(intent);
						}
					});
		}
	}

	public void showNeedMoneyDialog(String hint, RightButtonListener l) {
		if (ConstData.sIsAppFree) {
			l.onRightButtonClick();
		} else {
			new ZDialog(mContext).setContent(hint)
					.setLeftButton(getString(R.string.cancel))
					.setRightButton(getString(R.string.OK), l).show();
		}
	}

	protected void loadAdView() {
		try {
			// AdView adView = (AdView) findViewById(R.id.adView);
			// adView.loadAd(new AdRequest.Builder().build());

	/*		NendAdView nendAdView = (NendAdView) findViewById(R.id.nend);
			nendAdView.setListener(new NendAdListener() {

				// 受信エラー通知
				public void onFailedToReceiveAd(NendAdView nendAdView) {
					Toast.makeText(getApplicationContext(),
							"onFailedToReceiveAd", Toast.LENGTH_LONG).show();
				}

				// 受信成功通知
				public void onReceiveAd(NendAdView nendAdView) {
					Toast.makeText(getApplicationContext(), "onReceiveAd",
							Toast.LENGTH_LONG).show();
				}

				// クリック通知
				public void onClick(NendAdView nendAdView) {
					Toast.makeText(getApplicationContext(), "onClick",
							Toast.LENGTH_LONG).show();
				}

				// 復帰通知
				public void onDismissScreen(NendAdView nendAdView) {
					Toast.makeText(getApplicationContext(), "onDismissScreen",
							Toast.LENGTH_LONG).show();
				}

			});
			nendAdView.loadAd();*/
			// AdRequest.Builder builder;
			//
			// IMobileExtras extras = new IMobileExtras();
			// builder.addNetworkExtras(new IMobileExtras());
			//
			// mAdView.loadAd(adRequest);
		} catch (Exception e) {
		}
	}
}