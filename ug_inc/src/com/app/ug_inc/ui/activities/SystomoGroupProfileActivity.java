package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Group;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoGroupProfileActivity extends BaseNetActivity {

    private GroupRemovedReceiver mGroupRemovedReceiver;

    private TextView mNameTv;
    private ImageView mPicIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo_group_profile);

        findViewById(R.id.btn_systomo_group_profile_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Group group = (Group) getIntent().getSerializableExtra("group");

        mPicIv = (ImageView) findViewById(R.id.iv_systomo_group_profile_picture);
        ZImageLoader.asyncLoadImage(group.getPic(), mPicIv);

        mNameTv = (TextView) findViewById(R.id.tv_systomo_group_profile_name);
        mNameTv.setText("" + group.getName());

        findViewById(R.id.llayout_systom_group_profile_talk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, ChatActivity.class);
                intent.putExtra("title", group.getName() + " (" + group.getMemSum() + ")");
                intent.putExtra("gid", group.getId());
                startActivity(intent);
            }
        });
        findViewById(R.id.llayout_systom_group_profile_member).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, SystomoCreateGroupActivity.class);
                intent.putExtra("group", group);
                startActivityForResult(intent, 1);
            }
        });

        mGroupRemovedReceiver = new GroupRemovedReceiver();
        mGroupRemovedReceiver.register();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGroupRemovedReceiver.unregister();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String pic = data.getStringExtra("pic");
            String name = data.getStringExtra("name");

            mNameTv.setText("" + name);
            ZImageLoader.asyncLoadImage(pic, mPicIv);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class GroupRemovedReceiver extends BroadcastReceiver {

        public void register() {
            IntentFilter filter = new IntentFilter(SystomoCreateGroupActivity.ACTION_GROUP_MODIFIED);
            registerReceiver(this, filter);
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }
}