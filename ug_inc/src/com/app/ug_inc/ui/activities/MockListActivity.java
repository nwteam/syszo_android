package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Mock;
import com.app.ug_inc.models.User;
import com.app.ug_inc.models.Workplace;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MockListActivity extends BaseListNetActivity {
	private ListAdapter mAdapter;
	private PullListview mListview;

	private ArrayList<Mock> mMockList;

	private MockController mMockController;

	private Workplace mWorkplace;

	private Mock mNewMock;

	private EditText mCommentEt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mock_list);

		makeTitle("現場掲示板");
		enableBack();

		mWorkplace = (Workplace) getIntent().getSerializableExtra("workp");
		makeTitle("" + mWorkplace.getName());

		mCommentEt = (EditText) findViewById(R.id.et_mock_list_comment);

		mMockList = new ArrayList<Mock>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.plv_mock_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mMockController.getMockListByWorkplace(mWorkplace.getId(), mCurrPage + 1);
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mMockController = new MockController(this);
		mMockController.getMockListByWorkplace(mWorkplace.getId(), ConstData.PAGE_FIRST);
		mLoadingDialog.show();
		mCurrStatus = STATUS.REFRESH;

		// test();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMockController != null) {
			mMockController.clear();
			mMockController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_MOCK_LIST_BY_WORKPLACE.IF_ID) {
			List<Mock> list = (ArrayList<Mock>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mMockList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}

			mMockList.addAll(list);
			mAdapter.notifyDataSetChanged();
		} else if (ifId == InterfaceIds.COMMENT_MOCK.IF_ID) {
			toast(getString(R.string.success_comment));
			mMockList.add(mNewMock);
			mAdapter.notifyDataSetChanged();

			mCommentEt.setText("");
		}
	}

	public void submitReply(View v) {
		String content = mCommentEt.getText().toString();
		if (checkXX(TextUtils.isEmpty(content), "コメント内容")) {
			return;
		}

		mNewMock = new Mock();
		mNewMock.setContent(content);
		mNewMock.setTime(TimeUtil.getCurrentTime(TimeUtil.PATTERN_SEC));
		User user = new User();
		user.setId(Account.getUser().getId());
		user.setNick(Account.getUser().getNick());
		mNewMock.setUser(user);

		mMockController.commentMock(mWorkplace.getId(), content);
		mLoadingDialog.show();
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_MOCK; i++) {
			Mock mock = new Mock();
			mock.setId("" + i);
			mock.setTime("2014-12-31 12:00");
			mock.setContent("吐槽吐槽吐槽吐槽吐槽吐槽吐槽吐槽吐槽吐槽吐槽吐槽");

			User user = new User();
			user.setId("" + i);
			user.setNick("用户昵称" + i);
			mock.setUser(user);

			mMockList.add(mock);
		}

		mAdapter.notifyDataSetChanged();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mMockList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mMockList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_mock, null);
				h.uid = (TextView) arg1.findViewById(R.id.tv_item_list_mock_uid);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_mock_nickname);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_mock_time);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_mock_content);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			Mock mock = mMockList.get(arg0);
			final User usr = mock.getUser();

			h.uid.setText("" + usr.getId());
			h.nick.setText("" + usr.getNick());
			h.time.setText("" + mock.getTime());
			h.content.setText("" + mock.getContent());
			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(usr.getId());
				}
			});

			return arg1;
		}

		private class Holder {
			private TextView uid;
			private TextView nick;
			private TextView time;
			private TextView content;
		}
	}
}