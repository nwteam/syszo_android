package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetParams;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.ProfileDialog;
import com.app.ug_inc.utils.ConstData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;



import java.util.Set;

import net.nend.android.NendAdView;

/*
*
 * 个人主页
 *
 * @param REQUEST_NICK          マイネーム
 * @param REQUEST_SEX           性别
 * @param REQUEST_ADDRESS       現在地
 * @param REQUEST_JOB           業種
 * @param REQUEST_COMPANY_SCALE 会社規模
 * @param REQUEST_POSITION      役職・役割
 * @param REQUEST_BUDGETSCALE   予算規模
 * @param REQUEST_HISTORY       シス蔵
 * @param REQUEST_SOFTWARE      ソフトウェア
 * @param REQUEST_HARDWARE      ハードウェア
 * @param REQUEST_CERTIFICATION 保有資格
 * @param REQUEST_ANNUALSALARY  年間予算
 * @param REQUEST_FRIENDS       シストモ
 * @param REQUEST_FAVOUR        いいね
 * @param REQUEST_SELF_INTRO    自己紹介
 * @Author samson
*/
public class ProfileActivity extends BaseNetActivity {
    private static final int RIGHT_SECOND_INDEX = 0xFF76BE9A;
    public static final int REQUEST_NICK = 10000;
    public static final int REQUEST_SEX = 1001;
    public static final int REQUEST_ADDRESS = 1002;
    public static final int REQUEST_JOB = 1003;
    public static final int REQUEST_COMPANY_SCALE = 1004;
    public static final int REQUEST_POSITION = 1005;
    public static final int REQUEST_BUDGETSCALE = 1006;
    public static final int REQUEST_HISTORY = 1007;
    public static final int REQUEST_SOFTWARE = 1008;
    public static final int REQUEST_HARDWARE = 1009;
    public static final int REQUEST_CERTIFICATION = 1010;
    public static final int REQUEST_ANNUALSALARY = 1011;
    public static final int REQUEST_FRIENDS = 1012;
    public static final int REQUEST_FAVOUR = 1013;
    public static final int REQUEST_SELF_INTRO = 1014;
    private TextView mTimeTv;
    private TextView mCodeTv;
    private ImageView mPieIv;
    private TextView mNicknameTv;
    private TextView mEmailTv;
    private TextView mSexTv;
    private TextView mAddressTv;
    private TextView mJobTv;
    private TextView mCompanyScaleTv;
    private TextView mPositionTv;
    private TextView mBudgetScaleTv;
    private TextView mHistoryTv;
    private TextView mSoftwareTv;
    private TextView mHardwareTv;
    private TextView mCertificationTv;
//    private TextView mAnnualSalaryTv;
    private TextView mFriendsTv;
    private TextView mFavourTv;
    private TextView mSelfIntroductionTv;
    private AccountController mAccountController;
    private Bundle mBundle = null;
    private User mUser;
    private String mInitUid;
    private Context mContext;
//    private AdView mAdView;
//    private NendAdView mNendAdView;

    private ProfileChangedReceiver mProfileChangedReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mContext = ProfileActivity.this;
        initView();

        mProfileChangedReceiver = new ProfileChangedReceiver();
        mProfileChangedReceiver.register();
    }

    private void initView() {
        makeTitle("マイページ");
        enableBack();
        makeIcon(R.drawable.micon_systomo);
        mTimeTv = (TextView) findViewById(R.id.tv_profile_last_update);
        mCodeTv = (TextView) findViewById(R.id.tv_profile_completeness);
        mPieIv = (ImageView) findViewById(R.id.tv_hint_profile_showing);
        mNicknameTv = (TextView) findViewById(R.id.tv_profile_nickname);
        mEmailTv = (TextView) findViewById(R.id.tv_profile_email);
        mSexTv = (TextView) findViewById(R.id.tv_profile_sex);
        mAddressTv = (TextView) findViewById(R.id.tv_profile_address);
        mJobTv = (TextView) findViewById(R.id.tv_profile_job);
        mCompanyScaleTv = (TextView) findViewById(R.id.tv_profile_company_scale);
        mPositionTv = (TextView) findViewById(R.id.tv_profile_position);
        mBudgetScaleTv = (TextView) findViewById(R.id.tv_profile_budget_scale);
        mHistoryTv = (TextView) findViewById(R.id.tv_profile_history);
        mSoftwareTv = (TextView) findViewById(R.id.tv_profile_software);
        mHardwareTv = (TextView) findViewById(R.id.tv_profile_hardware);
        mCertificationTv = (TextView) findViewById(R.id.tv_profile_certification);
//        mAnnualSalaryTv = (TextView) findViewById(R.id.tv_profile_annual_salary);
        mFriendsTv = (TextView) findViewById(R.id.tv_profile_friends);
        mFavourTv = (TextView) findViewById(R.id.tv_profile_favour);
        mSelfIntroductionTv = (TextView) findViewById(R.id.tv_profile_self_introduction);
        addRightAction(R.drawable.ic_btn_mypost, new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, ArticleHistoryListActivity.class);
                startActivity(intent);
            }
        });
        addMoreRightAction(R.drawable.ic_btn_acbar_setting, RIGHT_SECOND_INDEX, new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SettingActivity.class);
                startActivity(intent);
            }
        });
//        mAdView = (AdView) findViewById(R.id.adView);
//        mAdView.loadAd(new AdRequest.Builder().build());
       /*
        mNendAdView = (NendAdView)findViewById(R.id.nend);
        mNendAdView.loadAd();*/
        
        mAccountController = new AccountController(this);
        mAccountController.getProfile(Account.getUserId());
        mLoadingDialog.show();

        findViewById(R.id.rlayout_profile_complete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    private void setParam() {
        mTimeTv.setText("最終更新：" + mUser.getTime());
        mCodeTv.setText(mUser.getScale() + "");
        mNicknameTv.setText(mUser.getNick() + "");
        mEmailTv.setText(mUser.getEmail()+"");
        mSexTv.setText(ConstData.getSexById(mUser.getSex()) + "");
        mAddressTv.setText(mUser.getCity() + "");
        mJobTv.setText(mUser.getIndustry() + "");
        mCompanyScaleTv.setText(mUser.getSizeCompany() + "");
        mPositionTv.setText(mUser.getJob() + "");
        mBudgetScaleTv.setText(mUser.getRegMoney() + "");
        mHistoryTv.setText(mUser.getCalendar() + "");
        mSoftwareTv.setText(mUser.getSoftware() + "");
        mHardwareTv.setText(mUser.getHardware() + "");
        mCertificationTv.setText(mUser.getCertifaction() + "");
//        mAnnualSalaryTv.setText(mUser.getAllyear() + "");
        mFriendsTv.setText(mUser.getFriends() + "");
        mFavourTv.setText(mUser.getFavour() + "");
        mSelfIntroductionTv.setText(mUser.getSelfIntro() + "");
    }

    private void showDialog() {
        ProfileDialog mProfileDialog = new ProfileDialog(this, "完成度を高めると、シストモで検索されやすくなったり新しいつながりがたくさん増えるよ！");
        mProfileDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mAccountController);

        if (mProfileChangedReceiver != null) {
            mProfileChangedReceiver.unregister();
            mProfileChangedReceiver = null;
        }
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.USER_GET_PROFILE.IF_ID) {
            mUser = (User) obj.getResultObject();
            setAccount();
            setParam();
        }
        mLoadingDialog.dismiss();
    }

    @Override
    public void onRequestError(int ifId, String errMsg) {
        super.onRequestError(ifId, errMsg);
        mLoadingDialog.dismiss();
    }

    private void setAccount() {
        User user = Account.getUser();
        user.setId(Account.getUserId());
        user.setNick(mUser.getNick() + "");
        user.setSex(mUser.getSex() + "");
        user.setCity(mUser.getCity() + "");
        user.setIndustry(mUser.getIndustry() + "");
        user.setSizeCompany(mUser.getSizeCompany() + "");
        user.setJob(mUser.getJob() + "");
        user.setRegMoney(mUser.getRegMoney() + "");
        user.setCalendar(mUser.getCalendar() + "");
        user.setSoftware(mUser.getSoftware() + "");
        user.setHardware(mUser.getHardware() + "");
        user.setCertifaction(mUser.getCertifaction() + "");
        user.setAllyear(mUser.getAllyear() + "");
        user.setFriends(mUser.getFriends() + "");
        user.setFavour(mUser.getFavour() + "");
        user.setTime(mUser.getTime() + "");
        user.setSelfIntro(mUser.getSelfIntro() + "");
        user.setScale(mUser.getScale() + "");
    }

    private class ProfileChangedReceiver extends BroadcastReceiver {

        public void register() {
            registerReceiver(this, new IntentFilter(ProfileSettingActivity.PROFILE_NEED_REFRESH));
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mAccountController.getProfile(Account.getUserId());
        }
    }
}