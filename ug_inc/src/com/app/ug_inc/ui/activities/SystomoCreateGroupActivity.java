package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Group;
import com.app.ug_inc.models.Message;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.GroupController;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.utils.FileUtils;
import com.app.ug_inc.utils.Utils;

import java.lang.reflect.Array;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;


import static com.app.ug_inc.R.id.et_systomo_create_group_name;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoCreateGroupActivity extends BaseNetActivity {
    public static final String ACTION_GROUP_MODIFIED = "action.ACTION_GROUP_MODIFIED";

    public static final String ACTION_GROUP_CREATE_NEED_FINISH = "action.ACTION_GROUP_CREATE_NEED_FINISH";

    private ImageView mAvatarIv;
    private EditText mNameEt;

    private TextView mHintCenter;

    private View mAddLayout;

    private ListAdapter mAdapter;

    private ListView mListView;

    private List<User> mUserList;

    private View mSaveBtn;
    private View mQuitBtn;

    private Group mGroup;

    private GroupController mGroupController;

    private boolean isHavePicture = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo_create_group);

        makeTitle("シストモ作成");
        makeIcon(R.drawable.micon_systomo);
        enableBack();

        mSaveBtn = addRightAction(R.drawable.ic_btn_acbar_save, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                save();
            }
        });

        mQuitBtn = findViewById(R.id.btn_systomo_create_group_quit);
        mQuitBtn.setVisibility(View.GONE);
        mQuitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quit();
            }
        });

        mAvatarIv = (ImageView) findViewById(R.id.iv_systomo_create_group_avatar);
        mAvatarIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture(v);
            }
        });
        mNameEt = (EditText) findViewById(et_systomo_create_group_name);

        mAddLayout = findViewById(R.id.llayout_systomo_create_group_add);
        mAddLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMember();
            }
        });

        mUserList = new ArrayList<User>();

        mAdapter = new ListAdapter();
        mListView = (ListView) findViewById(R.id.lv_systomo_create_group_member);
        mListView.setAdapter(mAdapter);

        mGroupController = new GroupController(this);

        try {
            mGroup = (Group) getIntent().getSerializableExtra("group");
            mGroupController.getGroup(mGroup.getId());
            mLoadingDialog.show();
            makeEdit();
        } catch (NullPointerException e) {
            makeAdd();
        }

        mhandler = new Handler(getMainLooper()){
            @Override
            public void handleMessage(android.os.Message msg) {
                super.handleMessage(msg);
                isHavePicture = false;
                mUploadedPicUrl = null;
                deletePicture();
            }
        };
    }

    private void makeAdd() {
        TextView editTv = (TextView) findViewById(R.id.tv_systomo_create_group_add);
        editTv.setText("追加");
    }

    private void makeEdit() {
        TextView editTv = (TextView) findViewById(R.id.tv_systomo_create_group_add);
        mQuitBtn.setVisibility(View.VISIBLE);
        editTv.setText("編集");
        setGroup(true);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

        if (ifId == InterfaceIds.GET_GROUP.IF_ID) {
            String id = mGroup.getId(); // 没反id
            String pic = mGroup.getPic(); // 也特码没反pic
            mGroup = (Group) obj.getResultObject();
            mGroup.setId(id);
            mGroup.setPic(pic);
            setGroup(true);

            mUserList = mGroup.getMemberList();
            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.UPLOAD_PIC.IF_ID) {
            toast("アップロードしました");
            mUploadedPicUrl = (String) obj.getResultObject();
            if(mUploadedPicUrl.isEmpty() || mUploadedPicUrl == null){
                isHavePicture = false;
            }else {
                isHavePicture = true;
            }
            ZImageLoader.asyncLoadImage(mUploadedPicUrl, mAvatarIv);
        } else if (ifId == InterfaceIds.QUIT_GROUP.IF_ID) {
            sendBroadcast(new Intent(ACTION_GROUP_MODIFIED));
            toast("退会しました");
            finish();
        } else if (ifId == InterfaceIds.ADD_SAVE_GROUP.IF_ID) {
            sendBroadcast(new Intent(ACTION_GROUP_MODIFIED));
            sendBroadcast(new Intent(ACTION_GROUP_CREATE_NEED_FINISH));
            toast("保存しました");
            finish();
        } else if (ifId == InterfaceIds.MODIFY_GROUP.IF_ID) {
            sendBroadcast(new Intent(ACTION_GROUP_MODIFIED));
            toast("保存しました");
            Intent intent = new Intent();
            intent.putExtra("name", mGroup.getName());
            intent.putExtra("pic", mGroup.getPic());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void quit() {
        new AlertDialog.Builder(mContext).setMessage("退会確認しますか？")
                .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mGroupController.quitGroup(mGroup.getId());
                        mLoadingDialog.show();
                        mLoadingDialog.setCancelable(true);
                    }
                }).setNegativeButton(getString(R.string.cancel), null).show();
    }

    private void setGroup(boolean other) {
        if (mGroup == null) {
            return;
        }

        if(mGroup.getPic().isEmpty() || mGroup.getPic() == null){
            isHavePicture = false;
            deletePicture();
        }else{
            isHavePicture = true;
            ZImageLoader.asyncLoadImage(mGroup.getPic(), mAvatarIv);
        }

//        ZImageLoader.asyncLoadImage(mGroup.getPic(), mAvatarIv);
        mNameEt.setText("" + mGroup.getName());
        if (other) {
            mQuitBtn.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_systomo_create_group_add)).setText("編集");
        } else {
            mQuitBtn.setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_systomo_create_group_add)).setText("追加");
        }
    }

    private void save() {
        String groupName = mNameEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(groupName), "グループ名")) {
            return;
        }

        List<String> list = new ArrayList<String>();
        for (User user : mUserList) {
            list.add(user.getId());
        }

        if (list.size() == 0) {
            toast("メンバーを選択してください");
            return;
        }

        String gourpId = mGroup == null ? "" : mGroup.getId();
        String avatar = mGroup == null ? mUploadedPicUrl : mGroup.getPic();;
        if(isHavePicture && mUploadedPicUrl != null){
            avatar = mUploadedPicUrl;
        }else if(!isHavePicture){
            avatar = null;
        }


        if (mGroup != null) {
            mGroupController.modifyGroup(mGroup.getId(), groupName, avatar, list);
        } else {
            mGroupController.addSavedGroup(groupName, gourpId, avatar, list);
        }

        mLoadingDialog.show();
    }

    private void getMember() {
        ArrayList<String> memList = new ArrayList<String>();
        for (User user : mUserList) {
            memList.add(user.getId());
        }

        Intent intent = new Intent();
        intent.setClass(mContext, MemberListActivity.class);
        intent.putExtra("groupId", mGroup == null ? "" : mGroup.getId());
        intent.putExtra("memberlist", memList);
        startActivityForResult(intent, 1);
    }

    private String mTmpPhotoPath;
    private String mUploadedPicUrl;
    private MySeriesController mMySeriesController;

    private void uploadPic() {
        if (!TextUtils.isEmpty(mTmpPhotoPath)) {
            if (mMySeriesController == null) {
                mMySeriesController = new MySeriesController(this);
            }
            mMySeriesController.uploadPic(mTmpPhotoPath);
            mLoadingDialog.show();
            mLoadingDialog.setCancelable(false);
        }
    }

    public  void deletePicture()
    {
        mAvatarIv.setImageResource(R.drawable.img_systomo_group_picture_default);
    }

    public void selectPicture(View v) {
        String[] hints;
        if(!isHavePicture) {

            hints = new String[]{getString(R.string.hint_upload_photo_take),
                    getString(R.string.hint_upload_photo_pick)};
        }else{
            hints = new String[]{getString(R.string.hint_upload_photo_take),
                    getString(R.string.hint_upload_photo_pick),getString(R.string.hint_upload_photo_delete)};
        }
        new AlertDialog.Builder(mContext).setItems(hints, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // take picture
                        try {
                            mTmpPhotoPath = Utils.takePhoto(SystomoCreateGroupActivity.this);
                        } catch (Exception e) {
                            toast(getString(R.string.hint_sdcard_not_mounted));
                        }
                        break;
                    case 1: // pick picture
                        Utils.pickPhoto(SystomoCreateGroupActivity.this);
                        break;
                    case 2:
                        mhandler.sendEmptyMessage(0);
                        break;
                    default:
                        break;
                }

                dialog.dismiss();
            }
        }).show();
    }

    Handler mhandler;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            mTmpPhotoPath = null;

            return;
        }

        switch (requestCode) {
            case 1:
                List<User> list = (List<User>) data.getSerializableExtra("list");
                for (User checkedUser : list) {
                    User user0 = findUserInList(checkedUser);
                    if ("1".equals(checkedUser.getIsFollowed())) { // 被选了
                        if (user0 == null) {
                            // 没有这个人就添加
                            mUserList.add(checkedUser);
                        } else {
                            // 有的话就什么都不做
                        }
                    } else {
                        if (user0 != null) {
                            // 有这个人的话就移除
                            mUserList.remove(user0);
                        } else {
                            // 没有的话就什么也不做
                        }
                    }
                }

                mAdapter.notifyDataSetChanged();
                break;
            case Utils.REQUEST_PICK_PHOTO:
                Uri photoUri = data.getData();
                mTmpPhotoPath = FileUtils.getPath(mContext, photoUri);

                uploadPic();
                break;
            case Utils.REQUEST_TAKE_PHOTO:
                uploadPic();
                break;

            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private User findUserInList(User target) {
        for (User user : mUserList) {
            if (target.getId().equals(user.getId())) {
                return user;
            }
        }
        return null;
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mUserList.size();
        }

        @Override
        public User getItem(int position) {
            return mUserList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h;
            if (convertView == null) {
                h = new Holder();

                convertView = mInflater.inflate(R.layout.item_list_member, null);
                h.check = (ImageView) convertView.findViewById(R.id.iv_item_list_member_check);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_member_name);
                h.profile = (ImageView) convertView.findViewById(R.id.iv_item_list_member_profile);
                h.arrow = (ImageView) convertView.findViewById(R.id.iv_item_list_member_arrow);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final User user = getItem(position);

            h.name.setText("" + user.getNick());
            h.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProfile(user.getId());
                }
            });
            h.arrow.setVisibility(View.INVISIBLE);

            return convertView;
        }

        private class Holder {
            private ImageView check;
            private TextView name;
            private ImageView profile;
            private ImageView arrow;
        }
    }
}
