package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends BaseNetActivity {
	private WebView mWebView;

	private String mUrl;

	private boolean mNeedBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);

		String title = getIntent().getStringExtra("title");
		makeTitle(title);
		enableBack();

		mNeedBack = getIntent().getBooleanExtra("needBack", true);

		mUrl = getIntent().getStringExtra("url");

		log("start loding url:" + mUrl);

		mWebView = (WebView) findViewById(R.id.webv_web);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebViewClient(new WebViewClient());
		mWebView.loadUrl(mUrl);
	}

	@Override
	public void onBackPressed() {
		if (mNeedBack && mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			super.onBackPressed();
		}
	}
}
