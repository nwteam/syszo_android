package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.Syst;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Article;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ArticleHistoryListActivity extends BaseListNetActivity {
    private ArrayList<Article> mArticleList;
    private View mErrorHintLayout;
    private PullListview mListview;
    private ListAdapter mAdapter;

    private AccountController mAccountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_his_list);
        initView();
    }

    private void initView() {
        makeTitle("投稿履歴");
        makeIcon(R.drawable.micon_systomo);
        enableBack();
        mErrorHintLayout = findViewById(R.id.llayout_chieb_list_error);
        mErrorHintLayout.setVisibility(View.GONE);
        mArticleList = new ArrayList<Article>();

        mAdapter = new ListAdapter();
        mListview = (PullListview) findViewById(R.id.plv_article_his_list);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Article article = mArticleList.get(arg2 - 1);
                Intent intent = new Intent();

                if ("1".equals(article.getType())) {
                    intent.setClass(mContext, ChiebDetailActivity2.class);
                    Chieb chieb = new Chieb();
                    chieb.setId(article.getId());
                    chieb.setTime(article.getTime());
                    chieb.setContent(article.getContent());
                    chieb.setUser(article.getUser());
                    chieb.setCommentCount(article.getCommentSum());
                    intent.putExtra("chieb", chieb);
                } else {
                    intent.setClass(mContext, SystDetailActivity.class);
                    Syst syst = new Syst();
                    syst.setId(article.getId());
                    syst.setTime(article.getTime());
                    syst.setContent(article.getContent());
                    syst.setUser(article.getUser());
                    intent.putExtra("syst", syst);
                }

                startActivity(intent);
            }
        });

        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mAccountController.getArticleHistoryList(ConstData.PAGE_FIRST);
                mCurrStatus = STATUS.REFRESH;
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mAccountController.getArticleHistoryList(mCurrPage + 1);
                mCurrStatus = STATUS.LOAD_MORE;
            }
        });

        mAccountController = new AccountController(this);
        mAccountController.getArticleHistoryList(ConstData.PAGE_FIRST);
        mLoadingDialog.show();
        mCurrStatus = STATUS.REFRESH;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAccountController != null) {
            mAccountController.clear();
            mAccountController = null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_ARTICLE_HISTORY_LIST.IF_ID) {
            List<Article> list = (ArrayList<Article>) obj.getResultObject();

            mListview.onRefreshComplete();

            if (mCurrStatus == STATUS.REFRESH) {
                mArticleList.clear();
                mCurrPage = ConstData.PAGE_FIRST;
            } else if (mCurrStatus == STATUS.LOAD_MORE) {
                judgeLoadMore(list);
            }

            mArticleList.addAll(list);
            mAdapter.notifyDataSetChanged();
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public void onRequestError(int ifId, String errMsg) {
        super.onRequestError(ifId, errMsg);
        mListview.onRefreshComplete();
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_ARTICLE_HIS; i++) {
            Article article = new Article();
            article.setId("" + (i + 1));
            article.setTime("标题" + i);
            article.setTime("2014-08-08");
            article.setCategory("分类" + i);
            article.setContent("内容内容内容内容内容内容内容" + i);

            mArticleList.add(article);
        }
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mArticleList.size();
        }

        @Override
        public Article getItem(int arg0) {
            return mArticleList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chieb, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_content);
                h.count = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_count);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_time);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_chieb_root);
                h.avatar = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_avatar);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            Article item = getItem(arg0);

            h.avatar.setImageResource(R.drawable.icon_comment_on);
            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getTitle());
            h.count.setText("" + item.getCommentSum());

            return arg1;
        }

        private class Holder {
            private ImageView avatar;
            private TextView content;
            private TextView time;
            private TextView count;
            private View bg;
        }
    }

}
