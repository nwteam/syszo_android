package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Article;
import com.app.ug_inc.models.ArticleComment;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MySeriesDetailActivity extends BaseListNetActivity {
	private ArrayList<ArticleComment> mCommentList;

	private PullListview mListView;

	private ListAdapter mAdapter;

	private MySeriesController mMySeriesController;

	private Article mArticle;

	private ArticleComment mNewComment;

	private EditText mCommentEt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myseries_detail);

		makeTitle("投稿詳細");
		enableBack();

		mArticle = (Article) getIntent().getExtras().getSerializable("article");

		mCommentEt = (EditText) findViewById(R.id.et_myseries_detail_comment);

		mCommentList = new ArrayList<ArticleComment>();

		mListView = (PullListview) findViewById(R.id.plv_myseries_list);

		View v = LayoutInflater.from(mContext).inflate(R.layout.list_header_myseries_detail, null);

		TextView content = (TextView) v.findViewById(R.id.tv_list_header_myseries_detail_content);
		content.setText(mArticle.getTitle() + "\n" + mArticle.getContent());

		String thumb = mArticle.getPicThumb();
		ImageView pic = (ImageView) v.findViewById(R.id.iv_list_header_myseries_detail_pic);
		if (TextUtils.isEmpty(thumb)) {
			pic.setVisibility(View.GONE);
		} else {
			ZImageLoader.asyncLoadImage(thumb, pic);
			pic.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(mContext, PicBrowseActivity.class);
					intent.putExtra("url", mArticle.getPic());
					intent.putExtra("thumb", mArticle.getPicThumb());
					startActivity(intent);
				}
			});
		}

		TextView time = (TextView) findViewById(R.id.tv_item_list_myseries_detail_time);
		time.setText("" + mArticle.getTime());
		User usr = mArticle.getUser();
		TextView nick = (TextView) findViewById(R.id.tv_item_list_myseries_detail_nickname);
		nick.setText("" + usr.getNick() + "【" + usr.getCity() + "】" + usr.getJob());
		mListView.getRefreshableView().addHeaderView(v, null, false);
		mListView.getRefreshableView().setHeaderDividersEnabled(false);

		mAdapter = new ListAdapter();
		mListView.setAdapter(mAdapter);
		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mMySeriesController.getMySeriesCommentList(mArticle.getId(), mCurrPage + 1);
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mMySeriesController = new MySeriesController(this);
		mMySeriesController.getMySeriesCommentList(mArticle.getId(), mCurrPage);
		mCurrStatus = STATUS.REFRESH;
		mLoadingDialog.show();

		// test();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_MY_SERIES_COMMENT_LIST.IF_ID) {
			List<ArticleComment> list = (ArrayList<ArticleComment>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mCommentList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListView.onRefreshComplete();
			}

			mCommentList.addAll(list);
			mAdapter.notifyDataSetChanged();
		} else if (ifId == InterfaceIds.COMMENT_MY_SERIES_ARTICLE.IF_ID) {
			toast(getString(R.string.success_comment));
			mCommentList.add(mNewComment);
			mCommentEt.setText("");
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mMySeriesController != null) {
			mMySeriesController.clear();
			mMySeriesController = null;
		}
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_MY_SERIES_COMMENT; i++) {
			ArticleComment comment = new ArticleComment();
			comment.setContent("回复回复" + i);
			comment.setTime("2013-02-02 12:00:00");
			User user = new User();
			user.setId("1");
			user.setNick("dadada");
			user.setCityId("1");
			comment.setUser(user);

			mCommentList.add(comment);
		}
		mAdapter.notifyDataSetChanged();
	}

	public void submitReply(View v) {
		String content = mCommentEt.getText().toString();

		if (checkXX(TextUtils.isEmpty(content), "コメント内容")) {
			return;
		}

		mNewComment = new ArticleComment();
		mNewComment.setContent(content);
		mNewComment.setTime(TimeUtil.getCurrentTime(TimeUtil.PATTERN_SEC));
		User user = new User();
		user.setId(Account.getUser().getId());
		user.setNick(Account.getUser().getNick());
		mNewComment.setUser(user);

		mMySeriesController.commentMySeriesArticle(mArticle.getId(), content);
		mLoadingDialog.show();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mCommentList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mCommentList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_myseries_detail, null);
				h.uid = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_detail_uid);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_detail_nickname);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_detail_time);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_detail_content);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			ArticleComment item = mCommentList.get(arg0);
			final User usr = item.getUser();

			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(usr.getId());
				}
			});

			h.uid.setText("" + usr.getId());
			h.nick.setText("" + usr.getNick());
			h.time.setText("" + item.getTime());
			// ZImageLoader.asyncLoadImage(item.getThumb(), h.image);
			h.content.setText("" + item.getContent());

			return arg1;
		}

		private class Holder {
			private TextView uid;
			private TextView nick;
			private TextView time;
			private TextView content;
		}
	}
}
