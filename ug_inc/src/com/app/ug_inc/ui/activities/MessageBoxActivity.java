package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.models.Message;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Conversation;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MessageController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.utils.ConstData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MessageBoxActivity extends BaseListNetActivity {
    private MessageController mMsgController;

    private ArrayList<Conversation> mConvList;
    private SparseArray<Boolean> mCheckedArray;

    private PullListview mListview;
    private MessageAdapter mAdapter;

    private ImageView mEditIv;

    private NewMsgReceiver mNewMsgReceiver;

    private class NewMsgReceiver extends BroadcastReceiver {

        public void register() {
            registerReceiver(this, new IntentFilter(ChatActivity.FILTER_NEW_MSG_RECEIVED));
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mMsgController.getConversationList(ConstData.PAGE_FIRST);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_box);

        makeTitle("メッセージ");
        makeIcon(R.drawable.micon_message);
        makeTitleBg(0xFFE7BC37);
        enableBack();

        mNewMsgReceiver = new NewMsgReceiver();
        mNewMsgReceiver.register();

//        mEditIv = new ImageView(mContext);
//        setEditBtn();
//        addRightAction(mEditIv);

        mConvList = new ArrayList<Conversation>();
        mCheckedArray = new SparseArray<Boolean>();

        mAdapter = new MessageAdapter();
        mListview = (PullListview) findViewById(R.id.plv_message_box);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
//                if (mAdapter.isEditMode()) {
//                    mCheckedArray.put(pos, !mCheckedArray.get(pos));
//                    mAdapter.notifyDataSetChanged();
//                } else {
                Conversation conv = mConvList.get(pos);

                Intent intent = new Intent();
                intent.setClass(mContext, ChatActivity.class);
                String gid = "", uid = "";
                if (conv.isGroup()) {
                    gid = conv.getId();
                } else {
                    uid = conv.getId();
                }
                intent.putExtra("gid", gid);
                intent.putExtra("uid", uid);
                intent.putExtra("title", conv.getName());

                startActivity(intent);
//                }
            }
        });
        mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//                disableEdit();
//
//                mCurrStatus = STATUS.LOAD_MORE;
//                mMsgController.getConversationList(mCurrPage + 1);
                mMsgController.getConversationList(ConstData.PAGE_FIRST);
            }
        });

        // test();
        mMsgController = new MessageController(this);
        mMsgController.getConversationList(ConstData.PAGE_FIRST);
        mLoadingDialog.show();
        mCurrStatus = STATUS.REFRESH;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNewMsgReceiver.unregister();

        clearController(mMsgController);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.MSG_GET_CONVERSATION_LIST.IF_ID) {
            mListview.onRefreshComplete();
            List<Conversation> list = (ArrayList<Conversation>) obj.getResultObject();
            if (mCurrStatus == STATUS.REFRESH) {
                mConvList.clear();
                mCurrPage = ConstData.PAGE_FIRST;
            } else if (mCurrStatus == STATUS.LOAD_MORE) {
                judgeLoadMore(list);
            }

            mConvList.addAll(list);

            mCheckedArray.clear();
            int size = mConvList.size();
            for (int i = 0; i < size; i++) {
                mCheckedArray.append(i, false);
            }

            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.MSG_DELETE_CONVERSATION.IF_ID) {
            toast(getString(R.string.success_delete_conversation));

            int count = 0;
            int size = mCheckedArray.size();
            for (int i = 0; i < size; i++) {
                if (mCheckedArray.get(i)) { // checked
                    mConvList.remove(i);
                    count++;
                }
            }

            mCheckedArray.clear();
            for (int i = 0; i < count; i++) { // reset checked
                mCheckedArray.append(i, false);
            }

            mAdapter.notifyDataSetChanged();

            disableEdit();
        }
    }

    private void setEditBtn() {
        mEditIv.setImageResource(R.drawable.ic_btn_acbar_edit);
        mEditIv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                enableEdit();
            }
        });
    }

    private void enableEdit() {
        mAdapter.makeEditMode(true);
//		mAcBarView.getLeftBtn().setImageResource(R.drawable.ic_btn_acbar_cancel);
//		mAcBarView.getLeftBtn().setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				disableEdit();
//			}
//		});

        mEditIv.setImageResource(R.drawable.ic_btn_acbar_delete);
        mEditIv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm))
                        .setLeftButton(getString(R.string.cancel))
                        .setRightButton(getString(R.string.OK), new RightButtonListener() {

                            @Override
                            public void onRightButtonClick() {
                                deleteMessages();
                            }
                        }).show();
            }
        });
    }

    private void disableEdit() {
        mAdapter.makeEditMode(false);
//		mAcBarView.getLeftBtn().setImageResource(R.drawable.ic_btn_acbar_back);
//		mAcBarView.getLeftBtn().setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				finish();
//			}
//		});
        setEditBtn();
    }

    private void deleteMessages() {
        String ids = "";
        int size = mCheckedArray.size();
        for (int i = 0; i < size; i++) {
            if (mCheckedArray.get(i)) {
                String id = mConvList.get(i).getId();
                ids += id;
                ids += ",";
            }
        }

        if (ids.length() == 0) {
            toast(getString(R.string.hint_at_least_select_one));
            return;
        }

        ids = ids.substring(0, ids.length() - 1); // remove last ','

        mMsgController.deleteConversation(ids);
        mLoadingDialog.show();
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_MESSAGE_BOX; i++) {
            Conversation msg = new Conversation();
            msg.setTime("2013-02-02 12:00:00");
            msg.setId("" + (i + 1));
            msg.setStatus("1");

            User from = new User();
            from.setNick("用户用户" + i);
            from.setCityId("3");
            msg.setFromUser(from);

            mConvList.add(msg);
        }

        mCheckedArray.clear();
        int size = mConvList.size();
        for (int i = 0; i < size; i++) {
            mCheckedArray.append(i, false);
        }
        mAdapter.notifyDataSetChanged();
    }

    private class MessageAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        private boolean mIsEditMode = false;

        public MessageAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        public void makeEditMode(boolean enabled) {
            mIsEditMode = enabled;

            if (mIsEditMode == false) {
                mCheckedArray.clear();
                int size = mConvList.size();
                for (int i = 0; i < size; i++) {
                    mCheckedArray.append(i, false);
                }
            }

            notifyDataSetChanged();
        }

        public boolean isEditMode() {
            return mIsEditMode;
        }

        @Override
        public int getCount() {
            return mConvList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_message_box, null);
//                h.check = convertView.findViewById(R.id.llayout_item_list_message_box_check);
//                h.checkImg = (ImageView) convertView
//                        .findViewById(R.id.iv_item_list_message_box_check);
                h.time = (TextView) convertView.findViewById(R.id.tv_item_list_message_box_time);
                h.title = (TextView) convertView.findViewById(R.id.tv_item_list_message_box_uname);
                h.content = (TextView) convertView.findViewById(R.id.tv_item_list_message_box_content);
//                h.status = (ImageView) convertView
//                        .findViewById(R.id.iv_item_list_message_box_status);
                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

//            if (mIsEditMode) {
//                final int tmpPos = position;
//                if (mCheckedArray.valueAt(tmpPos)) { // was checked
//                    h.checkImg.setImageResource(R.drawable.ic_message_checked);
//                } else {
//                    h.checkImg.setImageResource(R.drawable.ic_message_normal);
//                }
//                h.check.setVisibility(View.VISIBLE);
//                h.check.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        mCheckedArray.put(tmpPos, !mCheckedArray.valueAt(tmpPos));
//                        notifyDataSetChanged();
//                    }
//                });
//            } else {
//                h.check.setVisibility(View.GONE);
//            }

            Conversation conv = mConvList.get(position);
            h.time.setText("" + conv.getTime());
            h.title.setText("" + conv.getName());
            h.content.setText("" + conv.getIntro());
//            String message = conv.getFromUser().getNick() + "("
//                    + ConstData.getCityById(conv.getFromUser().getCityId()) + ")";

//            String status = conv.getStatus();
//            if (status.equals("1")) {
//                h.status.setImageResource(R.drawable.ic_chat_unread);
//            } else if (status.equals("2")) {
//                h.status.setImageResource(R.drawable.ic_chat_read);
//            } else if (status.equals("3")) {
//                h.status.setImageResource(R.drawable.ic_chat_answered);
//            } else if (status.equals("4")) {
//                h.status.setImageResource(R.drawable.ic_chat_sent);
//            }

            return convertView;
        }

        private class Holder {
            public View check;
            public ImageView checkImg;
            public TextView time;
            public TextView title;
            public TextView content;
            public ImageView status;
        }
    }
}