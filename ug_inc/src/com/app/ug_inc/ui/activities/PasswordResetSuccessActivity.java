package com.app.ug_inc.ui.activities;

		import com.app.ug_inc.R;
		import android.os.Bundle;
		import android.view.View;

public class PasswordResetSuccessActivity extends BaseNetActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_forget_success);

		makeTitle("パスワード再発行");
		enableBack();
	}

	public void okReturn(View v) {
		finish();
	}
}
