package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.text.SimpleDateFormat;
import java.util.*;

public class ChiebListActivity extends BaseListNetActivity {
    private ListAdapter mAdapter;
    private PullListview mListview;

    private ArrayList<Chieb> mChiebList;

    private ClosureController mClosureController;

    private View mErrorHintLayout;

    private boolean mIsRequesting = false;

    private int mCurrTab = 1;
    private int mTmpTab = 1;

    private RefreshReceiver mRefreshReceiver;

    private class RefreshReceiver extends BroadcastReceiver {

        public void register() {
            IntentFilter filter = new IntentFilter(ChiebNewActivity.ACTION_CHIEB_NEED_REFRESH);
            registerReceiver(this, filter);
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            loadData(mCurrTab, false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_list);

        makeTitle("知恵袋");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_search, new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, ChiebSearchActivity.class));
            }
        });

        addRightAction(R.drawable.ic_btn_acbar_contribute, new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, ChiebNewActivity.class));
            }
        });

        makeTab();

        mErrorHintLayout = findViewById(R.id.llayout_chieb_list_error);
        mErrorHintLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, ChiebListUrgentActivity.class);
                startActivity(intent);
            }
        });
        mErrorHintLayout.setVisibility(View.GONE);

        mChiebList = new ArrayList<Chieb>();

        mAdapter = new ListAdapter();
        mListview = (PullListview) findViewById(R.id.plv_chieb_list);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListview.getRefreshableView().getHeaderViewsCount();
                Chieb chieb = mChiebList.get(pos);
                Intent intent = new Intent();
                intent.setClass(mContext, ChiebDetailActivity2.class);
//                intent.setClass(mContext, ChiebDetailActivity.class);
                intent.putExtra("chieb", chieb);
                startActivity(intent);
            }
        });
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mClosureController.getClosureList(getType(), ConstData.PAGE_FIRST);
                mCurrStatus = STATUS.REFRESH;
                mIsRequesting = true;
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mClosureController.getClosureList(getType(), mCurrPage + 1);
                mCurrStatus = STATUS.LOAD_MORE;
                mIsRequesting = true;
            }
        });
//        test();

        mClosureController = new ClosureController(this);
        mCurrTab = 1;// default 1
        loadData(mCurrTab, true);
        selectTab(mCurrTab);

//        test();

        if (mRefreshReceiver == null) {
            mRefreshReceiver = new RefreshReceiver();
            mRefreshReceiver.register();
        }
    }

    private Button mTab1, mTab2, mTab3;

    private void makeTab() {
        mTab1 = (Button) findViewById(R.id.btn_chieb_list_order_1);
        mTab1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(1, true);
            }
        });
        mTab2 = (Button) findViewById(R.id.btn_chieb_list_order_2);
        mTab2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(2, true);
            }
        });
        mTab3 = (Button) findViewById(R.id.btn_chieb_list_order_3);
        mTab3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(3, true);
            }
        });
    }

    private void loadData(int index, boolean needLoading) {
        if (mIsRequesting) {
            return;
        }

        String type = getType(index);
        mClosureController.getClosureList(type, ConstData.PAGE_FIRST);
        mCurrStatus = STATUS.REFRESH;
        if (needLoading) {
            mLoadingDialog.show();
        }

        mTmpTab = index;

        mIsRequesting = true;
    }

    private void selectTab(int index) {
        switch (index) {
            case 1:
                mTab1.setBackgroundResource(R.drawable.subnavi_left_pressed);
                mTab2.setBackgroundResource(R.drawable.subnavi_center);
                mTab3.setBackgroundResource(R.drawable.subnavi_right);
                break;
            case 2:
                mTab1.setBackgroundResource(R.drawable.subnavi_left);
                mTab2.setBackgroundResource(R.drawable.subnavi_center_pressed);
                mTab3.setBackgroundResource(R.drawable.subnavi_right);
                break;
            case 3:
                mTab1.setBackgroundResource(R.drawable.subnavi_left);
                mTab2.setBackgroundResource(R.drawable.subnavi_center);
                mTab3.setBackgroundResource(R.drawable.subnavi_right_pressed);
                break;
            default:
                mTab1.setBackgroundResource(R.drawable.subnavi_left);
                mTab2.setBackgroundResource(R.drawable.subnavi_center);
                mTab3.setBackgroundResource(R.drawable.subnavi_right);
                break;
        }
    }

    private String getType() {
        return getType(mCurrTab);
    }

    private String getType(int index) {
        switch (index) {
            case 1:
                return "1";
            case 2:
                return "2";
            case 3:
                return "3";
            case 4:
                return "4";
            default:
                return "1";
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mClosureController != null) {
            mClosureController.clear();
            mClosureController = null;
        }

        if (mRefreshReceiver != null) {
            mRefreshReceiver.unregister();
            mRefreshReceiver = null;
        }
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_CHIEB; i++) {
            Chieb c = new Chieb();
            c.setCommentCount("" + (int) (new Random().nextDouble() * 100));
            c.setTime("2014-12-31 12:00:00");
            c.setTitle("标题标题标题标题标题标题标题标题" + i);
            c.setContent("内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容");
            c.setUrgent(i % 4 == 0);

            User user = new User();
            user.setNick("用户" + i);
            c.setUser(user);

            mChiebList.add(c);
        }

        mChiebList.get(0).setTitle("ne\n\nn\n\n\nns\ns\nsidfas\n\n\n\n\nndsanonodiani\ngiofanfodas\nnfiodanrio3nto43\n\n\n\n\n\n\n\n\nfnodanfoir\n509344r3GWGTR\n\n\n\n\n534nt5409ig4wi0g\n");

        mAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        mIsRequesting = false;

        List<Chieb> list = (ArrayList<Chieb>) obj.getResultObject();

        mListview.onRefreshComplete();
        if (mCurrStatus == STATUS.REFRESH) {
            mCurrPage = ConstData.PAGE_FIRST;
            mChiebList.clear();

            mCurrTab = mTmpTab;
            selectTab(mCurrTab);



        } else if (mCurrStatus == STATUS.LOAD_MORE) {
            judgeLoadMore(list);
        }

        mChiebList.addAll(list);
//        System.out.println("mChiebList.size()="+mChiebList.size());


        mAdapter.notifyDataSetChanged();
        checkError();
    }

    private void checkError() {
        boolean show = false;

        for (Chieb chieb : mChiebList) {
            Log.d("TAG", chieb.getTitle() + ":" + chieb.isUrgent());
            if (chieb.isUrgent() && timeAndNowDiff36(chieb.getTime())) {
                show = true;
                break;
            }
        }

        if (show) {
            mErrorHintLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestError(int ifId, String errMsg) {
        super.onRequestError(ifId, errMsg);
        mIsRequesting = false;
        mListview.onRefreshComplete();
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mChiebList.size();
        }

        @Override
        public Chieb getItem(int arg0) {
            return mChiebList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chieb, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_content);
                h.count = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_count);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_time);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_chieb_root);
                h.avatar = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_avatar);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            Chieb item = getItem(arg0);
            if (item.isUrgent() && timeAndNowDiff36(item.getTime())) {
                h.bg.setBackgroundColor(RED);
                h.time.setTextColor(WHITE);
                h.count.setTextColor(WHITE);
                h.content.setTextColor(WHITE);
            } else {
                h.bg.setBackgroundColor(arg0 % 2 == 0 ? WHITE : 0xFFFAFAFA);
                h.time.setTextColor(0xFF393A40);
                h.content.setTextColor(0xFF393A40);

                if (item.isCommited()) {
                    h.count.setTextColor(0xFFF1A90F);
                    h.avatar.setImageResource(R.drawable.icon_comment_on);
                } else {
                    h.count.setTextColor(0xFFA7ACA9);
                    h.avatar.setImageResource(R.drawable.icon_comment_off);
                }

            }
            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getTitle());
            h.count.setText("" + item.getCommentCount());

            return arg1;
        }

        private class Holder {
            private ImageView avatar;
            private TextView content;
            private TextView time;
            private TextView count;
            private View bg;
        }

    }

    private boolean timeAndNowDiff36(String time1){
//        System.out.println("time1="+time1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        try
        {
            Date d1 = df.parse(time1);
            Date d2 = new Date(System.currentTimeMillis());
            long diff = d2.getTime() - d1.getTime();
            long days = diff / (1000 * 60 * 60);
//            System.out.println("days="+days);
            if(days <= 36 && days >=0){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }

    }
}
