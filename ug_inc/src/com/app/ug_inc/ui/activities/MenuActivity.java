package com.app.ug_inc.ui.activities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.push.ZJPushReceiver;
import com.app.ug_inc.utils.ZLog;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class MenuActivity extends BaseNetActivity {
    public static final String TAG = MenuActivity.class.getSimpleName();

    public static final int[] MENU_BG = new int[]{
            R.drawable.nav1,
            R.drawable.nav2,
            R.drawable.nav3,
            R.drawable.nav4,
            R.drawable.nav5,
    };

    public static final int[] MENU_BG_PRESSED = new int[]{
            R.drawable.nav1_on,
            R.drawable.nav2_on,
            R.drawable.nav3_on,
            R.drawable.nav4_on,
            R.drawable.nav5_on,
    };

    public static final int MENU_SIZE = MENU_BG.length;

    public static final String[] MENU_TITLES = new String[]{
            "Chiebukuro", "Systter", "Systomo", "Message", "My Page"
    };

    private static final List<Item> sMenuList = new ArrayList<Item>();

//    private MenuPageController mMenuPageController;
//    private AccountController mAccountController;
//    private SettingController mSettingController;

    private MenuAdapter mAdapter;

    private ImageView mMsgComingIv;

    private ZActivityManager mZAcitivityManager;

    private int mUnreadMsgCount = 0;

    private int mIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        makeTitle("SYSZO");

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("auto-login")) {
            Account.setAutoLogin(intent.getBooleanExtra("auto-login", false));
        }

        mMsgComingIv = new ImageView(mContext);
        mMsgComingIv.setImageResource(R.drawable.ic_btn_acbar_msg_coming);
        mMsgComingIv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, MessageBoxActivity.class);
                startActivity(intent);
            }
        });
        mMsgComingIv.setVisibility(View.INVISIBLE);

//		addActionItem(mMsgComingIv);

        for (int i = 0; i < MENU_SIZE; i++) {
            Item item = new Item();
            item.bgId = MENU_BG[i];
            item.title = MENU_TITLES[i];
            sMenuList.add(item);
        }

        mAdapter = new MenuAdapter();
        ListView listView = (ListView) findViewById(R.id.lv_menu);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mIndex = position;
                mAdapter.notifyDataSetChanged();


                Intent intent = new Intent();

                String title = sMenuList.get(position).title;
                if (title.equals(MENU_TITLES[0])) { // 知会袋
                    intent.setClass(mContext, ChiebListActivity.class);
                    startActivity(intent);
                } else if (title.equals(MENU_TITLES[1])) { // systter
                    intent.setClass(mContext, SystListActivity.class);
                    startActivity(intent);
                } else if (title.equals(MENU_TITLES[2])) { // systomo
                    intent.setClass(mContext, SystomoActivity.class);
                    startActivity(intent);
                } else if (title.equals(MENU_TITLES[3])) { // message
                    intent.setClass(mContext, MessageBoxActivity.class);
                    startActivity(intent);
                } else if (title.equals(MENU_TITLES[4])) { // my page
                    intent.setClass(mContext, ProfileActivity.class);
                    startActivity(intent);
                }
            }
        });

//        mMenuPageController = new MenuPageController(this);
//        getMenuList();
//
//        mAccountController = new AccountController(this);
//
//        mSettingController = new SettingController(null);
//        mSettingController.getAppIsFree();

        checkPushToken();

        mZAcitivityManager = ZActivityManager.getInstance();
        mZAcitivityManager.isAppRunning = true;

        loadAdView();
    }

    private void twitter() {
        String tweetUrl = "";
        String strMessage = "   ";
        String strHashTag = "#現パラ";
        // String strUrl = "http://androidstudio.hatenablog.com/";
        try {
            tweetUrl = "http://twitter.com/intent/tweet?text="
                    + URLEncoder.encode(strMessage, "UTF-8") + "+"
                    + URLEncoder.encode(strHashTag, "UTF-8");
            // + "&url="
            // + URLEncoder.encode(strUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strTweet));
        // startActivity(intent);

        Intent intent = new Intent();
        intent.setClass(mContext, WebActivity.class);
        intent.putExtra("url", tweetUrl);
        intent.putExtra("title", "twitter");
        startActivity(intent);
    }

    private void checkPushToken() {
//        String savedPushToken = SharedPreferenceHelper.getString("push_token");
        String savedPushToken = ZJPushReceiver.TOKEN;
        if (TextUtils.isEmpty(savedPushToken)) {
            return;
        }

        // String usrPushToken = Account.getUser().getPushToken();
        // saved is null or not equal
        // if (!TextUtils.isEmpty(usrPushToken) ||
        // !savedPushToken.equals(usrPushToken)) {
        final String pushToken = savedPushToken;

		/* 在“adRequest”中设置所有广告网络通用的参数。 */

        // Millennial Media额外参数。
        // MillennialAdapterExtras millennialExtras = new
        // MillennialAdapterExtras();
        // millennialExtras.setIncomeInUsDollars(65000);
        //
        // // InMobi额外参数。
        // InMobiAdapterExtras inMobiExtras = new InMobiAdapterExtras();
        // inMobiExtras.setIncome(65000);

        AccountController controller = new AccountController(new UIDelegate() {

            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                Account.setPushToken(pushToken);
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                ZLog.e(TAG, "reset jpush regid=" + pushToken + " not uploaded, reason:" + errMsg);
            }
        });
        controller.submitPushToken(pushToken);
        // }
    }

    private void getMenuList() {
        // test();
//        mMenuPageController.getMainPageList();
//        mLoadingDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mIndex = -1;
        mAdapter.notifyDataSetChanged();
//        mAccountController.getUnreadMsgCount();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sMenuList.clear();

//        if (mMenuPageController != null) {
//            mMenuPageController.clear();
//            mMenuPageController = null;
//        }
//
//        if (mAccountController != null) {
//            mAccountController.clear();
//            mAccountController = null;
//        }

        mZAcitivityManager.isAppRunning = false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_MENU_LIST.IF_ID) {
//            ArrayList<MenuItem> menus = (ArrayList<MenuItem>) obj.getResultObject();
//            Collections.sort(menus, new Comparator<MenuItem>() {
//
//                @Override
//                public int compare(MenuItem lhs, MenuItem rhs) {
//                    return lhs.getPriority().compareTo(rhs.getPriority());
//                }
//            });
//            // find & match menu item then make a new list
//            List<Item> items = new ArrayList<Item>();
//            for (MenuItem menu : menus) {
//                Item item = new Item();
//                for (int i = 0; i < MENU_SIZE; i++) {
//                    if (menu.getId().equals(MENU_IDS[i])) {
//                        item.bgId = (Integer) MENU_MAP.get(MENU_IDS[i]).get("bg");
//                        item.resId = (Integer) MENU_MAP.get(MENU_IDS[i]).get("item");
//                        item.title = (String) MENU_MAP.get(MENU_IDS[i]).get("title");
//                    }
//
//                    mMenuList.add(item);
//                }
//                items.add(item);
//            }
//
//            mMenuList.clear();
//            mMenuList.addAll(items);
//
//            // add twitter
//            Item twitItem = new Item();
//            twitItem.title = MENU_TITLES[10];
//            twitItem.bgId = R.drawable.ic_stripted_dot_fuck_twitter;
//            twitItem.resId = R.drawable.img_menu_item10;
//            mMenuList.add(twitItem);
//
//            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.GET_UNREAD_MSG_COUNT.IF_ID) {
            mUnreadMsgCount = (Integer) obj.getResultObject();
            makeUnreadMsg();
        }
    }

    private void makeUnreadMsg() {
        mMsgComingIv.setVisibility(mUnreadMsgCount > 0 ? View.VISIBLE : View.INVISIBLE);
    }

    private class Item {
        public int bgId;
        public int resId;
        public String title = "";
    }

    private class MenuAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private final RelativeLayout.LayoutParams IMG_PARAMS;

        public MenuAdapter() {
            mInflater = LayoutInflater.from(mContext);

            int w = getResources().getDisplayMetrics().widthPixels;
            int h = (int) ((double) w / (640.00 / 160.00));
            IMG_PARAMS = new RelativeLayout.LayoutParams(w, h);
        }

        @Override
        public int getCount() {
            return sMenuList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return sMenuList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @SuppressWarnings("deprecation")
        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_menu, null);
                h.bg = (ImageView) arg1.findViewById(R.id.iv_list_item_menu_bg);
                h.bg.setLayoutParams(IMG_PARAMS);
                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            Item item = sMenuList.get(arg0);
            Log.v("MENU_POSITION", "position=" + arg0);

            item.bgId = (arg0 == mIndex ? MENU_BG_PRESSED[arg0] : MENU_BG[arg0]);

            h.bg.setImageResource(item.bgId);
//			BitmapDrawable bg = (BitmapDrawable) getResources().getDrawable(item.bgId);
//			bg.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
//			h.bg.setBackgroundDrawable(bg);

            return arg1;
        }

        private class Holder {
            public ImageView bg;
        }
    }
}
