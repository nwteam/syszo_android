package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Toilet;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ToiletController;
import com.app.ug_inc.services.LocationService;
import com.app.ug_inc.utils.TimeUtil;
import com.app.ug_inc.utils.Utils;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ToiletListActivity extends BaseNetActivity {
	private ArrayList<Toilet> mToiletList;

	private ListAdapter mAdapter;

	private double mLat = -1, mLon = -1;

	private LocationReceiver mLocReceiver;

	private ToiletController mToiletController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_toilet_list);
		enableBack();

		makeTitle("便所一覧");

		mToiletList = new ArrayList<Toilet>();

		mAdapter = new ListAdapter();
		ListView lv = (ListView) findViewById(R.id.lv_toilet);
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent();
				intent.setClass(mContext, ToiletMapActivity.class);
				intent.putExtra("toilet", mToiletList.get(arg2));
				// intent.putExtra("lat", 35.689890);
				// intent.putExtra("lon", 139.692020);
				intent.putExtra("lat", mLat);
				intent.putExtra("lon", mLon);
				startActivity(intent);
			}
		});

		mToiletController = new ToiletController(this);
		// getToiletList();
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				startLocate();
			}
		}, 500);
	}

	private void startLocate() {
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		boolean netEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		if (!gpsEnabled || !netEnabled) {
			toast("「位置情報サービス」を有効にしてください。");
			return;
		}

		mLocReceiver = new LocationReceiver();
		mLocReceiver.register();

		Intent intent = new Intent();
		intent.setClass(mContext, LocationService.class);
		startService(intent);

		log("starting locate...");

		mLoadingDialog.show();
	}

	private void stopLocate() {
		if (mLocReceiver != null) {
			log("stop locate.");
			mLocReceiver.unregister();
			mLocReceiver = null;

			Intent intent = new Intent();
			intent.setClass(mContext, LocationService.class);
			stopService(intent);
		}
	}

	public void getToiletList() {
		String ll = mLat + "," + mLon;
		// String ll = "35.689890,139.692020";
		String time = TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE2);
		mToiletController.getNearByToilet(ll, time);
		mLoadingDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopLocate();

		if (mToiletController != null) {
			mToiletController.clear();
			mToiletController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);

		if (ifId == InterfaceIds.GET_NEARBY_TOILET_LIST.IF_ID) {
			mToiletList.clear();
			mToiletList.addAll((ArrayList<Toilet>) obj.getResultObject());
			mAdapter.notifyDataSetChanged();
		}
	}

	private class LocationReceiver extends BroadcastReceiver {

		public void register() {
			registerReceiver(this, new IntentFilter(LocationService.ACTION_LOCATION_COMPLETE));
		}

		public void unregister() {
			unregisterReceiver(this);
		}

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			if (arg1 == null) {
				return;
			}

			String result = arg1.getStringExtra("result");
			if (result.equals("success")) {
				mLat = arg1.getDoubleExtra("lat", -1);
				mLon = arg1.getDoubleExtra("lon", -1);

				stopLocate();

				getToiletList();
			} else if (result.equals("timeout")) {
				mLoadingDialog.dismiss();
				Utils.toast(mContext, "位置情報データを取得できません。");
			}
		}
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mToiletList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mToiletList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_toilet, null);
				h.name = (TextView) arg1.findViewById(R.id.tv_item_list_toilet_name);
				h.distance = (TextView) arg1.findViewById(R.id.tv_item_list_toilet_distance);
				h.address = (TextView) arg1.findViewById(R.id.tv_item_list_toilet_address);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			Toilet toilet = mToiletList.get(arg0);

			h.name.setText("" + toilet.getName());
			h.distance.setText("" + toilet.getDistance() + "m");
			h.address.setText("" + toilet.getAddress());

			return arg1;
		}

		private class Holder {
			private TextView name;
			private TextView distance;
			private TextView address;
		}
	}
}
