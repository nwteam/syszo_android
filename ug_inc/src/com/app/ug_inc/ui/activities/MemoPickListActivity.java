package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.ui.utils.DisplayUtil;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MemoPickListActivity extends BaseActivity {
	public static final String[] ITEM_NAME = new String[] { "未分類", "お気に入り",
			"腕がいい", "金払い悪い", "腕が悪い/使えない", "ブラックリスト", };
	private static final int LENGTH = ITEM_NAME.length;

	private ArrayList<SimpleKV> mList;
	private ListAdapter mAdapter;

	private int mIndex = 0; // default 0

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ListView lv = new ListView(mContext);
		lv.setBackgroundResource(R.drawable.img_bg_stripted_purple);
		lv.setDividerHeight(DisplayUtil.dp2px(mContext, 1.5f));
		lv.setDivider(new ColorDrawable(0xFFA00048));
		lv.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
		setContentView(lv);

		makeTitle("カテゴリ");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultOK();

			}
		});
		enableBack();

		Intent intent = getIntent();

		mList = new ArrayList<SimpleKV>();
		for (int i = 0; i < LENGTH; i++) {
			SimpleKV simpleKV = new SimpleKV();
			simpleKV.setKey("" + i);
			simpleKV.setValue(ITEM_NAME[i]);
			mList.add(simpleKV);
		}

		mAdapter = new ListAdapter();
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mIndex = arg2;

				mAdapter.notifyDataSetChanged();
			}
		});
	}

	private void resultOK() {
		Intent intent = new Intent();
		intent.putExtra("result", mList.get(mIndex).getKey());
		setResult(RESULT_OK, intent);
		finish();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_setting_pick, null);

				h.check = (ImageView) arg1
						.findViewById(R.id.iv_item_list_setting_pick);
				h.name = (TextView) arg1
						.findViewById(R.id.tv_item_list_settting_pick_name);
				h.root = arg1
						.findViewById(R.id.llayout_item_list_setting_pick_root);
				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			if (arg0 == mIndex) {
				h.check.setImageResource(R.drawable.ic_btn_list_checked);
			} else {
				h.check.setImageResource(R.drawable.ic_btn_list_normal);
			}

			h.name.setText("" + mList.get(arg0).getValue());

			if (arg0 == LENGTH - 1) { // last
				h.root.setBackgroundColor(0xFF0F0F0F);
				h.name.setTextColor(0xFFFF0000);
			} else {
				h.root.setBackgroundColor(0xFFE9E9E9);
				h.name.setTextColor(0xFF333333);
			}

			return arg1;
		}

		private class Holder {
			public TextView name;
			public ImageView check;
			public View root;
		}
	}
}
