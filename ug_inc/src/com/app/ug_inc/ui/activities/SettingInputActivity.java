package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class SettingInputActivity extends BaseActivity {
    private EditText mModifyEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_input);

        String text = getIntent().getExtras().getString("text");
        String title = getIntent().getExtras().getString("title");
        makeTitle(title);
        enableBack();

        mModifyEt = (EditText) findViewById(R.id.et_setting_input);
        mModifyEt.setText(text);

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultOK();
            }
        });
    }

    private void resultOK() {
        String text = mModifyEt.getText().toString();
        if ("".equals(text)) {
            return;
        } else {
            Intent intent = new Intent();
            intent.putExtra("result", text);
            setResult(RESULT_OK, intent);
        }
        finish();

    }
}
