package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.app.ug_inc.R;
import com.app.ug_inc.ui.utils.FontUtil;

public class WorkPMainActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workp_main);

		enableBack();
		makeTitle("現場の貸し借り");

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}
	}

	private final int[] TVIEW_IDS = { R.id.tv_workp_main, };

	public void freeWorker(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, WorkPPickActivity.class);
		intent.setAction(WorkPPickActivity.ACTION_WORKER);
		startActivity(intent);
	}

	public void freeWorkPlace(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, WorkPPickActivity.class);
		intent.setAction(WorkPPickActivity.ACTION_WORKPLACE);
		startActivity(intent);
	}
}
