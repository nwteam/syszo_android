package com.app.ug_inc.ui.activities;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.*;
import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.ZLog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import net.nend.android.NendAdView;

/**
 * 个人主页设置
 * <p/>
 * REQUEST_NICK          マイネーム
 * REQUEST_SEX           性别
 * REQUEST_ADDRESS       現在地
 * REQUEST_JOB           業種
 * REQUEST_COMPANY_SCALE 会社規模
 * REQUEST_POSITION      役職・役割
 * REQUEST_BUDGETSCALE   予算規模
 * REQUEST_HISTORY       シス蔵
 * REQUEST_SOFTWARE      ソフトウェア
 * REQUEST_HARDWARE      ハードウェア
 * REQUEST_CERTIFICATION 保有資格
 * REQUEST_ANNUALSALARY  年間予算
 * REQUEST_FRIENDS       シストモ
 * REQUEST_FAVOUR        いいね
 * REQUEST_SELF_INTRO    自己紹介
 *
 * @Author samson
 */
public class ProfileSettingActivity extends BaseNetActivity {
    public static final String PROFILE_NEED_REFRESH = "action.PROFILE_NEED_REFRESH";

    public static final int REQUEST_NICK = 10000;
    public static final int REQUEST_SEX = 1001;
    public static final int REQUEST_ADDRESS = 1002;
    public static final int REQUEST_INDUSTRY = 1003;
    public static final int REQUEST_JOB = 1004;
    public static final int REQUEST_COMPANY_SCALE = 1005;
    public static final int REQUEST_POSITION = 1006;
    public static final int REQUEST_BUDGETSCALE = 1007;
    public static final int REQUEST_HISTORY = 1008;
    public static final int REQUEST_SOFTWARE = 1009;
    public static final int REQUEST_HARDWARE = 10010;
    public static final int REQUEST_CERTIFICATION = 1011;
    public static final int REQUEST_ANNUALSALARY = 1012;
    public static final int REQUEST_SELF_INTRO = 1015;
    public  static final int REQUEST_EMAIL = 1016;

    private ScrollView parentScrollView;
    private ImageView mPieIv;
    private EditText mNicknameTv;
    private ImageView mVisibleIv;

    private  TextView mEmailTv;
    private TextView mSexTv;
    private TextView mAddressTv;
    private TextView mIndustryTv;
    private TextView mCompanyScaleTv;
    private TextView mJobTv;
    private TextView mBudgetScaleTv;
    private TextView mHistoryTv;
//    private TextView mSoftwareTv;
    private ImageView mSoftware_add;
    private ListView mSoftware_list;
    private ListSettingAdapter mSoftwareAdapter;
//    private TextView mHardwareTv;
    private ImageView mHardware_add;
    private ListView mHardware_list;
    private ListSettingAdapter mHardwareAdapter;
//    private TextView mCertificationTv;
    private  ImageView mCertification_add;
    private ListView mCertification_list;
    private ListSettingAdapter mCertificationAdapter;

//    private TextView mAnnualSalaryTv;
    private TextView mFriendsTv;
    private TextView mFavourTv;
    private TextView mSelfIntroductionTv;
    private Bundle mBundle = null;
    private AccountController mAccountController;
    private boolean isVisible = false;
    private User mClonedUser;
//    private AdView mAdView;
//    private NendAdView mNendAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);

        try {
            mClonedUser = Account.getCLonedUser();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        addRightAction(R.drawable.btn_save, new OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext).setMessage("保存しますか？").setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveProfile();
                    }
                }).setNegativeButton(getString(R.string.cancel), null).show();
            }
        });

        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mAccountController);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.USER_GET_PROFILE.IF_ID) {
            mClonedUser = (User) obj.getResultObject();
            mClonedUser.setId(Account.getUserId());
            setDatas();
        } else if (ifId == InterfaceIds.UPDATE_PROFILE.IF_ID) {
            toast(getString(R.string.success_profile_update));
            sendBroadcast(new Intent(PROFILE_NEED_REFRESH));
            finish();
        }
        mLoadingDialog.dismiss();
    }

    private void saveProfile() {
        mClonedUser.setNick(mNicknameTv.getText().toString());
        mAccountController.updateProfile(mClonedUser);
        mLoadingDialog.show();
    }

    private void initView() {
        makeTitle("プロフィール編集");
        enableBack();
        makeIcon(R.drawable.micon_systomo);

        parentScrollView = (ScrollView)findViewById(R.id.scrollView_id);

        mPieIv = (ImageView) findViewById(R.id.tv_hint_profile_showing);
        mNicknameTv = (EditText) findViewById(R.id.tv_profile_nickname);
        mVisibleIv = (ImageView) findViewById(R.id.iv_profile_visible);
        mVisibleIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("as","mNicknameTv.getText().toString()="+mNicknameTv.getText().toString());
                mClonedUser.setNick(mNicknameTv.getText().toString());
                Intent intent = new Intent();
                intent.setClass(mContext, ProfileMePrevActivity.class);
                intent.putExtra("user", mClonedUser);
                startActivity(intent);
            }
        });
        mEmailTv = (TextView) findViewById(R.id.tv_profile_email);
        mSexTv = (TextView) findViewById(R.id.tv_profile_sex);
        mAddressTv = (TextView) findViewById(R.id.tv_profile_address);
        mIndustryTv = (TextView) findViewById(R.id.tv_profile_job);
        mCompanyScaleTv = (TextView) findViewById(R.id.tv_profile_company_scale);
        mJobTv = (TextView) findViewById(R.id.tv_profile_position);
        mBudgetScaleTv = (TextView) findViewById(R.id.tv_profile_budget_scale);
        mHistoryTv = (TextView) findViewById(R.id.tv_profile_history);
//        mSoftwareTv = (TextView) findViewById(R.id.tv_profile_software);
        mSoftware_add = (ImageView)findViewById(R.id.iv_profile_software_add);
        mSoftware_list = (ListView)findViewById(R.id.lv_item_software_list);
//        mSoftware_list.setParentScrollView(parentScrollView);
//        mSoftware_list.setMaxHeight((int)getResources().getDimension(R.dimen.list_H));
        mSoftwareAdapter = new ListSettingAdapter();
        mSoftware_list.setAdapter(mSoftwareAdapter);
//        mHardwareTv = (TextView) findViewById(R.id.tv_profile_hardware);
        mHardware_add = (ImageView)findViewById(R.id.iv_profile_hardware_add);
        mHardware_list = (ListView)findViewById(R.id.lv_item_hardware_list);
//        mHardware_list.setParentScrollView(parentScrollView);
//        mHardware_list.setMaxHeight((int)getResources().getDimension(R.dimen.list_H));
        mHardwareAdapter = new ListSettingAdapter();
        mHardware_list.setAdapter(mHardwareAdapter);
//        mCertificationTv = (TextView) findViewById(R.id.tv_profile_certification);
        mCertification_add = (ImageView)findViewById(R.id.iv_profile_certification_add);
        mCertification_list = (ListView)findViewById(R.id.lv_item_list);
//        mCertification_list.setParentScrollView(parentScrollView);
//        mCertification_list.setMaxHeight((int)getResources().getDimension(R.dimen.list_H));
        mCertificationAdapter = new ListSettingAdapter();
        mCertification_list.setAdapter(mCertificationAdapter);

//        mAnnualSalaryTv = (TextView) findViewById(R.id.tv_profile_annual_salary);
        mFriendsTv = (TextView) findViewById(R.id.tv_profile_friends);
        mFavourTv = (TextView) findViewById(R.id.tv_profile_favour);
        mSelfIntroductionTv = (TextView) findViewById(R.id.tv_profile_self_introduction);

//        setDatas();
        setListener();
       /* mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());*/
//        mNendAdView = (NendAdView) findViewById(R.id.nend);
//        mNendAdView.loadAd();
        mAccountController = new AccountController(this);
        mAccountController.getProfile(Account.getUserId());
        mLoadingDialog.show();
    }

    private void setListener() {

       /* mNicknameTv.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                mClonedUser.setNick(mNicknameTv.getText().toString());
                return false;
            }
        });*/

        mEmailTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingEmailActivity.class);
                intent.putExtra("mEmail", mClonedUser.getEmail());
                startActivityForResult(intent, REQUEST_EMAIL);
            }
        });

        mSexTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity.class);
                intent.putExtra("title", "性別");
                intent.putExtra("array", ConstData.getSexList());
                startActivityForResult(intent, REQUEST_SEX);
            }
        });
        mAddressTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "現在地");
                intent.putExtra("array", "get_address");
                startActivityForResult(intent, REQUEST_ADDRESS);
            }
        });
        mIndustryTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("array", "get_industry");
                intent.putExtra("title", "業種");
                startActivityForResult(intent, REQUEST_INDUSTRY);
            }
        });
        mCompanyScaleTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("array", "get_size_company");
                intent.putExtra("title", "会社規模");
                startActivityForResult(intent, REQUEST_COMPANY_SCALE);
            }
        });
        mJobTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("array", "get_job");
                intent.putExtra("title", "役職・役割");
                startActivityForResult(intent, REQUEST_POSITION);
            }
        });
        mBudgetScaleTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "年間予算");
                intent.putExtra("array", "get_reg_money");
                startActivityForResult(intent, REQUEST_BUDGETSCALE);
            }
        });
        mHistoryTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "情シス歴");
                intent.putExtra("array", "get_use");
                startActivityForResult(intent, REQUEST_HISTORY);
            }
        });
        mSoftware_add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "ソフトウェア");
                intent.putExtra("array", "get_software");
                startActivityForResult(intent, REQUEST_SOFTWARE);
            }
        });
        mHardware_add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "ハードウェア");
                intent.putExtra("array", "get_hardware");
                startActivityForResult(intent, REQUEST_HARDWARE);
            }
        });
        /*mCertificationTv*/
        mCertification_add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("array", "get_qualified");
                intent.putExtra("title", "保有資格");
                startActivityForResult(intent, REQUEST_CERTIFICATION);
            }
        });
        /*mAnnualSalaryTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingPickListActivity3.class);
                intent.putExtra("title", "年間予算");
                intent.putExtra("array", "get_allyear");
                startActivityForResult(intent, REQUEST_ANNUALSALARY);
            }
        });*/
        mSelfIntroductionTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, SettingInputActivity.class);
                intent.putExtra("title", "自己紹介");
                intent.putExtra("text", mSelfIntroductionTv.getText());
                startActivityForResult(intent, REQUEST_SELF_INTRO);
            }
        });
//        mNicknameTv.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, SettingInputActivity.class);
//                intent.putExtra("title", "ニックネーム");
//                intent.putExtra("text", mNicknameTv.getText());
//                startActivityForResult(intent, REQUEST_NICK);
//            }
//        });
    }

    /**
     * 动态设置ListView的高度
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        if(listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_NICK:
                String resultNick = data.getStringExtra("result");
                mClonedUser.setNick(resultNick);
                break;
            case REQUEST_SEX:
                SimpleKV resultSex = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setSex(resultSex.getKey());
                break;
            case REQUEST_ADDRESS:
                SimpleKV resultAddress = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setCity(resultAddress.getValue());
                mClonedUser.setCityId(resultAddress.getKey());
                break;
            case REQUEST_INDUSTRY:
                SimpleKV resultIndustry = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setIndustry(resultIndustry.getValue());
                mClonedUser.setIndustryId(resultIndustry.getKey());
                break;
            case REQUEST_COMPANY_SCALE:
                SimpleKV resultCompanyScale = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setSizeCompany(resultCompanyScale.getValue());
                mClonedUser.setSizeCompanyId(resultCompanyScale.getKey());
                break;
            case REQUEST_POSITION:
                SimpleKV resultPosition = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setJobId(resultPosition.getKey());
                mClonedUser.setJob(resultPosition.getValue());
                break;
            case REQUEST_BUDGETSCALE:
                SimpleKV resultBudgetScale = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setRegMoney(resultBudgetScale.getValue());
                mClonedUser.setRegMoneyId(resultBudgetScale.getKey());
                break;
            case REQUEST_HISTORY:
                SimpleKV resultHistory = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setCalendar(resultHistory.getValue());
                mClonedUser.setCalendarId(resultHistory.getKey());
                break;
            case REQUEST_SOFTWARE:
                SimpleKV resultSoftware = (SimpleKV) data.getSerializableExtra("result");
               /* mClonedUser.setSoftware(resultSoftware.getValue());
                mClonedUser.setSoftwareId(resultSoftware.getKey());*/
                if(mClonedUser.appendSoftwareId(resultSoftware.getKey())){
                    mClonedUser.appendSoftware(resultSoftware.getValue());
                }
                break;
            case REQUEST_HARDWARE:
                SimpleKV resultHardware = (SimpleKV) data.getSerializableExtra("result");
               /* mClonedUser.setHardware(resultHardware.getValue());
                mClonedUser.setHardwareId(resultHardware.getKey());*/
                if(mClonedUser.appendHardwareId(resultHardware.getKey())){
                    mClonedUser.appendHardware(resultHardware.getValue());
                }
                break;
            case REQUEST_CERTIFICATION:
                SimpleKV resultCertif = (SimpleKV) data.getSerializableExtra("result");
                if(mClonedUser.appendCertifactionId(resultCertif.getKey())) {
                    mClonedUser.appendCertifaction(resultCertif.getValue());
                }
                break;
            case REQUEST_ANNUALSALARY:
                SimpleKV resultAnnualSalary = (SimpleKV) data.getSerializableExtra("result");
                mClonedUser.setAllyear(resultAnnualSalary.getValue());
                mClonedUser.setAllyearId(resultAnnualSalary.getKey());
                break;
            case REQUEST_SELF_INTRO:
                String resultSelfIntro = data.getStringExtra("result");
                mClonedUser.setSelfIntro(resultSelfIntro);
                break;
            case REQUEST_EMAIL:
                String resultEmail = data.getStringExtra("result");
                mClonedUser.setEmail(resultEmail);
                break;
            default:
                break;
        }
        setDatas();
    }

    private void setDatas() {
        mNicknameTv.setText("" + mClonedUser.getNick());
        mEmailTv.setText(""+mClonedUser.getEmail());
        mSexTv.setText("" + ConstData.getSexById(mClonedUser.getSex()));
        mAddressTv.setText("" + mClonedUser.getCity());
        mIndustryTv.setText(mClonedUser.getIndustry() + "");
        mCompanyScaleTv.setText(mClonedUser.getSizeCompany() + "");
        mJobTv.setText(mClonedUser.getJob());
        mBudgetScaleTv.setText(mClonedUser.getRegMoney() + "");
        mHistoryTv.setText(mClonedUser.getCalendar() + "");
//        mSoftwareTv.setText(mClonedUser.getSoftware() + "");
        String software[] = mClonedUser.getSoftware().split(",");
        if(software.length == 0 || mClonedUser.getSoftware().equals("") || mClonedUser.getSoftware().isEmpty()){
            mSoftware_list.setVisibility(View.GONE);
        }else
        {
            mSoftware_list.setVisibility(View.VISIBLE);
            ZLog.d("PROFILE", "mClonedUser.getSoftware()=" + mClonedUser.getSoftware() + "  mClonedUser.getSoftwareId()=" + mClonedUser.getSoftwareId());
            mSoftwareAdapter.setUserAndType(SOFTWARE);
            mSoftwareAdapter.notifyDataSetInvalidated();
            setListViewHeightBasedOnChildren(mSoftware_list);
        }
//        mHardwareTv.setText(mClonedUser.getHardware() + "");
        String hardware[] = mClonedUser.getHardware().split(",");
        if(hardware.length == 0 || mClonedUser.getHardware().equals("") || mClonedUser.getHardware().isEmpty()){
            mHardware_list.setVisibility(View.GONE);
        }else
        {
            mHardware_list.setVisibility(View.VISIBLE);
            ZLog.d("PROFILE", "mClonedUser.getHardware()=" + mClonedUser.getHardware() + "  mClonedUser.getHardwareId()=" + mClonedUser.getHardwareId());
            mHardwareAdapter.setUserAndType(HARDWARE);
            mHardwareAdapter.notifyDataSetInvalidated();
            setListViewHeightBasedOnChildren(mHardware_list);
        }
//        mCertificationTv.setText(mClonedUser.getCertifaction());

        String certifaction[] = mClonedUser.getCertifaction().split(",");
        if(certifaction.length == 0 || mClonedUser.getCertifaction().equals("") || mClonedUser.getCertifaction().isEmpty()){
            mCertification_list.setVisibility(View.GONE);
        }else
        {
            mCertification_list.setVisibility(View.VISIBLE);
            ZLog.d("PROFILE", "mClonedUser.getCertifaction()=" + mClonedUser.getCertifaction() + "  mClonedUser.getCertifactionId()=" + mClonedUser.getCertifactionId());
            mCertificationAdapter.setUserAndType(CERTIFICATION);
            mCertificationAdapter.notifyDataSetInvalidated();
            setListViewHeightBasedOnChildren(mCertification_list);
        }

//        mAnnualSalaryTv.setText(mClonedUser.getAllyear() + "");
        mFriendsTv.setText(mClonedUser.getFriends() + "");
        mFavourTv.setText(mClonedUser.getFavour() + "");
        mSelfIntroductionTv.setText("" + mClonedUser.getSelfIntro());

        ZLog.e("PROFILE", mClonedUser.toString());
    }

    public static final int CERTIFICATION = 0;
    public static final int SOFTWARE = 1;
    public static final int HARDWARE = 2;

    private class ListSettingAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        String[] titles;

        int iType;

       public void setUserAndType(int type)
       {
            iType = type;

            if(type == CERTIFICATION){
                if(mClonedUser != null && mClonedUser.getCertifaction() != null) {
                    titles = mClonedUser.getCertifaction().split(",");
                }
            }else if(type == HARDWARE){
                if(mClonedUser != null && mClonedUser.getHardware() != null){
                    titles = mClonedUser.getHardware().split(",");
                }
            }else if(type == SOFTWARE){
                if(mClonedUser != null && mClonedUser.getSoftware() != null){
                    titles = mClonedUser.getSoftware().split(",");
                }
            }
       }

        public String getIds(){
            if(iType == CERTIFICATION){
                return mClonedUser.getCertifactionId();
            }else if (iType == HARDWARE){
                return mClonedUser.getHardwareId();
            }else if (iType == SOFTWARE){
                return mClonedUser.getSoftwareId();
            }
            return null;
        }

        public void setTitleAndIDS(String Titles,String IDS){
            Log.d("ProgfileSetting","Titles="+titles+" IDS="+IDS);
            if(iType == CERTIFICATION){
                mClonedUser.setCertifaction(Titles);
                mClonedUser.setCertifactionId(IDS);
                if(Titles.isEmpty()){
                    mCertification_list.setVisibility(View.GONE);
                }else
                {
                    setListViewHeightBasedOnChildren(mCertification_list);
                }
            }else if(iType == HARDWARE){
                mClonedUser.setHardware(Titles);
                mClonedUser.setHardwareId(IDS);
                if(Titles.isEmpty()){
                    mHardware_list.setVisibility(View.GONE);
                }else{
                    setListViewHeightBasedOnChildren(mHardware_list);
                }
            }else if(iType == SOFTWARE){
                mClonedUser.setSoftware(Titles);
                mClonedUser.setSoftwareId(IDS);
                if(Titles.isEmpty()){
                    mSoftware_list.setVisibility(View.GONE);
                }else{
                    setListViewHeightBasedOnChildren(mSoftware_list);
                }
            }
        }

        public ListSettingAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            if(titles == null){
                return 0;
            }
            return titles.length;
        }

        @Override
        public Object getItem(int arg0) {
            if(titles == null){
                return null;
            }
            return titles[arg0];
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @SuppressWarnings("deprecation")
        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_setting_path, null);
                h.deleteprofile = (ImageView) arg1.findViewById(R.id.deleteprofile);
                h.titleName = (TextView)arg1.findViewById(R.id.tv_item_list_settting_name);
                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            ZLog.d("PROFILE", "list item="+getItem(arg0));
            h.titleName.setText((String) getItem(arg0));
            final int index = arg0;
            h.deleteprofile.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String titleIdst[] = getIds().split(",");
                    String titleAll1 = "";
                    String titleIds1 = "";
                    for (int i = 0;i < getCount();i++){
                        if(i != index){
                            if(titleIds1.equals("")){
                                titleAll1 += titles[i];
                                titleIds1 += titleIdst[i];
                            }else {
                                titleAll1 += "," + titles[i];
                                titleIds1 += "," + titleIdst[i];
                            }
                        }
                    }

                    titles = titleAll1.split(",");
                    setTitleAndIDS(titleAll1,titleIds1);
                    notifyDataSetInvalidated();

                }
            });

            return arg1;
        }

        private class Holder {
            public ImageView deleteprofile;
            public TextView  titleName;
        }
    }

}