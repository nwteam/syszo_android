package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Memo;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MemoEditActivity extends BaseNetActivity {
	public static final String ACTION_NEED_GET_MEMO = "action.need_get_memo";

	public static final int REQUEST_MODIFY_CATEGORY = 1;
	public static final int REQUEST_MODIFY_CONTENT = 2;

	private Memo mMemo;

	private TextView mNickTv;
	private TextView mContentTv;
	private TextView mCategoryTv;

	private AccountController mAccountController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memo_edit);

		makeTitle("メモ編集");
		enableBack();

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm_content_ok))
						.setLeftButton(getString(R.string.cancel))
						.setRightButton(getString(R.string.OK), new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								submit();
							}
						}).show();
			}
		});

		mNickTv = (TextView) findViewById(R.id.tv_memo_edit_nick);
		mContentTv = (TextView) findViewById(R.id.tv_memo_edit_content);
		mCategoryTv = (TextView) findViewById(R.id.tv_memo_edit_category);

		Intent intent = getIntent();
		mMemo = (Memo) intent.getExtras().getSerializable("memo");
		setDatas();

		mAccountController = new AccountController(this);
		try {
			if (intent.getAction().equals(ACTION_NEED_GET_MEMO)) {
				mAccountController.getMemo(mMemo.getTargetUser().getId());
				mLoadingDialog.show();
			}
		} catch (NullPointerException e) {
			// action == null
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	private void setDatas() {
		mNickTv.setText("" + mMemo.getTargetUser().getNick());
		mContentTv.setText("" + mMemo.getContent());
		mCategoryTv.setText("" + getCategory());
	}

	private String getCategory() {
		int id = 0;
		try {
			id = Integer.valueOf(mMemo.getCategory());
		} catch (NumberFormatException e) {
		}

		return MemoPickListActivity.ITEM_NAME[id];
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.MODIFY_MEMO.IF_ID) {
			toast(getString(R.string.success_modify_memo));
			finish();
		} else if (ifId == InterfaceIds.GET_MEMO.IF_ID) {
			Memo tmpMemo = (Memo) obj.getResultObject();

			mMemo.setCategory(tmpMemo.getCategory());
			mMemo.setContent(tmpMemo.getContent());
			setDatas();
		}
	}

	private void submit() {
		mAccountController.modifyMemo(mMemo.getTargetUser().getId(), mMemo.getCategory(),
				mMemo.getContent());
		mLoadingDialog.show();
	}

	public void editContent(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MemoInputActivity.class);
		intent.putExtra("text", mMemo.getContent());
		startActivityForResult(intent, REQUEST_MODIFY_CONTENT);
	}

	public void pickCategory(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MemoPickListActivity.class);
		intent.putExtra("cat", mMemo.getCategory());
		startActivityForResult(intent, REQUEST_MODIFY_CATEGORY);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			return;
		}

		String result = data.getExtras().getString("result");
		switch (requestCode) {
		case REQUEST_MODIFY_CATEGORY:
			mMemo.setCategory(result);
			mCategoryTv.setText(getCategory());
			break;
		case REQUEST_MODIFY_CONTENT:
			mMemo.setContent(result);
			mContentTv.setText("" + result);
			break;

		default:
			break;
		}

		setDatas();

		// Intent intent = new Intent();
		// intent.putExtra("memo", mMemo);
		// setResult(RESULT_OK, intent);
	}
}