package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Contract;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.WorkplaceController;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ContractPreviewActivity extends BaseNetActivity {
	private Contract mContract;

	private WorkplaceController mWorkplaceController;

	private String mTargetUid;
	private String mConvId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contract_preview);

		enableBack();
		makeTitle("発注書");

		Intent intent = getIntent();

		mTargetUid = intent.getStringExtra("uid");
		mConvId = intent.getStringExtra("convid");
		mContract = (Contract) intent.getSerializableExtra("contract");

		setText(R.id.tv_contract_preview_name, mContract.getName());
		setText(R.id.tv_contract_preview_publish_time,
				mContract.getPublishTime());
		setText(R.id.tv_contract_preview_cost_without_tax,
				mContract.getCostWithoutTax());
		setText(R.id.tv_contract_preview_publisher_real_name,
				mContract.getPublisherRealName());
		setText(R.id.tv_contract_preview_code, mContract.getPublisherCode());
		setText(R.id.tv_contract_preview_publisher_address,
				mContract.getPublisherAddress());
		setText(R.id.tv_contract_preview_publisher_tel,
				mContract.getPublisherTel());
		setText(R.id.tv_contract_preview_publisher_fax,
				mContract.getPublisherFax());
		setText(R.id.tv_contract_preview_receiver_real_name,
				mContract.getReceiverRealName());
		setText(R.id.tv_contract_preview_job_time, mContract.getJobTime());
		setText(R.id.tv_contract_preview_pay_time, mContract.getPayTime());
		setText(R.id.tv_contract_preview_remark, mContract.getRemark());

		mWorkplaceController = new WorkplaceController(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mWorkplaceController != null) {
			mWorkplaceController.clear();
			mWorkplaceController = null;
		}
	}

	private void setText(int res, String text) {
		((TextView) findViewById(res)).setText(text);
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.PUBLISH_CONTRACT.IF_ID) {
			Contract newContract = (Contract) obj.getResultObject();

			Intent intent = new Intent();
			intent.putExtra("contract", newContract);
			setResult(RESULT_OK, intent);
			finish();
			toast(getString(R.string.success_publish_contract));
		}
	}

	public void contractSubmit(View v) {
		showNeedMoneyDialog(getString(R.string.hint_send_contract_need_money),
				new RightButtonListener() {

					@Override
					public void onRightButtonClick() {
						mWorkplaceController.publishContract(mTargetUid,
								mConvId, mContract);
						mLoadingDialog.show();
					}
				});

		// Contract newContract = new Contract();
		// newContract.setUrl("www.baidu.com");
		//
		// Intent intent = new Intent();
		// intent.putExtra("contract", newContract);
		// setResult(RESULT_OK, intent);
		// finish();
		// toast(getString(R.string.success_publish_contract));
	}

	@Override
	protected void outOfBalanceCancelLeft() {
		// super.outOfBalanceCancelLeft();
	}

	@Override
	protected void outOfBalanceCancelRight() {
		// super.outOfBalanceCancelRight();
	}
}