package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.app.ug_inc.R;

public class SettingEmailActivity extends BaseActivity {

    private String email;

    private TextView emailNow;
    private EditText emailUpdate;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        enableBack();

        Intent intent = getIntent();

        makeTitle("メールアドレス変更");

        email = intent.getStringExtra("mEmail");

        addRightAction(R.drawable.ic_btn_acbar_decided, new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultOK();
            }
        });


        emailNow = (TextView)findViewById(R.id.tv_account_email_now_1);
        emailNow.setText(email+"");

        emailUpdate = (EditText)findViewById(R.id.tv_account_email);
    }

    private void resultOK() {

            Intent intent = new Intent();
            intent.putExtra("result", emailUpdate.getText().toString());
            setResult(RESULT_OK, intent);
        finish();
    }

}
