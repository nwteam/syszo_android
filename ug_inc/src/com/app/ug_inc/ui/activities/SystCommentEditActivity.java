package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.MockController;

public class SystCommentEditActivity extends BaseNetActivity {

    private EditText mContentEt;

    private MockController mMockController;

    private String mCommentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_comment_edit);

        makeTitle("編集");
        makeIcon(R.drawable.micon_hitorigoto);
        makeTitleBg(0xFFD66968);
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_send, new OnClickListener() {

            @Override
            public void onClick(View v) {
                submit();
            }
        });

        mCommentId = getIntent().getStringExtra("id");

        mContentEt = (EditText) findViewById(R.id.et_chieb_new_title);

        mMockController = new MockController(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mMockController);
    }

    private String mTmpContent;

    private void submit() {
        mTmpContent = mContentEt.getText().toString();
        if (checkXX(TextUtils.isEmpty(mTmpContent), "内容")) {
            return;
        }

        new AlertDialog.Builder(mContext).setMessage(getString(R.string.hint_edit_confirm_content_ok))
                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mMockController.editMockComment(mCommentId, mTmpContent);
                        mLoadingDialog.show();
                    }
                }).setNegativeButton(getString(R.string.cancel), null).show();
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        Intent intent = new Intent();
        intent.putExtra("id", mCommentId);
        intent.putExtra("content", mTmpContent);

        setResult(RESULT_OK, intent);
        finish();
    }
}
