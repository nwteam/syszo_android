package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.ChiebComment;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.Utils;
import com.app.ug_inc.utils.ZLog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;

public class ChiebDetailActivity2 extends BaseListNetActivity {

    private PullListview mListview;
    private ListAdapter mAdapter;

    private List<ChiebComment> mCommentList;

    private ClosureController mClosureController;
    private ClosureController mRefreshController;
    private ClosureController mLoadmoreController;

    private EditText mSendEt;

    private boolean mIsDetailLoaded = false;

    private ChiebComment mRequestComment;

    private View mEditBtn;

    private RefreshReceiver mRefreshReceiver;

    private class RefreshReceiver extends BroadcastReceiver {

        public void register() {
            IntentFilter filter = new IntentFilter(ChiebNewActivity.ACTION_CHIEB_NEED_REFRESH);
            registerReceiver(this, filter);
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            ChiebComment chieb = mCommentList.get(0);
            mClosureController.getClosureDetail(chieb.getId());
            mClosureController.get_good_count(chieb.getId());
            mRefreshController.getClosureCommentList(chieb.getId(), ConstData.PAGE_FIRST);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_detail);

        makeTitle("知恵袋");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        mRefreshReceiver = new RefreshReceiver();
        mRefreshReceiver.register();

        final Chieb chieb = (Chieb) getIntent().getSerializableExtra("chieb");

        mEditBtn = addRightAction(R.drawable.ic_btn_acbar_contribute, new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mIsDetailLoaded) {
                    Chieb chieb2 = new Chieb();
                    ChiebComment chieb0 = mCommentList.get(0);
                    chieb2.setId(chieb0.getId());
                    chieb2.setTime(chieb0.getTime());
                    chieb2.setTitle(chieb0.getTitle());
                    chieb2.setContent(chieb0.getContent());
                    chieb2.setCommentCount(chieb0.getCommentCount());
                    chieb2.setUser(chieb0.getUser());
                    chieb2.setUrgent(chieb0.isUrgent());
                    chieb2.setCommited(chieb0.isCommited());

                    Intent intent = new Intent();
                    intent.setClass(mContext, ChiebNewActivity.class);
                    intent.putExtra("chieb", chieb2);
                    startActivity(intent);
                }
            }
        });
        mEditBtn.setVisibility(View.INVISIBLE);

        mCommentList = new ArrayList<ChiebComment>();
        ChiebComment comment = chiebToComment(chieb);
        mCommentList.add(comment);

        mAdapter = new ListAdapter();

        mListview = (PullListview) findViewById(R.id.plv_chieb_detail);
        mListview.setAdapter(mAdapter);
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mClosureController.getClosureDetail(chieb.getId());
                mClosureController.get_good_count(chieb.getId());
                mRefreshController.getClosureCommentList(chieb.getId(), ConstData.PAGE_FIRST);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mLoadmoreController.getClosureCommentList(chieb.getId(), mCurrPage + 1);
            }
        });
//        mListview.getRefreshableView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                int pos = position - mListview.getRefreshableView().getHeaderViewsCount();
//
//                ChiebComment comme = mCommentList.get(pos);
//
//                ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//                clip.getText(); // 粘贴
//                clip.setText(comme.getContent()); // 复制
//
//                toast(getString(R.string.success_copy));
//
//                return true;
//            }
//        });

        mSendEt = (EditText) findViewById(R.id.et_chieb_detail_chat);

        mClosureController = new ClosureController(this);
        mClosureController.getClosureDetail(chieb.getId());
        mClosureController.get_good_count(chieb.getId());
        mRefreshController = new ClosureController(mRefreshDelegate);
        mRefreshController.getClosureCommentList(chieb.getId(), ConstData.PAGE_FIRST);
        mLoadmoreController = new ClosureController(mLoadmoreDelegate);

        findViewById(R.id.btn_chieb_detail_send).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = mSendEt.getText().toString();
                if (checkXX(TextUtils.isEmpty(content), "送信内容")) {
                    return;
                }

                mClosureController.commentClosure(chieb.getId(), content);
                mLoadingDialog.show();
            }
        });
    }

    private ChiebComment chiebToComment(Chieb chieb) {
        ChiebComment comment = new ChiebComment();
        comment.setId(chieb.getId());
        comment.setTime(chieb.getTime());
        comment.setTitle(chieb.getTitle());
        comment.setContent(chieb.getContent());
        comment.setCommentCount(chieb.getCommentCount());
        comment.setUser(chieb.getUser());
        comment.setUrgent(chieb.isUrgent());
        comment.setCommited(chieb.isCommited());
        return comment;
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_CHIEB; i++) {
            ChiebComment comment = new ChiebComment();
            comment.setTime("2014-12-31 10:00:00");
            comment.setContent("评论内容fasdonfdnaosfndioasnfiodasnfas");

            User user = new User();
            user.setNick("用户" + i);
            comment.setUser(user);

            mCommentList.add(comment);
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mClosureController);
        clearController(mRefreshController);
        clearController(mLoadmoreController);

        mRefreshReceiver.unregister();
    }

    private UIDelegate mRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();

            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            ChiebComment chieb = mCommentList.get(0);
            mCommentList.clear();
            mCommentList.add(chieb);
            mCommentList.addAll(list);
            mCurrPage = ConstData.PAGE_FIRST;
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    private UIDelegate mLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();
            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            judgeLoadMore(list);

            mCommentList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_CLOSURE_DETAIL.IF_ID) {
            Chieb chieb = (Chieb) obj.getResultObject();

            ZLog.e("chieb="+chieb.toString());
            if (Account.getUserId().equals(chieb.getUser().getId())) {
                mEditBtn.setVisibility(View.VISIBLE);
            }

            ChiebComment chieb0 = mCommentList.get(0);

            chieb0.setTitle(chieb.getTitle());
            chieb0.setUser(chieb.getUser());
            chieb0.setContent(chieb.getContent());
            chieb0.setPicUrl(chieb.getPicUrl());
            chieb0.setLiked(chieb.isLike());

            ZLog.e("chieb0=" + chieb0.toString());
            if (Account.getUserId().equals(chieb.getUser().getId())) {
                mEditBtn.setVisibility(View.VISIBLE);
            }

            mIsDetailLoaded = true;

            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.COMMENT_CLOSURE.IF_ID) {
            toast("送信しました。");
            mSendEt.setText("");
            ChiebComment chieb = mCommentList.get(0);
            mRefreshController.getClosureCommentList(chieb.getId(), ConstData.PAGE_FIRST);
        } else if (ifId == InterfaceIds.LIKE_COLSURE.IF_ID) {
           /* mRequestComment.setLiked(true);
            mAdapter.notifyDataSetChanged();*/
        	 ChiebComment chieb = mCommentList.get(0);
        	 mClosureController.getClosureDetail(chieb.getId());
        	 mClosureController.get_good_count(chieb.getId());
             mRefreshController.getClosureCommentList(chieb.getId(), ConstData.PAGE_FIRST);
        } else if (ifId == InterfaceIds.DELETE_COLSURE_COMMENT.IF_ID) {
            mCommentList.remove(mRequestComment);
            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.DELETE_CLOSURE.IF_ID) {
            sendBroadcast(new Intent(ChiebNewActivity.ACTION_CHIEB_NEED_REFRESH));
            finish();
        }else if(ifId == InterfaceIds.GETGOODCOUNT.IF_ID){
        	String result = (String) obj.getResultObject();
        	ChiebComment chieb = mCommentList.get(0);
        	chieb.setLikeNum(Integer.valueOf(result).intValue());
        	mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
            String id = data.getStringExtra("id");
            String newContent = data.getStringExtra("content");

            for (ChiebComment comment : mCommentList) {
                if (comment.getId().equals(id)) {
                    comment.setContent(newContent);
                    break;
                }
            }

            mAdapter.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class ListAdapter extends BaseAdapter {
        private final int HEADER = 0, ITEM = 1;
        private final int TYPE_COUNT = 2;

        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mCommentList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? HEADER : ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }

        @Override
        public ChiebComment getItem(int arg0) {
            return mCommentList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View arg1, ViewGroup arg2) {
            Holder h = null;
            int type = getItemViewType(position);

            ZLog.e("position="+position+"type="+type);
            if (arg1 == null) {
                h = new Holder();
                if (type == HEADER) {
                    arg1 = mInflater.inflate(R.layout.item_list_chieb_detail_header, null);
                    h.h_title = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_title);
                    h.h_count = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_count);
                    h.h_time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_time);
                    h.h_uname = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_uname);
                    h.h_count = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_count);
                    h.h_pic = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_detail_header_img);
                    h.h_content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_detail_header_content);
                    h.del = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_del);
                    h.h_profile = arg1.findViewById(R.id.llayout_item_list_chieb_detail_header_uname);
                    h.h_avatar = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_detail_header_avatar);
                    h.likeNum = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_likenum);
                    h.like = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_like);
                    h.vipIV = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_vip);
                } else {
                    arg1 = mInflater.inflate(R.layout.item_list_chieb_comment, null);
                    h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_content);
                    h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_time);
                    h.like = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_like);
                    h.del = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_del);
                    h.edit = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_edit);
                    h.avatar = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_avatar);
                    h.selfLayout = arg1.findViewById(R.id.llayout_item_list_chieb_comment_del_edit);
                    h.uname = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_uname);
                    h.profile = arg1.findViewById(R.id.llayout_list_profile);
                    h.likeNum = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_likenum);
                    h.vipIV = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_vip);
                }

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            final ChiebComment item = getItem(position);
            ZLog.e("item="+item.toString());
            if (type == HEADER) {
                String title = item.getTitle();
                SpannableString ss = new SpannableString(title);
                ss.setSpan(new BackgroundColorSpan(0xFFFFFF00), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                h.h_title.setText(ss);
                h.h_count.setText("" + item.getCommentCount());
                h.h_time.setText("" + item.getTime());
                h.h_content.setText("" + item.getContent());
                
                try {
                    h.h_uname.setText("" + item.getUser().getNick());

                    if(item.getUser().getVip_flg() == 1){
                        h.vipIV.setVisibility(View.VISIBLE);
                    }else{
                        h.vipIV.setVisibility(View.GONE);
                    }

                    if (Account.getUserId().equals(item.getUser().getId())) {
                        h.h_avatar.setImageResource(R.drawable.icon_user_on);
                        h.h_uname.setTextColor(0xFFF1A90F);
                    } else {
                        h.avatar.setImageResource(R.drawable.icon_user_off);
                        h.uname.setTextColor(0xFF393A40);
                    }

                } catch (NullPointerException e) {
                }

                final String picUrl = item.getPicUrl();
                if (!TextUtils.isEmpty(picUrl)) {
                    h.h_pic.setVisibility(View.VISIBLE);
                    ZImageLoader.asyncLoadImage(picUrl, h.h_pic);
                    h.h_pic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setClass(mContext, PicBrowseActivity.class);
                            intent.putExtra("url", picUrl);
                            startActivity(intent);
                        }
                    });
                } else {
                    h.h_pic.setVisibility(View.GONE);
                    h.h_pic.setOnClickListener(null);
                }

                h.like.setVisibility(View.VISIBLE);
                h.likeNum.setVisibility(View.VISIBLE);
                h.likeNum.setText(item.getLikeNum()+"");
                
                if (item.isLiked()) {
                    h.like.setImageResource(R.drawable.btn_iine_pressed);
                } else {
                    h.like.setImageResource(R.drawable.btn_iine_normal);
                }
                h.like.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	likeMain(item);
                    }
                });
                
                try {
                    if (Account.getUserId().equals(item.getUser().getId())) {
                        h.del.setVisibility(View.VISIBLE);
                        h.del.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                deleteChieb(item);
                            }
                        });
                        
                        h.like.setImageResource(R.drawable.btn_iine_pressed);
                        h.like.setEnabled(false);
                        
                    } else {
                        h.del.setVisibility(View.INVISIBLE);
                    }

                    h.h_profile.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toProfile(item.getUser().getId());
                        }
                    });
                } catch (NullPointerException e) {
                    h.del.setVisibility(View.INVISIBLE);
                }
            } else {
                h.time.setText("" + item.getTime());
                h.content.setText("" + item.getContent());
                h.profile.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toProfile(item.getUser().getId());
                    }
                });
                
                User user = item.getUser();
                h.profile.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toProfile(item.getUser().getId());
                    }
                });

                if (Account.getUserId().equals(user.getId())) {
                    h.avatar.setImageResource(R.drawable.icon_user_on);
                    h.uname.setTextColor(0xFFF1A90F);
                    h.selfLayout.setVisibility(View.VISIBLE);
                    h.like.setVisibility(View.VISIBLE);
                    h.like.setImageResource(R.drawable.btn_iine_pressed);
                    h.like.setEnabled(false);
                    h.likeNum.setVisibility(View.VISIBLE);
                    h.likeNum.setText(item.getComment_goods()+"");
                    h.del.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            delete(item);
                        }
                    });

                    h.edit.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            edit(item);
                        }
                    });
                } else {
                    h.avatar.setImageResource(R.drawable.icon_user_off);
                    h.uname.setTextColor(0xFF393A40);
                    h.selfLayout.setVisibility(View.INVISIBLE);
                    h.like.setVisibility(View.VISIBLE);
                    h.like.setEnabled(true);
                    h.likeNum.setVisibility(View.VISIBLE);
                    h.likeNum.setText(item.getComment_goods()+"");
                    if (item.isLiked()) {
                        h.like.setImageResource(R.drawable.btn_iine_pressed);
                    } else {
                        h.like.setImageResource(R.drawable.btn_iine_normal);
                    }
                    h.like.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            like(item);
                        }
                    });
                }
                h.uname.setText(user.getNick());

                if(user.getVip_flg() == 1){
                    h.vipIV.setVisibility(View.VISIBLE);
                }else{
                    h.vipIV.setVisibility(View.GONE);
                }
            }

            arg1.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    copy(item.getContent());
                    return true;
                }
            });

            return arg1;
        }

        private void copy(CharSequence s) {
            Utils.copyToClipBoard(mContext, s);
        }

        private void deleteChieb(final ChiebComment item) {
            new AlertDialog.Builder(mContext).setMessage("投稿を削除しますか？").setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mClosureController.deleteClosure(item.getId());
                    mLoadingDialog.show();
                    mLoadingDialog.show();
                }
            }).setNegativeButton(getString(R.string.cancel), null).show();
        }

        private void like(ChiebComment comment) {
            mRequestComment = comment;
            mClosureController.likeClosure(mCommentList.get(0).getId(), mRequestComment.getId());
            mLoadingDialog.show();
        }

        private void likeMain(ChiebComment comment) {
            mRequestComment = comment;
            mClosureController.likeClosure(mCommentList.get(0).getId(), "0");
            mLoadingDialog.show();
        }
        
        private void edit(ChiebComment comment) {
            Intent intent = new Intent();
            intent.setClass(mContext, ChiebCommentEditActivity.class);
            intent.putExtra("id", comment.getId());
            intent.putExtra("content", comment.getContent());
            startActivityForResult(intent, 5);
        }

        private void delete(final ChiebComment comment) {
            new AlertDialog.Builder(mContext).setMessage("投稿を削除しますか？").setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mRequestComment = comment;
                    mClosureController.deleteClosureComment(mRequestComment.getId());
                    mLoadingDialog.show();
                }
            }).setNegativeButton(getString(R.string.cancel), null).show();
        }

        private class Holder {
            // header
            private TextView h_title;
            private TextView h_count;
            private TextView h_time;
            private TextView h_uname;
            private TextView h_content;
            private ImageView h_pic;
            private ImageView h_avatar,vipIV;
            private View h_profile;
            // item
            private TextView content;
            private TextView time;
            private TextView uname;
            private ImageView avatar;
            private View selfLayout;
            private ImageView like;
            private ImageView del;
            private ImageView edit;
            private View bg;
            private View profile;
            private TextView likeNum;
        }
    }
}
