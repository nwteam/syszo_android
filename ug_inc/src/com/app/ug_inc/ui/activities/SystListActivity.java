package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Syst;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.MockController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.ZLog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SystListActivity extends BaseListNetActivity {
    private ListAdapter mAdapter;
    private PullListview mListview;

    private ArrayList<Syst> mSystList;

    private MockController mRefreshController;
    private MockController mLoadmoreController;

    private boolean mIsRequesting = false;

    private int mCurrTab = 1;
    private int mTmpTab = 1;

    private EditText mSearchEt;

    private String mTmpSearchWord;
    private String mSearchWord;

    private View mSearchLayout;

    private RefreshReceiver mRefreshReceiver;

    private class RefreshReceiver extends BroadcastReceiver {

        public void register() {
            IntentFilter filter = new IntentFilter(SystNewActivity.ACTION_SYST_NEED_REFRESH);
            registerReceiver(this, filter);
        }

        public void unregister() {
            unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            loadData(mCurrTab, false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syst_list);

        makeTitle("シスッター");
        makeIcon(R.drawable.micon_hitorigoto);
        makeTitleBg(0xFFD66968);
        enableBack();

//        makeTitleBg();

        addRightAction(R.drawable.ic_btn_acbar_search, new OnClickListener() {

            @Override
            public void onClick(View v) {
                mSearchLayout.setVisibility(
                        mSearchLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        addRightAction(R.drawable.ic_btn_acbar_contribute, new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SystNewActivity.class));
            }
        });

        makeTab();

        mSearchLayout = findViewById(R.id.rlayout_syst_list_search);
        mSearchLayout.setVisibility(View.GONE);

        mSearchEt = (EditText) findViewById(R.id.et_syst_search_keyword);

        mSystList = new ArrayList<Syst>();

        mAdapter = new ListAdapter();
        mListview = (PullListview) findViewById(R.id.plv_syst_list);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListview.getRefreshableView().getHeaderViewsCount();
                Syst syst = mSystList.get(pos);
                Intent intent = new Intent();
                intent.setClass(mContext, SystDetailActivity.class);
                intent.putExtra("syst", syst);
                startActivity(intent);
            }
        });
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mRefreshController.getSystList(mSearchWord, getType(), ConstData.PAGE_FIRST);
                mIsRequesting = true;
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mLoadmoreController.getSystList(mSearchWord, getType(), mCurrPage);
                mIsRequesting = true;
            }
        });

//        test();

        initController();

        mCurrTab = 1;// default 1
        loadData(mCurrTab, true);
        selectTab(mCurrTab);

        if (mRefreshReceiver == null) {
            mRefreshReceiver = new RefreshReceiver();
            mRefreshReceiver.register();
        }
    }

    private void initController() {
        mRefreshController = new MockController(new UIDelegate() {
            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                mLoadingDialog.dismiss();
                mListview.onRefreshComplete();
                mSearchWord = mTmpSearchWord;

                List<Syst> list = (List<Syst>) obj.getResultObject();

                judgeLoadMore(list);

                mSystList.clear();
                mSystList.addAll(list);
                mAdapter.notifyDataSetChanged();
                mIsRequesting = false;

                mCurrTab = mTmpTab;
                selectTab(mCurrTab);
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                mLoadingDialog.dismiss();
                mListview.onRefreshComplete();
                mIsRequesting = false;
            }
        });

        mLoadmoreController = new MockController(new UIDelegate() {
            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                mListview.onRefreshComplete();

                List<Syst> list = (List<Syst>) obj.getResultObject();
                judgeLoadMore(list);

                mSystList.addAll(list);
                mAdapter.notifyDataSetChanged();
                mIsRequesting = false;
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                mListview.onRefreshComplete();
                mIsRequesting = false;
            }
        });
    }

    public void searchClick(View v) {
        if (mIsRequesting) {
            return;
        }

        String word = mSearchEt.getText().toString();

        mSearchWord = word;

        mRefreshController.getSystList(mSearchWord, getType(), ConstData.PAGE_FIRST);
        mCurrStatus = STATUS.REFRESH;
        mLoadingDialog.show();
    }

    private Button mTab1, mTab2, mTab3;

    private void makeTab() {
        mTab1 = (Button) findViewById(R.id.btn_syst_list_order_1);
        mTab1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(1, true);
            }
        });
        mTab2 = (Button) findViewById(R.id.btn_syst_list_order_2);
        mTab2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(2, true);
            }
        });
        mTab3 = (Button) findViewById(R.id.btn_syst_list_order_3);
        mTab3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(3, true);
            }
        });
    }

    private void loadData(int index, boolean needLoading) {
        if (mIsRequesting) {
            return;
        }

        mCurrPage = ConstData.PAGE_FIRST;
        mRefreshController.getSystList("", getType(index), ConstData.PAGE_FIRST);
        mCurrStatus = STATUS.REFRESH;

        mTmpTab = index;

        mIsRequesting = true;
        if (needLoading) {
            mLoadingDialog.show();
        }
    }

    private void selectTab(int index) {
        switch (index) {
            case 1:
                mTab1.setBackgroundResource(R.drawable.tab2_left_on);
                mTab2.setBackgroundResource(R.drawable.tab2_center_off);
                mTab3.setBackgroundResource(R.drawable.tab2_right_off);
                break;
            case 2:
                mTab1.setBackgroundResource(R.drawable.tab2_left_off);
                mTab2.setBackgroundResource(R.drawable.tab2_center_on);
                mTab3.setBackgroundResource(R.drawable.tab2_right_off);
                break;
            case 3:
                mTab1.setBackgroundResource(R.drawable.tab2_left_off);
                mTab2.setBackgroundResource(R.drawable.tab2_center_off);
                mTab3.setBackgroundResource(R.drawable.tab2_right_on);
                break;
        }
    }

    private String getType() {
        return getType(mCurrTab);
    }

    private String getType(int index) {
        switch (index) {
            case 1:
                return "1";
            case 2:
                return "2";
            case 3:
                return "3";
            default:
                return "1";
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mRefreshController);
        clearController(mLoadmoreController);

        if (mRefreshReceiver != null) {
            mRefreshReceiver.unregister();
            mRefreshReceiver = null;
        }
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_CHIEB; i++) {
            Syst c = new Syst();
            c.setTime("2014-12-31 12:00:00");
            c.setContent("内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容");

            User user = new User();
            user.setNick("用户" + i);
            c.setUser(user);

            mSystList.add(c);
        }
        mAdapter.notifyDataSetChanged();
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mSystList.size();
        }

        @Override
        public Syst getItem(int arg0) {
            return mSystList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_syst, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_syst_content);
                h.uname = (TextView) arg1.findViewById(R.id.tv_item_list_syst_uname);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_syst_time);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_syst_root);
                h.vipIV = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_vip);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            Syst item = getItem(arg0);

            h.bg.setBackgroundColor(arg0 % 2 == 0 ? WHITE : 0xFFFAFAFA);

            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getContent());
            h.uname.setText("" + item.getUser().getNick());

            if(item.getUser().getVip_flg() == 1){
                h.vipIV.setVisibility(View.VISIBLE);
            }else{
                h.vipIV.setVisibility(View.GONE);
            }

            return arg1;
        }

        private class Holder {
            private TextView content;
            private TextView time;
            private TextView uname;
            private View bg;
            private ImageView vipIV;
        }
    }
}
