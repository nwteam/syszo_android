package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.User;
import com.app.ug_inc.models.WorkerInfo;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.WorkplaceController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.ui.views.TopSearchView;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class WorkPListActivity extends BaseListNetActivity {
	private ArrayList<WorkerInfo> mWorkerList;

	private PullListview mListview;
	private TopSearchView mSearchView;

	private ListAdapter mAdapter;

	private boolean mIsWorker;

	private WorkplaceController mWorkpController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workp_list);

		mIsWorker = getIntent().getAction().equals(WorkPPickActivity.ACTION_WORKER);

		enableBack();
		makeTitle(mIsWorker ? "手空いてる?" : "現場一覧");

        addRightAction(R.drawable.ic_btn_acbar_search, new OnClickListener() {

			@Override
			public void onClick(View v) {
				search();
			}
		});

		mSearchView = (TopSearchView) findViewById(R.id.search_view);

		mWorkerList = new ArrayList<WorkerInfo>();

		mAdapter = new ListAdapter();
		mListview = (PullListview) findViewById(R.id.lv_workp_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				if (mIsQuery) {
					if (mIsWorker) {
						mWorkpController.queryFreeWorkerList(mTmpSearchWord, mCurrPage + 1);
					} else {
						mWorkpController.queryFreeWorkplaceList(mTmpSearchWord, mCurrPage + 1);
					}
				} else {
					if (mIsWorker) {
						mWorkpController.getFreeWorkerList(mCurrPage + 1);
					} else {
						mWorkpController.getFreeWorkplaceList(mCurrPage + 1);
					}
				}
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mWorkpController = new WorkplaceController(this);
		if (mIsWorker) {
			mWorkpController.getFreeWorkerList(ConstData.PAGE_FIRST);
		} else {
			mWorkpController.getFreeWorkplaceList(ConstData.PAGE_FIRST);
		}
		mCurrStatus = STATUS.REFRESH;
		mLoadingDialog.show();

		// test();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mWorkpController != null) {
			mWorkpController.clear();
			mWorkpController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_FREE_WORKER_LIST.IF_ID
				|| ifId == InterfaceIds.GET_FREE_WORKPLACE_LIST.IF_ID) { // same
			List<WorkerInfo> list = (ArrayList<WorkerInfo>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mWorkerList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}

			mWorkerList.addAll(list);
			mAdapter.notifyDataSetChanged();
		} else if (ifId == InterfaceIds.QUERY_FREE_WORKER_LIST.IF_ID
				|| ifId == InterfaceIds.QUERY_FREE_WORKPLACE_LIST.IF_ID) { // same
			List<WorkerInfo> list = (ArrayList<WorkerInfo>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mWorkerList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}

			mWorkerList.addAll(list);
			mAdapter.notifyDataSetChanged();
		}
	}

	private void search() {
		String word = mSearchView.getText().toString();
		if (!TextUtils.isEmpty(mTmpSearchWord) && mTmpSearchWord.equals(word)) {
			return;
		}

		mTmpSearchWord = word;

		mWorkerList.clear();
		mAdapter.notifyDataSetChanged();

		mCurrPage = ConstData.PAGE_FIRST;
		mCurrStatus = STATUS.REFRESH;

		if (mIsWorker) {
			mWorkpController.queryFreeWorkerList(mTmpSearchWord, ConstData.PAGE_FIRST);
		} else {
			mWorkpController.queryFreeWorkplaceList(mTmpSearchWord, ConstData.PAGE_FIRST);
		}
		mIsQuery = true;
		mLoadingDialog.show();
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_WORKPLACE; i++) {
			WorkerInfo worker = new WorkerInfo();
			User usr = new User();
			usr.setNick("哈哈" + i);
			worker.setUser(usr);

			worker.setPublishTime("2014-08-08 20:20:20");
			worker.setDescribe("工厂描述" + i + "工厂面熟" + i + "fduaiobfdiuasf");

			mWorkerList.add(worker);
		}
		mAdapter.notifyDataSetChanged();
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mWorkerList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mWorkerList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_workp_list, null);
				h.nick = (TextView) arg1.findViewById(R.id.tv_item_list_workp_list_nickname);
				h.time = (TextView) arg1.findViewById(R.id.tv_item_list_workp_list_time);
				h.content = (TextView) arg1.findViewById(R.id.tv_item_list_workp_list_content);
				h.detail = arg1.findViewById(R.id.iv_item_list_workp_list_detail);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			final WorkerInfo worker = mWorkerList.get(arg0);
			final User usr = worker.getUser();
			h.nick.setText("" + usr.getNick() + "【" + usr.getCity() + "】" + usr.getJob());
			h.nick.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					seeProfile(usr.getId());
				}
			});
			h.time.setText("" + worker.getPublishTime());
			h.content.setText("" + worker.getDescribe());
			h.detail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(mContext, WorkPDetailActivity.class);
					intent.putExtra("worker", worker);
					intent.setAction(mIsWorker ? WorkPPickActivity.ACTION_WORKER
							: WorkPPickActivity.ACTION_WORKPLACE);
					startActivity(intent);
				}
			});

			return arg1;
		}

		private class Holder {
			private TextView nick;
			private TextView time;
			private TextView content;
			private View detail;
		}
	}
}
