package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.utils.FileUtils;
import com.app.ug_inc.utils.Utils;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class MySeriesNewSubmitActivity extends BaseNetActivity {

	private MySeriesController mMySeriesController;

	private SimpleKV mCategory;

	private String mTitle;
	private String mContent;

	private String mUploadedPicUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myseries_new_submit);

		makeTitle("新規投稿");
		enableBack();
        addRightAction(R.drawable.ic_btn_acbar_contribute, new OnClickListener() {

			@Override
			public void onClick(View v) {
				mTitle = ((EditText) findViewById(R.id.et_register_title)).getText().toString();
				mContent = ((EditText) findViewById(R.id.et_register_content)).getText().toString();

				if (checkXX(TextUtils.isEmpty(mTitle), "タイトル")) {
					return;
				}

				if (checkXX(TextUtils.isEmpty(mContent), "内容")) {
					return;
				}

				new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm_content_ok))
						.setLeftButton(getString(R.string.cancel))
						.setRightButton(getString(R.string.OK), new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								mMySeriesController.publishMySeriesArticle(mCategory.getKey(),
										mTitle, mContent, mUploadedPicUrl);
								mLoadingDialog.show();
							}
						}).show();

			}
		});

		mCategory = (SimpleKV) getIntent().getSerializableExtra("cat");

		mMySeriesController = new MySeriesController(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMySeriesController != null) {
			mMySeriesController.clear();
			mMySeriesController = null;
		}
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);

		if (ifId == InterfaceIds.UPLOAD_PIC.IF_ID) {
			toast("アップロードしました");
			mUploadedPicUrl = (String) obj.getResultObject();
		} else if (ifId == InterfaceIds.PUBLISH_MY_SERIES_ARTICLE.IF_ID) {
			// toast(getString(R.string.success_submit_my_series_article));
			finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			mTmpPhotoPath = null;

			return;
		}

		switch (requestCode) {
		case Utils.REQUEST_PICK_PHOTO:
			Uri photoUri = data.getData();
			mTmpPhotoPath = FileUtils.getPath(mContext, photoUri);

			break;
		case Utils.REQUEST_TAKE_PHOTO:

			break;

		default:
			break;
		}

		if (!TextUtils.isEmpty(mTmpPhotoPath)) {
			mMySeriesController.uploadPic(mTmpPhotoPath);
			mLoadingDialog.show();
			mLoadingDialog.setCancelable(false);
		}
	}

	private String mTmpPhotoPath;

	public void selectPicture(View v) {
		String[] hints = new String[] { getString(R.string.hint_upload_photo_take),
				getString(R.string.hint_upload_photo_pick) };
		new AlertDialog.Builder(mContext).setItems(hints, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0: // take picture
					try {
						mTmpPhotoPath = Utils.takePhoto(MySeriesNewSubmitActivity.this);
					} catch (Exception e) {
						toast(getString(R.string.hint_sdcard_not_mounted));
					}
					break;
				case 1: // pick picture
					Utils.pickPhoto(MySeriesNewSubmitActivity.this);
					break;

				default:
					break;
				}

				dialog.dismiss();
			}
		}).show();
	}
}