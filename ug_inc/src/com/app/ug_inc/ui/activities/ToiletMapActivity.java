package com.app.ug_inc.ui.activities;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Toilet;
import android.os.Bundle;

public class ToiletMapActivity extends BaseNetActivity {

	private GoogleMap mMap;

	private MapFragment mMapFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_toilet_map);

		enableBack();
		makeTitle("便所一览");

		mMapFragment = (MapFragment) getFragmentManager().findFragmentById(
				R.id.map_toilet);

		mMap = mMapFragment.getMap();

		initMap();
	}

	private void initMap() {
		mMap.setMyLocationEnabled(true);

		Bundle datas = getIntent().getExtras();
		LatLng latLng = new LatLng(datas.getDouble("lat"),
				datas.getDouble("lon"));

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));

		Toilet toilet = (Toilet) datas.getSerializable("toilet");
		System.out.println("toilet: lat=" + toilet.getLat() + ", lon="
				+ toilet.getLon());

		mMap.addMarker(new MarkerOptions()
				.draggable(false)
				.position(new LatLng(toilet.getLat(), toilet.getLon()))
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.ic_map_toilet)));
	}
}
