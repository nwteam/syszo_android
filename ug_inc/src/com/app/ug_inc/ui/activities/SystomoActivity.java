package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Group;
import com.app.ug_inc.models.SPGroup;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.GroupController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoActivity extends BaseNetActivity {
    private List<CatHint> mCatList;
    private List<Group> mGroupList;
    private List<User> mGUserList;
    private PullListview mFullLv;
    private FullAdapter mFullAdapter;

    private List<User> mFriendList;
    private FriendAdapter mFriendAdapter;
    private ListView mFriendLv;

    private Button mFollowBtn;
    private Button mFollowedBtn;

    private GroupController mGroupRefreshController;
    private GroupController mGroupLoadmoreController;
    private GroupController mFriendRefreshController;
    private GroupController mFriendLoadmoreController;

    private int mCurr;
    private boolean mIsRequesting = false;

    private int mGroupPage = ConstData.PAGE_FIRST;

    private GroupModifiedReceiver mGroupModifiedReceiver;

    private class GroupModifiedReceiver extends BroadcastReceiver {

        private void unregister() {
            unregisterReceiver(this);
        }

        private void register() {
            IntentFilter filter = new IntentFilter(SystomoCreateGroupActivity.ACTION_GROUP_MODIFIED);
            registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mGroupRefreshController.getFriendList(ConstData.PAGE_FIRST);
        }
    }

    private FriendModifiedReceiver mFriendModifiedReceiver;

    private class FriendModifiedReceiver extends BroadcastReceiver {
        private void unregister() {
            unregisterReceiver(this);
        }

        private void register() {
            IntentFilter filter = new IntentFilter(ProfileOthersActivity.ACTION_FRIEND_NEED_REFRESH);
            registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mGroupRefreshController.getFriendList(ConstData.PAGE_FIRST);
            if (mFullLv.getVisibility() == View.VISIBLE) {
                mGroupRefreshController.getFriendList(ConstData.PAGE_FIRST);
            } else {
                mFriendRefreshController.addFriendList(ConstData.PAGE_FIRST);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo);

        makeTitle("シストモ");
        makeIcon(R.drawable.micon_systomo);

        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_systomo_add_group, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SystomoCreateGroupActivity.class);
                startActivity(intent);
            }
        });
        addRightAction(R.drawable.ic_btn_acbar_add, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SystomoSearchActivity.class);
                startActivity(intent);
            }
        });

        mGroupModifiedReceiver = new GroupModifiedReceiver();
        mGroupModifiedReceiver.register();
        mFriendModifiedReceiver = new FriendModifiedReceiver();
        mFriendModifiedReceiver.register();

        mFollowBtn = (Button) findViewById(R.id.btn_systomo_follow);
        mFollowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurr == 1 || mIsRequesting) {
                    return;
                }

                mFriendRefreshController.addFriendList(ConstData.PAGE_FIRST);
                mIsRequesting = true;
                mLoadingDialog.show();
            }
        });
        mFollowedBtn = (Button) findViewById(R.id.btn_systomo_followed);
        mFollowedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurr == 2 || mIsRequesting) {
                    return;
                }

                mFriendRefreshController.addedFriendList(ConstData.PAGE_FIRST);
                mIsRequesting = true;
                mLoadingDialog.show();
            }
        });

        mCatList = new ArrayList<CatHint>();
        mGroupList = new ArrayList<Group>();
        mGUserList = new ArrayList<User>();
        mFullAdapter = new FullAdapter();
        mFullLv = (PullListview) findViewById(R.id.lv_systomo_group);
        mFullLv.setAdapter(mFullAdapter);
        mFullLv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mGroupRefreshController.getFriendList(ConstData.PAGE_FIRST);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mGroupLoadmoreController.getFriendList(mGroupPage + 1);
            }
        });
        mFullLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = mFullAdapter.getItem(position - 1);
                if (item instanceof Group) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, SystomoGroupProfileActivity.class);
                    intent.putExtra("group", (Group) item);
                    startActivity(intent);
                } else if (item instanceof User) {
                    Intent intent = new Intent(mContext, ChatActivity.class);
                    intent.putExtra("uid", ((User) item).getId());
                    intent.putExtra("title", ((User) item).getNick());
                    startActivity(intent);
                }
            }
        });

        mFriendList = new ArrayList<User>();
        mFriendAdapter = new FriendAdapter();
        mFriendLv = (ListView) findViewById(R.id.lv_systomo_friends);
        mFriendLv.setAdapter(mFriendAdapter);
        mFriendLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = mFriendList.get(position);
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("uid", user.getId());
                intent.putExtra("title", user.getNick());
                startActivity(intent);
            }
        });

        mFriendRefreshController = new GroupController(mFriendRefreshDelegate);
        mFriendLoadmoreController = new GroupController(mFriendLoadmoreDelegate);

        mGroupRefreshController = new GroupController(mGroupRefreshDelegate);
        mGroupLoadmoreController = new GroupController(mGroupLoadmoreDelegate);

        mGroupRefreshController.getFriendList(ConstData.PAGE_FIRST);
        mLoadingDialog.show();
//        test();s
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGroupModifiedReceiver.unregister();
        mFriendModifiedReceiver.unregister();

        clearController(mGroupLoadmoreController);
        clearController(mGroupRefreshController);
        clearController(mFriendRefreshController);
        clearController(mFriendLoadmoreController);
    }

    @Override
    protected void test() {
        SPGroup spGroup = new SPGroup();
        spGroup.setAddSum(20);
        spGroup.setAddedSum(25);
        spGroup.setFriendSum(10);
        spGroup.setGroupSum(5);

        List<User> userList = new ArrayList<User>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setNick("用户" + i);

            userList.add(user);
        }
        spGroup.setFriendList(userList);

        List<Group> groupList = new ArrayList<Group>();
        for (int i = 0; i < 5; i++) {
            Group group = new Group();
            group.setName("组名" + i);
            group.setMemSum(i + 10);

            groupList.add(group);
        }
        spGroup.setGroupList(groupList);

        mGUserList.clear();
        mGUserList.addAll(spGroup.getFriendList());
        mGroupList.clear();
        mGroupList.addAll(spGroup.getGroupList());

        mCatList.clear();
        mCatList.add(new CatHint("グループ (" + spGroup.getGroupSum() + ")"));
        mCatList.add(new CatHint("シストモ (" + spGroup.getFriendSum() + ")"));

        mFullAdapter.notifyDataSetChanged();

        refreshCounts(spGroup);
    }

    private void setButton(int index) {
        if (index == 1) {
            mCurr = 1;

            mFollowBtn.setBackgroundResource(R.drawable.subnavi_left_pressed);
            mFollowedBtn.setBackgroundResource(R.drawable.subnavi_right_normal);
        } else if (index == 2) {
            mCurr = 2;

            mFollowBtn.setBackgroundResource(R.drawable.subnavi_left_normal);
            mFollowedBtn.setBackgroundResource(R.drawable.subnavi_right_pressed);
        }
    }

    private void refreshCounts(SPGroup spGroup) {
        mFollowBtn.setText(spGroup.getAddSum() + " フォロー");
        mFollowedBtn.setText(spGroup.getAddedSum() + " フォロワー");

        mCatList.clear();
        mCatList.add(new CatHint("グループ (" + spGroup.getGroupSum() + ")"));
        mCatList.add(new CatHint("シストモ (" + spGroup.getFriendSum() + ")"));
    }

    private UIDelegate mGroupRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mLoadingDialog.dismiss();
            mFullLv.onRefreshComplete();

            mGroupPage = ConstData.PAGE_FIRST;
            SPGroup spGroup = (SPGroup) obj.getResultObject();

            refreshCounts(spGroup);

            mGUserList.clear();
            mGUserList.addAll(spGroup.getFriendList());
            mGroupList.clear();
            mGroupList.addAll(spGroup.getGroupList());

            mFullAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mLoadingDialog.dismiss();
            mFullLv.onRefreshComplete();
            toast(errMsg);
        }
    };

    private UIDelegate mGroupLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mFullLv.onRefreshComplete();

            mGroupPage++;
            SPGroup spGroup = (SPGroup) obj.getResultObject();

            refreshCounts(spGroup);

            mGUserList.addAll(spGroup.getFriendList());
            mGroupList.addAll(spGroup.getGroupList());
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mFullLv.onRefreshComplete();
            toast(errMsg);
        }
    };

    private UIDelegate mFriendRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mLoadingDialog.dismiss();
            mIsRequesting = false;
            mFriendLv.setVisibility(View.VISIBLE);
            mFullLv.setVisibility(View.GONE);

            List<User> list = (List<User>) obj.getResultObject();

            mFriendList.clear();
            mFriendList.addAll(list);

            mFriendAdapter.notifyDataSetChanged();
            if (ifId == InterfaceIds.ADD_FRIEND_LIST.IF_ID) {
                setButton(1);
            } else if (ifId == InterfaceIds.ADDED_FRIEND_LIST.IF_ID) {
                setButton(2);
            }
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mLoadingDialog.dismiss();
            mIsRequesting = false;
        }
    };

    private UIDelegate mFriendLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            if (ifId == InterfaceIds.ADD_FRIEND_LIST.IF_ID) {

            } else if (ifId == InterfaceIds.ADDED_FRIEND_LIST.IF_ID) {

            }
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            toast(errMsg);
        }
    };

    private class CatHint {
        public String hint;

        public CatHint(String hint) {
            this.hint = hint;
        }
    }

    private class FullAdapter extends BaseAdapter {
        private final int CATEGORY_HINT = 0, FULL_ITEM = 1;
        private final int TYPE_COUNT = 2;

        private LayoutInflater mInflater;

        public FullAdapter() {
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            int size = mCatList.size() + mGUserList.size() + mGroupList.size();
            return size;
        }

        @Override
        public Object getItem(int position) {
            if (position == 0) {
                return mCatList.get(0);
            } else if (position < mGroupList.size() + 1) {
                return mGroupList.get(position - 1);
            } else if (position == mGroupList.size() + 1) {
                return mCatList.get(1);
            } else if (position > mGroupList.size() + 1) {
                return mGUserList.get(position - 1 - mGroupList.size() - 1);
            }

            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }


        @Override
        public int getItemViewType(int position) {
            int type = -1;

            if (position == 0) {
                type = CATEGORY_HINT;
            } else if (position < mGroupList.size() + 1) {
                type = FULL_ITEM;
            } else if (position == mGroupList.size() + 1) {
                type = CATEGORY_HINT;
            } else if (position > mGroupList.size() + 1) {
                type = FULL_ITEM;
            }

            return type;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            int type = getItemViewType(position);

            if (convertView == null) {
                h = new Holder();
                switch (type) {
                    case CATEGORY_HINT:
                        convertView = mInflater.inflate(R.layout.item_list_separator2, null);
                        h.name = (TextView) convertView.findViewById(R.id.tv_item_list_separator_hint);
                        break;
                    case FULL_ITEM:
                        convertView = mInflater.inflate(R.layout.item_list_member, null);
                        h.check = (ImageView) convertView.findViewById(R.id.iv_item_list_member_check);
                        h.name = (TextView) convertView.findViewById(R.id.tv_item_list_member_name);
                        h.profile = (ImageView) convertView.findViewById(R.id.iv_item_list_member_profile);
                        break;
                }

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            Object item = getItem(position);
            if (item instanceof CatHint) {
                CatHint catHint = (CatHint) item;
                h.name.setText(catHint.hint);
            } else if (item instanceof Group) {
                Group group = (Group) item;
                h.name.setText(group.getName() + " (" + group.getMemSum() + ")");
                h.check.setImageResource(R.drawable.icon_group);
                h.profile.setVisibility(View.GONE);
            } else if (item instanceof User) {
                final User user = (User) item;
                h.name.setText("" + user.getNick());
                h.check.setImageResource(R.drawable.ic_followed);
                h.profile.setVisibility(View.VISIBLE);
                h.profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toProfile(user.getId());
                    }
                });
            }

            return convertView;
        }

        private class Holder {
            public TextView name;
            public ImageView profile;
            public ImageView check;
        }
    }


    private class FriendAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public FriendAdapter() {
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mFriendList.size();
        }

        @Override
        public User getItem(int position) {
            return mFriendList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_member, null);
                h.check = (ImageView) convertView.findViewById(R.id.iv_item_list_member_check);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_member_name);
                h.profile = (ImageView) convertView.findViewById(R.id.iv_item_list_member_profile);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final User user = getItem(position);

            h.name.setText("" + user.getNick());
            h.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProfile(user.getId());
                }
            });
            h.check.setImageResource(mCurr == 1 ?
                    R.drawable.icon_nofollow : R.drawable.icon_nofollow);

            return convertView;
        }

        private class Holder {
            public TextView name;
            public ImageView profile;
            public ImageView check;
        }
    }
}
