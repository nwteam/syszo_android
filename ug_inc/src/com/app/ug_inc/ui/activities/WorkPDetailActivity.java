package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.WorkerInfo;
import com.app.ug_inc.utils.ConstData;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WorkPDetailActivity extends BaseNetActivity {
	private WorkerInfo mWorkerInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workp_detail);

		boolean isWorker = getIntent().getAction().equals(WorkPPickActivity.ACTION_WORKER);
		mWorkerInfo = (WorkerInfo) getIntent().getSerializableExtra("worker");

		enableBack();
		makeTitle(isWorker ? "空き情報" : "現場情報");

		TextView hintPlaceTv = (TextView) findViewById(R.id.tv_profile_hint_area);
		hintPlaceTv.setText(isWorker ? "対応可能地域" : "現場住所");

		TextView hintTimeTv = (TextView) findViewById(R.id.tv_profile_hint_time);
		hintTimeTv.setText(isWorker ? "空き日程" : "日程");

		TextView hintSelfTv = (TextView) findViewById(R.id.tv_profile_hint_self);
		hintSelfTv.setText(isWorker ? "自己紹介" : "詳細等");

		setDatas();
	}

	private void setDatas() {
		setText(R.id.tv_profile_free_time,
				mWorkerInfo.getStartTime() + "~" + mWorkerInfo.getEndTime());
		setText(R.id.tv_profile_people, ConstData.getPeopleById(mWorkerInfo.getPeopleSum()));
		setText(R.id.tv_profile_self, mWorkerInfo.getDescribe());
		setText(R.id.tv_profile_nickname, mWorkerInfo.getUser().getNick());
		setText(R.id.tv_profile_sex, ConstData.getSexById(mWorkerInfo.getUser().getSex()));
		setText(R.id.tv_profile_job, mWorkerInfo.getJobName());
		setText(R.id.tv_profile_area, ConstData.getCityById(mWorkerInfo.getCityId()) + " "
				+ mWorkerInfo.getDistrict());
	}

	private void setText(int resId, String text) {
		((TextView) findViewById(resId)).setText("" + text);
	}

	public void sendMsgClick(View v) {
		if (mWorkerInfo == null) {
			return;
		}

		sendMessage(mWorkerInfo.getUser().getId(), true);
	}

	public void seeProfile(View v) {
		seeProfile(mWorkerInfo.getUser().getId());
	}
}