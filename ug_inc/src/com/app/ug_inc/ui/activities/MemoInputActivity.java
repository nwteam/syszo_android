package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class MemoInputActivity extends BaseActivity {
	private EditText mModifyEt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memo_input);

		makeTitle("メモ入力");
		enableBack();

		Intent intent = getIntent();
		String text = intent == null ? "" : intent.getExtras().getString("text");

		mModifyEt = (EditText) findViewById(R.id.et_setting_input);
		mModifyEt.setText(text);

		ImageView btn = new ImageView(mContext);
		btn.setImageResource(R.drawable.ic_btn_acbar_decided);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultOK();
			}
		});
        addRightAction(btn);
	}

	private void resultOK() {
		String text = mModifyEt.getText().toString();

		Intent intent = new Intent();
		intent.putExtra("result", text);
		setResult(RESULT_OK, intent);
		finish();
	}
}
