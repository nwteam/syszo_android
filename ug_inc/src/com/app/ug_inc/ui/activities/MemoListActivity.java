package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Memo;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.app.ug_inc.utils.ConstData;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MemoListActivity extends BaseListNetActivity {
	private ArrayList<Memo> mMemoList;
	private SparseArray<Boolean> mCheckedArray;

	private PullListview mListview;
	private MemoAdapter mAdapter;

	private ImageView mEditIv;
	private AccountController mAccountController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memo_list);

		makeTitle("メモ");
		enableBack();

		mEditIv = new ImageView(mContext);
		setEditBtn();
        addRightAction(mEditIv);

		mMemoList = new ArrayList<Memo>();
		mCheckedArray = new SparseArray<Boolean>();

		mAdapter = new MemoAdapter();
		mListview = (PullListview) findViewById(R.id.plv_memo_list);
		mListview.setAdapter(mAdapter);
		mListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int pos = position - 1;
				if (mAdapter.isEditMode()) {
					mCheckedArray.put(pos, !mCheckedArray.get(pos));
					mAdapter.notifyDataSetChanged();
				} else {
					Intent intent = new Intent();
					intent.setClass(mContext, MemoActivity.class);
					intent.putExtra("memo", mMemoList.get(pos));
					startActivity(intent);
				}
			}
		});
		mListview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mAccountController.getMemoList(mCurrPage + 1);
				mCurrStatus = STATUS.LOAD_MORE;
			}
		});

		mAccountController = new AccountController(this);
		mAccountController.getMemoList(ConstData.PAGE_FIRST);
		mLoadingDialog.show();
		mCurrStatus = STATUS.REFRESH;

		// test();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAccountController != null) {
			mAccountController.clear();
			mAccountController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_MEMO_LIST.IF_ID) {
			List<Memo> list = (ArrayList<Memo>) obj.getResultObject();
			if (mCurrStatus == STATUS.REFRESH) {
				mCurrPage = ConstData.PAGE_FIRST;
				mMemoList.clear();
			} else if (mCurrStatus == STATUS.LOAD_MORE) {
				judgeLoadMore(list);
				mListview.onRefreshComplete();
			}

			mMemoList.addAll(list);

			mCheckedArray.clear();
			int size = mMemoList.size();
			for (int i = 0; i < size; i++) {
				mCheckedArray.append(i, false);
			}

			mAdapter.notifyDataSetChanged();
		} else if (ifId == InterfaceIds.MEMO_DELETE.IF_ID) {
			toast(getString(R.string.success_delete_memo));

			int count = 0;
			int size = mCheckedArray.size();
			for (int i = 0; i < size; i++) {
				if (mCheckedArray.get(i)) { // checked
					mMemoList.remove(i);
					count++;
				}
			}

			mCheckedArray.clear();
			for (int i = 0; i < count; i++) { // reset checked
				mCheckedArray.append(i, false);
			}

			mAdapter.notifyDataSetChanged();

			disableEdit();
		}
	}

	private void disableEdit() {
		mAdapter.makeEditMode(false);
//		mAcBarView.getLeftBtn().setImageResource(R.drawable.ic_btn_acbar_back);
//		mAcBarView.getLeftBtn().setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				finish();
//			}
//		});
		setEditBtn();
	}

	private void setEditBtn() {
		mEditIv.setImageResource(R.drawable.ic_btn_acbar_edit);
		mEditIv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				enableEdit();
			}
		});
	}

	private void enableEdit() {
		mAdapter.makeEditMode(true);
//		mAcBarView.getLeftBtn().setImageResource(R.drawable.ic_btn_acbar_cancel);
//		mAcBarView.getLeftBtn().setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				disableEdit();
//			}
//		});

		mEditIv.setImageResource(R.drawable.ic_btn_acbar_delete);
		mEditIv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new ZDialog(mContext).setContent(getString(R.string.hint_edit_confirm))
						.setLeftButton(getString(R.string.cancel))
						.setRightButton(getString(R.string.OK), new RightButtonListener() {

							@Override
							public void onRightButtonClick() {
								deleteMessages();
							}
						}).show();
			}
		});
	}

	private void deleteMessages() {
		String ids = "";
		int size = mCheckedArray.size();
		for (int i = 0; i < size; i++) {
			if (mCheckedArray.get(i)) {
				String id = mMemoList.get(i).getTargetUser().getId();
				ids += id;
				ids += ",";
			}
		}

		if (ids.length() == 0) {
			toast(getString(R.string.hint_at_least_select_one));
			return;
		}

		ids = ids.substring(0, ids.length() - 1); // remove last ','

		mAccountController.deleteMemo(ids);
		mLoadingDialog.show();
	}

	@Override
	protected void test() {
		super.test();

		for (int i = 0; i < ConstData.PAGE_SIZE_MEMO; i++) {
			Memo memo = new Memo();
			User user = new User();
			user.setCityId("城市名");
			user.setNick("用户昵称" + i);
			memo.setTime("2014-08-08 12:00:00");
			memo.setTargetUser(user);

			mMemoList.add(memo);
		}

		mCheckedArray.clear();
		int size = mMemoList.size();
		for (int i = 0; i < size; i++) {
			mCheckedArray.append(i, false);
		}
		mAdapter.notifyDataSetChanged();
	}

	private class MemoAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		private boolean mIsEditMode = false;

		public MemoAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		public boolean isEditMode() {
			return mIsEditMode;
		}

		public void makeEditMode(boolean enabled) {
			mIsEditMode = enabled;

			if (mIsEditMode == false) {
				mCheckedArray.clear();
				int size = mMemoList.size();
				for (int i = 0; i < size; i++) {
					mCheckedArray.append(i, false);
				}
			}

			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mMemoList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h = null;
			if (convertView == null) {
				h = new Holder();
				convertView = mInflater.inflate(R.layout.item_list_memo, null);
				h.check = convertView.findViewById(R.id.llayout_item_list_memo_check);
				h.checkImg = (ImageView) convertView.findViewById(R.id.iv_item_list_memo_check);
				h.nick = (TextView) convertView.findViewById(R.id.tv_item_list_memo_nickname);
				h.city = (TextView) convertView.findViewById(R.id.tv_item_list_memo_city);
				h.time = (TextView) convertView.findViewById(R.id.tv_item_list_memo_time);
				convertView.setTag(h);
			} else {
				h = (Holder) convertView.getTag();
			}

			if (mIsEditMode) {
				final int tmpPos = position;
				if (mCheckedArray.valueAt(tmpPos)) { // was checked
					h.checkImg.setImageResource(R.drawable.ic_message_checked);
				} else {
					h.checkImg.setImageResource(R.drawable.ic_message_normal);
				}
				h.check.setVisibility(View.VISIBLE);
				h.check.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						mCheckedArray.put(tmpPos, !mCheckedArray.valueAt(tmpPos));
						notifyDataSetChanged();
					}
				});
			} else {
				h.check.setVisibility(View.GONE);
			}

			Memo memo = mMemoList.get(position);
			User usr = memo.getTargetUser();

			h.nick.setText("" + usr.getNick());
			h.city.setText("【" + usr.getCity() + "】");
			h.time.setText("" + memo.getTime());

			return convertView;
		}

		private class Holder {
			private TextView nick;
			private TextView city;
			private TextView time;

			public View check;
			public ImageView checkImg;
		}
	}
}