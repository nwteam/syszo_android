package com.app.ug_inc.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.HotSearch;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.utils.ConstData;

import java.util.ArrayList;
import java.util.List;

public class ChiebSearchActivity extends BaseListNetActivity {

    private EditText mKeywordEt;
    private ListView mListview;

    private List<String> mKeywordList;
    private ListAdapter mAdapter;

    private TextView mTimeTv;

    private ClosureController mClosureController;

    public static final String ACTION_CHIEB_SEARCH_NEED_FINISH = "action.ACTION_CHIEB_NEED_FINISH";

    private class FinishReceiver extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            registerReceiver(this, new IntentFilter(ACTION_CHIEB_SEARCH_NEED_FINISH));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    private FinishReceiver mFinishReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_search);

        makeTitle("知恵袋");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        mKeywordList = new ArrayList<String>();

        mKeywordEt = (EditText) findViewById(R.id.et_chieb_search_keyword);

        mAdapter = new ListAdapter();
        mListview = (ListView) findViewById(R.id.lv_chieb_search_hot);
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String word = mKeywordList.get(position);
                toSearch(word);
            }
        });

        mTimeTv = (TextView) findViewById(R.id.lv_chieb_search_update_time);

        mClosureController = new ClosureController(this);
        mClosureController.getClosureHotWordList(ConstData.PAGE_FIRST);

        findViewById(R.id.btn_chieb_search_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String kw = mKeywordEt.getText().toString();
                if (checkXX(TextUtils.isEmpty(kw), "キーワード")) {
                    return;
                }
                toSearch(kw);
            }
        });

        mFinishReceiver = new FinishReceiver();
        mFinishReceiver.register();
    }

    private void toSearch(String word) {
        Intent intent = new Intent();
        intent.setClass(mContext, ChiebSearchResultActivity.class);
        intent.putExtra("word", word);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mClosureController != null) {
            mClosureController.clear();
            mClosureController = null;
        }

        mFinishReceiver.unregister();
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < 10; i++) {
            mKeywordList.add("热门词汇" + i);
        }

        mAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_CLOSURE_HOT_WORD_LIST.IF_ID) {
            HotSearch search = (HotSearch) obj.getResultObject();

            String time = search.getUpdateTime();
            mTimeTv.setText(time + " 更新");

            mKeywordList = search.getHotwordList();
            mAdapter.notifyDataSetChanged();
        }
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mKeywordList.size();
        }

        @Override
        public String getItem(int arg0) {
            return mKeywordList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chied_search, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_search_word);
                h.num = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_search_number);
                h.bg = arg1.findViewById(R.id.rlayout_item_list_chieb_search_bg);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            String word = getItem(arg0);
            h.content.setText("" + word);
            h.num.setText("" + (arg0 + 1));
            h.bg.setBackgroundColor(arg0 % 2 == 0 ? 0xFFFAFAFA : 0xFFFFFFFF);

            if (arg0 == 0) {
                h.num.setBackgroundColor(0xFFFFC740);
            } else if (arg0 == 1) {
                h.num.setBackgroundColor(0xFFC1C94A);
            } else if (arg0 == 2) {
                h.num.setBackgroundColor(0xFFC0A78C);
            } else {
                h.num.setBackgroundColor(0xFF76BE9D);
            }

            return arg1;
        }

        private class Holder {
            private TextView content;
            private TextView num;
            private View bg;
        }
    }

}
