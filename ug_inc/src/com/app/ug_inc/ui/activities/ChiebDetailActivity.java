package com.app.ug_inc.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.ChiebComment;
import com.app.ug_inc.models.ClosureItem;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.ClosureController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.ui.views.PullListview;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.TimeUtil;
import com.app.ug_inc.utils.ZLog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.ArrayList;
import java.util.List;

// because of content's length
@Deprecated
public class ChiebDetailActivity extends BaseListNetActivity {

    private PullListview mListview;
    private ListAdapter mAdapter;

    private List<ChiebComment> mCommentList;

    private ClosureController mClosureController;
    private ClosureController mRefreshController;
    private ClosureController mLoadmoreController;

    private TextView mUnameTv;
    private TextView mContentTv;
    private TextView mTitleTv;
    private TextView mCountTv;
    private TextView mTimeTv;
    private ImageView mPicIv;

    private EditText mSendEt;

    private boolean mIsDetailLoaded = false;
    private Chieb mChieb;

    private ChiebComment mRequestComment;

    private View mEditBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chieb_detail);

        makeTitle("知恵袋");
        makeIcon(R.drawable.ic_acbar_chieb);
        makeTitleBg(0xFF76A4CE);
        enableBack();

        mChieb = (Chieb) getIntent().getSerializableExtra("chieb");

        mEditBtn = addRightAction(R.drawable.ic_btn_acbar_contribute, new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mIsDetailLoaded) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, ChiebNewActivity.class);
                    intent.putExtra("chieb", mChieb);
                    startActivity(intent);
                }
            }
        });
        mEditBtn.setVisibility(View.INVISIBLE);

        View header = makeHeader();

        mCommentList = new ArrayList<ChiebComment>();
        mAdapter = new ListAdapter();

        mListview = (PullListview) findViewById(R.id.plv_chieb_detail);
//        mListview.getRefreshableView().addHeaderView(header, null, false);
        mListview.getRefreshableView().addHeaderView(header, null, false);
        mListview.setAdapter(mAdapter);
        mListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mClosureController.getClosureDetail(mChieb.getId());
                mRefreshController.getClosureCommentList(mChieb.getId(), ConstData.PAGE_FIRST);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mLoadmoreController.getClosureCommentList(mChieb.getId(), mCurrPage + 1);
            }
        });

        mSendEt = (EditText) findViewById(R.id.et_chieb_detail_chat);

        mClosureController = new ClosureController(this);
        mClosureController.getClosureDetail(mChieb.getId());

        mRefreshController = new ClosureController(mRefreshDelegate);
        mRefreshController.getClosureCommentList(mChieb.getId(), ConstData.PAGE_FIRST);
        mLoadmoreController = new ClosureController(mLoadmoreDelegate);

        findViewById(R.id.btn_chieb_detail_send).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = mSendEt.getText().toString();
                if (checkXX(TextUtils.isEmpty(content), "送信内容")) {
                    return;
                }

                mClosureController.commentClosure(mChieb.getId(), content);
            }
        });
    }

    private View makeHeader() {
        View header = getLayoutInflater().inflate(R.layout.item_list_chieb_detail_header, null);
        mTitleTv = (TextView) header.findViewById(R.id.tv_item_list_chieb_detail_header_title);
        mCountTv = (TextView) header.findViewById(R.id.tv_item_list_chieb_detail_header_count);
        mTimeTv = (TextView) header.findViewById(R.id.tv_item_list_chieb_detail_header_time);
        mUnameTv = (TextView) header.findViewById(R.id.tv_item_list_chieb_detail_header_uname);
        mContentTv = (TextView) header.findViewById(R.id.tv_item_list_chieb_detail_header_content);
        mPicIv = (ImageView) header.findViewById(R.id.iv_item_list_chieb_detail_header_img);

        String title = mChieb.getTitle();
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new BackgroundColorSpan(0xFFFFFF00), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTitleTv.setText(ss);
        mCountTv.setText("" + mChieb.getCommentCount());
        mTimeTv.setText("" + mChieb.getTime());
        return header;
    }

    @Override
    protected void test() {
        super.test();

        for (int i = 0; i < ConstData.PAGE_SIZE_CHIEB; i++) {
            ChiebComment comment = new ChiebComment();
            comment.setTime("2014-12-31 10:00:00");
            comment.setContent("评论内容fasdonfdnaosfndioasnfiodasnfas");

            User user = new User();
            user.setNick("用户" + i);
            comment.setUser(user);

            mCommentList.add(comment);
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearController(mClosureController);
        clearController(mRefreshController);
        clearController(mLoadmoreController);
    }

    private UIDelegate mRefreshDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();

            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            mCommentList.clear();
            mCurrPage = ConstData.PAGE_FIRST;
            mCommentList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    private UIDelegate mLoadmoreDelegate = new UIDelegate() {
        @Override
        public void onRequestSuccess(int ifId, NetResult obj) {
            mListview.onRefreshComplete();
            List<ChiebComment> list = (ArrayList<ChiebComment>) obj.getResultObject();
            judgeLoadMore(list);

            mCommentList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onRequestError(int ifId, String errMsg) {
            mListview.onRefreshComplete();
            toast(errMsg);
        }
    };

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.GET_CLOSURE_DETAIL.IF_ID) {
            Chieb chieb = (Chieb) obj.getResultObject();

            if (Account.getUserId().equals(chieb.getUser().getId())) {
                mEditBtn.setVisibility(View.VISIBLE);
            }

            mUnameTv.setText("" + chieb.getUser().getNick());
            mContentTv.setText("" + chieb.getContent());

            mChieb.setUser(chieb.getUser());
            mChieb.setContent(chieb.getContent());
            mChieb.setPicUrl(chieb.getPicUrl());

            String picUrl = chieb.getPicUrl();
            if (!TextUtils.isEmpty(picUrl)) {
                mPicIv.setVisibility(View.VISIBLE);
                ZImageLoader.asyncLoadImage(picUrl, mPicIv);
            } else {
                mPicIv.setVisibility(View.GONE);
            }

            mIsDetailLoaded = true;
        } else if (ifId == InterfaceIds.COMMENT_CLOSURE.IF_ID) {
            toast("送信しました。");
            mSendEt.setText("");
            mRefreshController.getClosureCommentList(mChieb.getId(), ConstData.PAGE_FIRST);
        } else if (ifId == InterfaceIds.LIKE_COLSURE.IF_ID) {
            mRequestComment.setLiked(true);
            mAdapter.notifyDataSetChanged();
        } else if (ifId == InterfaceIds.DELETE_COLSURE_COMMENT.IF_ID) {
            mCommentList.remove(mRequestComment);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
            String id = data.getStringExtra("id");
            String newContent = data.getStringExtra("content");

            for (ChiebComment comment : mCommentList) {
                if (comment.getId().equals(id)) {
                    comment.setContent(newContent);
                    break;
                }
            }

            mAdapter.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class ListAdapter extends BaseAdapter {
        public final int RED = 0xFFBE5959;
        public final int WHITE = 0xFFFFFFFF;
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mCommentList.size();
        }

        @Override
        public ChiebComment getItem(int arg0) {
            return mCommentList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_chieb_comment, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_content);
                h.time = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_time);
                h.like = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_like);
                h.del = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_del);
                h.edit = (ImageView) arg1.findViewById(R.id.iv_item_list_chieb_comment_edit);
                h.avatar = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_avatar);
                h.selfLayout = arg1.findViewById(R.id.llayout_item_list_chieb_comment_del_edit);
                h.uname = (TextView) arg1.findViewById(R.id.tv_item_list_chieb_comment_uname);
                h.vipIV = (ImageView) arg1.findViewById(R.id.tv_item_list_chieb_comment_vip);
                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            final ChiebComment item = getItem(arg0);

            h.time.setText("" + item.getTime());
            h.content.setText("" + item.getContent());

            User user = item.getUser();
            if (Account.getUserId().equals(user.getId())) {
                h.avatar.setImageResource(R.drawable.icon_user_on);
                h.uname.setTextColor(0xFFF1A90F);
                h.selfLayout.setVisibility(View.VISIBLE);
                h.like.setVisibility(View.INVISIBLE);
                h.del.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        delete(item);
                    }
                });

                h.edit.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        edit(item);
                    }
                });
            } else {
                h.avatar.setImageResource(R.drawable.icon_user_off);
                h.uname.setTextColor(0xFF393A40);
                h.selfLayout.setVisibility(View.INVISIBLE);
                h.like.setVisibility(View.VISIBLE);
                if (item.isLiked()) {
                    h.like.setEnabled(false);
                    h.like.setImageResource(R.drawable.btn_iine_pressed);
                } else {
                    h.like.setEnabled(true);
                    h.like.setImageResource(R.drawable.btn_iine_normal);
                    h.like.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            like(item);
                        }
                    });
                }
            }

            if(user.getVip_flg() == 1){
                h.vipIV.setVisibility(View.VISIBLE);
            }else{
                h.vipIV.setVisibility(View.GONE);
            }
            h.uname.setText(user.getNick());

            return arg1;
        }

        private void like(ChiebComment comment) {
            mRequestComment = comment;
            mClosureController.likeClosure(mChieb.getId(), mRequestComment.getId());
            mLoadingDialog.show();
        }

        private void edit(ChiebComment comment) {
            Intent intent = new Intent();
            intent.setClass(mContext, ChiebCommentEditActivity.class);
            intent.putExtra("id", comment.getId());
            intent.putExtra("content", comment.getContent());
            startActivityForResult(intent, 5);
        }

        private void delete(final ChiebComment comment) {
            new AlertDialog.Builder(mContext).setMessage("削除を確認しますか?").setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mRequestComment = comment;
                    mClosureController.deleteClosureComment(mRequestComment.getId());
                    mLoadingDialog.show();
                }
            }).setNegativeButton(getString(R.string.cancel), null).show();
        }

        private class Holder {
            private TextView content;
            private TextView time;
            private TextView uname;
            private ImageView avatar;
            private View selfLayout;
            private ImageView like;
            private ImageView del;
            private ImageView edit,vipIV;
            private View bg;
        }
    }
}
