package com.app.ug_inc.ui.activities;

import java.util.ArrayList;

import com.app.ug_inc.R;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.MySeriesController;
import com.app.ug_inc.ui.utils.FontUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MySeriesMainActivity extends BaseNetActivity {
	private ArrayList<SimpleKV> mCategoryList;

	private ListAdapter mAdapter;

	private MySeriesController mMySeriesController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myseries_main);

		makeTitle("俺のシリーズ");
		enableBack();

		mCategoryList = new ArrayList<SimpleKV>();

		int w = getResources().getDisplayMetrics().widthPixels;
		int h = (int) ((double) w * 184 / 1280);
		ImageView iv = (ImageView) findViewById(R.id.iv_myseries_main_banner);
		iv.setLayoutParams(new LinearLayout.LayoutParams(w, h));

		mAdapter = new ListAdapter();
		ListView lv = (ListView) findViewById(R.id.lv_myseries_main_list);
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent();
				intent.setClass(mContext, MySeriesListActivity.class);
				intent.putExtra("cat", mCategoryList.get(arg2));
				startActivity(intent);
			}
		});

		// test();

		for (int i = 0; i < TVIEW_IDS.length; i++) {
			FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
					FontUtil.FONT_ROUNDED_MPLUS);
		}

		mMySeriesController = new MySeriesController(this);
		mMySeriesController.getMySeriesCatList();
		mLoadingDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMySeriesController != null) {
			mMySeriesController.clear();
			mMySeriesController = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		mCategoryList = (ArrayList<SimpleKV>) obj.getResultObject();
		mAdapter.notifyDataSetChanged();
	}

	private final int[] TVIEW_IDS = new int[] { R.id.tv_myseries_main_hint1, };

	@Override
	protected void test() {
		super.test();
		for (int i = 0; i < 5; i++) {
			SimpleKV kv = new SimpleKV();
			kv.setKey("" + i);
			kv.setValue("俺の分类" + i);

			mCategoryList.add(kv);
		}
		mAdapter.notifyDataSetChanged();
	}

	public void addSeries(View v) {
		Intent intent = new Intent();
		intent.setClass(mContext, MySeriesAddCategoryActivity.class);
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			SimpleKV kv = (SimpleKV) data.getSerializableExtra("result");
			mCategoryList.add(kv);
		}
	}

	private class ListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListAdapter() {
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			return mCategoryList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mCategoryList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			Holder h = null;
			if (arg1 == null) {
				h = new Holder();
				arg1 = mInflater.inflate(R.layout.item_list_myseries_main, null);
				h.name = (TextView) arg1.findViewById(R.id.tv_item_list_myseries_main_name);
				FontUtil.makeTextViewFont(h.name, FontUtil.FONT_ROUNDED_MPLUS);

				arg1.setTag(h);
			} else {
				h = (Holder) arg1.getTag();
			}

			h.name.setText("" + mCategoryList.get(arg0).getValue());

			return arg1;
		}

		private class Holder {
			public TextView name;
		}
	}
}
