package com.app.ug_inc.ui.activities;

import net.nend.android.NendAdView;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.ui.views.UnFollowConfirmDialog;
import com.app.ug_inc.utils.ConstData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * 其他人个人页面
 * Created by 昕 on 2015/5/15.
 */
public class ProfileMePrevActivity extends BaseNetActivity {
    private static final int RIGHT_SECOND_INDEX = 0xFF76BE9A;
    private ImageView mMessageIv;
    private ImageView mAddFriendsIv;
    private TextView mNicknameTv;
    private TextView mEmailTv;
    private TextView mSexTv;
    private TextView mAddressTv;
    private TextView mJobTv;
    private TextView mCompanyScaleTv;
    private TextView mPositionTv;
    private TextView mBudgetScaleTv;
    private TextView mHistoryTv;
    private TextView mSoftwareTv;
    private TextView mHardwareTv;
    private TextView mCertificationTv;
//    private TextView mAnnualSalaryTv;
    private TextView mFriendsTv;
    private TextView mFavourTv;
    private TextView mSelfIntroductionTv;
    private User mUser;
//    private AdView mAdView;
//    private NendAdView mNendAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_others);
        mContext = ProfileMePrevActivity.this;
        initView();

        mUser = (User) getIntent().getSerializableExtra("user");
        setParam();
    }

    private void initView() {
        makeTitle("プロフィール");
        enableBack();
        makeIcon(R.drawable.micon_systomo);
        mMessageIv = (ImageView) findViewById(R.id.iv_profile_message);
        mMessageIv.setVisibility(View.INVISIBLE);
        mAddFriendsIv = (ImageView) findViewById(R.id.iv_prifile_add_friend);
        mAddFriendsIv.setVisibility(View.INVISIBLE);
        mNicknameTv = (TextView) findViewById(R.id.tv_profile_nickname);
        mEmailTv = (TextView)findViewById(R.id.tv_profile_email);
        mSexTv = (TextView) findViewById(R.id.tv_profile_sex);
        mAddressTv = (TextView) findViewById(R.id.tv_profile_address);
        mJobTv = (TextView) findViewById(R.id.tv_profile_job);
        mCompanyScaleTv = (TextView) findViewById(R.id.tv_profile_company_scale);
        mPositionTv = (TextView) findViewById(R.id.tv_profile_position);
        mBudgetScaleTv = (TextView) findViewById(R.id.tv_profile_budget_scale);
        mHistoryTv = (TextView) findViewById(R.id.tv_profile_history);
        mSoftwareTv = (TextView) findViewById(R.id.tv_profile_software);
        mHardwareTv = (TextView) findViewById(R.id.tv_profile_hardware);
        mCertificationTv = (TextView) findViewById(R.id.tv_profile_certification);
//        mAnnualSalaryTv = (TextView) findViewById(R.id.tv_profile_annual_salary);
        mFriendsTv = (TextView) findViewById(R.id.tv_profile_friends);
        mFavourTv = (TextView) findViewById(R.id.tv_profile_favour);
        mSelfIntroductionTv = (TextView) findViewById(R.id.tv_profile_self_introduction);

       /* mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());*/
        
//        mNendAdView = (NendAdView) findViewById(R.id.nend);
//        mNendAdView.loadAd();
    }

    private void setParam() {
        mNicknameTv.setText(mUser.getNick() + "");
        mEmailTv.setText(mUser.getEmail()+"");
        mSexTv.setText(ConstData.getSexById(mUser.getSex()) + "");
        mAddressTv.setText(mUser.getCity() + "");
        mJobTv.setText(mUser.getIndustry() + "");
        mCompanyScaleTv.setText(mUser.getSizeCompany() + "");
        mPositionTv.setText(mUser.getJob() + "");
        mBudgetScaleTv.setText(mUser.getRegMoney() + "");
        mHistoryTv.setText(mUser.getCalendar() + "");
        mSoftwareTv.setText(mUser.getSoftware() + "");
        mHardwareTv.setText(mUser.getHardware() + "");
        mCertificationTv.setText(mUser.getCertifaction() + "");
//        mAnnualSalaryTv.setText(mUser.getAllyear() + "");
        mFriendsTv.setText(mUser.getFriends() + "");
        mFavourTv.setText(mUser.getFavour() + "");
        mSelfIntroductionTv.setText(mUser.getSelfIntro() + "");

        makeAddFriend("1".equals(mUser.getIsFollowed()));
    }

    private void makeAddFriend(boolean isFriend) {
        if (isFriend) {
            mAddFriendsIv.setImageResource(R.drawable.ic_followed);
        } else {
            mAddFriendsIv.setImageResource(R.drawable.btn_nofollow_big);
        }
    }
}
