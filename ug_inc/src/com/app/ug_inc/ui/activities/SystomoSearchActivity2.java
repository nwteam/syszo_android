package com.app.ug_inc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Chieb;
import com.app.ug_inc.models.SimpleKV;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.GroupController;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 2015/5/10.
 */
public class SystomoSearchActivity2 extends BaseNetActivity {

    private GroupController mGroupController;

    private List<SimpleKV> mCatList;
    private ListAdapter mAdapter;

    private String mGroupCatId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systomo_search2);

        makeTitle("シストモ追加");
        makeIcon(R.drawable.micon_systomo);
        enableBack();

        mCatList = new ArrayList<SimpleKV>();
        mAdapter = new ListAdapter();

        ListView lv = (ListView) findViewById(R.id.lv_systomo_search_category);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setAction(SystomoSearchResultActivity.ACTION_FROM_CATEGORY);
                intent.putExtra("catId", mCatList.get(position).getKey());
                intent.putExtra("groupId", mGroupCatId);
                intent.setClass(mContext, SystomoSearchResultActivity.class);
                startActivity(intent);
            }
        });

        mGroupCatId = "" + getIntent().getIntExtra("catId", 1);
        String name = getIntent().getStringExtra("catName");

        ((TextView) findViewById(R.id.tv_item_list_separator_hint)).setText("カテゴリ検索 - " + name);

        mGroupController = new GroupController(this);
        mGroupController.groupCategory(mGroupCatId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGroupController != null) {
            mGroupController.clear();
            mGroupController = null;
        }
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);

        List<SimpleKV> list = (List<SimpleKV>) obj.getResultObject();
        mCatList.clear();
        mCatList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mCatList.size();
        }

        @Override
        public SimpleKV getItem(int arg0) {
            return mCatList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            Holder h = null;
            if (arg1 == null) {
                h = new Holder();
                arg1 = mInflater.inflate(R.layout.item_list_systomo_search2, null);
                h.content = (TextView) arg1.findViewById(R.id.tv_item_list_systomo_search2_name);

                arg1.setTag(h);
            } else {
                h = (Holder) arg1.getTag();
            }

            SimpleKV item = getItem(arg0);
            h.content.setText("" + item.getValue());

            return arg1;
        }

        private class Holder {
            private TextView content;
        }
    }
}
