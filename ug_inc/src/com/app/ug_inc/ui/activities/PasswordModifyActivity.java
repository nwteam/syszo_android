package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.SettingController;
import com.app.ug_inc.ui.utils.FontUtil;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class PasswordModifyActivity extends BaseNetActivity {
    private EditText mOrgPwdEt;
    private EditText mNewPwdEt1;
    private EditText mNewPwdEt2;

    private SettingController mSettingController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        makeTitle("パスワード再発行");
        enableBack();

        addRightAction(R.drawable.ic_btn_acbar_change, new OnClickListener() {

            @Override
            public void onClick(View v) {
                modifyPwd();
            }
        });

        mOrgPwdEt = (EditText) findViewById(R.id.et_password_reset_hint_orgin_pwd);
        mNewPwdEt1 = (EditText) findViewById(R.id.et_password_reset_hint_new_pwd1);
        mNewPwdEt2 = (EditText) findViewById(R.id.et_password_reset_hint_new_pwd2);

        for (int i = 0; i < TVIEW_IDS.length; i++) {
            FontUtil.makeTextViewFont((TextView) findViewById(TVIEW_IDS[i]),
                    FontUtil.FONT_ROUNDED_MPLUS);
        }

        mSettingController = new SettingController(this);
    }

    private final int[] TVIEW_IDS = {R.id.tv_password_reset_hint_orgin_pwd1,
            R.id.tv_password_reset_hint_orgin_pwd2,
            R.id.tv_password_reset_hint_pwd1,
            R.id.tv_password_reset_hint_new_pwd1,
            R.id.tv_password_reset_hint_new_pwd2,
            R.id.tv_password_reset_hint_pwd2, R.id.tv_password_reset_hint_pwd3,
            R.id.tv_password_reset_hint_pwd4,};

    private void modifyPwd() {
        String old = mOrgPwdEt.getText().toString();
        String new1 = mNewPwdEt1.getText().toString();
        String new2 = mNewPwdEt2.getText().toString();

        if (checkXX(TextUtils.isEmpty(old), "現在のパスワード")) {
            return;
        }

        if (checkXX(TextUtils.isEmpty(new1), "新しいパスワード")) {
            return;
        }

        if (new1.equals(old)) {
            toast(getString(R.string.hint_input_password_cannot_equals));
            return;
        }

        if (new1.length() < 6) {
            toast(getString(R.string.hint_input_password_shorter_than_6));
            return;
        }

        if (!new1.equals(new2)) {
            toast(getString(R.string.hint_input_password_not_equals));
            return;
        }


        mSettingController.modifyPwd(old, new1);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.MODIFY_PWD.IF_ID) {
            toast(getString(R.string.success_password_modified));
            finish();
        }
    }
}
