package com.app.ug_inc.ui.activities;

public class ZActivityManager {
	private static ZActivityManager INSTANCE = null;

	public static ZActivityManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ZActivityManager();
		}

		return INSTANCE;
	}

	public boolean isChatForeground = false;
	public String chatWithUid = "";

	/**
	 * in this instance, we use MenuActivity to judge if app is running.
	 */
	public boolean isAppRunning = false;
}
