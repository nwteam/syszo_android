package com.app.ug_inc.ui.activities;

import java.util.Arrays;
import java.util.List;

import net.nend.android.NendAdView;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.NetConst;
import com.app.ug_inc.ui.views.ZDialog;
import com.app.ug_inc.ui.views.ZDialog.RightButtonListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SettingActivity extends BaseActivity {
    private final String[] ITEMS = new String[]{
            "プロフィール編集",
            "プッシュ通知",
            "アカウント",
            "お問い合わせ",
            "利用規約",
            "ログアウト"
    };
    // "料金について", // 0
    // "ヘルプ",// 0
    // "資金決済法に基づく表示",// 0
    // "運営会社", // 0
    // "プライバシーポリシー",// 0

    private List<String> mItemList;
//    private AdView mAdView;
//    private NendAdView mNendAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_list);
        initView();
    }

    private void initView() {
        makeIcon(R.drawable.ic_btn_acbar_setting_normal);
        makeTitle("設定");
        enableBack();
        mItemList = Arrays.asList(ITEMS);
        ListView lv = (ListView) findViewById(R.id.lv_setting_list);
        lv.setAdapter(new ListAdapter());
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent();
                switch (position) {
                    case 0:
                        intent.setClass(mContext, ProfileSettingActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent.setClass(mContext, PushSettingActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent.setClass(mContext, AccountActivity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        Intent data = new Intent(Intent.ACTION_SENDTO);
                        data.setData(Uri.parse("mailto:support@syszo.com"));
                        data.putExtra(Intent.EXTRA_SUBJECT, "お問い合わせ");
                        String extraText = getString(R.string.app_name) + "-お問い合わせ-";
                        data.putExtra(Intent.EXTRA_TEXT, extraText);
                        startActivity(data);
                        // Intent i = new Intent(Intent.ACTION_SEND);
                        // i.putExtra(Intent.EXTRA_EMAIL, new String[] {
                        // "info@gen-para.com" });
                        // i.putExtra(Intent.EXTRA_SUBJECT, "【現パラ】お問い合わせ");
                        // i.putExtra(Intent.EXTRA_TEXT, "");
                        // startActivity(Intent.createChooser(i, "メールを送る"));
                        // startActivity(i);
                        break;
                    case 4:
                        toWebActivity(ITEMS[4], NetConst.SERVER_URL + "new/01.html");
                        break;
                    case 5:
                        logout();
                        break;
                    default:
                        break;
                }
            }
        });
      /*  mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());*/
        
//        mNendAdView = (NendAdView) findViewById(R.id.nend);
//        mNendAdView.loadAd();
    }

    private void logout() {
        new AlertDialog.Builder(mContext).setMessage("ログアウト確認しますか")
                .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Account.clear();

                        sendBroadcast(new Intent(ACTION_LOGOUT));

                        Intent intent = new Intent();
                        intent.setClass(mContext, EntranceActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null)
                .show();
    }

    private void toWebActivity(String title, String url) {
        Intent intent = new Intent();
        intent.setClass(mContext, WebActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_setting,
                        null);
                h.name = (TextView) convertView
                        .findViewById(R.id.tv_item_list_settting_name);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            h.name.setText("" + mItemList.get(position));

            return convertView;
        }

        private class Holder {
            private TextView name;
        }

    }
}
