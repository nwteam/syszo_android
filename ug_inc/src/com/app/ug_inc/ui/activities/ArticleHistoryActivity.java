package com.app.ug_inc.ui.activities;

import com.app.ug_inc.R;
import com.app.ug_inc.models.Article;
import com.app.ug_inc.ui.utils.ZImageLoader;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ArticleHistoryActivity extends BaseNetActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_article_his_detail);

		makeTitle("掲示板投稿履歴");
		enableBack();

		Article article = (Article) getIntent().getSerializableExtra("article");

		TextView timeTv = (TextView) findViewById(R.id.tv_article_his_detail_time);
		timeTv.setText("投稿時間:" + article.getTime());
		TextView titleTv = (TextView) findViewById(R.id.tv_article_his_detail_title);
		titleTv.setText("" + article.getTitle());
		TextView contentTv = (TextView) findViewById(R.id.tv_article_his_detail_content);
		contentTv.setText("" + article.getContent());
		TextView catTv = (TextView) findViewById(R.id.tv_article_his_detail_cat);
		catTv.setText("" + article.getCategory());

		ImageView iv = (ImageView) findViewById(R.id.iv_article_his_detail_image);
		if (TextUtils.isEmpty(article.getPicThumb())) {
			iv.setVisibility(View.GONE);
		} else {
			ZImageLoader.asyncLoadImage(article.getPicThumb(), iv);
		}
	}
}