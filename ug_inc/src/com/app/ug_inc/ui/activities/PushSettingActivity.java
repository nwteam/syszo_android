package com.app.ug_inc.ui.activities;

import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.SharedPreferenceHelper;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.SettingController;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PushSettingActivity extends BaseNetActivity {
    private ListAdapter mAdapter;

    private List<Item> mItemList;

    private SettingController mSettingController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_setting);

        makeTitle("プッシュ通知");

        enableBack();

        boolean push = SharedPreferenceHelper.getBoolean("push", true);
        mItemList = new ArrayList<Item>();
        mItemList.add(new Item("1", "プッシュ通知", push));

        mAdapter = new ListAdapter();
        ListView lv = (ListView) findViewById(R.id.lv_push_setting);
        lv.setAdapter(mAdapter);

        mSettingController = new SettingController(this);
        mSettingController.pushSwitch(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSettingController != null) {
            mSettingController.clear();
            mSettingController = null;
        }
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.SETTING_PUSH_SWITCH.IF_ID) {
            Boolean trigger = (Boolean) obj.getResultObject();

            switchTrigger(trigger);
        }
    }

//    private String getSwitched() {
//        return mItemList.get(0).switched ? "1" : "2";
//    }

    private void switchTrigger(boolean trigger) {
        mItemList.get(0).switched = trigger;
        mAdapter.notifyDataSetChanged();

        SharedPreferenceHelper.getEditor().putBoolean("push", trigger).commit();
    }

    private class ListAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public ListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(
                        R.layout.item_list_push_setting, null);
                h.switchPic = (ImageView) convertView
                        .findViewById(R.id.iv_item_list_push_setting_switch);
                h.name = (TextView) convertView
                        .findViewById(R.id.tv_item_list_push_setting_name);
                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            final Item item = mItemList.get(position);
            h.name.setText("" + item.name);
            h.switchPic
                    .setImageResource(item.switched ? R.drawable.img_push_switch_on
                            : R.drawable.img_push_switch_off);
            h.switchPic.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    mSettingController.pushSwitch((!item.switched) ? "1" : "2"); // reverse
                    mLoadingDialog.show();

//                    item.switched = !item.switched;
//                    mAdapter.notifyDataSetChanged();

//                    SharedPreferenceHelper.getEditor().putBoolean("push", item.switched).commit();
                }
            });

            return convertView;
        }

        private class Holder {
            public TextView name;
            public ImageView switchPic;
        }
    }

    private class Item {
        public Item(String id, String name, boolean switched) {
            this.id = id;
            this.name = name;
            this.switched = switched;
        }

        public boolean switched;
        public String name;
        public String id;
    }
}
