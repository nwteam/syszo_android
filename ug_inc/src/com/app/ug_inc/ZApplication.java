package com.app.ug_inc;

import com.app.ug_inc.datas.Account;
import com.app.ug_inc.datas.SharedPreferenceHelper;
import com.app.ug_inc.ui.utils.ZImageLoader;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.ZLog;

import android.app.Application;

import cn.jpush.android.api.JPushInterface;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.HashMap;

public class ZApplication extends Application {

    // Google Analytics 申请的项目ID
    private static final String PROPERTY_ID = "UA-62303596-1";

    // Logging TAG
    // private static final String TAG = "MyApp";
    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, // 追踪本程序
        GLOBAL_TRACKER, // 追踪公司应用
        ECOMMERCE_TRACKER, // 跟踪电子交易
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public ZApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ConstData.init(this);

        SharedPreferenceHelper.init(this);

        Account.init();

        JPushInterface.init(this);
        JPushInterface.setDebugMode(true);

        ZLog.DEBUG = true;

        initImageLoader();
        ZImageLoader.init();
    }

    private void initImageLoader() {
        int screenW = getResources().getDisplayMetrics().widthPixels;
        int screenH = getResources().getDisplayMetrics().heightPixels;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .memoryCacheExtraOptions(screenW, screenH)
                        // .discCacheExtraOptions(screenW, screenH,
                        // CompressFormat.JPEG,75, null)
                        // .taskExecutor(...)
                        // .taskExecutorForCachedImages(...)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13)
                        // .discCache(new UnlimitedDiscCache(cacheDir))
                .diskCacheSize(50 * 1024 * 1024).diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .imageDownloader(new BaseImageDownloader(this))
                .imageDecoder(new BaseImageDecoder(true)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                        // .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    /**
     * 注册追踪器 tracher
     *
     * @param trackerId
     * @return
     */
    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
                    .newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
                    .newTracker(PROPERTY_ID) : analytics
                    .newTracker(R.xml.app_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }
}
