package com.app.ug_inc.net;

public interface NetParams {
	public String QUERY_TIME = "query_time";
	public String QUERY_WORD = "query_word";
	public String FREE_WORD = "free_word";
	public String REPLY_CONTENT = "reply_content";
	public String PAGE_SIZE = "p_size";
	public String PAGE_NUM = "p_num";
	public String PIC_URL = "pic_url";
	public String PIC_THUMB_URL = "pic_thumb_url";
	public String PWD_OLD = "pwd_old";
	public String PWD_NEW = "pwd_new";
	public String FEEDBACK_TYPE = "feedback_type";
	public String FEEDBACK_CONTENT = "feedback_content";
	public String RATE_SCORE = "rate_score";
	public String RATE_CONTENT = "rate_content";
	public String START_TIME = "start_time";
	public String END_TIME = "end_time";
	public String DETAIL = "detail";

	// ------ user ------
	public String TARGET_UID = "target_uid";
	public String USER_ID = "user_id";
	public String USER_TOKEN = "user_token";
	public String USER_NICK = "user_nick";
	public String USER_EMAIL = "user_email";
	public String USER_PWD = "user_pwd";
	public String USER_SEX = "user_sex";
	public String USER_BIRTH = "user_birth";
	public String USER_BLOOD = "blood";
	public String USER_HEALTH = "health";
	public String JOB = "job";
	public String JOB_ID = "job_id";
	public String JOB_NAME = "job_name";
	public String CITY_ID = "city_id";
	public String ADDRESS = "address";
	public String DIST_ID = "dist_id";
	public String USER_DIST = "district";
	public String CERTIF_ID = "certification_id";
	public String USER_SELF_INTRO = "self_intro";
	public String USER_RATE = "rate";
	public String USER_FOLLOWED_PEOPLE = "follow_sum";
	public String USER_IS_FOLLOWED = "is_followed";
	public String USER_BALANCE = "user_balance";
	public String INDUSTRY = "industry";
	public String INDUSTRY_ID = "industry_id";
	public String SIZE_COMPANY = "size_company";
	public String SIZE_COMPANY_ID = "size_company_id";
	public String REG_MONEY = "reg_money";
	public String REG_MONEY_ID = "reg_money_id";
	public String CALENDAR_ID="use_id";
	public String CALENDAR = "calendar";
	public String SOFTWARE = "software";
	public String SOFTWARE_ID = "software_id";
	public String HARDWARE = "hardware";
	public String HARDWARE_ID = "hardware_id";
	public String QUALIFIED = "qualified";
	public String QUALIFIED_ID = "qualified_id";
	public String ALLYEAR = "allyear";
	public String NUMBER = "number";
	public String PRICE = "price";
	public String TIME = "time";
	public String INTRODUCTION = "introduction";
	public String SCALE = "scale";
	public String AREA_ID = "area_id";
	public String AREA_NAME = "area_name";
	// ------ menu ------
	public String MENU_ID = "menu_id";
	public String MENU_PRIORITY = "priority";
	
	// ------ closure comment ------
	public String CLOSURE_COMMENT_ID = "closure_time";
	public String CLOSURE_COMMENT_TIME = "closure_time";
	public String CLOSURE_COMMENT_CONTENT = "closure_content";
	// ------ company ------
	public String COMPANY_ID = "company_id";
	public String COMPANY_NAME = "company_name";
	public String COMPANY_CLOSURE_COMMENT_SUM = "comment_sum";

	// ------ workplace ------
	public String WORKPLACE_ID = "workp_id";
	public String WORKPLACE_NAME = "workp_name";

	// ------ mock ------
	public String MOCK_ID = "mock_id";
	public String MOCK_TIME = "mock_time";
	public String MOCK_CONTENT = "mock_content";

	// ------ my series ------
	public String MY_SERIES_CATEGORY_ID = "category_id";
	public String MY_SERIES_CATEGORY_NAME = "category_name";
	public String MY_SERIES_TYPE = "type";
	public String MY_SERIES_ARTICLE_ID = "article_id";
	public String MY_SERIES_ARTICLE_TIME = "article_time";
	public String MY_SERIES_ARTICLE_TITLE = "article_title";
	public String MY_SERIES_ARTICLE_CONTENT = "article_content";

	// ------ conversation ------
	public String CONVERSATION_ID = "conv_id";
	public String CONVERSATION_TYPE = "conv_type";
	public String CONVERSATION_STATUS = "conv_status";
	public String CONVERSATION_TIME = "last_msg_time";

	// ------ memo ------
	public String MEMO_ID = "memo_id";
	public String MEMO_CATEGORY = "type";
	public String MEMO_TIME = "collect_time";
	public String MEMO_CONTENT = "remark";

	// ------ article ------
	public String ARTICLE_ID = "article_id";
	public String ARTICLE_TITLE = "article_title";
	public String ARTICLE_CONTENT = "article_content";
	public String ARTICLE_TIME = "article_time";
	public String ARTICLE_CATEGORY = "article_category";
	public String KNOW_ID = "know_id";
	public String TITLE = "title";
	public String COMMENT_SUM = "comment_sum";
	public String URGENT = "urgent";

	// ------ worker ------
	public String WORKER_SUM = "worker_sum";

	// ------ rate ------
	public String RATE_TITLE = "rate_title";
	public String RATE_TIME = "rate_time";

	// ------ message ------
	public String MSG_CONTENT = "msg_content";
	public String MSG_TYPE = "msg_type";
	public String MSG_TIME = "msg_time";
	public String MSG_STATUS = "msg_status";

	// ------ contract ------
	public String CONTRACT_ID = "contract_id";
	public String CONTRACT_STATUS = "status";
	public String CONTRACT_URL = "contract_url";
	public String CONTRACT_AGREEMENT = "agreement";

	// ------ product ------
	public String PRODUCT_ID = "product_id";
	public String PRODUCT_NAME = "product_name";
	public String PRODUCT_PRICE = "price";
}