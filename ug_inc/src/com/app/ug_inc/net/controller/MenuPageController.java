package com.app.ug_inc.net.controller;

public class MenuPageController extends BaseController {

	public MenuPageController(UIDelegate delegate) {
		super(delegate);
	}

	public void getMainPageList() {
		mNetTask.getMenuList();
	}
}