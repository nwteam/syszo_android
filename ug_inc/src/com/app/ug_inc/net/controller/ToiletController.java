package com.app.ug_inc.net.controller;

import com.app.ug_inc.net.NetResult;

public class ToiletController extends BaseController {

	public ToiletController(UIDelegate delegate) {
		super(delegate);
	}

	public void getNearByToilet(String ll, String time) {
		mNetTask.getNearByToilet(ll, time);
	}

//	@SuppressWarnings("unchecked")
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
//		if (ifId == InterfaceIds.GET_NEARBY_TOILET_LIST.IF_ID) {
//			List<Toilet> list = (List<Toilet>) obj.getResultObject();
//			Collections.sort(list, new Comparator<Toilet>() {
//
//				@Override
//				public int compare(Toilet lhs, Toilet rhs) {
//					return Integer.valueOf(lhs.getDistance()).compareTo(
//							Integer.valueOf(rhs.getDistance()));
//				}
//			});
//		}
		super.onRequestSuccess(ifId, obj);
	}
}
