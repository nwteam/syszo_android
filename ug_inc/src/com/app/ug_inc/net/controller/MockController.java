package com.app.ug_inc.net.controller;

public class MockController extends BaseController {

    public MockController(UIDelegate delegate) {
        super(delegate);
    }

    public void getSystList(String keyword, String type, int page) {
        mNetTask.getSystList(keyword, type, page);
    }

    public void getSystDetail(String id) {
        mNetTask.getSystDetail(id);
    }

    public void getWorkplaceList(String cityId, String companyId, String queryWord) {
        mNetTask.getWorkplaceList(cityId, companyId, queryWord);
    }

    public void getMockListByWorkplace(String workpId, int page) {
        mNetTask.getMockListByWorkplace(workpId, page);
    }

    public void getMockCommentList(String systId, int page) {
        mNetTask.getMockCommentList(systId, page);
    }

    public void commentMock(String content, String img) {
        mNetTask.commentMock(content, img);
    }

    public void addWorkplace(String cityId, String companyId, String workpName) {
        mNetTask.addWorkplace(cityId, companyId, workpName);
    }

    public void likeMock(String mockId) {
        mNetTask.likeMock(mockId);
    }

    public void likeMockComment(String mockId, String commentId) {
        mNetTask.likeMockComment(mockId, commentId);
    }

    public void editMock(String mockId, String content, String pic) {
        mNetTask.editMock(mockId, content, pic);
    }

    public void editMockComment(String mockId, String content) {
        mNetTask.editMockComment(mockId, content);
    }

    public void deleteMockComment(String mockId) {
        mNetTask.deleteMockComment(mockId);
    }

    public void commentMock2(String birdId, String content) {
        mNetTask.commentMock2(birdId, content);
    }

    public void deleteMock(String id) {
        mNetTask.deleteMock(id);
    }
}
