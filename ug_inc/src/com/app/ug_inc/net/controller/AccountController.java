package com.app.ug_inc.net.controller;

import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;

public class AccountController extends BaseController {

    public AccountController(UIDelegate delegate) {
        super(delegate);
    }

    private String mTmpEmail;

    public void login(String email, String password) {
        mTmpEmail = email;
        mNetTask.login(email, password);
    }

    public void register(User tmpUser) {
        mNetTask.register(tmpUser);
    }

    public void getProfile(String uid) {
        mNetTask.getProfile(uid);
    }

    public void userFollow(String targetUser) {
        mNetTask.userFollow(targetUser);
    }

    public void userUnfollow(String targetUser) {
        mNetTask.userUnfollow(targetUser);
    }

    public void userGetFollowedList(String targetUser, int page) {
        mNetTask.userGetFollowedList(targetUser, page);
    }

    public void userRate(String targetUser, String score, String title, String content) {
        mNetTask.userRate(targetUser, score, title, content);
    }

    public void userGetRateList(String targetUser, int page) {
        mNetTask.userGetRateList(targetUser, page);
    }

    public void updateProfile(User newUser) {
        mNetTask.updateProfile(newUser);
    }

    public void getArticleHistoryList(int page) {
        mNetTask.getArticleHistoryList(page);
    }

    public void getMemoList(int page) {
        mNetTask.getMemoList(page);
    }

    public void deleteMemo(String targetIds) {
        mNetTask.deleteMemo(targetIds);
    }

    public void getMemo(String targetUid) {
        mNetTask.getMemo(targetUid);
    }

    public void modifyMemo(String targetUId, String type, String remark) {
        mNetTask.modifyMemo(targetUId, type, remark);
    }

    public void submitPushToken(String token) {
        mNetTask.submitPushToken(token);
    }

    public void resetPassword(String email) {
        mNetTask.resetPassword(email);
    }

    public void getUnreadMsgCount() {
        mNetTask.getUnreadMsgCount();
    }

    public void getBalance() {
        mNetTask.getBalance();
    }

    public void getAddress() {
        mNetTask.getAddress();
    }

    public void getIndustry() {
        mNetTask.getIndustry();
    }

    public void getUse() {
        mNetTask.getUse();
    }

    public void getProfileSettingList(String id, String pid) {
        mNetTask.getProfileSettingList(id, pid);
    }

    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        if (ifId == InterfaceIds.USR_LOGIN.IF_ID || ifId == InterfaceIds.USR_REGISTER.IF_ID) {
            Account.clear();
            // if login and register, initialize user object
            User retUser = (User) obj.getResultObject();
            retUser.setEmail(mTmpEmail);
            Account.setUser(retUser);
        } else if (ifId == InterfaceIds.USER_GET_PROFILE.IF_ID) {
            User mUser = (User) obj.getResultObject();
//            User user = Account.getUser();
//            if (mUser.getId().equals(user.getId())) {
//                user.setNick(mUser.getNick() + "");
//                user.setSex(mUser.getSex() + "");
//                user.setCity(mUser.getCity() + "");
//                user.setIndustry(mUser.getIndustry() + "");
//                user.setSizeCompany(mUser.getSizeCompany() + "");
//                user.setJob(mUser.getJob() + "");
//                user.setRegMoney(mUser.getRegMoney() + "");
//                user.setCalendar(mUser.getCalendar() + "");
//                user.setSoftware(mUser.getSoftware() + "");
//                user.setHardware(mUser.getHardware() + "");
//                user.setCertifaction(mUser.getCertifaction() + "");
//                user.setAllyear(mUser.getAllyear() + "");
//                user.setFriends(mUser.getFriends() + "");
//                user.setFavour(mUser.getFavour() + "");
//                user.setTime(mUser.getTime() + "");
//                user.setSelfIntro(mUser.getSelfIntro() + "");
//                user.setScale(mUser.getScale() + "");
//            }
        } else if (ifId == InterfaceIds.GET_USER_BALANCE.IF_ID) {
            Account.setBalance((String) obj.getResultObject());
        }
    }
}