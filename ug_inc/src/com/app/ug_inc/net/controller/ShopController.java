package com.app.ug_inc.net.controller;

import com.app.ug_inc.datas.Account;
import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;

public class ShopController extends BaseController {

	public ShopController(UIDelegate delegate) {
		super(delegate);
	}

	public void getShopItemList() {
		mNetTask.getShopItemList();
	}

	public void notifySuccess(String sku, String orderResult) {
		mNetTask.notifyPurchase(sku, orderResult);
	}
	
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.NOTIFY_PURCHASE.IF_ID) {
			String balance = (String) obj.getResultObject();
			Account.setBalance(balance);
		}
	}
}