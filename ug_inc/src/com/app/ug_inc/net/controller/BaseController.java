package com.app.ug_inc.net.controller;

import com.app.ug_inc.net.HttpIF;
import com.app.ug_inc.net.NetCallBack;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.HttpTask;

public class BaseController implements NetCallBack {

	protected HttpIF mNetTask = new HttpTask(this);

	protected UIDelegate mDelegate;

	public BaseController(UIDelegate delegate) {
		mDelegate = delegate;
	}

	/**
	 * disable callback to UI
	 */
	public void clear() {
		mDelegate = null;
	}

	@Override
	public void onRequestStart(int ifId) {
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		if (mDelegate != null) {
			mDelegate.onRequestSuccess(ifId, obj);
		}
	}

	@Override
	public void onRequestError(int ifId, String errMsg) {
		if (mDelegate != null) {
			mDelegate.onRequestError(ifId, errMsg);
		}
	}
}
