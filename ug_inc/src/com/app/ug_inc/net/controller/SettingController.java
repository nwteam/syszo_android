package com.app.ug_inc.net.controller;

import com.app.ug_inc.net.InterfaceIds;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.utils.ConstData;

public class SettingController extends BaseController {

	public SettingController(UIDelegate delegate) {
		super(delegate);
	}

	public void feedBack(String type, String content) {
		mNetTask.feedBack(type, content);
	}

	public void modifyPwd(String oldPwd, String newPwd) {
		mNetTask.modifyPwd(oldPwd, newPwd);
	}

	public void pushSwitch(String trigger) {
		mNetTask.pushSwitch(trigger);
	}

	public void getAppIsFree() {
		mNetTask.getAppIsFree();
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		super.onRequestSuccess(ifId, obj);
		if (ifId == InterfaceIds.GET_APP_IS_FREE.IF_ID) {
			boolean isFree = ((String) obj.getResultObject()).equals("1");
			ConstData.sIsAppFree = isFree;
		}
	}
}
