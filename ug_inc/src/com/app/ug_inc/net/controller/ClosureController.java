package com.app.ug_inc.net.controller;

public class ClosureController extends BaseController {

    public ClosureController(UIDelegate delegate) {
        super(delegate);
    }

    public void getClosureList(String type, int page) {
        mNetTask.getClosureList(type, page);
    }

    public void getClosureCommentList(String id, int page) {
        mNetTask.getClosureCommentList(id, page);
    }
    
    public void get_good_count(String id){
    	mNetTask.get_good_count(id);
    }

    public void getClosureDetail(String id) {
        mNetTask.getClosureDetail(id);
    }

    public void commentClosure(String companyId, String content) {
        mNetTask.commentClosure(companyId, content);
    }

    public void likeClosure(String knowId, String commentId) {
        mNetTask.likeClosure(knowId, commentId);
    }

    public void deleteClosureComment(String commentId) {
        mNetTask.deleteClosureComment(commentId);
    }

    public void editClosureComment(String knowId, String content) {
        mNetTask.editClosureComment(knowId, content);
    }

    public void getClosureHotWordList(int page) {
        mNetTask.getClosureHotWordList(page);
    }

    public void searchClosure(String word, int page) {
        mNetTask.searchClosure(word, page);
    }

    public void newClosure(String title, String content, String img, boolean urgent) {
        mNetTask.newClosure(title, content, img, urgent);
    }

    public void editClosure(String id, String title, String content, String img, boolean urgent) {
        mNetTask.editClosure(id, title, content, img, urgent);
    }

    public void deleteClosure(String id) {
        mNetTask.deleteClosure(id);
    }
}