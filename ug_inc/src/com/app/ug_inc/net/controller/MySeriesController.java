package com.app.ug_inc.net.controller;

public class MySeriesController extends BaseController {

	public static enum TYPE {
		ALL, FOLLOWED, SELF
	}

	public MySeriesController(UIDelegate delegate) {
		super(delegate);
	}

	public void getMySeriesCatList() {
		mNetTask.getMySeriesCatList();
	}

	public void addMyseriesCategory(String category, String content) {
		mNetTask.addMyseriesCategory(category, content);
	}

	public void getMySeriesArticleList(String categoryId, TYPE type, int page) {
		mNetTask.getMySeriesArticleList(categoryId, type == TYPE.ALL ? "0"
				: type == TYPE.FOLLOWED ? "1" : "2", page);
	}

	public void getMySeriesCommentList(String articleId, int page) {
		mNetTask.getMySeriesCommentList(articleId, page);
	}

	public void commentMySeriesArticle(String articleId, String content) {
		mNetTask.commentMySeriesArticle(articleId, content);
	}

	public void publishMySeriesArticle(String categoryId, String title, String content,
			String picUrl) {
		mNetTask.publishMySeriesArticle(categoryId, title, content, picUrl);
	}

	public void uploadPic(String filePath) {
		mNetTask.uploadPic(filePath);
	}
}
