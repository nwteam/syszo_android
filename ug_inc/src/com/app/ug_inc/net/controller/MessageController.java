package com.app.ug_inc.net.controller;

public class MessageController extends BaseController {

    public MessageController(UIDelegate delegate) {
        super(delegate);
    }

    public void takeChat(String uid, String gid, int page) {
        mNetTask.takeChat(uid, gid, page);
    }

    public void sendMsgText(String gid, String uid, String content) {
        mNetTask.sendMsgTo(gid, uid, content, "");
    }

    public void sendMsgPic(String gid, String uid, String pic) {
        mNetTask.sendMsgTo(gid, uid, "", pic);
    }

    public void getConversationList(int page) {
        mNetTask.getConversationList(page);
    }

    public void deleteConversation(String ids) {
        mNetTask.deleteConversation(ids);
    }
}
