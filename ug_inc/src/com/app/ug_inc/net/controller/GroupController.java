package com.app.ug_inc.net.controller;

import android.text.TextUtils;

import java.util.List;

public class GroupController extends BaseController {

    public GroupController(UIDelegate delegate) {
        super(delegate);
    }

    public void getFriendList(int page) {
        mNetTask.getFriendList(page);
    }

    public void addFriendList(int page) {
        mNetTask.addFriendList(page);
    }

    public void addedFriendList(int page) {
        mNetTask.addedFriendList(page);
    }

    public void friendSearchList(String freeWord, int page) {
        mNetTask.friendSearchList(freeWord, page);
    }

    public void friendSearchListCat(String catId, String groupId, int page) {
        mNetTask.friendSearchListCat(catId, groupId, page);
    }

    public void addSavedGroup(String groupName, String groupId, String groupImg, List<String> idList) {
        mNetTask.addSavedGroup(groupName, groupId, groupImg, idList);
    }

    public void addGroupMember(String groupId) {
        mNetTask.addGroupMember(groupId);
    }

    public void getGroup(String groupId) {
        mNetTask.getGroup(groupId);
    }

    public void groupCategory(String catType) {
        mNetTask.groupCategory(catType);
    }

    public void modifyGroup(String groupId, String groupName, String groupImg, List<String> idList) {
        mNetTask.modifyGroup(groupId, groupName, groupImg, idList);
    }

    public void quitGroup(String groupId) {
        mNetTask.quitGroup(groupId);
    }
}
