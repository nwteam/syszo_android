package com.app.ug_inc.net.controller;

import com.app.ug_inc.models.Contract;
import com.app.ug_inc.models.WorkerInfo;

public class WorkplaceController extends BaseController {

	public WorkplaceController(UIDelegate delegate) {
		super(delegate);
	}

	public void getFreeWorkplaceList(int page) {
		mNetTask.getFreeWorkplaceList(page);
	}

	public void queryFreeWorkplaceList(String queryWord, int page) {
		mNetTask.queryFreeWorkplaceList(queryWord, page);
	}

	public void getFreeWorkerList(int page) {
		mNetTask.getFreeWorkerList(page);
	}

	public void queryFreeWorkerList(String queryWord, int page) {
		mNetTask.queryFreeWorkerList(queryWord, page);
	}

	public void publishFreeWorker(WorkerInfo info) {
		mNetTask.publishFreeWorker(info);
	}

	public void publishFreeWorkplace(WorkerInfo info) {
		mNetTask.publishFreeWorkplace(info);
	}

	public void publishContract(String targetUid, String convId,
			Contract contract) {
		mNetTask.publishContract(targetUid, convId, contract);
	}
}
