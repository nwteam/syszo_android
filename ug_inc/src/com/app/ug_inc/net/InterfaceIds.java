package com.app.ug_inc.net;

public enum InterfaceIds {
    USR_LOGIN(1, "登录", //
            NetConst.SERVER_URL + "index.php/login/login", //
            "loginParse"), //
    USR_REGISTER(2, "用户注册", //
            NetConst.SERVER_URL + "index.php/login/reg", //
            "registerParse"), //
    GET_MENU_LIST(3, "获取菜单页列表", //
            NetConst.SERVER_URL + "index.php/menu/index", //
            "getMenuListParse"), //
    GET_MENU_AD(4, "获取菜单页广告", //
            NetConst.SERVER_URL + "index.php/menu/adver", //
            "getMenuAdParse"), //
    GET_CLOSURE_LIST(5, "知恵袋列表接口", //
            NetConst.SERVER_URL + "index.php/know/info_list", //
            "getClosureListParse"), //
    QUERY_CLOSURE(6, "查询知惠袋", //
            NetConst.SERVER_URL + "index.php/know/search", //
            "getClosureListParse2"), //
    GET_CLOSURE_COMMENT_LIST(7, "知恵袋评论列表接口", //
            NetConst.SERVER_URL + "index.php/know/comments_list", //
            "getClosureCommentListParse"), //
    GET_CLOSURE_DETAIL(8, "查询知惠袋详情", //
            NetConst.SERVER_URL + "index.php/know/info", //
            "getClosureDetailParse"), //
    COMMENT_CLOSURE(9, "知惠袋评论", //
            NetConst.SERVER_URL + "index.php/know/comments", //
            "commentClosureParse"), //
    GET_SYST_LIST(10, "获取鸟列表", //
            NetConst.SERVER_URL + "index.php/bird/info_list", //
            "getCompanyListByCityParse"), //
    GET_WORKPLACE_LIST(11, "获取现场列表", //
            NetConst.SERVER_URL + "index.php/mock/mock", //
            "getWorkplaceListParse"), //
    GET_MOCK_LIST_BY_WORKPLACE(12, "获取吐槽列表", //
            NetConst.SERVER_URL + "index.php/mock/mock_list", //
            "getMockListByWorkplaceParse"), //
    COMMENT_MOCK(13, "鸟信息发布", //
            NetConst.SERVER_URL + "index.php/bird/add", //
            "commentMockParse"), //
    ADD_WORKPLACE(14, "添加现场", //
            NetConst.SERVER_URL + "index.php/mock/mock_add", //
            "addWorkplaceParse"), //
    GET_MY_SERIES_CAT_LIST(15, "获取我的系列分类列表", //
            NetConst.SERVER_URL + "index.php/user/my_series", //
            "getMySeriesCatListParse"), //
    ADD_MY_SERIES_CATEGORY(16, "新增我的系列分类", //
            NetConst.SERVER_URL + "index.php/user/add_series", //
            "addMyseriesCategoryParse"), //
    GET_MY_SERIES_ARTICLE_LIST(17, "获取我的系列文章列表", //
            NetConst.SERVER_URL + "index.php/user/content", //
            "getMySeriesArticleListParse"), //
    GET_MY_SERIES_COMMENT_LIST(18, "获取我的系列文章评论列表", //
            NetConst.SERVER_URL + "index.php/user/my_series_info_reply_list", //
            "getMySeriesCommentListParse"), //
    COMMENT_MY_SERIES_ARTICLE(19, "评论我的系列某篇文章", //
            NetConst.SERVER_URL + "index.php/user/my_series_info_reply", //
            "commentMySeriesArticleParse"), //
    PUBLISH_MY_SERIES_ARTICLE(20, "发表我的系列投稿", //
            NetConst.SERVER_URL + "index.php/user/my_series_info", //
            "publishMySeriesArticleParse"), //
    UPLOAD_PIC(21, "图片上传", //
            NetConst.SERVER_URL + "index.php/upload", //
            "publishMySeriesUploadParse"), //
    UPDATE_PROFILE(22, "修改个人信息", //
            NetConst.SERVER_URL + "index.php/myself/mydata_edit", //
            "modifyProfileParse"), //
    SETTING_PUSH_SWITCH(23, "推送开关修改", //
            NetConst.SERVER_URL + "index.php/myself/push_open", //
            "pushSwitchParse"), //
    MODIFY_PWD(24, "修改密码", //
            NetConst.SERVER_URL + "index.php/myself/pwd_edit", //
            "modifyPwdParse"), //
    SETTING_FEEDBACK(25, "提问反馈", //
            NetConst.SERVER_URL + "index.php/user/consultation", //
            "feedBackParse"), //
    USER_GET_PROFILE(26, "获取用户信息", //
            NetConst.SERVER_URL + "index.php/myself/mydata", //
            "getProfileParse"), //
    USER_FOLLOW(27, "关注某人", //
            NetConst.SERVER_URL + "index.php/chat/add_f", //
            "userFollowParse"), //
    USER_UNFOLLOW(28, "取消关注某人", //
            NetConst.SERVER_URL + "index.php/chat/del_f", //
            "userUnfollowParse"), //
    UESR_GET_FOLLOWED_LIST(29, "获取关注的人列表", //
            NetConst.SERVER_URL + "index.php/friend/friend_info", //
            "userGetFollowedListParse"), //
    USER_RATE(30, "评价某人", //
            NetConst.SERVER_URL + "index.php/user/appraisal", //
            "userRateParse"), //
    GET_MEMO_LIST(31, "获取备注列表", //
            NetConst.SERVER_URL + "index.php/notes/note_info", //
            "getMemoListParse"), //
    GET_MEMO(32, "获取某人备注", //
            NetConst.SERVER_URL + "index.php/notes/info", //
            "getMemoParse"), //
    MODIFY_MEMO(33, "修改对某人的备注", //
            NetConst.SERVER_URL + "index.php/notes/change_note", //
            "modifyMemoParse"), //
    GET_ARTICLE_HISTORY_LIST(34, "获取投稿历史列表", //
            NetConst.SERVER_URL + "index.php/myself/myinfolist", //
            "getArticleHistoryListParse"), //
    SEND_MSG(35, "发送消息", //
            NetConst.SERVER_URL + "index.php/chat/send_info", //
            "sendMsgToParse"), //
    MSG_GET_CONVERSATION_LIST(36, "获取用户会话列表", //
            NetConst.SERVER_URL + "index.php/message/look", //
            "getConversationListParse"), //
    MSG_DELETE_CONVERSATION(37, "删除用户会话", //
            NetConst.SERVER_URL + "index.php/message/msg_del", //
            "deleteConversationParse"), //
    GET_NEARBY_TOILET_LIST(38, "查找附近的厕所", //
//			"https://api.foursquare.com/v2/venues/search", //
            "https://maps.googleapis.com/maps/api/place/nearbysearch/json",
            "getNearByToiletParse"), //
    GET_FREE_WORKPLACE_LIST(39, "获取空闲现场列表", //
            NetConst.SERVER_URL + "index.php/free/scene_list", //
            "getFreeWorkplaceListParse"), //
    QUERY_FREE_WORKPLACE_LIST(40, "查询空闲现场列表", //
            NetConst.SERVER_URL + "index.php/free/scene_list", //
            "getFreeWorkplaceListParse"), //
    GET_FREE_WORKER_LIST(41, "获取空闲工匠列表", //
            NetConst.SERVER_URL + "index.php/free/free_list", //
            "getFreeWorkerListParse"), // , //
    QUERY_FREE_WORKER_LIST(42, "查询空闲工匠列表", //
            NetConst.SERVER_URL + "index.php/free/free_list", //
            "getFreeWorkerListParse"), //
    PUBLISH_FREE_WORKER(43, "发布空闲工匠信息", //
            NetConst.SERVER_URL + "index.php/free/push", //
            "publishFreeWorkerParse"), //
    PUBLISH_FREE_WORKPLACE(44, "发布空闲现场信息", //
            NetConst.SERVER_URL + "index.php/free/scene_push", //
            "publishFreeWorkplaceParse"), //
    USER_GET_RATE_LIST(45, "获取评价列表", //
            NetConst.SERVER_URL + "index.php/user/goal_list", //
            "userGetRateListParse"), //
    TAKE_CHAT(46, "聊天列表信息获取", //
            NetConst.SERVER_URL + "index.php/chat/info_list", //
            "takeChatParse"), //
    PUBLISH_CONTRACT(47, "发布发注书", //
            NetConst.SERVER_URL + "index.php/message/add_order", //
            "publishContractParse"), //
    SUBMIT_PUSH_TOKEN(48, "提交推送token", //
            NetConst.SERVER_URL + "index.php/login/tuisong", //
            "submitPushTokenParse"), //
    RESET_PASSWORD(49, "重置密码", //
            NetConst.SERVER_URL + "index.php/login/get_pwd", //
            "resetPasswordParse"), //
    GET_UNREAD_MSG_COUNT(50, "获取未读消息个数", //
            NetConst.SERVER_URL + "index.php/user/have_unread_messages", //
            "getUnreadMsgCountParse"), //
    GET_USER_BALANCE(51, "查询余额", //
            NetConst.SERVER_URL + "index.php/user/find_balance", //
            "getUserBalanceParse"), //
    GET_APP_IS_FREE(52, "查询app是否免费", //
            NetConst.SERVER_URL + "index.php/user/if_free", //
            "getAppIsFreeParse"), //
    MEMO_DELETE(53, "删除记事", //
            NetConst.SERVER_URL + "index.php/notes/del", //
            "deleteMemoParse"), //
    GET_SHOP_ITEM_LIST(54, "获取内购商品列表", //
            NetConst.SERVER_URL + "index.php/order/product_list", //
            "getShopItemListParse"), //
    NOTIFY_PURCHASE(55, "通知购买商品", //
            NetConst.SERVER_URL + "index.php/order/verify_android", //
            "notifyPurchaseParse"), //
    GET_CLOSURE_DATE_LIST(56, "获取倒产情报日期列表", //
            NetConst.SERVER_URL + "index.php/company/index", //
            "getClosureDateListParse"), //
    LIKE_COLSURE(57, "知惠袋点赞", //
            NetConst.SERVER_URL + "index.php/know/good", //
            "likeClosureParse"), //
    GET_CLOSURE_HOT_WORD_LIST(57, "知惠袋热门检索", //
            NetConst.SERVER_URL + "index.php/know/search_index", //
            "getClosureHotWordListParse"), //
    NEW_CLOSURE(57, "知惠袋信息发布", //
            NetConst.SERVER_URL + "index.php/know/add", //
            "newClosureParse"), //
    GET_SYST_DETAIL(57, "鸟的详情", //
            NetConst.SERVER_URL + "index.php/bird/info", //
            "getSystDetailParse"), //
    GET_FRIEND_LIST(58, "朋友列表画面", //
            NetConst.SERVER_URL + "index.php/chat/f_list", //
            "getFriendList"), //
    ADD_FRIEND_LIST(59, "添加好友列表", //
            NetConst.SERVER_URL + "index.php/chat/a_list", //
            "addFriendListParse"), //
    ADDED_FRIEND_LIST(60, "被添加好友列表", //
            NetConst.SERVER_URL + "index.php/chat/b_list", //
            "addedFriendListParse"), //
    FRIEND_SEARCH_LIST(61, "朋友检索画面", //
            NetConst.SERVER_URL + "index.php/chat/check", //
            "friendSearchListParse"), //
    FRIEND_SEARCH_LIST_CATEGORY(62, "朋友检索画面-分类", //
            NetConst.SERVER_URL + "index.php/chat/cat_rs", //
            "friendSearchListParse"), //
    ADD_SAVE_GROUP(63, "新建群组画面", //
            NetConst.SERVER_URL + "index.php/chat/group_add", //
            "newClosureParse"), //
    ADD_GROUP_MEMBER(64, "添加群成员", //
            NetConst.SERVER_URL + "index.php/chat/member_add", //
            "addGroupMemberParse"), //
    GET_GROUP(65, "点击群组之后", //
            NetConst.SERVER_URL + "index.php/chat/group_d", //
            "getGroupParse"), //
    MODIFY_GROUP(66, "修改群组画面", //
            NetConst.SERVER_URL + "index.php/chat/group_edit_1", //
            "likeClosureParse"), //, //
    GROUP_CATEGORY(67, "分类检索画面", //
            NetConst.SERVER_URL + "index.php/chat/cat", //
            "groupCategoryParse"),
    QUIT_GROUP(68, "退出群组", //
            NetConst.SERVER_URL + "index.php/chat/group_del", //
            "likeClosureParse"),
    GET_ADDRESS(69, "分类检索画面", //
            NetConst.SERVER_URL + "index.php/myself/get_address", //
            "getAddress"), //
    GET_INDUSTRY(70, "分类检索画面", //
            NetConst.SERVER_URL + "index.php/myself/get_industry", //
            "getIndustry"), //
    GET_USE(71, "分类检索画面", //
            NetConst.SERVER_URL + "index.php/myself/get_use", //
            "getUse"),
    EDIT_COLSURE_COMMENT(72, "知惠袋评论编辑", //
            NetConst.SERVER_URL + "index.php/know/comments_edit", //
            "likeClosureParse"),
    DELETE_COLSURE_COMMENT(73, "知惠袋评论删除", //
            NetConst.SERVER_URL + "index.php/know/comments_del", //
            "likeClosureParse"), //
    LIKE_MOCK(74, "鸟点赞", //
            NetConst.SERVER_URL + "index.php/bird/good", //
            "likeClosureParse"), //
    EDIT_MOCK(76, "鸟编辑", //
            NetConst.SERVER_URL + "index.php/bird/edit", //
            "likeClosureParse"), //
    LIKE_MOCK_COMMENT(75, "鸟评论点赞", //
            NetConst.SERVER_URL + "index.php/bird/good_comments", //
            "likeClosureParse"), //
    EDIT_MOCK_COMMENT(77, "鸟评论编辑", //
            NetConst.SERVER_URL + "index.php/bird/comments_edit", //
            "likeClosureParse"), //
    DELETE_MOCK_COMMENT(78, "鸟评论删除", //
            NetConst.SERVER_URL + "index.php/bird/comments_del", //
            "likeClosureParse"),//
    GET_MOCK_COMMENT_LIST(79, "鸟评论列表接口", //
            NetConst.SERVER_URL + "index.php/bird/bird_comments_list", //
            "getMockCommentListParse"),
    USER_GET_OTHER_PROFILE(80, "获取其他用户信息", //
            NetConst.SERVER_URL + "index.php/chat/fdata", //
            "getProfileParse"),
    GET_PROFILE_SETTING_LIST(81, "获取用户设置信息列表", //
            NetConst.SERVER_URL + "index.php/myself/get_static_data", //
            "getProfileSettingListParse"),
    BIRD_COMMENT(82, "鸟评论", //
            NetConst.SERVER_URL + "index.php/bird/bird_comments", //
            "likeClosureParse"),
    EDIT_BIRD_COMMENT(83, "编辑鸟评论", //
            NetConst.SERVER_URL + "index.php/bird/comments_edit", //
            "likeClosureParse"),
    EDIT_COLSURE(84, "知惠袋编辑", //
            NetConst.SERVER_URL + "index.php/know/edit", //
            "likeClosureParse"),
    DELETE_CLOSURE(85, "知惠袋删除", //
            NetConst.SERVER_URL + "index.php/know/del", //
            "likeClosureParse"),
    DELETE_MOCK(86, "鸟删除", //
            NetConst.SERVER_URL + "index.php/bird/del", //
            "likeClosureParse"),
     GETGOODCOUNT(87, "获取点赞数", //
                    NetConst.SERVER_URL + "index.php/know/get_good_count", //
                    "likeNumParse"),
    GET_AD(88, "获得广告", //
            NetConst.SERVER_URL + "index.php/ad/get_ad", //
            "getADParse");//

    public int IF_ID; // interface id
    public String REMARK; // interface id
    public String URL = null; // post url
    public String PARSE_METHOD = null; // method in

    private InterfaceIds(int ifId, String remark, String url, String method) {
        IF_ID = ifId;
        REMARK = remark;
        URL = url;
        PARSE_METHOD = method;
    }

    /**
     * Find url by given interface id.
     *
     * @param apiId given id
     * @return
     */
    public static String findUrlById(int apiId) {
        for (InterfaceIds IF : InterfaceIds.values()) {
            if (IF.IF_ID == apiId) {
                return IF.URL;
            }
        }
        return null;
    }

    /**
     * Find json parse method by given interface id.
     *
     * @param apiId given id
     * @return should called method in JsonParser
     */
    public static String findMethodById(int apiId) {
        for (InterfaceIds IF : InterfaceIds.values()) {
            if (IF.IF_ID == apiId) {
                return IF.PARSE_METHOD;
            }
        }
        return null;
    }
}
