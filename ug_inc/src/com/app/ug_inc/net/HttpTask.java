package com.app.ug_inc.net;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.app.ug_inc.datas.Account;
import com.app.ug_inc.models.Contract;
import com.app.ug_inc.models.User;
import com.app.ug_inc.models.WorkerInfo;
import com.app.ug_inc.net.HttpRequest.WAY;
import com.app.ug_inc.utils.ConstData;
import com.app.ug_inc.utils.Utils;
import com.app.ug_inc.utils.ZLog;

import android.os.AsyncTask;
import android.text.TextUtils;

public class HttpTask implements HttpIF, NetParams {
    public static final String TAG = HttpTask.class.getSimpleName();

    private NetCallBack mNetCallBack;

    public static ExecutorService executorService = Executors.newFixedThreadPool(6);

    /**
     * This request id is auto-added, added in ExecutedTask, if a request was
     * created, the variable will sign which it is.
     */
    static int sRequestId;

    public HttpTask(NetCallBack callBack) {
        mNetCallBack = callBack;
    }

    /**
     * Common K-V request
     *
     * @param ifId
     * @param keyParams
     */
    private synchronized int startTask(InterfaceIds ifId, LinkedHashMap<String, Object> keyParams) {
        return startTask(ifId, keyParams, null);
    }

    /**
     * Simple GET request
     *
     * @param ifId
     * @param params
     */
    private synchronized int startGetTask(InterfaceIds ifId, LinkedHashMap<String, Object> params) {
        HttpRequest request = new HttpRequest(ifId, params);
        request.setSubmitWay(WAY.GET);
        new ExecutedTask(request).executeOnExecutor(executorService);
        return sRequestId;
    }

    /**
     * Full request
     *
     * @param ifId
     * @param keyParams
     * @param fileParas
     */
    private synchronized int startTask(InterfaceIds ifId, LinkedHashMap<String, Object> keyParams,
                                       LinkedHashMap<String, File> fileParas) {
        HttpRequest request = new HttpRequest(ifId, keyParams, fileParas);

        new ExecutedTask(request).executeOnExecutor(executorService);
        return sRequestId;
    }

    private class ExecutedTask extends AsyncTask<Void, Integer, NetResult> {
        private HttpRequest mRequest;

        public ExecutedTask(HttpRequest request) {
            mRequest = request;
            sRequestId++;
        }

        @Override
        protected NetResult doInBackground(Void... params) {
            String retJson = null;

            WAY way = mRequest.getSubmitWay();
            if (way == WAY.GET) {
                retJson = mRequest.doGet();
            } else if (way == WAY.POST) {
                retJson = mRequest.doPost();
            }

            ZLog.v(TAG, "Return STRING= " + retJson);

            if (TextUtils.isEmpty(retJson)) { // invalid
                NetResult result = new NetResult();
                result.setResultCode(NetConst.CODE_NET_FAILED);
                result.setErrMsg(NetConst.ERR_MSG_SERVER_ERR);
                return result;
            } else {
                return JsonParser.parseJson(mRequest.getIfId(), retJson);
            }
        }

        @Override
        protected void onPostExecute(NetResult result) {
            if (result.getResultCode() == NetConst.CODE_NET_SUCCESS) {
                mNetCallBack.onRequestSuccess(mRequest.getIfId().IF_ID, result);
            } else {
                mNetCallBack.onRequestError(mRequest.getIfId().IF_ID, result.getErrMsg());
            }
        }
    }

    @Override
    public void login(String email, String password) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_EMAIL, email);
        params.put(USER_PWD, password);
        startTask(InterfaceIds.USR_LOGIN, params);
    }

    @Override
    public void register(User user) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_EMAIL, user.getEmail());
        params.put(USER_NICK, user.getNick());
        params.put(USER_PWD, user.getPwd());
        params.put(USER_SEX, user.getSex());
        params.put(USER_BIRTH, user.getBirth());
        params.put(CITY_ID, user.getCityId());
        params.put(USER_DIST, user.getDistrict());

        startTask(InterfaceIds.USR_REGISTER, params);
    }

    @Override
    public void getMenuList() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        startTask(InterfaceIds.GET_MENU_LIST, params);
    }

    @Override
    public void getClosureList(String type, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("type", type);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CLOSURE);
        startTask(InterfaceIds.GET_CLOSURE_LIST, params);
    }

    @Override
    public void likeClosure(String knowId, String commentId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", knowId);
        params.put("comments_id", commentId);
        startTask(InterfaceIds.LIKE_COLSURE, params);
    }

    @Override
    public void editClosureComment(String commentId, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("info_id", commentId);
        params.put("content", content);
        startTask(InterfaceIds.EDIT_COLSURE_COMMENT, params);
    }

    @Override
    public void deleteClosureComment(String commentId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("info_id", commentId);
//        params.put("content", queryWord);
        startTask(InterfaceIds.DELETE_COLSURE_COMMENT, params);
    }

    @Override
    public void queryClosure(String queryTime, String queryWord, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(QUERY_TIME, queryTime);
        params.put(QUERY_WORD, queryWord);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CLOSURE);
        startTask(InterfaceIds.QUERY_CLOSURE, params);
    }

    @Override
    public void deleteClosure(String id) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", id);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CLOSURE);
        startTask(InterfaceIds.DELETE_CLOSURE, params);
    }

    @Override
    public void getClosureCommentList(String id, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", id);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CLOSURE);
        startTask(InterfaceIds.GET_CLOSURE_COMMENT_LIST, params);
    }

    @Override
    public void getClosureDetail(String id) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", id);
        startTask(InterfaceIds.GET_CLOSURE_DETAIL, params);
    }

    @Override
    public void commentClosure(String companyId, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", companyId);
        params.put(REPLY_CONTENT, content);
        startTask(InterfaceIds.COMMENT_CLOSURE, params);
    }

    @Override
    public void getSystList(String keyword, String type, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("type", type);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CLOSURE);
        params.put(USER_ID, Account.getUserId());
        params.put("keyword", keyword);
        startTask(InterfaceIds.GET_SYST_LIST, params);
    }

    @Override
    public void getWorkplaceList(String cityId, String companyId, String queryWord) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(DIST_ID, cityId);
        params.put(COMPANY_ID, companyId);
        params.put(FREE_WORD, queryWord);
        startTask(InterfaceIds.GET_WORKPLACE_LIST, params);
    }

    @Override
    public void getMockListByWorkplace(String workpId, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(WORKPLACE_ID, workpId);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_MOCK);
        startTask(InterfaceIds.GET_MOCK_LIST_BY_WORKPLACE, params);
    }

    @Override
    public void commentMock(String content, String img) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("content", content);
        params.put("img", img);
        startTask(InterfaceIds.COMMENT_MOCK, params);
    }

    @Override
    public void addWorkplace(String cityId, String companyId, String workpName) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("dist_id", cityId);
        params.put(COMPANY_ID, companyId);
        params.put(WORKPLACE_NAME, workpName);
        startTask(InterfaceIds.ADD_WORKPLACE, params);
    }

    @Override
    public void getMySeriesCatList() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        startTask(InterfaceIds.GET_MY_SERIES_CAT_LIST, params);
    }

    @Override
    public void addMyseriesCategory(String category, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("category_name", category);
        params.put(REPLY_CONTENT, content);

        startTask(InterfaceIds.ADD_MY_SERIES_CATEGORY, params);
    }

    @Override
    public void getMySeriesArticleList(String categoryId, String type, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(MY_SERIES_CATEGORY_ID, categoryId);
        params.put(MY_SERIES_TYPE, type);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_MY_SERIES_ARTICLE);
        startTask(InterfaceIds.GET_MY_SERIES_ARTICLE_LIST, params);
    }

    @Override
    public void getMySeriesCommentList(String articleId, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(MY_SERIES_ARTICLE_ID, articleId);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_MY_SERIES_COMMENT);
        startTask(InterfaceIds.GET_MY_SERIES_COMMENT_LIST, params);
    }

    @Override
    public void commentMySeriesArticle(String articleId, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(MY_SERIES_ARTICLE_ID, articleId);
        params.put(REPLY_CONTENT, content);
        startTask(InterfaceIds.COMMENT_MY_SERIES_ARTICLE, params);
    }

    @Override
    public void publishMySeriesArticle(String categoryId, String title, String content,
                                       String picUrl) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(MY_SERIES_CATEGORY_ID, categoryId);
        params.put("reply_title", title);
        params.put(REPLY_CONTENT, content);
        params.put("pic_upload", picUrl);
        startTask(InterfaceIds.PUBLISH_MY_SERIES_ARTICLE, params);
    }

    @Override
    public void uploadPic(String filePath) {
        LinkedHashMap<String, File> fileParas = new LinkedHashMap<String, File>();
        fileParas.put("file_path", new File(filePath));
        startTask(InterfaceIds.UPLOAD_PIC, null, fileParas);
    }

    @Override
    public void updateProfile(User newUser) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Integer.parseInt(newUser.getId()));
        params.put(USER_NICK, newUser.getNick());
        params.put(USER_EMAIL,newUser.getEmail());
        params.put(USER_SEX, newUser.getSex());
        params.put(ADDRESS, newUser.getCityId());
        params.put(INDUSTRY, newUser.getIndustryId());
        params.put(SIZE_COMPANY, newUser.getSizeCompanyId());
        params.put(JOB, newUser.getJobId());
        params.put(REG_MONEY, newUser.getRegMoneyId());
        params.put(CALENDAR, newUser.getCalendarId());
        params.put(SOFTWARE, newUser.getSoftwareId());
        params.put(HARDWARE, newUser.getHardwareId());
        params.put(QUALIFIED, newUser.getCertifactionId());
        params.put(ALLYEAR, newUser.getAllyearId());
        params.put(NUMBER, newUser.getFriends());
        params.put(PRICE, newUser.getFavour());
        params.put(INTRODUCTION, newUser.getSelfIntro());
        params.put(SCALE, newUser.getScale());
        startTask(InterfaceIds.UPDATE_PROFILE, params);
    }

    @Override
    public void pushSwitch(String trigger) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("push_flg", trigger);
        startTask(InterfaceIds.SETTING_PUSH_SWITCH, params);
    }

    @Override
    public void modifyPwd(String oldPwd, String newPwd) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("old_pwd", oldPwd);
        params.put(USER_PWD, newPwd);
        startTask(InterfaceIds.MODIFY_PWD, params);
    }

    @Override
    public void feedBack(String type, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(FEEDBACK_TYPE, type);
        params.put(FEEDBACK_CONTENT, content);
        startTask(InterfaceIds.SETTING_FEEDBACK, params);
    }

    @Override
    public void getProfile(String userId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        if (userId.equals(Account.getUserId())) {
            // 看自己的
            startTask(InterfaceIds.USER_GET_PROFILE, params);
        } else {
            params.put("member_id", userId);
            startTask(InterfaceIds.USER_GET_OTHER_PROFILE, params);
        }
    }

    @Override
    public void userFollow(String targetUser) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("rec_id", targetUser);
        startTask(InterfaceIds.USER_FOLLOW, params);
    }

    @Override
    public void userUnfollow(String targetUser) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("rec_id", targetUser);
        startTask(InterfaceIds.USER_UNFOLLOW, params);
    }

    @Override
    public void userGetFollowedList(String targetUser, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_FOLLOWED);
        params.put(TARGET_UID, targetUser);
        startTask(InterfaceIds.UESR_GET_FOLLOWED_LIST, params);
    }

    @Override
    public void userRate(String targetUser, String score, String title, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(TARGET_UID, targetUser);
        params.put("score", score);
        params.put(RATE_TITLE, title);
        params.put(RATE_CONTENT, content);
        startTask(InterfaceIds.USER_RATE, params);
    }

    @Override
    public void sendMsgTo(String gid, String uid, String content, String pic) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("group_id", gid);
        params.put("rec_id", uid);
        params.put("info_content", content);
        params.put("info_img", pic);
        startTask(InterfaceIds.SEND_MSG, params);
    }

    @Override
    public void getConversationList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_CONVERSATION);
        startTask(InterfaceIds.MSG_GET_CONVERSATION_LIST, params);
    }

    @Override
    public void deleteConversation(String convIds) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("conv_id", convIds);
        startTask(InterfaceIds.MSG_DELETE_CONVERSATION, params);
    }

    @Override
    public void getMemoList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_MEMO);
        startTask(InterfaceIds.GET_MEMO_LIST, params);
    }

    @Override
    public void getMemo(String targetUid) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("target_id", targetUid);
        startTask(InterfaceIds.GET_MEMO, params);
    }

    @Override
    public void modifyMemo(String targetUId, String type, String remark) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("target_id", targetUId);
        params.put(MEMO_CATEGORY, type);
        params.put(MEMO_CONTENT, remark);
        startTask(InterfaceIds.MODIFY_MEMO, params);
    }

    @Override
    public void getArticleHistoryList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_ARTICLE_HIS);
        startTask(InterfaceIds.GET_ARTICLE_HISTORY_LIST, params);
    }

    @Override
    public void getNearByToilet(String ll, String time) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        // params.put("client_id", NetConst.FOURE_SQUARE_CLIENT_ID);
        // params.put("client_secret", NetConst.FOURE_SQUARE_CLIENT_SECRET);
        // params.put("limit", 50);
        // params.put("query", "トイレ");
        // params.put("radius", 10000);
        // params.put("ll", ll);
        // params.put("v", time);
        params.put("location", ll);
        params.put("radius", 100000);
        params.put("keyword", "トイレ");
        params.put("sensor", "false");
        params.put("language", "ja");
        params.put("key", "AIzaSyCXYVinxvKf8lKl_y3BN3sgT59Q_i-zfeU");

        startGetTask(InterfaceIds.GET_NEARBY_TOILET_LIST, params);
    }

    @Override
    public void getFreeWorkplaceList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_WORKPLACE);
        startTask(InterfaceIds.GET_FREE_WORKPLACE_LIST, params);
    }

    @Override
    public void queryFreeWorkplaceList(String queryWord, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(QUERY_WORD, queryWord);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_WORKPLACE);
        startTask(InterfaceIds.QUERY_FREE_WORKPLACE_LIST, params);
    }

    @Override
    public void getFreeWorkerList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_WORKPLACE);
        startTask(InterfaceIds.GET_FREE_WORKER_LIST, params);
    }

    @Override
    public void queryFreeWorkerList(String queryWord, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(QUERY_WORD, queryWord);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_WORKPLACE);
        startTask(InterfaceIds.QUERY_FREE_WORKER_LIST, params);
    }

    @Override
    public void publishFreeWorker(WorkerInfo info) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(JOB_ID, info.getJobId());
        params.put(JOB_NAME, info.getJobName());
        params.put(WORKER_SUM, info.getPeopleSum());
        params.put(START_TIME, info.getStartTime());
        params.put(END_TIME, info.getEndTime());
        params.put(CITY_ID, info.getCityId());
        params.put("district", info.getDistrict());
        params.put(DETAIL, info.getDescribe());
        startTask(InterfaceIds.PUBLISH_FREE_WORKER, params);
    }

    @Override
    public void publishFreeWorkplace(WorkerInfo info) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(JOB_ID, info.getJobId());
        params.put(JOB_NAME, info.getJobName());
        params.put(WORKER_SUM, info.getPeopleSum());
        params.put(START_TIME, info.getStartTime());
        params.put(END_TIME, info.getEndTime());
        params.put(CITY_ID, info.getCityId());
        params.put("district", info.getDistrict());
        params.put("workp_detail", info.getDescribe());
        startTask(InterfaceIds.PUBLISH_FREE_WORKPLACE, params);
    }

    @Override
    public void userGetRateList(String targetUser, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put(TARGET_UID, targetUser);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.USER_GET_RATE_LIST, params);
    }

    @Override
    public void takeChat(String uid, String gid, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("rec_id", uid);
        params.put("group_id", gid);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, 999);
        startTask(InterfaceIds.TAKE_CHAT, params);
    }

    @Override
    public void publishContract(String targetUid, String convId, Contract contract) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("target_id", targetUid);
        params.put("conv_id", convId);
        params.put("project_name", contract.getName());
        // params.put("publish_time", contract.getPublishTime());
        params.put("cost_without_tax", contract.getCostWithoutTax());
        params.put("publisher_real_name", contract.getPublisherRealName());
        params.put("publisher_code", contract.getPublisherCode());
        params.put("publisher_address", contract.getPublisherAddress());
        params.put("publisher_tel", contract.getPublisherTel());
        params.put("publisher_fax", contract.getPublisherFax());
        params.put("receive_real_name", contract.getReceiverRealName());
        params.put("project_time", contract.getJobTime());
        params.put("pay_time", contract.getPayTime());
        params.put("remark", contract.getRemark());

        startTask(InterfaceIds.PUBLISH_CONTRACT, params);
    }

    @Override
    public void submitPushToken(String token) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("device_id", token);
        params.put("device", 1);
        startTask(InterfaceIds.SUBMIT_PUSH_TOKEN, params);
    }

    @Override
    public void resetPassword(String email) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_EMAIL, email);
        startTask(InterfaceIds.RESET_PASSWORD, params);
    }

    @Override
    public void getUnreadMsgCount() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        startTask(InterfaceIds.GET_UNREAD_MSG_COUNT, params);
    }

    @Override
    public void getBalance() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        startTask(InterfaceIds.GET_USER_BALANCE, params);
    }

    @Override
    public void getAppIsFree() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        startTask(InterfaceIds.GET_APP_IS_FREE, params);
    }

    @Override
    public void deleteMemo(String targetIds) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("target_ids", targetIds);
        startTask(InterfaceIds.MEMO_DELETE, params);
    }

    @Override
    public void getShopItemList() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put("client_type", "2");
        startTask(InterfaceIds.GET_SHOP_ITEM_LIST, params);
    }

    @Override
    public void notifyPurchase(String purchasedSKU, String orderResult) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_TOKEN, Account.getToken());
        params.put(USER_ID, Account.getUserId());
        params.put("purchased_sku", purchasedSKU);
        params.put("order_result", orderResult);
        startTask(InterfaceIds.NOTIFY_PURCHASE, params);
    }

    @Override
    public void getClosureHotWordList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(PAGE_NUM, page);
        startTask(InterfaceIds.GET_CLOSURE_HOT_WORD_LIST, params);
    }

    @Override
    public void searchClosure(String word, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("keyword", word);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.QUERY_CLOSURE, params);
    }

    @Override
    public void newClosure(String title, String content, String img, boolean urgent) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("title", title);
        params.put("content", content);
        params.put("img", img);
        params.put("urgent", urgent ? "2" : "1");
        startTask(InterfaceIds.NEW_CLOSURE, params);
    }

    @Override
    public void editClosure(String id, String title, String content, String img, boolean urgent) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("know_id", id);
        params.put("title", title);
        params.put("content", content);
        params.put("img", img);
        params.put("urgent", urgent ? "2" : "1");
        startTask(InterfaceIds.EDIT_COLSURE, params);
    }

    @Override
    public void getSystDetail(String id) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", id);
        startTask(InterfaceIds.GET_SYST_DETAIL, params);
    }

    @Override
    public void getFriendList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.GET_FRIEND_LIST, params);
    }

    @Override
    public void addFriendList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, 200);
        startTask(InterfaceIds.ADD_FRIEND_LIST, params);
    }

    @Override
    public void addedFriendList(int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.ADDED_FRIEND_LIST, params);
    }

    @Override
    public void friendSearchList(String freeWord, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("user_nick", freeWord);
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.FRIEND_SEARCH_LIST, params);
    }

    @Override
    public void friendSearchListCat(String catId, String groupId, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("cat_sonType", catId);
        params.put("cat_type", groupId);
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, ConstData.PAGE_SIZE_RATE);
        startTask(InterfaceIds.FRIEND_SEARCH_LIST_CATEGORY, params);
    }

    @Override
    public void addSavedGroup(String groupName, String groupId, String groupImg, List<String> idList) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("user_id", Account.getUserId());
        params.put("group_id", groupId);
        params.put("group_name", groupName);
        params.put("group_img", groupImg);

        String ids = Utils.listToString(idList);
        params.put("member_id", "[" + ids + "]");
        startTask(InterfaceIds.ADD_SAVE_GROUP, params);
    }

    @Override
    public void addGroupMember(String groupId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("user_id", Account.getUserId());
        params.put("group_id", groupId);
        startTask(InterfaceIds.ADD_GROUP_MEMBER, params);
    }

    @Override
    public void getGroup(String groupId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("group_id", groupId);
        startTask(InterfaceIds.GET_GROUP, params);
    }

    @Override
    public void groupCategory(String catType) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("cat_type", catType);
        startTask(InterfaceIds.GROUP_CATEGORY, params);
    }

    @Override
    public void modifyGroup(String groupId, String groupName, String groupImg, List<String> memId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("group_id", groupId);
        params.put("group_name", groupName);
        params.put("user_id", Account.getUserId());
        params.put("group_img", groupImg);
        String ids = Utils.listToString(memId);
        params.put("aid_type", "[" + ids + "]");
        startTask(InterfaceIds.MODIFY_GROUP, params);
    }

    @Override
    public void quitGroup(String groupId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("group_id", groupId);
        params.put("member_id", Account.getUserId());
        startTask(InterfaceIds.QUIT_GROUP, params);
    }

    @Override
    public void getAddress() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        startTask(InterfaceIds.GET_ADDRESS, params);
    }

    @Override
    public void getIndustry() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        startTask(InterfaceIds.GET_INDUSTRY, params);
    }

    @Override
    public void getUse() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        startTask(InterfaceIds.GET_USE, params);
    }

    @Override
    public void likeMockComment(String mockId, String commentId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", mockId);
        params.put("comments_id", commentId);
        startTask(InterfaceIds.LIKE_MOCK_COMMENT, params);
    }

    @Override
    public void editMock(String mockId, String content, String pic) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", mockId);
        params.put("content", content);
        params.put("img", pic);
        startTask(InterfaceIds.EDIT_MOCK, params);
    }

    @Override
    public void deleteMockComment(String mockId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", mockId);
        startTask(InterfaceIds.DELETE_MOCK_COMMENT, params);
    }

    @Override
    public void getMockCommentList(String systId, int page) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put(PAGE_NUM, page);
        params.put(PAGE_SIZE, 10);
        params.put("bird_id", systId);
        startTask(InterfaceIds.GET_MOCK_COMMENT_LIST, params);
    }

    @Override
    public void likeMock(String mockId) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("user_id", Account.getUserId());
        params.put("bird_id", mockId);
        startTask(InterfaceIds.LIKE_MOCK, params);
    }

    @Override
    public void getProfileSettingList(String id, String pid) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("function", id);
        params.put("parent_id", pid);
        startTask(InterfaceIds.GET_PROFILE_SETTING_LIST, params);
    }

    @Override
    public void commentMock2(String birdId, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", birdId);
        params.put("reply_content", content);
        startTask(InterfaceIds.BIRD_COMMENT, params);
    }

    @Override
    public void editMockComment(String mockId, String content) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", mockId);
        params.put("content", content);
        startTask(InterfaceIds.EDIT_BIRD_COMMENT, params);
    }

    @Override
    public void deleteMock(String id) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(USER_ID, Account.getUserId());
        params.put("bird_id", id);
        startTask(InterfaceIds.DELETE_MOCK, params);
    }

    @Override
    public void get_ad() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("type", "1");
        startTask(InterfaceIds.GET_AD, params);
    }

    @Override
	public void get_good_count(String id) {
		// TODO Auto-generated method stub
		 LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	     params.put("know_id", id);
	     startTask(InterfaceIds.GETGOODCOUNT, params);
	}
}