package com.app.ug_inc.net;

public class NetResult {
	private int resultCode; // return code, 1=succeed, 0=failed
	private String errMsg; // err reason if failed, from server
	private Object resultObject = null; // result bean

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public Object getResultObject() {
		return resultObject;
	}

	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
