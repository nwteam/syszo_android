package com.app.ug_inc.net;

public class NetConst {
//	public static final String SERVER_URL = "http://jss-test.nestwinds.com/";
	// formal
	public static final String SERVER_URL = "http://api.syszo.com/";
	// test
//	public static final String SERVER_URL = "http://api-t.syszo.com/";
//	 public static final String SERVER_URL = "http://xianchang.wx.vicgoo.com/";

	public static final String SERVER_NEWS_URL = SERVER_URL + "index.php/new/";
	public static final String SERVER_SPORTS_URL = SERVER_URL
			+ "index.php/new/sport";

	public static final int TIMEOUT_CONNECTION = 30 * 1000; // 20s to connect
	public static final int TIMEOUT_READ = 30 * 1000; // 20s to read buffer

	public static final String ERR_OUT_OF_BALANCE = "-110";

	public static final String RESULT = "result";
	public static final String MSG = "msg";
	public static final String DATA = "data";

	public static final String FOURE_SQUARE_CLIENT_ID = "ILUYEYP4XFMI2Q4KH5YH4EPTZ3GIUNJPTVYL2JW3HDKDSJLB";
	public static final String FOURE_SQUARE_CLIENT_SECRET = "01IKZKHQF0XB3DRJSOGYQFZKIMI1T2MYC3JS4ALZKCQQRK1K";

	public static final int CODE_NET_FAILED = 0;
	public static final int CODE_NET_SUCCESS = 1;

	/** 无法连接到服务器 */
	public static final String ERR_MSG_SERVER_ERR = "通信エラーが発生しました。再度お試しください。";
	/** 发生未知错误 */
	public static final String ERR_MSG_UNKNOWN = "未知エラーが発生しました。";
	/** 请求超时，请稍后重试 */
	public static final String ERR_MSG_TIMEOUT = "タイムアウト、もう一度試してください。";
	public static final String ERR_MSG_NET_ERR = "Net error.";
}