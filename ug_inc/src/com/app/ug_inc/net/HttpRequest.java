package com.app.ug_inc.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

import com.app.ug_inc.utils.ZLog;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

public class HttpRequest {
	public static final String TAG = "NET";

	public static enum WAY {
		POST, GET
	}

	private InterfaceIds mIfId;

	private LinkedHashMap<String, Object> mKeyParams;
	private LinkedHashMap<String, File> mFileParams;

	private WAY mSubmitWay = WAY.POST;

	public HttpRequest(InterfaceIds ifId,
			LinkedHashMap<String, Object> keyParams) {
		mIfId = ifId;

		mKeyParams = keyParams;
		ZLog.e(TAG, "Request:: " + ifId.REMARK);
	}

	public HttpRequest(InterfaceIds ifId,
			LinkedHashMap<String, Object> keyParams,
			LinkedHashMap<String, File> fileParas) {
		mIfId = ifId;

		mKeyParams = keyParams;
		mFileParams = fileParas;

		if (mKeyParams == null && mFileParams == null) {
			throw new NullPointerException(
					"NetRequest params K-V and FILE cannot be null at the same time.");
		}

		ZLog.e(TAG, "Request:: " + ifId.REMARK);
	}

	public WAY getSubmitWay() {
		return mSubmitWay;
	}

	public void setSubmitWay(WAY submitWay) {
		mSubmitWay = submitWay;
	}

	public InterfaceIds getIfId() {
		return mIfId;
	}

	public String doGet() {
		ZLog.e(TAG, "--- doGet : " + mIfId.URL);

		String result = null;
		BufferedReader reader = null;
		try {
			HttpClient client = new DefaultHttpClient();
			client.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT,
					NetConst.TIMEOUT_CONNECTION);
			client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
					NetConst.TIMEOUT_READ);

			String reqUrl = "";
			StringBuilder sb = new StringBuilder(mIfId.URL);
			sb.append("?");

			if (mKeyParams != null && !mKeyParams.isEmpty()) {
				for (LinkedHashMap.Entry<String, Object> item : mKeyParams
						.entrySet()) {
					String k = item.getKey();
					String v = item.getValue() == null ? "" : item.getValue()
							.toString();

					ZLog.e(TAG, "Param: K=" + k + " V=" + v);
					sb.append(k + "=" + v);
					sb.append("&");
				}

				// remove last &
				reqUrl = sb.toString().substring(0, sb.length() - 1);
			}

			ZLog.e(TAG, "Request: " + reqUrl);

			HttpGet request = new HttpGet();
			request.setURI(new URI(reqUrl));
			HttpResponse response = client.execute(request);
			reader = new BufferedReader(new InputStreamReader(response
					.getEntity().getContent()));

			StringBuffer strBuffer = new StringBuffer("");
			String line = null;
			while ((line = reader.readLine()) != null) {
				strBuffer.append(line);
			}
			result = strBuffer.toString();
		} catch (ConnectTimeoutException e) {
			errJson(NetConst.ERR_MSG_TIMEOUT);
			e.printStackTrace();
		} catch (Exception e) {
			errJson(NetConst.ERR_MSG_UNKNOWN);
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
					reader = null;
				} catch (IOException e) {
					errJson(NetConst.ERR_MSG_UNKNOWN);
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	public String doPost() {
		ZLog.e(TAG, "--- doPost : " + mIfId.URL);

		String result = null;
		BufferedReader reader = null;
		try {
			HttpClient client = new DefaultHttpClient();
			client.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT,
					NetConst.TIMEOUT_CONNECTION);
			client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
					NetConst.TIMEOUT_READ);

			HttpPost request = new HttpPost();
			request.setURI(new URI(mIfId.URL));

			MultipartEntity entity = new MultipartEntity();

			// Generate K-V Entities
			if (mKeyParams != null && !mKeyParams.isEmpty()) {
				for (LinkedHashMap.Entry<String, Object> item : mKeyParams
						.entrySet()) {
					String k = item.getKey();
					String v = item.getValue() == null ? "" : item.getValue()
							.toString();

					ZLog.e(TAG, "Param: K=" + k + " V=" + v);

					entity.addPart(k,
							new StringBody(v, Charset.forName("UTF-8")));
				}
			}

			// Generate File Entities
			if (mFileParams != null && !mFileParams.isEmpty()) {
				for (Map.Entry<String, File> item : mFileParams.entrySet()) {
					File f = item.getValue();
					if (f.exists()) {
						ZLog.e(TAG, "Param: File=" + f.getAbsolutePath());
						entity.addPart(item.getKey(),
								new FileBody(item.getValue()));
					} else {
						ZLog.e(TAG,
								"Direct file is NOT exist! File="
										+ f.getAbsolutePath());
					}
				}
			}

			request.setEntity(entity);

			HttpResponse response = client.execute(request);
			reader = new BufferedReader(new InputStreamReader(response
					.getEntity().getContent()));

			StringBuffer strBuffer = new StringBuffer("");
			String line = null;
			while ((line = reader.readLine()) != null) {
				strBuffer.append(line);
			}
			result = strBuffer.toString();

		} catch (ConnectTimeoutException e) {
			errJson(NetConst.ERR_MSG_TIMEOUT);
			e.printStackTrace();
		} catch (Exception e) {
			errJson(NetConst.ERR_MSG_UNKNOWN);
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
					reader = null;
				} catch (IOException e) {
					errJson(NetConst.ERR_MSG_UNKNOWN);
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	private String errJson(String errMsg) {
		return new String("{\"result\": 1,\"msg\":\"" + errMsg
				+ "\",\"data\": \"\"}");
	}
}