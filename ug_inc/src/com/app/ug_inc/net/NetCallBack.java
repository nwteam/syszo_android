package com.app.ug_inc.net;

public interface NetCallBack {

	public void onRequestStart(int ifId);

	// public void onHttpRequesting();

	/**
	 * Called when http request succeed.
	 * 
	 * @param ifId
	 *            sign which interface
	 * @param obj
	 *            result object
	 * @param json
	 */
	public void onRequestSuccess(int ifId, NetResult obj);

	/**
	 * Called when http request failed / logical failed.
	 * 
	 * @param apiId
	 *            IF_ID - in HttpDef.HTTP_API_IDS
	 * @param errMsg
	 *            err msg from server
	 * @param errCode
	 *            defined err code
	 */
	public void onRequestError(int ifId, String errMsg);
}
