package com.app.ug_inc.net;

import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.app.ug_inc.models.*;

import com.app.ug_inc.utils.ZLog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("unused")
public class JsonParser implements NetParams {

    /**
     * @param id
     * @param json
     * @return
     */
    public static NetResult parseJson(InterfaceIds id, String json) {
        NetResult obj = null;
        obj = call(id.PARSE_METHOD, json); // 通过函数名调用函数
        return obj;
    }

    /**
     * call method in this class to parse json
     *
     * @param methodName
     * @param json
     * @return
     */
    private static NetResult call(String methodName, String json) {
        NetResult resultInfo = null;
        try {
            Method m = JsonParser.class.getDeclaredMethod(methodName, String.class); // 找到对应函数名的函数
            resultInfo = (NetResult) m.invoke(null, json); // 由于是静态函数所以不需要实例去执行它
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return resultInfo;
    }

    private static String outterParse(String json, NetResult result) throws JSONException {
        JSONObject jo = new JSONObject(json);
        int resultCode = Integer.valueOf(jo.getString(NetConst.RESULT));
        result.setResultCode(resultCode);
        if (resultCode != NetConst.CODE_NET_SUCCESS) { // failed
            result.setErrMsg(jo.getString(NetConst.MSG));
            return null;
        } else {
            return jo.getString(NetConst.DATA);
        }
    }

    private static NetResult makeErrorResult(Exception e, NetResult result) {
        e.printStackTrace();
        result.setResultCode(NetConst.CODE_NET_FAILED);
        result.setErrMsg(NetConst.ERR_MSG_SERVER_ERR);

        return result;
    }

    private static NetResult loginParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            User user = new User();

            user.setId(dataJo.optString(USER_ID));
//            user.setToken(dataJo.optString(USER_TOKEN));
//            user.setCityId(dataJo.optString(CITY_ID));
            user.setNick(dataJo.optString(USER_NICK));
//            user.setEmail(dataJo.optString(USER_EMAIL));
//            user.setPwd(dataJo.optString(USER_PWD));
//            user.setSex(dataJo.optString(USER_SEX));
//            user.setBirth(dataJo.optString(USER_BIRTH));
//            user.setCityId(dataJo.optString(CITY_ID));
//            user.setDistrict(dataJo.optString(USER_DIST));
//            user.setBalance(dataJo.optString(USER_BALANCE));
//            user.setFollowedPeople(dataJo.optString(USER_FOLLOWED_PEOPLE));
//            user.setScore(dataJo.optString(USER_RATE));
//            user.setSelfIntro(dataJo.optString(USER_SELF_INTRO));
//            user.setJobId(dataJo.optString(JOB_ID));
//            user.setJob(dataJo.optString(JOB_NAME));

            netResult.setResultObject(user);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult registerParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            User user = new User();

            user.setId(dataJo.optString(USER_ID));
            user.setToken(dataJo.optString(USER_TOKEN));
            user.setCityId(dataJo.optString(CITY_ID));
            user.setNick(dataJo.optString(USER_NICK));
            user.setEmail(dataJo.optString(USER_EMAIL));
            user.setPwd(dataJo.optString(USER_PWD));
            user.setSex(dataJo.optString(USER_SEX));
            user.setBirth(dataJo.optString(USER_BIRTH));
            user.setCityId(dataJo.optString(CITY_ID));
            user.setDistrict(dataJo.optString(USER_DIST));
            user.setBalance(dataJo.optString(USER_BALANCE));
            user.setFollowedPeople(dataJo.optString(USER_FOLLOWED_PEOPLE));
            user.setScore(dataJo.optString(USER_RATE));
            user.setSelfIntro(dataJo.optString(USER_SELF_INTRO));
            user.setJobId(dataJo.optString(JOB_ID));
            user.setJob(dataJo.optString(JOB_NAME));

            netResult.setResultObject(user);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMenuListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<MenuItem> list = new ArrayList<MenuItem>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                MenuItem menu = new MenuItem();
                menu.setId(obj.optString(MENU_ID));
                menu.setPriority(obj.optString(MENU_PRIORITY));
                list.add(menu);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getClosureDateListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<String> list = new ArrayList<String>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                String date = dataArray.getString(i);
                list.add(date);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getClosureListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Chieb> list = new ArrayList<Chieb>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                Chieb chieb = new Chieb();
                chieb.setId(obj.optString("know_id"));
                chieb.setCommentCount("" + obj.optInt("comment_sum"));
                chieb.setTitle(obj.optString("title"));
                chieb.setTime(obj.optString("time"));
                chieb.setUrgent(obj.optInt("urgent") == 2 ? true : false);
                chieb.setCommited(obj.optInt("comments") == 2 ? true : false);

                list.add(chieb);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getClosureListParse2(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataObject = new JSONObject(datas);
            JSONArray dataArray = dataObject.getJSONArray("list");

            List<Chieb> list = new ArrayList<Chieb>();

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                Chieb chieb = new Chieb();
                chieb.setId(obj.optString("know_id"));
                chieb.setCommentCount("" + obj.optInt("comment_sum"));
                chieb.setTitle(obj.optString("title"));
                chieb.setTime(obj.optString("time"));
                chieb.setUrgent(obj.optInt("urgent") == 2 ? true : false);
                chieb.setCommited(obj.optInt("comments") == 2 ? true : false);

                list.add(chieb);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMockCommentListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<ChiebComment> list = new ArrayList<ChiebComment>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                ChiebComment comment = new ChiebComment();
                comment.setContent(obj.optString("comments"));
                comment.setLiked(obj.optInt("if_good") == 2 ? true : false);
                comment.setTime(obj.optString("comments_time"));
                comment.setId(obj.optString("comments_id"));
                comment.setIsSelf(obj.optInt("if_myself") == 2 ? true : false);

                User user = new User();
                user.setId(obj.optString("user_id"));
                user.setNick(obj.optString("user_name"));
                user.setVip_flg(obj.optInt("vip_flg"));
                comment.setUser(user);

                list.add(comment);
            }

            netResult.setResultObject(list);


            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getClosureCommentListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<ChiebComment> list = new ArrayList<ChiebComment>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                ChiebComment comment = new ChiebComment();
                comment.setContent(obj.optString("comments"));
                comment.setLiked(obj.optInt("if_good") == 2 ? true : false);
                comment.setTime(obj.optString("comments_time"));
                comment.setId(obj.optString("comments_id"));
                comment.setIsSelf(obj.optInt("if_myself") == 2 ? true : false);
                comment.setComment_goods(obj.optInt("comment_goods"));

                User user = new User();
                user.setId(obj.optString("user_id"));
                user.setNick(obj.optString("user_name"));
                user.setVip_flg(obj.optInt("vip_flg"));
                comment.setUser(user);

                list.add(comment);
            }

            netResult.setResultObject(list);


            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }


    private static NetResult getClosureDetailParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataObject = new JSONObject(datas);

            Chieb chieb = new Chieb();
            chieb.setTitle(dataObject.optString("title"));
            chieb.setTime(dataObject.optString("time"));
            chieb.setContent(dataObject.optString("content"));
            chieb.setCommentCount(dataObject.optString("comment_sum"));
            chieb.setPicUrl(dataObject.optString("info_img"));
            chieb.setLike(dataObject.optInt("if_good")==1?false:true);
            User user = new User();
            user.setNick(dataObject.optString("user_name"));
            user.setId(dataObject.optString("user_id"));
            user.setVip_flg(dataObject.optInt("vip_flg"));
            chieb.setUser(user);

            netResult.setResultObject(chieb);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult commentClosureParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getCompanyListByCityParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Syst> list = new ArrayList<Syst>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Syst syst = new Syst();
                syst.setId(obj.optString("bird_id"));
                syst.setContent(obj.optString("content"));
                syst.setTime(obj.optString("time"));

                User user = new User();
                user.setId(obj.optString("user_id"));
                user.setNick(obj.optString("user_name"));
                user.setVip_flg(obj.optInt("vip_flg"));
                syst.setUser(user);

                list.add(syst);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getWorkplaceListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Workplace> list = new ArrayList<Workplace>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Workplace workplace = new Workplace();
                workplace.setId(obj.optString(WORKPLACE_ID));
                workplace.setName(obj.optString(WORKPLACE_NAME));
                workplace.setMockSum(obj.optInt("comment_sum") + "");

                list.add(workplace);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMockListByWorkplaceParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Mock> list = new ArrayList<Mock>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Mock mock = new Mock();
                mock.setId(obj.optString(MOCK_ID));
                mock.setContent(obj.optString(MOCK_CONTENT));
                mock.setTime(obj.optString(MOCK_TIME));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString(USER_NICK));
                mock.setUser(user);

                list.add(mock);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult commentMockParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult addWorkplaceParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMySeriesCatListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<SimpleKV> list = new ArrayList<SimpleKV>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                SimpleKV category = new SimpleKV();
                category.setKey(obj.optString(MY_SERIES_CATEGORY_ID));
                category.setValue(obj.optString(MY_SERIES_CATEGORY_NAME));

                list.add(category);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult addMyseriesCategoryParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMySeriesArticleListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Article> list = new ArrayList<Article>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Article article = new Article();
                article.setId(obj.optString(MY_SERIES_ARTICLE_ID));
                article.setTitle(obj.optString("title"));
                article.setTime(obj.optString(MY_SERIES_ARTICLE_TIME));
                article.setContent(obj.optString(MY_SERIES_ARTICLE_CONTENT));
                article.setPic(obj.optString(PIC_URL));
                article.setPicThumb(obj.optString(PIC_THUMB_URL));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString("user_name"));
                user.setCity(obj.optString("city"));
                user.setJob(obj.optString("job_name"));
                article.setUser(user);

                list.add(article);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMySeriesCommentListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<ArticleComment> list = new ArrayList<ArticleComment>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                ArticleComment comment = new ArticleComment();
                comment.setTime(obj.optString("comment_time"));
                comment.setContent(obj.optString("comment_content"));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString(USER_NICK));
                user.setCity(obj.optString("city_name"));
                user.setJob(obj.optString("job_name"));
                comment.setUser(user);

                list.add(comment);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult commentMySeriesArticleParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishMySeriesArticleParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishMySeriesUploadParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            String url = dataJo.optString("url");

            netResult.setResultObject(url);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult modifyProfileParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult pushSwitchParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Boolean trigger = "1".equals(dataJo.optString("push_flg")) ? true : false;
            netResult.setResultObject(trigger);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult modifyPwdParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult feedBackParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getProfileParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }
            Log.e("JsonParser","datas="+datas);
            JSONObject dataJo = new JSONObject(datas);

            User user = new User();
            user.setNick(dataJo.optString(USER_NICK));
            user.setSex(dataJo.optString(USER_SEX));
            user.setEmail(dataJo.optString("user_email"));
            user.setCityId(dataJo.optString(AREA_ID));
            user.setCity(dataJo.optString(ADDRESS));
            user.setIndustryId(dataJo.optString(INDUSTRY_ID));
            user.setIndustry(dataJo.optString(INDUSTRY));
            user.setSizeCompanyId(dataJo.optString(SIZE_COMPANY_ID));
            user.setSizeCompany(dataJo.optString(SIZE_COMPANY));
            user.setJobId(dataJo.optString(JOB_ID));
            user.setJob(dataJo.optString(JOB));
            user.setRegMoneyId(dataJo.optString(REG_MONEY_ID));
            user.setRegMoney(dataJo.optString(REG_MONEY));
            user.setCalendarId(dataJo.optString(CALENDAR_ID));
            user.setCalendar(dataJo.optString(CALENDAR));
            user.setSoftware(dataJo.optString(SOFTWARE));
            user.setSoftwareId(dataJo.optString(SOFTWARE_ID));
            user.setHardware(dataJo.optString(HARDWARE));
            user.setHardwareId(dataJo.optString(HARDWARE_ID));
            user.setCertifaction(dataJo.optString(QUALIFIED));
            user.setCertifactionId(dataJo.optString(QUALIFIED_ID));
            user.setAllyear(dataJo.optString(ALLYEAR));
            user.setFriends(dataJo.optString(NUMBER));
            user.setFavour(dataJo.optString(PRICE));
            user.setTime(dataJo.optString(TIME));
            user.setSelfIntro(dataJo.optString(INTRODUCTION));
            user.setScale(dataJo.optString(SCALE));
            user.setIsFollowed(dataJo.optString("if_member"));

            netResult.setResultObject(user);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userFollowParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userUnfollowParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userGetFollowedList(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<User> list = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString(USER_NICK));
                user.setCityId(obj.optString(CITY_ID));
                user.setJobId(obj.optString(JOB_ID));

                list.add(user);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userRateParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult sendMsgToParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getConversationListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }
            List<Conversation> list = new ArrayList<Conversation>();
            try {
                JSONArray dataJson = new JSONArray(datas);
                int len = dataJson.length();
                for (int i = 0; i < len; i++) {
                    JSONObject object = dataJson.getJSONObject(i);
                    Conversation conv = new Conversation();
                    String type = object.optString("type");
                    if ("1".equals(type)) { // 群组
                        conv.setId(object.optString("group_id"));
                        String name = object.optString("group_name");
                        String num = object.optString("ginfo_sum");
                        conv.setName(name + " (" + num + ")");
                        conv.setTime(object.optString("insert_time"));
                        String imgMsg = object.optString("ginfo_img");
                        if (TextUtils.isEmpty(imgMsg) || "null".equals(imgMsg)) {
                            conv.setIntro(object.optString("ginfo_content"));
                        } else {
                            conv.setIntro("[画像]");
                        }
                        conv.setIsGroup(true);
                    } else { // 个人
                        conv.setId(object.optString("user_id"));
                        String name = object.optString("user_name");
                        conv.setName(name);
                        conv.setTime(object.optString("insert_time_m"));
                        String imgMsg = object.optString("minfo_img");
                        if (TextUtils.isEmpty(imgMsg) || "null".equals(imgMsg)) {
                            conv.setIntro(object.optString("minfo_content"));
                        } else {
                            conv.setIntro("[画像]");
                        }

                        conv.setIsGroup(false);
                    }
                    list.add(conv);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult deleteConversationParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMemoListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Memo> list = new ArrayList<Memo>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Memo memo = new Memo();
                memo.setCategory(obj.optString(MEMO_CATEGORY));
                memo.setTime(obj.optString(MEMO_TIME));
                memo.setContent(obj.optString(MEMO_CONTENT));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString(USER_NICK));
                user.setCity(obj.optString("user_area"));
                memo.setTargetUser(user);

                list.add(memo);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getMemoParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Memo memo = new Memo();
            memo.setCategory(dataJo.optString(MEMO_CATEGORY));
            memo.setContent(dataJo.optString(MEMO_CONTENT));

            netResult.setResultObject(memo);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getArticleHistoryListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Article> list = new ArrayList<Article>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Article article = new Article();
                article.setId(obj.optString("info_id"));
                article.setTitle(obj.optString("title"));
                article.setCommentSum(obj.optString("comment_sum"));
                article.setTime(obj.optString(TIME));
                article.setType(obj.optString("type"));

                list.add(article);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult modifyMemoParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getNearByToiletParse(String json) {
        NetResult netResult = new NetResult();
        try {
            JSONObject fullJo = new JSONObject(json);

            String status = fullJo.getString("status");
            if (!status.equals("OK") && !status.equals("ZERO_RESULTS")) {
                netResult.setResultCode(NetConst.CODE_NET_FAILED);
                netResult.setErrMsg(NetConst.ERR_MSG_SERVER_ERR);

                return netResult;
            }

            netResult.setResultCode(NetConst.CODE_NET_SUCCESS);

            JSONArray dataArray = new JSONArray(fullJo.getString("results"));

            List<Toilet> list = new ArrayList<Toilet>();
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                Toilet toilet = new Toilet();

                toilet.setName(obj.optString("name"));
                toilet.setAddress(obj.optString("vicinity"));

                JSONObject locJo = obj.getJSONObject("geometry").getJSONObject("location");
                // toilet.setDistance(locJo.optString("distance"));
                toilet.setLat(locJo.optDouble("lat"));
                toilet.setLon(locJo.optDouble("lng"));

                list.add(toilet);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    // foursquare parse
    // private static NetResult getNearByToiletParse(String json) {
    // NetResult netResult = new NetResult();
    // try {
    // JSONObject fullJo = new JSONObject(json);
    //
    // String meta = fullJo.getString("meta");
    // String code = new JSONObject(meta).getString("code");
    // if (!code.equals("200")) {
    // netResult.setResultCode(NetConst.CODE_NET_FAILED);
    // netResult.setErrMsg(NetConst.ERR_MSG_SERVER_ERR);
    //
    // return netResult;
    // }
    //
    // netResult.setResultCode(NetConst.CODE_NET_SUCCESS);
    //
    // String response = fullJo.getString("response");
    // String venues = new JSONObject(response).getString("venues");
    // JSONArray dataArray = new JSONArray(venues);
    //
    // List<Toilet> list = new ArrayList<Toilet>();
    // for (int i = 0; i < dataArray.length(); i++) {
    // JSONObject obj = dataArray.getJSONObject(i);
    // Toilet toilet = new Toilet();
    //
    // toilet.setName(obj.optString("name"));
    //
    // JSONObject locJo = new JSONObject(obj.getString("location"));
    // toilet.setAddress(locJo.optString("address"));
    // toilet.setDistance(locJo.optString("distance"));
    // toilet.setLat(locJo.optDouble("lat"));
    // toilet.setLon(locJo.optDouble("lng"));
    //
    // list.add(toilet);
    // }
    //
    // netResult.setResultObject(list);
    //
    // return netResult;
    // } catch (JSONException e) {
    // return makeErrorResult(e, netResult);
    // }
    // }

    private static NetResult getFreeWorkplaceListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<WorkerInfo> list = new ArrayList<WorkerInfo>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                WorkerInfo info = new WorkerInfo();
                info.setJobId(obj.optString(JOB_ID));
                info.setJobName(obj.optString(JOB_NAME));
                info.setPeopleSum(obj.optString("worker_sum"));
                info.setStartTime(obj.optString(START_TIME));
                info.setEndTime(obj.optString(END_TIME));
                info.setCityId(obj.optString(CITY_ID));
                info.setDistrict(obj.optString("district"));
                info.setDescribe(obj.optString("workp_detail"));
                info.setPublishTime(obj.optString("publish_time"));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString("user_name"));
                user.setSex(obj.optString(USER_SEX));
                user.setJob(obj.optString("user_job"));
                user.setCity(obj.optString("user_city"));
                info.setUser(user);

                list.add(info);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getFreeWorkerListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<WorkerInfo> list = new ArrayList<WorkerInfo>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                WorkerInfo info = new WorkerInfo();
                info.setJobId(obj.optString(JOB_ID));
                info.setJobName(obj.optString(JOB_NAME));
                info.setPeopleSum(obj.optString("worker_sum"));
                info.setStartTime(obj.optString("work_start_time"));
                info.setEndTime(obj.optString("work_end_time"));
                info.setCityId(obj.optString(CITY_ID));
                info.setDistrict(obj.optString("district"));
                info.setDescribe(obj.optString("self_intro"));
                info.setPublishTime(obj.optString("publish_time"));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString("user_name"));
                user.setSex(obj.optString(USER_SEX));
                user.setJob(obj.optString("user_job"));
                user.setCity(obj.optString("user_city"));
                info.setUser(user);

                list.add(info);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishFreeWorkerParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishFreeWorkplaceParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userGetRateListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<Rate> list = new ArrayList<Rate>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                Rate rate = new Rate();
                rate.setTitle(obj.optString(RATE_TITLE));
                rate.setContent(obj.optString(RATE_CONTENT));
                rate.setScore(obj.optString("score"));
                rate.setTime(obj.optString(RATE_TIME));

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString("user_name"));
                rate.setFromUser(user);

                list.add(rate);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult userGetFollowedListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<User> list = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                User user = new User();
                user.setId(obj.optString(USER_ID));
                user.setNick(obj.optString(USER_NICK));
                user.setCityId(obj.optString(CITY_ID));
                user.setJobId(obj.optString(JOB_ID));
                user.setJob(obj.optString(JOB_NAME));
                user.setSelfIntro(obj.optString(USER_SELF_INTRO));

                list.add(user);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult takeChatParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Conversation conv = new Conversation();
            conv.setIsGroup(dataJo.optString("if_group").equals("1"));

            List<Message> list = new ArrayList<Message>();
            JSONArray msgArray = dataJo.getJSONArray("group_info");
            for (int i = 0; i < msgArray.length(); i++) {
                JSONObject obj = msgArray.getJSONObject(i);
                Message msg = new Message();
                String picUrl = obj.optString("info_img");
                if (!TextUtils.isEmpty(picUrl)) {
                    msg.setType("1");
                    msg.setPicUrl(picUrl);
                } else {
                    msg.setType("0");
                    msg.setContent(obj.optString("info_content"));
                }

                msg.setTime(obj.optString("info_time"));

                User fromUser = new User();
                fromUser.setId(obj.optString(USER_ID));
                fromUser.setNick(obj.optString("uname"));
                msg.setFromUser(fromUser);

                list.add(msg);
            }

            conv.setChatList(list);

            netResult.setResultObject(conv);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishContractParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }
            JSONObject dataJo = new JSONObject(datas);

            Contract contract = new Contract();
            contract.setId(dataJo.optString(CONTRACT_ID));
            contract.setUrl(dataJo.optString(CONTRACT_URL));

            netResult.setResultObject(contract);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult submitPushTokenParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult resetPasswordParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getUnreadMsgCountParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Integer unReadMsgCount = Integer.valueOf(dataJo.optString("unread_msg_count"));

            netResult.setResultObject(unReadMsgCount);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getAppIsFreeParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            String isFree = dataJo.optString("is_free");

            netResult.setResultObject(isFree);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getUserBalanceParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            String balance = dataJo.optString("user_balance");

            netResult.setResultObject(balance);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult deleteMemoParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult likeClosureParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult likeNumParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }
            Log.e("likeNumParse", "json="+json);
            JSONObject JO = new JSONObject(datas);
            Log.e("likeNumParse", "good_count="+JO.optString("good_count"));
            netResult.setResultObject(JO.optString("good_count"));

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }
    
    private static NetResult getShopItemListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<ShopItem> list = new ArrayList<ShopItem>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                ShopItem item = new ShopItem();
                item.setId(obj.optString(PRODUCT_ID));
                item.setName(obj.optString(PRODUCT_NAME));
                item.setPrice(obj.optString(PRODUCT_PRICE));

                list.add(item);
            }

            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult notifyPurchaseParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            String newBalance = dataJo.optString(USER_BALANCE);

            netResult.setResultObject(newBalance);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getClosureHotWordListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataObj = new JSONObject(datas);
            String time = dataObj.optString("time");

            List<String> list = new ArrayList<String>();
            JSONArray array = dataObj.optJSONArray("list");
            int len = array.length();
            for (int i = 0; i < len; i++) {
                JSONObject obj = array.getJSONObject(i);
                list.add(obj.optString("keyword"));
            }

            HotSearch hot = new HotSearch();
            hot.setHotwordList(list);
            hot.setUpdateTime(time);

            netResult.setResultObject(hot);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult newClosureParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getSystDetailParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Syst syst = new Syst();
            syst.setId(dataJo.optString("bird_id"));
            syst.setContent(dataJo.optString("content"));
            syst.setTime(dataJo.optString("time"));
            syst.setLiked(dataJo.optInt("if_good") == 2 ? true : false);
            syst.setPicUrl(dataJo.optString("pic"));

            User user = new User();
            user.setId(dataJo.optString("user_id"));
            user.setNick(dataJo.optString("user_name"));
            user.setVip_flg(dataJo.optInt("vip_flg"));
            syst.setUser(user);

            netResult.setResultObject(syst);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult addFriendListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<User> list = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                User user = new User();
                user.setNick(obj.optString("member_name"));
                user.setId(obj.optString("member_id"));

                list.add(user);
            }
            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult addedFriendListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<User> list = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                User user = new User();
                user.setNick(obj.optString("member_name"));
                user.setId(obj.optString("member_id"));

                list.add(user);
            }
            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult addGroupMemberParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<User> list = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);

                User user = new User();
                user.setNick(obj.optString("member_name"));
                user.setId(obj.optString("member_id"));
                user.setIsFollowed(obj.optString("if_add"));

                list.add(user);
            }
            netResult.setResultObject(list);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getFriendList(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }


            JSONObject dataJo = new JSONObject(datas);

            SPGroup spGroup = new SPGroup();
            spGroup.setAddSum(dataJo.optInt("uid_a_sum"));
            spGroup.setAddedSum(dataJo.optInt("uid_b_sum"));
            spGroup.setGroupSum(dataJo.optInt("group_sum"));
            spGroup.setFriendSum(dataJo.optInt("member_sum"));

            List<Group> groupList = new ArrayList<Group>();
            JSONArray groupArray = dataJo.optJSONArray("list_g");
            if (groupArray != null) {
                for (int i = 0; i < groupArray.length(); i++) {
                    JSONObject obj = groupArray.getJSONObject(i);
                    Group group = new Group();
                    group.setId(obj.optString("group_id"));
                    group.setMemSum(obj.optInt("group_number"));
                    group.setPic(obj.optString("group_img"));
                    group.setName(obj.optString("group_name"));
                    groupList.add(group);
                }
            }
            spGroup.setGroupList(groupList);

            List<User> userList = new ArrayList<User>();
            JSONArray userArray = dataJo.optJSONArray("list_m");
            if (userArray != null) {
                for (int i = 0; i < userArray.length(); i++) {
                    JSONObject obj = userArray.getJSONObject(i);
                    User user = new User();
                    user.setId(obj.optString("member_id"));
                    user.setNick(obj.optString("member_name"));
                    userList.add(user);
                }
            }
            spGroup.setFriendList(userList);

            netResult.setResultObject(spGroup);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult groupCategoryParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            List<SimpleKV> list = new ArrayList<SimpleKV>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                SimpleKV kv = new SimpleKV();
                kv.setKey(obj.optString("cat_id"));
                kv.setValue(obj.optString("cat_name"));

                list.add(kv);
            }
            netResult.setResultObject(list);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult friendSearchListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

//            JSONObject dataJo = new JSONObject(datas);
//
//            SystomoResult result = new SystomoResult();
//            result.setTotal(dataJo.optInt("check_num"));
//            List<User> userList = new ArrayList<>();
//            JSONArray groupArray = dataJo.getJSONArray("list_g");
//            for (int i = 0; i < groupArray.length(); i++) {
//                JSONObject obj = groupArray.getJSONObject(i);
//                User user = new User();
//                user.setId(obj.optString("member_id"));
//                user.setNick(obj.optString("member_name"));
//                userList.add(user);
//            }
//            result.setUserList(userList);


            List<User> result = new ArrayList<User>();
            JSONArray dataArray = new JSONArray(datas);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                User user = new User();
                user.setNick(obj.optString("member_name"));
                user.setId(obj.optString("member_id"));

                result.add(user);
            }

            netResult.setResultObject(result);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }


    private static NetResult getGroupParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }


            JSONObject dataJo = new JSONObject(datas);

            Group group = new Group();
            group.setId(dataJo.optString("group_id"));
            group.setPic(dataJo.optString("group_img"));
            group.setName(dataJo.optString("group_name"));

            List<User> userList = new ArrayList<User>();
            JSONArray userArray = dataJo.getJSONArray("group_data");
            for (int i = 0; i < userArray.length(); i++) {
                JSONObject obj = userArray.getJSONObject(i);
                User user = new User();
                user.setId(obj.optString("member_id"));
                user.setNick(obj.optString("member_name"));
                userList.add(user);
            }
            group.setMemberList(userList);

            netResult.setResultObject(group);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getAddress(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Group group = new Group();
            List<SimpleKV> simpleKVList = new ArrayList<SimpleKV>();
            JSONArray userArray = dataJo.getJSONArray("list");
            for (int i = 0; i < userArray.length(); i++) {
                JSONObject obj = userArray.getJSONObject(i);
                SimpleKV simpleKV = new SimpleKV();
                simpleKV.setKey(obj.optString(AREA_ID));
                simpleKV.setValue(obj.optString(AREA_NAME));
                simpleKVList.add(simpleKV);
            }
            netResult.setResultObject(simpleKVList);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getIndustry(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Group group = new Group();
            List<SimpleKV> simpleKVList = new ArrayList<SimpleKV>();
            JSONArray userArray = dataJo.getJSONArray("list");
            for (int i = 0; i < userArray.length(); i++) {
                JSONObject obj = userArray.getJSONObject(i);
                SimpleKV simpleKV = new SimpleKV();
                simpleKV.setKey(obj.optString(AREA_ID));
                simpleKV.setValue(obj.optString(AREA_NAME));
                simpleKVList.add(simpleKV);
            }
            netResult.setResultObject(simpleKVList);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getUse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            Group group = new Group();
            List<SimpleKV> simpleKVList = new ArrayList<SimpleKV>();
            JSONArray userArray = dataJo.getJSONArray("list");
            for (int i = 0; i < userArray.length(); i++) {
                JSONObject obj = userArray.getJSONObject(i);
                SimpleKV simpleKV = new SimpleKV();
                simpleKV.setKey(obj.optString(AREA_ID));
                simpleKV.setValue(obj.optString(AREA_NAME));
                simpleKVList.add(simpleKV);
            }
            netResult.setResultObject(simpleKVList);
            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        } catch (NumberFormatException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getUserProfCategoryParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            netResult.setResultObject(null);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getProfileSettingListParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONArray dataArray = new JSONArray(datas);

            List<SimpleKV2> simpleKVList = new ArrayList<SimpleKV2>();
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.getJSONObject(i);
                SimpleKV2 simpleKV = new SimpleKV2();
                simpleKV.setKey(obj.optString(AREA_ID));
                simpleKV.setValue(obj.optString(AREA_NAME));
                simpleKV.setHasChild(!("1".equals(obj.optString("last_flg"))));
                simpleKVList.add(simpleKV);
            }
            netResult.setResultObject(simpleKVList);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult getADParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJO = new JSONObject(datas);

            AdInfo info = new AdInfo();
            info.setAd_id(dataJO.optString("ad_id"));
            info.setAd_name(dataJO.optString("ad_name"));
            info.setAd_type(dataJO.optString("ad_type"));
            info.setAd_size(dataJO.optString("ad_size"));
            info.setAd_img(dataJO.optString("ad_img"));
            info.setLink_url(dataJO.optString("link_url"));

            netResult.setResultObject(info);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }
}