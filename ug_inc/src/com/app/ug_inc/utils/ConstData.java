package com.app.ug_inc.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.text.TextUtils;
import android.util.Xml;

import com.app.ug_inc.models.SimpleKV;

public class ConstData {
    public static boolean sIsAppFree = false;

    public static final String TAG = ConstData.class.getSimpleName();

    public static final int PAGE_FIRST = 1;

    public static final int PAGE_SIZE_CLOSURE = 10;
    public static final int PAGE_SIZE_CHIEB = 10;
    public static final int PAGE_SIZE_CLOSURE_COMMENT = 10;
    public static final int PAGE_SIZE_MOCK = 10;
    public static final int PAGE_SIZE_MY_SERIES_ARTICLE = 20;
    public static final int PAGE_SIZE_MY_SERIES_COMMENT = 10;
    public static final int PAGE_SIZE_MESSAGE_BOX = 10;
    public static final int PAGE_SIZE_MEMO = 10;
    public static final int PAGE_SIZE_CONVERSATION = 10;
    public static final int PAGE_SIZE_ARTICLE_HIS = 10;
    public static final int PAGE_SIZE_FOLLOWED = 10;
    public static final int PAGE_SIZE_RATE = 10;
    public static final int PAGE_SIZE_WORKPLACE = 10;

    private static final String XML_PATH_AREA = "consts/area.xml";
    private static final String XML_PATH_CITIES = "consts/cities.xml";
    private static final String XML_PATH_JOBS = "consts/jobs.xml";
    private static final String XML_PATH_CERTIF = "consts/certif.xml";
    private static final String XML_PATH_BLOOD_TYPE = "consts/blood_type.xml";

    private static final String[] PEOPLE = new String[]{"1人", "2人", "3人", "4人", "5人", "6人", "7人",
            "8人", "9人", "10人以上",};
    private static final String[] SEX = new String[]{"男", "女", "その他"};
    private static final String[] HEALTH = new String[]{"未実施", "実施済"};

    private static final String KEY_ITEM = "item";

    public static final String UTF8 = "UTF-8";

    private static Context sContext;
    private static HashMap<SimpleKV, ArrayList<SimpleKV>> sAreaList = new HashMap<SimpleKV, ArrayList<SimpleKV>>();
    private static ArrayList<SimpleKV> sCityList = new ArrayList<SimpleKV>();
    private static ArrayList<SimpleKV> sJobList = new ArrayList<SimpleKV>();
    private static ArrayList<SimpleKV> sBloodList = new ArrayList<SimpleKV>();
    private static ArrayList<SimpleKV> sCertifList = new ArrayList<SimpleKV>();
    // people 1~10
    private static ArrayList<SimpleKV> sPeopleList = new ArrayList<SimpleKV>();
    private static ArrayList<SimpleKV> sHealthList = new ArrayList<SimpleKV>();

    public static synchronized void init(Context context) {
        sContext = context;

        initAreaList();
        // getCityListFromXML();
        getJobListFromXML();
        getBloodTypeListFromXML();
        getCertifListFromXML();
        initPeopleList();
        initHealthList();
    }

    private static void initHealthList() {
        for (int i = 0; i < HEALTH.length; i++) {
            SimpleKV kv = new SimpleKV();
            kv.setKey("" + i);
            kv.setValue(HEALTH[i]);
            sHealthList.add(kv);
        }
    }

    private static void initPeopleList() {
        for (int i = 0; i < PEOPLE.length; i++) {
            SimpleKV kv = new SimpleKV();
            kv.setKey("" + (i + 1));
            kv.setValue(PEOPLE[i]);
            sPeopleList.add(kv);
        }
    }

    private static void initAreaList() {
        InputStream is = null;
        try {
            is = sContext.getAssets().open(XML_PATH_AREA);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, UTF8);

            int i = 1, j = 1;
            ArrayList<SimpleKV> tmpDistList = null;
            String tmpCityName = "";

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            String full = parser.getAttributeValue(0);
                            String[] strings = full.split(",");

                            String cityName = strings[0];
                            if (!tmpCityName.equals(cityName)) { // first
                                tmpCityName = cityName;
                                tmpDistList = new ArrayList<SimpleKV>();
                                SimpleKV city = new SimpleKV("" + i, tmpCityName);
                                sAreaList.put(city, tmpDistList); // map
                                sCityList.add(city);

                                i++;
                            }

                            SimpleKV dist = new SimpleKV("" + j, strings[1]);
                            tmpDistList.add(dist);

                            j++;
                        }
                        break;

                    default:
                        break;
                }

                eventType = parser.next();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            is = null;
        }
    }

    @Deprecated
    @SuppressWarnings("unused")
    private static void getCityListFromXML() { // use initAreaList()
        InputStream is = null;
        try {
            is = sContext.getAssets().open(XML_PATH_CITIES);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, UTF8);
            SimpleKV skv = null;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        sCityList = new ArrayList<SimpleKV>();
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            skv = new SimpleKV();
                            skv.setKey(parser.getAttributeValue(0));
                            skv.setValue(parser.getAttributeValue(1));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            sCityList.add(skv);
                            skv = null;
                        }
                        break;

                    default:
                        break;
                }

                eventType = parser.next();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            is = null;
        }
    }

    private static void getJobListFromXML() {
        InputStream is = null;
        try {
            is = sContext.getAssets().open(XML_PATH_JOBS);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, UTF8);
            SimpleKV skv = null;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        sJobList = new ArrayList<SimpleKV>();
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals("category")) {
                            skv = new SimpleKV();
                            skv.setKey(parser.getAttributeValue(0));
                            skv.setValue(parser.getAttributeValue(1));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals("category")) {
                            sJobList.add(skv);
                            skv = null;
                        }
                        break;

                    default:
                        break;
                }

                eventType = parser.next();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            is = null;
        }
    }

    public static void getBloodTypeListFromXML() {
        InputStream is = null;
        try {
            is = sContext.getAssets().open(XML_PATH_BLOOD_TYPE);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, UTF8);
            SimpleKV skv = null;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        sBloodList = new ArrayList<SimpleKV>();
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            skv = new SimpleKV();
                            skv.setKey(parser.getAttributeValue(0));
                            skv.setValue(parser.getAttributeValue(1));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            sBloodList.add(skv);
                            skv = null;
                        }
                        break;

                    default:
                        break;
                }

                eventType = parser.next();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            is = null;
        }
    }

    public static void getCertifListFromXML() {
        InputStream is = null;
        try {
            is = sContext.getAssets().open(XML_PATH_CERTIF);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, UTF8);
            SimpleKV skv = null;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        sCertifList = new ArrayList<SimpleKV>();
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            skv = new SimpleKV();
                            skv.setKey(parser.getAttributeValue(0));
                            skv.setValue(parser.getAttributeValue(1));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals(KEY_ITEM)) {
                            sCertifList.add(skv);
                            skv = null;
                        }
                        break;

                    default:
                        break;
                }

                eventType = parser.next();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            is = null;
        }
    }

    public static ArrayList<SimpleKV> getSexList() {
        ArrayList<SimpleKV> list = new ArrayList<SimpleKV>();

        for (int i = 0; i < SEX.length; i++) {
            SimpleKV sex = new SimpleKV();
            sex.setKey("" + (i + 1));
            sex.setValue(SEX[i]);
            list.add(sex);
        }

        return list;
    }

    public static ArrayList<SimpleKV> getCityList() {
        if (sCityList.isEmpty()) {
            return null;
        }

        return sCityList;
    }

    public static ArrayList<SimpleKV> getDistrictListByCity(SimpleKV city) {
        if (sCityList.isEmpty()) {
            return null;
        }

        if (!sCityList.contains(city)) {
            throw new IllegalArgumentException(
                    "can not use customized CITY object, must get from sCityList");
        }

        return sAreaList.get(city);
    }

    public static ArrayList<SimpleKV> getCertifList() {
        if (sCertifList.isEmpty()) {
            return null;
        }

        return sCertifList;
    }

    public static ArrayList<SimpleKV> getHealthList() {
        if (sHealthList.isEmpty()) {
            return null;
        }

        return sHealthList;
    }

    public static String getHealthById(String healthId) {
        if (TextUtils.isEmpty(healthId)) {
            return "";
        }

        for (SimpleKV kv : sHealthList) {
            if (healthId.equals(kv.getKey())) {
                return kv.getValue();
            }
        }

        return "";
    }

    public static String getCityById(String cityId) {
        if (TextUtils.isEmpty(cityId)) {
            return "";
        }

        for (SimpleKV kv : sCityList) {
            if (cityId.equals(kv.getKey())) {
                return kv.getValue();
            }
        }

        return "";
    }

    public static String getDistrict(SimpleKV city, String distId) {
        if (city == null) {
            return "";
        }

        ArrayList<SimpleKV> distList = getDistrictListByCity(city);

        for (SimpleKV dist : distList) {
            if (distId.equals(dist.getKey())) {
                return dist.getValue();
            }
        }

        return "";
    }

    public static String getPeopleById(String peopleId) {
        if (TextUtils.isEmpty(peopleId)) {
            return "";
        }

        for (SimpleKV people : sPeopleList) {
            if (people.getKey().equals(peopleId)) {
                return people.getValue();
            }
        }

        return "";
    }

    public static String getSexById(String sexId) {
        if (TextUtils.isEmpty(sexId)) {
            return "";
        }

        return sexId.equals("1") ? SEX[0] : sexId.equals("2") ? SEX[1] : SEX[2];
    }

    public static String getBloodById(String bloodId) {
        if (TextUtils.isEmpty(bloodId)) {
            return "";
        }

        for (SimpleKV kv : sBloodList) {
            if (bloodId.equals(kv.getKey())) {
                return kv.getValue();
            }
        }

        return "";
    }

    public static String getCertifById(String certifId) {
        if (TextUtils.isEmpty(certifId)) {
            return "";
        }

        for (SimpleKV kv : sCertifList) {
            if (certifId.equals(kv.getKey())) {
                return kv.getValue();
            }
        }

        return "";
    }

//	public static String getJobById(String jobId) {
//		if (TextUtils.isEmpty(jobId)) {
//			return "";
//		}
//
//		for (SimpleKV kv : sJobList) {
//			if (jobId.equals(kv.getKey())) {
//				return kv.getValue();
//			}
//		}
//
//		return "";
//	}

    public static ArrayList<SimpleKV> getJobList() {
        if (sJobList.isEmpty()) {
            return null;
        }

        return sJobList;
    }

    public static ArrayList<SimpleKV> getBloodTypeList() {
        if (sBloodList.isEmpty()) {
            return null;
        }

        return sBloodList;
    }

    public static ArrayList<SimpleKV> getPeopleList() {
        if (sPeopleList.isEmpty()) {
            return null;
        }

        return sPeopleList;
    }

    public static boolean isFollowed(String followString) {
        try {
            return followString.equals("1") ? true : false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}