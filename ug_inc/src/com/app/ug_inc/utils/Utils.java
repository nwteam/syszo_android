package com.app.ug_inc.utils;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.ClipboardManager;
import android.widget.Toast;

import com.app.ug_inc.R;
import com.app.ug_inc.models.ChiebComment;
import com.app.ug_inc.ui.activities.ProfileActivity;

public class Utils {

    public static void toast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static String arrayToString(String[] ids) {
        String string = "";
        for (String id : ids) {
            string += (id + ",");
        }

        return string.substring(0, string.length() - 1);
    }

    public static String listToString(List<String> ids) {
        String string = "";
        for (String id : ids) {
            string += (id + ",");
        }

        return string.substring(0, string.length() - 1);
    }

    public static final int REQUEST_TAKE_PHOTO = 1100;
    public static final int REQUEST_PICK_PHOTO = 1200;

    public static String takePhoto(Activity activity) {
        if (!FileUtils.isSDMounted()) { // sdcard mounted
            throw new NullPointerException("sdcard not mounted, cannot take photo.");
        }

        File dir = new File(FileUtils.DCIM);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String filePath;

        String file = "genpa" + System.currentTimeMillis() + ".jpg";
        File picFile = new File(dir, file);
        filePath = picFile.getAbsolutePath();
        Uri uri = Uri.fromFile(picFile);

        Intent take = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        take.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        take.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        activity.startActivityForResult(take, REQUEST_TAKE_PHOTO);

        return filePath;
    }

    public static void pickPhoto(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);// ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/jpeg");
        activity.startActivityForResult(intent, REQUEST_PICK_PHOTO);
    }

    public static void toProfile(Context context, String uid) {
        Intent intent = new Intent();
        intent.setClass(context, ProfileActivity.class);
        intent.putExtra("uid", uid);
        context.startActivity(intent);
    }

    public static void copyToClipBoard(Context context, CharSequence s) {
        ClipboardManager clip = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clip.getText(); // ճ��
        clip.setText(s); // ����

        toast(context, context.getString(R.string.success_copy));
    }
}