package com.app.ug_inc.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.app.ug_inc.R;
import com.app.ug_inc.datas.Account;
import com.app.ug_inc.datas.SharedPreferenceHelper;
import com.app.ug_inc.models.Message;
import com.app.ug_inc.models.User;
import com.app.ug_inc.net.NetResult;
import com.app.ug_inc.net.controller.AccountController;
import com.app.ug_inc.net.controller.UIDelegate;
import com.app.ug_inc.ui.activities.ChatActivity;
import com.app.ug_inc.ui.activities.MessageBoxActivity;
import com.app.ug_inc.ui.activities.ZActivityManager;
import com.app.ug_inc.utils.ZLog;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

import static android.app.Notification.DEFAULT_ALL;

public class ZJPushReceiver extends BroadcastReceiver {
    public static final String TAG = ZJPushReceiver.class.getSimpleName();

    public static String TOKEN = "";

    private AccountController mAccountController;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }

        Bundle bundle = intent.getExtras();
        String action = intent.getAction();

        if (action.equals("cn.jpush.android.intent.REGISTRATION")) {
            String registationId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            ZLog.d("JPUSH regid=" + registationId);

            if (registationId == null) {
                return;
            }

            TOKEN = registationId;

            boolean isUserAlive = !TextUtils.isEmpty(Account.getUserId());
            if (isUserAlive) { // user must logined
                uploadPushToken(registationId);
            }
        } else if (action.equals("cn.jpush.android.intent.MESSAGE_RECEIVED")) {
            // String extraExtra = bundle.getString(JPushInterface.EXTRA_EXTRA);
            String extraMessage = bundle.getString(JPushInterface.EXTRA_MESSAGE);
            String extraTitle = bundle.getString(JPushInterface.EXTRA_TITLE);

            // 规定：
            // article_repied = 自己的投稿被回复时
            // was_followed = 被别人设置为 “知り合い’”
            // new_msg_incoming = Message聊天里来新消息时
            // system_msg_incoming = 后台运营方发来的系统通知 “お知らせ” 时
            // String test =
            // "{\"type\":\"article_repied\",\"title\":\"新着メッセージがありました\",\"content\":\"A给b回复了\",\"time\":\"2014-11-04 18:08:33\"}";

            String newExtraMsg = extraMessage.replaceAll("\\\\", "");

            if (TextUtils.isEmpty(Account.getUserId())) {
                // if both empty, returs.
                return;
            }

            try {
                parseMessage(extraTitle, newExtraMsg, context);
            } catch (JSONException e) {
                e.printStackTrace();
                // TODO somthing
            }
        }
    }

    private void parseMessage(String title, String extraMessage, Context context)
            throws JSONException {
//        JSONObject object = new JSONObject(extraMessage);
//        String msg = object.optString("message");
//        String type = object.optString("type");
//        String content = object.optString("content");

        String notifTitle = context.getString(R.string.app_name);

//        if (type.equals("article_repied")) {
//            Intent pIntent = new Intent();
//            if (!ZActivityManager.getInstance().isAppRunning) {
//                // if app-ui is not running, run it
//                pIntent.setClass(context, MenuActivity.class);
//                // pIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                pIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            }
//
//            showNotification(context, notifTitle, title,
//                    PendingIntent.getActivity(context, 1, pIntent, PendingIntent.FLAG_ONE_SHOT));
//        } else if (type.equals("was_followed")) {
//            Intent pIntent = new Intent();
//            if (!ZActivityManager.getInstance().isAppRunning) {
//                // if app-ui is not running, run it
//                pIntent.setClass(context, MenuActivity.class);
//                // pIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                pIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            }
//
//            showNotification(context, notifTitle, title,
//                    PendingIntent.getActivity(context, 1, pIntent, PendingIntent.FLAG_ONE_SHOT));
//            // showNotification(context, notifTitle, title, null);
//        } else if (type.equals("new_msg_incoming")) {
        parseChatJson(notifTitle, extraMessage, context);
//        } else if (type.equals("system_msg_incoming")) {
//            parseChatJson(notifTitle, content, context);
//        }
    }

    private void parseChatJson(String content, String extraContent, Context context)
            throws JSONException {
        String notifTitle = context.getString(R.string.app_name);

        JSONObject object = new JSONObject(extraContent);
//        String msg_type = object.optString("msg_type");

        ZActivityManager chatActivityManager = ZActivityManager.getInstance();
        // if is chatting with some one
        if (!TextUtils.isEmpty(chatActivityManager.chatWithUid)) {
            Message msg = new Message();
            msg.setTime(object.optString("send_time"));

            String picUrl = object.optString("info_img");
            if (!TextUtils.isEmpty(picUrl)) { // 图片消息
                msg.setPicUrl(picUrl);
                msg.setType("1");
            } else {
                String msg_content = object.optString("info_content");
                msg.setContent(msg_content);
                msg.setType("0");
            }

            User user = new User();
            user.setId(object.optString("user_id"));
            user.setNick(object.optString("user_name"));

            msg.setFromUser(user);

            String groupId = object.optString("group_id");
            String userId = object.optString("rec_id");

            Intent intent = new Intent(ChatActivity.FILTER_NEW_MSG_RECEIVED);
            intent.putExtra("msg", msg);
            intent.putExtra("gid", groupId);
            intent.putExtra("uid", userId);

            context.sendBroadcast(intent);

            // if activity is not foreground, need to start over
            if (!chatActivityManager.isChatForeground) {
                Intent pIntent = new Intent();
                pIntent.putExtra("gid", groupId);
                pIntent.putExtra("uid", userId);
                pIntent.setClass(context, ChatActivity.class);
                pIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, pIntent,
                        PendingIntent.FLAG_ONE_SHOT);
                showNotification(context, notifTitle, "新着メッセージがありました", pendingIntent);
            }
        } else {
            context.sendBroadcast(new Intent(ChatActivity.FILTER_NEED_FINISH));
            Intent intent = new Intent();
            intent.setClass(context, MessageBoxActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            showNotification(context, notifTitle, "新着メッセージがありました", pendingIntent);
        }
    }

    private void showNotification(Context context, String title, String content,
                                  PendingIntent pendingIntent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setAutoCancel(true);
        mBuilder.setDefaults(DEFAULT_ALL);
        mBuilder.setSmallIcon(R.drawable.ic_launcher); // necessary
        // mBuilder.setTicker(content);
        mBuilder.setTicker("新着メッセージがありました");
        // mBuilder.setContentTitle(title);
        mBuilder.setContentTitle(context.getString(R.string.app_name));
        mBuilder.setContentText("新着メッセージがありました");
        mBuilder.setContentIntent(pendingIntent);

        Notification notification = mBuilder.build();
        notification.flags |= PendingIntent.FLAG_ONE_SHOT;

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    private void uploadPushToken(final String token) {
        mAccountController = new AccountController(new UIDelegate() {

            @Override
            public void onRequestSuccess(int ifId, NetResult obj) {
                Account.setPushToken(token);
            }

            @Override
            public void onRequestError(int ifId, String errMsg) {
                ZLog.e(TAG, "jpush regid=" + token + " not uploaded, reason:" + errMsg);
            }
        });
        mAccountController.submitPushToken(token);
    }
}