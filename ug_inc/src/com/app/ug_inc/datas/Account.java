package com.app.ug_inc.datas;

import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.app.ug_inc.models.User;

public class Account {
    private static String sUserId;
//    private static String sToken;

    private static User sUser;

    private Account() {
    }

    public static boolean isUserSaved() {
        String id = SharedPreferenceHelper.getString("user_id");
        String token = SharedPreferenceHelper.getString("user_token");

        return !TextUtils.isEmpty(id) && !TextUtils.isEmpty(token);
    }

    public static void init() {
        loadUserFromSp();

        sUserId = sUser.getId();
//        sToken = sUser.getToken();

        // sUserId = "10";
        // sToken = "379aefee233b07279956d0f537bd0032";
    }

    public static String getUserId() {
        return sUserId;
//        return "1";
    }

    public static String getToken() {
        return "";
//        return sToken;
    }

    public static User getUser() {
        return sUser;
    }

    public static User getCLonedUser() throws CloneNotSupportedException {
        User user = new User();
        user = (User) sUser.clone();

        return user;
    }

    public static synchronized void setUser(User user) {
        sUser = user;
        sUserId = sUser.getId();
//        sToken = sUser.getToken();

        saveUserToSp();
    }

    public static synchronized void setBalance(String balance) {
        sUser.setBalance(balance);

        Editor editor = SharedPreferenceHelper.getEditor();
        editor.putString("user_balance", balance);
        editor.commit();
    }

    public static synchronized void setPushToken(String pushToken) {
        sUser.setPushToken(pushToken);

        Editor editor = SharedPreferenceHelper.getEditor();
        editor.putString("push_token", pushToken);
        editor.commit();
    }

    private static synchronized void saveUserToSp() {
        Editor editor = SharedPreferenceHelper.getEditor();
        editor.putString("user_id", sUser.getId());
        editor.putString("user_token", sUser.getToken());
        editor.putString("user_nick", sUser.getNick());
        editor.putString("user_email", sUser.getEmail());
        editor.putString("user_pwd", sUser.getPwd());
        editor.putString("user_sex", sUser.getSex());
        editor.putString("user_birth", sUser.getBirth());
        editor.putString("user_district", sUser.getDistrict());
        editor.putString("user_city_id", sUser.getCityId());
        editor.putString("user_job_id", sUser.getJobId());
        editor.putString("user_job_name", sUser.getJob());
        editor.putString("user_blood", sUser.getBlood());
        editor.putString("user_health", sUser.getHealth());
        editor.putString("user_self_intro", sUser.getSelfIntro());
        editor.putString("user_score", sUser.getScore());
        editor.putString("user_certif_id", sUser.getCertifactionId());
        editor.putString("user_balance", sUser.getBalance());
        editor.putString("user_followed_people", sUser.getFollowedPeople());

        editor.commit();
    }

    private static void loadUserFromSp() {
        sUser = new User();
        sUser.setId(SharedPreferenceHelper.getString("user_id"));
        sUser.setToken(SharedPreferenceHelper.getString("user_token"));
        sUser.setNick(SharedPreferenceHelper.getString("user_nick"));
        sUser.setEmail(SharedPreferenceHelper.getString("user_email"));
        sUser.setPwd(SharedPreferenceHelper.getString("user_pwd"));
        sUser.setSex(SharedPreferenceHelper.getString("user_sex"));
        sUser.setBirth(SharedPreferenceHelper.getString("user_birth"));
        sUser.setDistrict(SharedPreferenceHelper.getString("user_district"));
        sUser.setCityId(SharedPreferenceHelper.getString("user_city_id"));
        sUser.setJobId(SharedPreferenceHelper.getString("user_job_id"));
        sUser.setJob(SharedPreferenceHelper.getString("user_job_name"));
        sUser.setBlood(SharedPreferenceHelper.getString("user_blood"));
        sUser.setHealth(SharedPreferenceHelper.getString("user_health"));
        sUser.setSelfIntro(SharedPreferenceHelper.getString("user_self_intro"));
        sUser.setCertifactionId(SharedPreferenceHelper.getString("user_certif_id"));
        sUser.setScore(SharedPreferenceHelper.getString("user_score"));
        sUser.setFollowedPeople(SharedPreferenceHelper.getString("user_followed_people"));
        sUser.setPushToken(SharedPreferenceHelper.getString("push_token"));
        sUser.setBalance(SharedPreferenceHelper.getString("user_balance"));
    }

    public static final boolean isAutoLogin() {
        return SharedPreferenceHelper.getBoolean("auto-login", false);
    }

    public static final void setAutoLogin(boolean auto) {
        SharedPreferenceHelper.getEditor().putBoolean("auto-login", auto).commit();
    }

    public static void clear() {
        sUser = null;
        sUserId = null;
//        sToken = null;

        Editor editor = SharedPreferenceHelper.getEditor();
        editor.clear();
//        editor.remove("user_id");
//        editor.remove("user_token");
//        editor.remove("user_nick");
//        editor.remove("user_email");
//        editor.remove("user_pwd");
//        editor.remove("user_sex");
//        editor.remove("user_birth");
//        editor.remove("user_district");
//        editor.remove("user_city_id");
//        editor.remove("user_job_id");
//        editor.remove("user_blood");
//        editor.remove("user_health");
//        editor.remove("user_self_intro");
//        editor.remove("user_score");
//        editor.remove("user_certif_id");
//        editor.remove("user_balance");
//        editor.remove("user_followed_people");

        editor.commit();
    }
}